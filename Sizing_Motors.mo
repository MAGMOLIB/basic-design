﻿within ;
package Sizing

  package PredesignAlgorithms
    function Iduction_Motor_Sizing "Sizing_IM_FLUX_24"
      import Modelica.Constants;
      import Modelica.SIunits;
      parameter input Modelica.SIunits.Voltage U_ph(min=0)=380
        "Rated phase voltage"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real K_E_U_ratio=0.97 "Per phase E/U modulus ratio"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer m(min=1)=3 "Number of phases"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer p(min=1)=1 "Number of pole pairs"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer a(min=1)=1 "Number of parallel paths"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input SIunits.Torque T(min=0)=23.87324 "Rated torque"
      annotation (Dialog(group="Nominal parameters"));
      parameter input
        Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm               n_rpm(min=0.1)=3000
        "Rated Speed"
      annotation (Dialog(group="Nominal parameters"));
      parameter input SIunits.Length delta_htr(min=0)=0.002
        "Stator tooth shoe height"
      annotation (Dialog(tab="Stator", group="Geometry"));
      parameter input Real eff(min=0,max=1)=0.87 "Efficiency"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real PF_ref(min=0,max=1)=0.84 "Desired power factor"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Boolean C_mec_family_flag=false "C_mec family flag"
      annotation (Dialog(group="Mechanical constant",__Dymola_compact=true, __Dymola_descriptionLabel=true),choices(__Dymola_checkBox=true));
      parameter input Real C_mec_ini(min=0)=80 "Desired mechanical constant"
      annotation (Dialog(enable=C_mec_family_flag == true,group="Mechanical constant"));
      parameter input String Stator_Material_File="FLUX_STEEL_NLIN2.txt"
        "Stator Material B(H) characteristic"
      annotation(Dialog(tab="Stator",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Stator Material B(H) characteristic file")));
      parameter input String Rotor_Material_File="FLUX_STEEL_NLIN2.txt"
        "Rotor Material B(H) characteristic"
      annotation(Dialog(tab="Rotor",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Rotor Material B(H) characteristic file")));
      parameter input String Ksat_alpha_File="Ksat_alpha.txt"
        "Saturation factor characteristic"
      annotation(Dialog(tab="Stator",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Ksat_alpha(alpha) characteristic file")));
      parameter input Modelica.SIunits.CurrentDensity J_s(min=0)=5.1*10^6
        "Stator current density"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Modelica.SIunits.CurrentDensity J_r(min=0)=5.1*10^6
        "Rotor current density"
      annotation (Dialog(tab="Rotor",group="Cage"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_tooth_s(min=0)=1.828
        "Maximum stator tooth flux density"
        annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_yoke_s(min=0)=1.3
        "Maximum stator yoke flux density"
        annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_tooth_r(min=0)=1.98
        "Maximum rotor tooth flux density"
        annotation (Dialog(tab="Rotor",group="Magnetic circuit"));
      parameter input Integer n_v(min=0)=0 "Number of ventilation ducts"
      annotation (Dialog(tab="Stator", group="Ventilation"));
      parameter input Modelica.SIunits.Length b_v(min=0)=0
        "Ventilation duct width"
      annotation (Dialog(tab="Stator", group="Ventilation"));
      parameter input Integer coilspan(min=1)=8 "Coil span"
      annotation (Dialog(tab="Stator", group="Winding"));
      parameter input Integer Q_s(min=1)=24 "Number of stator slots"
      annotation (Dialog(tab="Stator", group="Geometry"));
      parameter input Integer Q_r(min=1)=20 "Number of rotor slots"
      annotation (Dialog(tab="Rotor", group="Cage"));
      parameter input Real K_u(min=0,max=1)=0.5 "Stator slot filling factor"
      annotation (Dialog(tab="Stator", group="Winding"));
      parameter input Real f_r(min=0,max=1) = 0.98 "Stator lamination factor"
      annotation (Dialog(tab="Stator", group="Winding"), Evaluate=true);
      parameter input Modelica.SIunits.Length g_ref(min=0)=0.0005
        "Desired air gap length"
      annotation (Dialog(tab="Airgap"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_g_ref(min=0)=0.620
        "Maximum air gap flux density"
      annotation (Dialog(tab="Airgap"));
      parameter input Real X(min=0)=1.03333 "Aspect ratio"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input Modelica.SIunits.Length b_1=2.5*10^(-3) "Slot opening"
      annotation (Dialog(tab="Stator", group="Geometry"));
      parameter input SIunits.Conversions.NonSIunits.Angle_deg gamma(min=0)=0
        "Skewing angle"
      annotation (Dialog(tab="Stator", group="Geometry"));

      output Modelica.SIunits.MagneticFluxDensity B_g_final
        "Air gap flux density"
      annotation (Dialog(tab="Outputs", group="Magnetic circuit"));
      output Modelica.SIunits.MagneticFlux flux_g "Air gap flux"
      annotation (Dialog(tab="Outputs", group="Magnetic circuit"));
      output Real alpha_i=0.75 "Flux density arithemtical average factor"
      annotation (Dialog(tab="Outputs", group="Magnetic circuit"));
      output Modelica.SIunits.Voltage E "Back EMF"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Modelica.SIunits.Power P_mec "Mechanical power"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Real PF=PF_ref "Power factor"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Modelica.SIunits.Current I_r "Rotor RMS current per slot"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Modelica.SIunits.Current I_s "Stator RMS current per slot"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Modelica.SIunits.Diameter D "Bore diameter"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Diameter D_os "Stator outer diameter"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Length b_z "Stator tooth width"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Length b_s "Stator slot width"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Height h_y "Stator yoke height"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Height h_z "Stator tooth height"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Height h_tr "Stator slot height"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Diameter D_or "Rotor outer diameter"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Length b_z_r "Rotor tooth width"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Length b_s_r "Rotor slot width"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Height h_z_r "Rotor tooth height"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Height h_tr_r "Rotor slot height"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Length l "Stack length"
      annotation (Dialog(tab="Outputs",group="Machine geometry"));
      output Modelica.SIunits.Length g=g_ref "Air gap length"
      annotation (Dialog(tab="Outputs", group="Machine geometry"));
      output Real N_ph "Number of turns per phase"
      annotation (Dialog(tab="Outputs", group="Winding"));

    protected
      Real q "Slots per phase per pole ratio";
      parameter Real[:,:] Mat_s=DataFiles.readCSVmatrix(Stator_Material_File)
        "Stator Material B(H) characteristic";
      parameter Real[:,:] Mat_r=DataFiles.readCSVmatrix(Rotor_Material_File)
        "Rotor Material B(H) characteristic";
      parameter Real[:,:] Ksat_alpha=DataFiles.readCSVmatrix(Ksat_alpha_File)
        "Ksat_alpha(alpha) characteristic";
      parameter Real[:,:] PowerDshaft=DataFiles.readCSVmatrix("PowerDshaft.txt")
        "Normalized shaft diameters by mechanical power";

      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm k=60
        "Mechanical frequency scaling factor";

      //parameter Modelica.SIunits.Frequency f_m=f/p "Rated mechanical frequency";
      Real C_mec=C_mec_ini "Mechanical constant";
      Real A_load=0 "Electric loading";
      Real K_w "Winding factor";
      Modelica.SIunits.Length l_eff "Effective length";
      Real t_u "Stator pitch";
      Real t_p "Polar pitch";
      Modelica.SIunits.Time C "Apparent machine constant";
      Real z_Q "Number of conductors in slot";
      Modelica.SIunits.MagneticFluxDensity B_g=B_g_ref "Air gap flux density";
      Real t_r "Rotor slot pitch";
      Modelica.SIunits.Area S_bar "Area of one bar";
      Modelica.SIunits.Current Iend_ring "End ring current";
      Modelica.SIunits.Area Send_ring "End ring area";
      Real K_c "Carter fringing coefficient";
      Modelica.SIunits.MagnetomotiveForce U_g "Air gap magnetic voltage";
      Modelica.SIunits.MagneticFieldStrength H_tooth_s
        "Stator tooth magnetic field strength";
      Modelica.SIunits.MagneticFieldStrength H_tooth_s_real
        "Stator tooth magnetic field strength";
      Modelica.SIunits.MagneticFluxDensity B_tooth_s_real
        "Stator tooth flux density";
      Modelica.SIunits.MagnetomotiveForce U_tooth_s
        "Stator tooth magnetic voltage";
      Modelica.SIunits.MagneticFieldStrength H_tooth_r
        "Rotor tooth magnetic field strength";
      Modelica.SIunits.MagneticFieldStrength H_tooth_r_real
        "Rotor tooth magnetic field strength";
      Modelica.SIunits.MagneticFluxDensity B_tooth_r_real
        "Rotor tooth flux density";
      Modelica.SIunits.MagnetomotiveForce U_tooth_r
        "Rotor tooth magnetic voltage";
      Real Ksat "Ksat??";
      Modelica.SIunits.MagneticFluxDensity B_g_new "Air gap flux density";
      SIunits.Frequency n_Hz "Mechanical rated speed in Hz";
      SIunits.Frequency f "Electrical frequency";
      SIunits.Frequency n "Speed in Hz";
      SIunits.Length D_shaft=0 "Shaft external diameter";
      SIunits.Length D_shaft_internal=0 "Shaft internal diameter";
      Real Internal_shaft_coeficient=1.15
        "Ratio between external and internal shafts";

    algorithm
      assert(K_E_U_ratio>0, "K_E_U_ratio input value outside of valid range (0,inf)");
      assert(U_ph>0, "U_ph input value outside of valid range (0,inf)");
      assert(m >=1, "m input value outside of valid range <1,inf)");
      assert(p >=1, "p input value outside of valid range <1,inf)");
      assert(a >=1, "a input value outside of valid range <1,inf)");
      assert(n_rpm >0, "n input value outside of valid range (0,inf)");
      assert(delta_htr >=0, "delta_htr input value outside of valid range <0,inf)");
      assert(eff >0 and eff <1, "eff input value outside of valid range (0,1)");
      assert(PF_ref >0 and PF_ref<=1,"PF_ref input value outside of valid range (0,1)");
      assert(C_mec >0,"C_mec input value outside of valid range (0,inf)");
      assert(J_s >0,"J_s input value outside of valid range (0,inf)");
      assert(J_r >0,"J_r input value outside of valid range (0,inf)");
      assert(B_tooth_s >0,"B_tooth_s input value outside of valid range (0,inf)");
      assert(B_yoke_s >0,"B_yoke_s input value outside of valid range (0,inf)");
      assert(B_tooth_r >0,"B_tooth_r input value outside of valid range (0,inf)");
      assert(B_g_ref >0,"B_g_ref input value outside of valid range (0,inf)");
      assert(n_v >=0,"n_v input value outside of valid range <0,inf)");
      assert(b_v >=0,"b_v input value outside of valid range <0,inf)");
      assert(coilspan >=1,"coilspan input value outside of valid range <1,inf)");
      assert(Q_s >=1,"Q_s input value outside of valid range <1,inf)");
      assert(Q_r >=1,"Q_r input value outside of valid range <1,inf)");
      assert(K_u >0 and K_u<1,"K_u input value outside of valid range (0,1)");
      assert(f_r >0 and f_r<1,"f_r input value outside of valid range (0,1)");
      assert(g_ref >0,"g_ref input value outside of valid range (0,inf)");
      assert(X >0,"X input value outside of valid range (0,inf)");
      assert(gamma >=0,"gamma input value outside of valid range <0,inf)");

      // STEP 1: Data preparation
      n_Hz:=n_rpm/60;
      f:=n_Hz*p;
      q:=Q_s/(2*p*m);
      K_w:=(AuxiliaryFunc.windingf(Q_s,p,m,coilspan,gamma,1))[1];
      if K_w<0 then
        Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be negative, please verify input data: Q_s, p, m","");
        K_w:=abs(K_w);
      elseif K_w>1 then
        Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be negative, please verify input data: Q_s, p, m","");
        K_w:=1;
      end if;

      f:=n_rpm/60*p;
      n:=f/p;
      P_mec:=(n_rpm*Constants.pi/30)*T;
      C_mec:=C_mec_ini*1000;

      // STEP 2: Mechanical machine constant and calculation of D
      if (C_mec==0 and C_mec_family_flag==false) then
        C_mec:=AuxiliaryFunc.CmecCALC(p, P_mec);
        C_mec:=C_mec*1000;
      elseif C_mec_family_flag==true then
        C_mec:=Modelica.Math.Vectors.interpolate(
        (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 1],
        (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 2],
        P_mec/(2*p));
        C_mec:=C_mec*1000;
      end if;

      D_shaft:=0;
      if D_shaft==0 then
        for i in 1:size(PowerDshaft,1) loop
          if PowerDshaft[i,1]*1000>=P_mec then
            D_shaft:=PowerDshaft[i, 4]/1000;
            break;
          elseif i==size(PowerDshaft,1) then
            D_shaft:=PowerDshaft[size(PowerDshaft, 1), 4]/1000;
            Modelica.Utilities.Streams.print("*** ERROR: D_shaft could not be properly selected Maximun Value as default","");
          end if;
        end for;
      end if;

      if D_shaft_internal==0 then
        D_shaft_internal:=D_shaft*Internal_shaft_coeficient;
      end if;

      for i in 1:1000 loop
        D:=(P_mec/(C_mec*X*n_Hz))^0.3333;
        l_eff:=D*X;
        I_s:=P_mec/(eff*m*U_ph*PF);

        // STEP 3: Select winding type and rotor bars number
        t_u:=Constants.pi*D/Q_s;
        t_p:=Constants.pi*D/(2*p);
        l:=l_eff - 2*g;

        // STEP 4: Calculate rotor diameter
        D_or:=D-2*g;

        // STEP 5: Obtaining Aload
        C:=C_mec/(PF*eff/K_E_U_ratio);
        A_load:=C*sqrt(2)/(Constants.pi*Constants.pi*K_w*B_g);
        if
          (A_load>30000 and A_load < 65000) then
          break;
        end if;
        if (A_load<30000) then
          C_mec:=C_mec*1.1;
        else
          C_mec:=C_mec/1.1;
        end if;
      end for;

      // STEP 6: Back-EMF and Nph
      alpha_i:=0.75;
      flux_g:=alpha_i*B_g*t_p*l_eff;
      N_ph:=K_E_U_ratio*U_ph*sqrt(2)/(2*Constants.pi*f*K_w*flux_g);
      z_Q:=AuxiliaryFunc.round(2*a*m*N_ph/Q_s);
      N_ph:=z_Q*Q_s/(2*a*m);
      flux_g:=K_E_U_ratio*U_ph*sqrt(2)/(2*Constants.pi*f*K_w*N_ph);

      // STEP 7: New value of B_g
      B_g:=flux_g/(alpha_i*t_p*l_eff);

      for j in 1:1000 loop
        B_g:=flux_g/(alpha_i*t_p*l_eff);

        // STEP 8: Stator and rotor geometry
        h_y:=flux_g/(B_yoke_s*2*f_r*(l - n_v*b_v));
        b_z:=(l_eff*t_u*B_g)/(B_tooth_s*f_r*(l - n_v*b_v));
        b_s:=t_u - b_z;
        h_tr:=z_Q*(I_s/(a*J_s))/(b_s*K_u);
        h_tr:=AuxiliaryFunc.htrmodif(Q_s,h_tr,b_s);
        h_z:=h_tr + delta_htr;
        t_r:=Constants.pi*D_or/Q_r;
        I_r:=(0.8*PF+0.2)*(2*m*N_ph*K_w*I_s)/Q_r;
        b_z_r:=B_g*t_r/(f_r*B_tooth_r);
        b_s_r:=t_r-b_z_r;
        S_bar:=I_r/J_r;
        h_z_r:=(S_bar-0.5*1.5*10^(-6))/b_s_r+0.5*10^(-3);
        h_tr_r:=h_z_r + 0.5E-3;
        Iend_ring:=I_r*Q_r/(2*sin(Constants.pi*p/Q_r));
        Send_ring:=Iend_ring/(0.77*J_r);

        // STEP 9: Recalculate alphai through magnetic voltages method
        //b_1:=2.5*10^(3);
        K_c:=t_u/(t_u-(b_1*b_1/g)/(5+b_1/g));
        U_g:=(B_g/Constants.mue_0)*K_c*g;
        H_tooth_s:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_tooth_s);
        for j in 1:30 loop
          if (j==1) then
            B_tooth_s_real:=B_tooth_s;
          end if;
          H_tooth_s_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_tooth_s_real);
          B_tooth_s_real:= B_tooth_s-Constants.mue_0*H_tooth_s_real*(l_eff*t_u/(f_r*(l-n_v*b_v)*b_z)-1);
        end for;
        U_tooth_s:=H_tooth_s_real*h_z;
        H_tooth_r:=Modelica.Math.Vectors.interpolate(Mat_r[:, 1],Mat_r[:, 2],B_tooth_r);
        for j in 1:30 loop
          if (j==1) then B_tooth_r_real:=B_tooth_r;
          end if;
        H_tooth_r_real:=Modelica.Math.Vectors.interpolate(Mat_r[:, 1],Mat_r[:, 2],B_tooth_r_real);
        B_tooth_r_real:=B_tooth_s - Constants.mue_0*H_tooth_s_real*(l_eff*t_u/(f_r*(l - n_v*b_v)*b_z_r) -
            1);
        end for;
        U_tooth_r:=H_tooth_r_real*h_z_r;
        Ksat:=(U_tooth_s + U_tooth_r)/U_g;
        alpha_i:=Modelica.Math.Vectors.interpolate(Ksat_alpha[:, 1],Ksat_alpha[:, 2], Ksat);

        // STEP 10: Recalculate Bg_new
        B_g_new:=flux_g/(alpha_i*t_p*l_eff);
        if (abs(B_g_new-B_g) < 0.01) then
          break;
        end if;
      end for;

      // STEP 11
      B_g_final:=B_g_new;
      flux_g:=l_eff*t_p*B_g_final*alpha_i;
      E:=2*Constants.pi*f*K_w*N_ph*flux_g/sqrt(2);
      P_mec:=C_mec*D*D*l_eff*n_Hz;
      D_os:=D+2*h_y+2*h_z;

      DataFiles.writeCSVmatrix("MotorIM_Structure.csv",{"B_g_final","flux_g","alpha_i","E","P_mec","PF","I_r","I_s","D","D_os","b_z","b_s","h_y","h_z","h_tr","D_or","b_z_r","b_s_r","h_z_r","h_tr_r","l","g","N_ph"},[B_g_final,flux_g,alpha_i,E,P_mec,PF,I_r,I_s,D,D_os,b_z,b_s,h_y,h_z,h_tr,D_or,b_z_r,b_s_r,h_z_r,h_tr_r,l,g,N_ph],",");
      //DataFiles.writeMATmatrix("MotorIM_Structure.mat",{"B_g_final"},[B_g_final],false);
      //DataFiles.writeMATmatrix("MotorIM_Structure.mat",{"flux_g"},[flux_g],true);

    //      Modelica.Utilities.Streams.print("B_g_final="+Modelica.Math.Vectors.toString(vector(B_g_final)),"");
    //      Modelica.Utilities.Streams.print("flux_g="+Modelica.Math.Vectors.toString(vector(flux_g)),"");
    //      Modelica.Utilities.Streams.print("alpha_i="+Modelica.Math.Vectors.toString(vector(alpha_i)),"");
    //
    //      Modelica.Utilities.Streams.print("E="+Modelica.Math.Vectors.toString(vector(E)),"");
    //      Modelica.Utilities.Streams.print("P_mec="+Modelica.Math.Vectors.toString(vector(P_mec)),"");
    //      Modelica.Utilities.Streams.print("PF="+Modelica.Math.Vectors.toString(vector(PF)),"");
    //      Modelica.Utilities.Streams.print("I_r="+Modelica.Math.Vectors.toString(vector(I_r)),"");
    //      Modelica.Utilities.Streams.print("I_s="+Modelica.Math.Vectors.toString(vector(I_s)),"");
    //
    //      Modelica.Utilities.Streams.print("D="+Modelica.Math.Vectors.toString(vector(D*1000)),"");
    //      Modelica.Utilities.Streams.print("D_os="+Modelica.Math.Vectors.toString(vector(D_os*1000)),"");
    //      Modelica.Utilities.Streams.print("b_z="+Modelica.Math.Vectors.toString(vector(b_z*1000)),"");
    //      Modelica.Utilities.Streams.print("b_s="+Modelica.Math.Vectors.toString(vector(b_s*1000)),"");
    //      Modelica.Utilities.Streams.print("h_y="+Modelica.Math.Vectors.toString(vector(h_y*1000)),"");
    //      Modelica.Utilities.Streams.print("h_z="+Modelica.Math.Vectors.toString(vector(h_z*1000)),"");
    //      Modelica.Utilities.Streams.print("h_tr="+Modelica.Math.Vectors.toString(vector(h_tr*1000)),"");
    //      Modelica.Utilities.Streams.print("D_or="+Modelica.Math.Vectors.toString(vector(D_or*1000)),"");
    //      Modelica.Utilities.Streams.print("b_z_r="+Modelica.Math.Vectors.toString(vector(b_z_r*1000)),"");
    //      Modelica.Utilities.Streams.print("b_s_r="+Modelica.Math.Vectors.toString(vector(b_s_r*1000)),"");
    //      Modelica.Utilities.Streams.print("h_z_r="+Modelica.Math.Vectors.toString(vector(h_z_r*1000)),"");
    //      Modelica.Utilities.Streams.print("h_tr_r="+Modelica.Math.Vectors.toString(vector(h_tr_r*1000)),"");
    //
    //      Modelica.Utilities.Streams.print("l="+Modelica.Math.Vectors.toString(vector(l*1000)),"");
    //      Modelica.Utilities.Streams.print("g="+Modelica.Math.Vectors.toString(vector(g*1000)),"");
    //      Modelica.Utilities.Streams.print("N_ph="+Modelica.Math.Vectors.toString(vector(N_ph)),"");

      annotation (
        Documentation(info="<html>

<head>
<title>General IM Pre-sizing help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<h2><span lang=EN-US>1.1<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Table of Inputs</span></h2>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Name</span></b></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Description</span></b></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border:solid windowtext 1.0pt;
  border-left:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Units</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>U_ph</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Phase Voltage</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>V</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>m</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of phases</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>a</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of parallel paths</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>p</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of pair poles</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>P_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  mechanical Power</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>W</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>n</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Speed</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>rpm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>eff</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Efficiency</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>PF</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Power
  Factor</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>B_toothr</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Rotor Tooth</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>B_tooths</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Stator Tooth</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>B_yoke</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Yoke</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>B_g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Airgap</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>J_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Current
  density at stator</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A<sub>rms</sub>/mm<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>J_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Current
  density at rotor</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A<sub>rms</sub>/mm<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Airgap</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>Q_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of Stator Slots</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>Q_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of Rotor Slots</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>n_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of ventilating ducts</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>b_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Ventilating
  Ducts width</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>F_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Lamination
  factor</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>K_u</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Slot
  Filling Factor</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>K_w</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Winding
  Factor</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>C_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Mechanical
  Constant</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>kWs/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>X</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Aspect
  Ratio</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>coilspan</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Coil
  Span</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span style='font-size:10.0pt'>Material.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Stator/Rotor
  B-H material characteristic curve</span></p>
  </td>
  <td width=58 valign=top style='width:43.35pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T-A/m</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019757'></a><a name='_Toc385322398'></a><a
name='_Toc385318049'></a><a name='_Ref413683965'><span lang=EN-GB>Table </span></a><span lang=EN-GB>2</span><span lang=EN-GB>. List of inputs: variable name, description
and units in IS</span></p>

<h2><span lang=EN-US>1.2<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Algorithm concept</span></h2>

<p class=MsoNormal><span lang=EN-GB>Induction Machine pre-sizing is based on an
integral sizing algorithm; both stator and rotor are sized at the same time. In
the first step stator is found by means of mechanical constant and linear load,
variables that determine the bore diameter. Stator geometry is found by means
of the number of conductors required to fulfil electrical inputs (phase voltage
and current density) and the desired flux densities in each part. These flux densities
are corrected from apparent to real value by an interpolation based on material
curve. Rotor bar currents and then calculated and the whole machine circuit is
evaluated in order to obtain its saturation factor. This saturation factor then
has a flux density shape correction factor that is applied in stator sizing
again, performing the following iteration. When successive shape correction
factors difference is smaller than a threshold the sizing loop ends. Pre-sizing
was validated by means of a finite elements model.</span></p>

<h2><span lang=EN-US>1.3<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Program Structure Explanation</span></h2>

<p class=MsoNormal><span lang=EN-GB>Please use </span><span
lang=EN-GB>Figure 1</span><span lang=EN-GB> to follow this explanation. Once
inputs have been set mechanical constant (C_mec) is calculated if its input
value was 0. Then the first loop starts. This loop is intended to match bore
diameter (D), electrical loading (A_load) and mechanical constant within
acceptable limits for induction machines. It starts by setting bore diameter (D)
and stator current (I_s). From bore diameter basic are obtained measures like
length (l) and rotor and stator pitches (t_u, t_s), from stator current (I_s) a
new value of electrical loading is generated. If this is within the specified
range loop breaks, if not, depending on this value mechanical constant is increased
or decreased and the process starts again.</span></p>

<p class=MsoNormal><span lang=EN-GB>Once these initial values are known the
program proceeds to calculate airgap flux (flux_g) and by means of this value
the number of conductors in series (N_ph) per phase and the number of
conductors per slot (z_Q). These to values are rounded and flux value is
recalculated from the rounded values, therefore also a new value of flux
density in airgap (B_g) is obtained. </span></p>

<p class=MsoNormal><span lang=EN-GB>Then the second loop starts. This loop is
intended to calculate the detailed geometry of stator and rotor slots. Geometry
must be one able to fulfil the desired value of flux density in the airgap but
this is modified in shape by a saturation factor (alpha_i). First stator
geometry is calculated (h_tr, b_s, etc.), ad then rotor geometry. Once rotor
geometry is known current in rotor bars is obtained (I_r). And from it the end
ring current (Iend_ring) and required area (Send_ring).</span></p>

<p class=MsoNormal><span lang=EN-GB>Geometry leads to magnetic voltage drops
(H_tooth, H_yoke) in each part of the machine. These magnetic voltages are
corrected between its apparent and real values (this process requires another
small loop for each corrected magnetic voltage) and then compared to airgap
magnetic voltage (U_g). Airgap magnetic voltage is corrected by Carter
coefficient (K_c). The relative weight of both factors returns a saturation factor
(k_s) that leads to a new shape factor (alpha_i) which corrects airgap flux
density max value. Once two consecutive iterations difference is less than a
desired threshold loop is broken.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=298 height=956 src='General_IM_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Ref414366089'><span lang=EN-GB>Figure </span></a><span lang=EN-GB>1</span><span lang=EN-GB>. Global Sizing Algorithm explanation.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>"));
    end Iduction_Motor_Sizing;

    function IPM_Motor_Sizing "Sizing_IPM_23"
     import Modelica.Constants;
     import Modelica.SIunits;
     constant Real c=68e-6 "Cu Temperature Constant";
     parameter input Modelica.SIunits.Conversions.NonSIunits.Angle_deg alpha_v(min=0,max=360)=15
        "Angle for the magnets 'V' aperture"
     annotation (Dialog(tab="Rotor"));
     parameter input Integer Shape(min=0,max=2)=0
        "Desired IPM rotor shape, 0=>Spoke, 1=>V-shape, 2=>Planar";
     parameter input SIunits.Voltage U_ph(min=0)=300 "Rated phase voltage"
     annotation (Dialog(group="Nominal parameters"));
     parameter input Integer m(min=0)=3 "Number of phases"
     annotation (Dialog(group="Nominal parameters"));
     parameter input Integer p(min=2)=4 "Number of pole pairs"
     annotation (Dialog(group="Nominal parameters"));

     parameter input SIunits.Conversions.NonSIunits.AngularVelocity_rpm n_rpm(min=0)=3500
        "Rated mechanical speed"
     annotation (Dialog(group="Nominal parameters"));

     parameter input Real PF(min=0,max=1)=0.93 "Power factor"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.Torque T(min=0)=190 "Rated torque"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.Current I_s(min=0)=75 "Rated current"
     annotation (Dialog(group="Nominal parameters"));
     parameter input Boolean C_mec_family_flag=false "C_mec_family_flag"
     annotation (Dialog(group="Nominal parameters"));
     parameter input Real C_mec_ini(min=0)=300 "Mechanical constant reference"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.CurrentDensity J_s(min=0)=7.5*10^6
        "Stator current density"
     annotation (Dialog(tab="Stator",group="Winding"));
     parameter input SIunits.Length g(min=0)=0.0007 "Airgap"
     annotation (Dialog(tab="Airgap"));
     parameter input SIunits.MagneticFluxDensity B_g_ini(min=0)=0.95
        "Airgap MAX flux density initial value"
     annotation (Dialog(tab="Airgap"));
     parameter input String Stator_Material_File="M270_35A.txt"
        "Stator Material B(H) characteristic"
     annotation(Dialog(tab="Stator",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Stator Material B(H) characteristic file")));
     parameter input String Ksat_alpha_File="Ksat_alpha.txt"
        "Saturation factor characteristic"
     annotation(Dialog(tab="Stator",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Ksat_alpha(alpha) characteristic file")));
     parameter input SIunits.MagneticFluxDensity B_yoke(min=0)=1.7
        "Yoke MAX flux density initial value"
     annotation (Dialog(tab="Stator",group="Magnetic circuit"));
     parameter input SIunits.MagneticFluxDensity B_tooth(min=0)=1.3
        "Stator MAX Tooth flux density initial value"
     annotation (Dialog(tab="Stator",group="Magnetic circuit"));

     parameter input Integer a(min=1)=1 "Parallel paths"
     annotation (Dialog(tab="Stator",group="Winding"));
     parameter input Real X(min=0)=0.8 "Aspect ratio"
     annotation (Dialog(tab="Stator",group="Geometry"));
     parameter input Integer Q_s(min=1)=48 "Number of stator slots"
     annotation (Dialog(tab="Stator", group="Geometry"));
     parameter input Real f_r(min=0,max=1)=0.95 "Lamination factor"
     annotation (Dialog(tab="Stator", group="Winding"), Evaluate=true);
     parameter input Real K_u(min=0,max=1)=0.65 "Slot filling factor"
     annotation (Dialog(tab="Stator", group="Winding"));
     parameter input SIunits.Angle gamma(min=0,max=360)=0 "Skewing angle"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length b_1(min=0)=3e-3 "Slot opening"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length D_shaft_internal_ref(min=0)=0.075
        "Shaft internal diameter reference"
     annotation (Dialog(tab="Rotor"));
     parameter input Real Internal_shaft_coefficient=1.3
        "Ratio between external and internal shafts"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length D_shaftB_ref(min=0)=0.03
        "Shaft external diameter reference"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length D_hub_ref(min=0)=0.100
        "Rotor magnet hub barrier diameter reference"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length l_shaft_ref(min=0)=0
        "Shaft length reference"
     annotation (Dialog(tab="Rotor"));

     parameter input SIunits.Length l_shaft_extra_ref(min=0)=50/1000
        "Shaft external lenght"
     annotation (Dialog(tab="Rotor"));
     parameter input Integer case_type(min=1,max=2)=1
        "Casing type: 1-Circular, 2-Square solid"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.MagneticFluxDensity B_r(min=0)=1.2
        "Remanent flux density"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.MagneticFieldStrength H_c(max=0)=-0.95e6
        "Coercive field"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.RelativePermeability mu_r_PM(min=0)=1.0052
        "Magnet relative permeability"
     annotation (Dialog(tab="Rotor"));
     parameter input Real mcf_PM(min=0,max=1)=0.833 "Magnet cover factor"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length h_i(min=0)=0.0002
        "Thickness insulation layer between PMs and rotor"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length DeltaD(min=0)=0.001
        "Distance between magnet pod and rotor surface"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length SlotAir(min=0)=0.0012 "Slot air"
     annotation (Dialog(tab="Rotor"));

     parameter input SIunits.Density ro_mst(min=0)=7460
        "Magnetic steel density"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Density ro_PM(min=0)=7400 "Magnet density"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Density ro_cu(min=0)=8750 "Cu density"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Density ro_case(min=0)=7850
        "Metal cabinet density"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Density ro_hub(min=0)=7460
        "Hub insulation steel density"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Density ro_shaft(min=0)=7460 "Shaft steel density"
     annotation (Dialog(tab="Rotor"));
     parameter input Real K_h(min=0)=50 "Magnetic steel hysteresis constant"
     annotation (Dialog(tab="Stator"));
     parameter input Real K_e(min=0)=0.05 "Magnetic steel eddy constant"
     annotation (Dialog(tab="Rotor"));
     parameter input Real n_i(min=1)=1.6 "Steinmetz constant"
     annotation (Dialog(tab="Rotor"));
     parameter input Integer R_fe_consider(min=0,max=1)=0
        "R_fe effect flag: 1-Consider R_fe effect, 2-do not consider R_fe effect"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_aro(min=0)=20
        "Reference resistivity temperature"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Resistivity ro_t_aro(min=0)=1.68E-8
        "Cu resistivity"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_amb(min=0)=35
        "Ambient temperature"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Conversions.NonSIunits.Temperature_degC Delta_t_work(min=0)=55
        "Temperature Increase Relative to Ambient"
     annotation (Dialog(tab="Stator"));
     parameter input Real K_uhc(min=0,max=1)=0.8 "Head coil filling factor"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Length l_extraH_ref(min=0)=0.008
        "Head coil longitudinal extra length reference"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Length l_extraW_ref(min=0)=0.015
        "Head coil winding longitudinal extra length reference"
        annotation (Dialog(tab="Stator"));
     parameter input Integer Embedded(min=0,max=1)=1
     annotation (Dialog(tab="Rotor"));

     output Integer pp "pole pairs number"
      annotation (Dialog(tab="Outputs"));
      output SIunits.MomentOfInertia Jr "Rotor's moment of inertia"
      annotation (Dialog(tab="Outputs"));
      output SIunits.MomentOfInertia Js "Stator's moment of inertia"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Current iMax "Maximum current"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Torque tauMax "Maximum torque"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Resistance R "Phase Resistance"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Conversions.NonSIunits.Temperature_degC TRef
        "Reference temperature"
      annotation (Dialog(tab="Outputs"));
      output Real Alpha "Linear temperature coefficient"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Inductance Ld "Magnetizing inductance along the d-axis"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Inductance Lq "Magnetizing inductance along the q-axis"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Voltage VsOpenCircuit "Open circuit voltage per phase"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Frequency fn "Nominal frequency"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Mass nM "Magnetic material mass"
      annotation (Dialog(tab="Outputs"));
      output SIunits.Mass mT "Motor mass"
      annotation (Dialog(tab="Outputs"));
    protected
     IPM_Structure.motorIPM motorIPM "Structure with whole motor info";
     SIunits.Frequency f=n_rpm/60*p "Rated electrical frequency";
     SIunits.Frequency n "Speed in Hz";
     SIunits.Power P_e "Rated electrical power";
     SIunits.Power P_mec "Rated mechanical power";
     Real eff "Efficiency";
     Integer coilspan "Coilspan";
     Real alphai_new=0.41
        "Seed value for alpha_i to perform the first iteration";
     Real alpha_i=40
        "Flux form factor, 'Old' value to compare for the first iteration (at the end a_i<=a_i_new)";
     Integer stator_counter=1
        "Limiter of the number of iterations to calculate stator";
     SIunits.Length delta_h_PM=0.11/1000 "h_PM step magnitude";
     SIunits.Length hpmlimiter=80 "maximum allowed h_PM";
     Real rotor_limiter=hpmlimiter/1000/delta_h_PM
        "Counter for the rotor calculation";
     parameter Integer delta_rotor_counter=15;
     Real p_lf "Magnet permeance leakage factor";
     parameter Real[:,:] Mat_s=DataFiles.readCSVmatrix(Stator_Material_File)
        "Stator material B(H) characteristic";
     parameter Real[:,:] Ksat_alpha=DataFiles.readCSVmatrix(Ksat_alpha_File)
        "Ksat_alpha(alpha) characteristic";
     SIunits.Length l_extraH=l_extraH_ref "Head coil longitudinal extra length";
     SIunits.Length l_extraW=l_extraW_ref
        "Head coil winding longitudinal extra length";
     SIunits.Length h_PM "Magnet thickness";
     Real C_mec=C_mec_ini "Mechanical constant";
     SIunits.MagneticFluxDensity B_sat "Iron saturation flux density";
     Real q "Slot per pole per phase";
     Real K_w "Winding factor";
     String[3,1] possibleshapes=["Spoke";"V-Shape";"Planar"];
     SIunits.Length D_hub=D_hub_ref "Rotor magnet hub barrier diameter";
     SIunits.Length D_shaftB=D_shaftB_ref "Shaft external diameter";
     SIunits.Length delta_htr "Height of teeth expansion";
     Integer htrmodifflag "Use of trapezoidal slot flat: 0-No, 1-Yes";
     SIunits.Length D;
     SIunits.Length l_eff;
     SIunits.Length l;
     SIunits.Length t_u "Slot pitch";
     SIunits.Length t_p "Polar pitch";
     //IPM_Structure.motorIPM motorIPM "Structure with whole motor info";
     SIunits.MagneticFlux flux_g "Airgap flux";
     Real N_ph "Number of turns per phase integer";
     Real N_ph1 "Number of turns per phase not rounded";
     Real z_Q;
     SIunits.MagneticFluxDensity B_g;
     SIunits.Length h_y;
     SIunits.Length b_z;
     SIunits.Length b_s;
     SIunits.Length h_tr;
     SIunits.Length h_z;
     SIunits.MagneticFieldStrength H_yoke;
     SIunits.RelativePermeability mu_r_yoke;
     SIunits.RelativePermeability mu_r_yoke2;
     SIunits.MagneticFieldStrength H_tooth;
     SIunits.RelativePermeability mu_r_tooth;
     SIunits.RelativePermeability mu_r_tooth2;
     SIunits.MagneticFluxDensity B_err;
     SIunits.MagneticFluxDensity B_tooth_real;
     Real real_counter;
     SIunits.MagneticFluxDensity B_tooth_real_old;
     SIunits.MagneticFieldStrength H_tooth_real;
     Real K_c "Carter fringing coefficient";
     SIunits.Length t_p_m "Polar pitch at half the airgap";
     SIunits.Length l_g "Length covered by magnet's projection";
     SIunits.Reluctance R_g "Airgap reluctance";
     SIunits.MagnetomotiveForce U_g "Airgap magnetic voltage";
     SIunits.Length b_yR;
     Real K_sat "Saturation constant";
     SIunits.MagnetomotiveForce U_stator;
     SIunits.Length D_os "Outer stator diameter";
     SIunits.Length D_or "Outer rotor diameter";
     SIunits.Length l_shaft=l_shaft_ref "Shaft length";
     SIunits.Length D_shaft "Shaft internal diameter";
     SIunits.Length D_shaft_internal=D_shaft_internal_ref
        "Shaft internal diameter";
     IPM_Structure.motorIPM_rotor RotorGeo "Structure with rotor information";
     Integer shapeflag "Geometry information variable";
     Integer failflag "Geometry calculation fail flag";
     Integer failpath "Geometry caluclation path fail flag";
     Real K_fc "Flux concentration factor";
     SIunits.Length l_PM_max "Maximum allowed magnet length for the h_PM";
     SIunits.Length l_PM "Magnet's length for the h_PM";
     SIunits.Length actualPMSlotAir "PMSlotAir after slot creation";
     SIunits.Length h_Tb "Top barrier reluctance height";
     SIunits.Length l_Tb "Top barrier reluctance length";
     SIunits.Length h_Tf "Top iron reluctance height";
     SIunits.Length l_Tf "Top iron reluctance length";
     SIunits.Length h_Bb "Bottom air barrier reluctance height";
     SIunits.Length l_Bb "Bottom air barrier reluctance length";
     SIunits.Length h_Hub "Bottom non-magnetic iron hub reluctance height";
     SIunits.Length l_Hub "Bottom non-magnetic iron hub reluctance length";
     SIunits.Length h_fe_first "First rotor iron reluctance height";
     SIunits.Length l_fe_first "First rotor iron reluctance length";
     SIunits.Length l_fe_last "Last rotor iron reluctance length";
     SIunits.Length l_web=0
        "Distance between manget pods at half barrier radius";
     String msg_handle "Handle for messages";
     SIunits.MagneticFluxDensity B_Tooth;
     SIunits.MagneticFieldStrength H_Tooth;
     SIunits.MagnetomotiveForce MMF_STo;
     SIunits.MagneticFluxDensity B_Yoke;
     SIunits.MagneticFieldStrength H_Yoke;
     SIunits.MagnetomotiveForce MMF_Yoke;
     SIunits.MagnetomotiveForce MMF_g;
     Real Sum_MMF;
     SIunits.MagneticFieldStrength H_Tb "Field strength top air barrier";
     SIunits.MagneticFieldStrength H_Tf "Field strength top iron barrier";
     SIunits.MagneticFieldStrength H_Bb "Field strength bottom air barrier";
     SIunits.MagneticFieldStrength H_Hub "Field strength hub (only spoke)";
     SIunits.MagneticFluxDensity B_Tb "Flux density top air barrier";
     SIunits.MagneticFluxDensity B_Bb "Flux density bottom air barrier";
     SIunits.MagneticFluxDensity B_Hub "Flux density hub";
     SIunits.MagneticFluxDensity B_Tf "Flux density top iron barrier (wedge)";
     SIunits.MagneticFlux flux_Tb "Flux top air barrier";
     SIunits.MagneticFlux flux_Tf "Flux top iron barrier";
     SIunits.MagneticFlux flux_Bb "Flux bottom air barrier";
     SIunits.MagneticFlux flux_Hub "Flux hub";
     SIunits.Permeance y_Tb;
     SIunits.Permeance y_Tf;
     SIunits.Permeance y_Bb;
     SIunits.Permeance y_Hub;
     SIunits.Permeance y_g;
     SIunits.Permeance y_T;
     SIunits.Permeance y_Y;
     SIunits.Permeance y_g_prima
        "Equivalent permeance of airgap + tooth + yoke";
     SIunits.Permeance y_0
        "Equivalent permeance of whole circuit g_prima || losses";
     SIunits.Permeance y_insu "Magnet pod margin permeance";
     SIunits.Permeance y_Rotor_first "Magnet pod margin permeance";
     SIunits.Permeance y_serie;
     SIunits.Permeance y_TOTAL;
     SIunits.MagneticFlux Sum_flux_loss;
     SIunits.MagneticFlux Sum_flux;
     Real MMF_PM;
     SIunits.Conversions.NonSIunits.Angle_deg roundalpha_v;
     SIunits.RelativePermeability murr;
     SIunits.MagneticFieldStrength EMC1;
     SIunits.MagneticFieldStrength EMC2;
     SIunits.RelativePermeability mu_r_EMC;
     SIunits.Length h_PM_EMC;
     SIunits.Length l_PM_EMC;
     SIunits.Permeance y_PM
        "Permanent magnet permeance (without equivalent magnet)";
     SIunits.MagneticFlux flux_gen "Generated flux";
     SIunits.MagneticFlux flux_m_disp
        "Permanent magnet flux (able to leave magnet)";
     SIunits.MagneticFlux flux_pm_lost
        "Flux that could not leave magnet because of MMF";
     SIunits.MagneticFlux flux_pm2g
        "Required flux (flux that left magnet and arrived to PM";
     SIunits.MagneticFlux flux_RE
        "Relative flux error between actual flux and required flux (another way to calculate error)";
     Real[200,10] evolutiondata;
     SIunits.Length l_extra "Head coil length";
     SIunits.Length h_case "Square case height";
     SIunits.Length l_case_exp_void "Internal margin length";
     SIunits.Length l_case_exp_front "Extra case front length";
     SIunits.Length l_case_exp_rear "Extra case rear length";
     SIunits.Length l_case "Square case length";
     SIunits.Length l_total "Total case length";
     SIunits.Length h_total "Total case height";
     SIunits.Length l_shaft_extra=l_shaft_extra_ref "Shaft external lenght";
     Real Omega "Web angle cover factor";
     Real p_lf_ref "Magnet permeance leakage factor reference";
     Integer n_v=0 "Number of ventilation ducts";
     SIunits.Length b_v=0 "Ventilation duct width";
     SIunits.Voltage BEMF "Back EMF at rated speed, no load RMS";

    algorithm
     assert(Embedded==0 or Embedded==1,"Embedded input value outside of valid range {0,1}");
     assert(alpha_v>=0 or alpha_v<=360,"alpha_v input value outside of valid range <0,360>");
     assert(Shape>=0 or Shape<=2,"Shape input value outside of valid range {0,1,2}");
     assert(U_ph>0,"U_ph input value outside of valid range (0,inf)");
     assert(m>0,"m input value outside of valid range (0,inf)");

     assert(T>0,"T input value outside of valid range (0,inf)");
     assert(I_s>0,"I_s input value outside of valid range (0,inf)");
     assert(PF>0 and PF<=1,"PF input value outside of valid range (0,1>");
     assert(n_rpm>0,"n input value outside of valid range (0,inf)");
     assert(C_mec>0,"C_mec input value outside of valid range (0,inf)");
     assert(J_s>0,"J_s input value outside of valid range (0,inf)");
     assert(p>=2,"p input value outside of valid range <2,inf)");
     assert(B_g_ini>0,"B_g_ini input value outside of valid range (0,inf)");
     assert(B_yoke>0,"B_yoke input value outside of valid range (0,inf)");
     assert(B_tooth>0,"B_tooth input value outside of valid range (0,inf)");
     assert(g>0,"g input value outside of valid range (0,inf)");
     assert(a>=1,"a input value outside of valid range <1,inf)");
     assert(X>0,"X input value outside of valid range (0,inf)");
     assert(Q_s>=1,"Q_s input value outside of valid range <1,inf)");
     assert(f_r>0 and f_r<=1,"f_r input value outside of valid range (0,1>");
     assert(K_u>0 and K_u<=1,"K_u input value outside of valid range (0,1>");
     assert(gamma>=0 and gamma<=360,"gamma input value outside of valid range <0,360>");
     assert(K_uhc>0 and K_uhc<=1,"K_uhc input value outside of valid range (0,1>");
     assert(b_1>0,"b_1 input value outside of valid range (0,inf)");
     assert(D_shaft_internal_ref >=0,"D_shaft_ref input value outside of valid range <0,inf)");
     assert(D_shaftB_ref >=0,"X input value outside of valid range <0,inf)");
     assert(D_hub_ref >=0,"X input value outside of valid range <0,inf)");
     assert(l_shaft_ref >=0,"l_shaft_ref input value outside of valid range <0,inf)");
     assert(l_shaft_extra_ref >=0,"l_shaft_extra_ref input value outside of valid range <0,inf)");
     assert(case_type >=1 and case_type<=2,"case_type input value outside of valid range {1,2}");
     assert(B_r >0,"B_r input value outside of valid range (0,inf)");
     assert(H_c <0,"H_c input value outside of valid range (-inf,0)");
     assert(mu_r_PM >0,"mu_r_PM input value outside of valid range (0,inf)");
     assert(mcf_PM >0 and mcf_PM<1,"mcf_PM input value outside of valid range (0,1)");
     assert(h_i >=0,"mu_r_PM input value outside of valid range <0,inf)");
     assert(DeltaD >0,"mu_r_PM input value outside of valid range (0,inf)");
     assert(SlotAir >0,"mu_r_PM input value outside of valid range (0,inf)");
     assert(ro_mst >0,"ro_mst input value outside of valid range (0,inf)");
     assert(ro_PM >0,"ro_PM input value outside of valid range (0,inf)");
     assert(ro_cu >0,"ro_cu input value outside of valid range (0,inf)");
     assert(ro_case >0,"ro_case input value outside of valid range (0,inf)");
     assert(ro_hub >0,"ro_hub input value outside of valid range (0,inf)");
     assert(ro_shaft >0,"ro_shaft input value outside of valid range (0,inf)");

     assert(K_h >0,"ro_shaft input value outside of valid range (0,inf)");
     assert(K_e >0,"ro_shaft input value outside of valid range (0,inf)");
     assert(n_i >=1,"ro_shaft input value outside of valid range <1,inf)");
     assert(R_fe_consider >=0 and R_fe_consider<=1,"R_fe_consider input value outside of valid range {0,1}");
     assert(t_aro >0,"t_aro input value outside of valid range (0,inf)");
     assert(ro_t_aro >0,"ro_t_aro input value outside of valid range (0,inf)");
     assert(t_amb >0,"t_amb input value outside of valid range (0,inf)");
     assert(Delta_t_work >0,"Delta_t_work input value outside of valid range (0,inf)");
     assert(l_extraH_ref >=0,"l_extraH_ref input value outside of valid range <0,inf)");
     assert(l_extraW_ref >=0,"l_extraW_ref input value outside of valid range <0,inf)");

     alphai_new:=0.41;
     alpha_i:=40;
     stator_counter:=1;
     delta_h_PM:=0.11/1000;
     hpmlimiter:=80;
     if hpmlimiter==0 then
       hpmlimiter:=12;
     end if;
     h_PM:=0.1/1000;
     rotor_limiter:=hpmlimiter/1000/delta_h_PM;
     // STEP 1.1: Data preparation

     // Initial check to see if angle and shape are coherent
     if ((alpha_v*Constants.pi/180<=Constants.pi/(2*p) and Shape==0) or (alpha_v*Constants.pi/180>=Constants.pi and Shape==2) or (((alpha_v*Constants.pi/180>=Constants.pi/(2*p)) or (alpha_v*Constants.pi/180<=Constants.pi)) and Shape==1)) then
       Modelica.Utilities.Streams.print("--Shape correctly selected","");
     else
       Modelica.Utilities.Streams.print("****** ERROR: Incoherent Data Input ****** Check Shape Selected & alpha_v chosen.","");
     end if;

     n_v:=0;
     b_v:=0;
     P_e:=3*U_ph*I_s*PF;
     P_mec:= 2*Constants.pi*T*n_rpm/60;
     eff:=P_mec/P_e;
     n:=n_rpm/60;
     f:=n*p;

     C_mec:=C_mec*1000;
     B_sat:=0.95*max(Mat_s[:, 1]);

     // Winding factor calculation

     q:=Q_s/(2*p*m);
     coilspan:=integer(Q_s/p/2);
     K_w:=(AuxiliaryFunc.windingf(Q_s,p,m,coilspan,gamma,1))[1];
     if K_w < 0 then
       Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be negative, please verify input data: Q_s, p, m","");
       K_w:=abs(K_w);
     elseif K_w>1 then
       Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be >1, please verify input data: Q_s, p, m","");
       K_w:=1;
     end if;
     // STEP 2: Variable Input Calculations
     D_shaft:=AuxiliaryFunc.PowerDShaft(P_mec, Internal_shaft_coefficient);

     if D_shaft_internal==0 then
       D_shaft_internal:=D_shaft*Internal_shaft_coefficient;
     end if;
     if D_hub==0 then
       D_hub:=D_shaft_internal*1.12;
     end if;
    //  if D_shaftB==0 then
    //    D_shaftB:=D_shaft*0.4;
    //  end if;

     // Machine Size Considerations
     if P_mec<10000 then
       delta_htr:=0.001;
       htrmodifflag:=0;
     elseif P_mec>=10000 then
       delta_htr:=0.002;
       htrmodifflag:=1;
     end if;

     // Efficiency calculation (only for MOTOR, not for GENERATOR)
     if eff==0 then
       if P_e==0 then
         P_e:=3*U_ph*I_s*PF;
       end if;
       if P_mec==0 then
         P_mec:=T*n_rpm*Constants.pi/30;
       end if;
       eff:=P_mec/P_e;
     end if;

     C_mec:=C_mec_ini*1000;
     if C_mec==0 and C_mec_family_flag==false then
       C_mec:=AuxiliaryFunc.CmecCALC(p, P_mec);
       C_mec:=C_mec*1000;
     elseif (C_mec_family_flag==true) then
        C_mec:=Modelica.Math.Vectors.interpolate(
          (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 1],
          (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 2],
          P_mec/(2*p));
          C_mec:=C_mec*1000;
     end if;

     // STEP 3: Obtaining Aload & Basic Geometry

     D:=(P_mec/(C_mec*X*n))^0.3333;
     l_eff:=D*X;
     l:=l_eff - 2*g;
     t_u:=Constants.pi*D/Q_s;
     t_p:=Constants.pi*(D - 2*g)/(2*p);

     // Structure assembling
     motorIPM.p:=p;
     motorIPM.m:=m;
     motorIPM.g:=g;
     motorIPM.RatedPower:=P_mec;
     motorIPM.classe:=-1;
     motorIPM.specs.f_r:=f_r;
     motorIPM.flags.R_fe_consider:=R_fe_consider;

     // STEP 5: Stator dimensioning based on constant B_G

     while (abs(alphai_new-alpha_i) >= 0.001 and stator_counter<30) loop
       alpha_i:=alphai_new;
       flux_g:=B_g_ini*t_p*l_eff*alpha_i;
       N_ph1:=0.98*U_ph*sqrt(2)/(2*Constants.pi*f*K_w*flux_g);
       z_Q:=AuxiliaryFunc.round(2*a*m*N_ph1/Q_s);
       N_ph:=z_Q*Q_s/(2*a*m);

       // STEP 8: New values because of integer N_ph

       flux_g:=0.98*U_ph*sqrt(2)/(2*Constants.pi*f*K_w*N_ph);
       B_g:=flux_g/(t_p*l_eff*alpha_i);

       // STEP 9: Stator Geometry

       h_y:=flux_g/(B_yoke*2*f_r*(l - n_v*b_v));
       b_z:=(l_eff*t_u*(B_g*alpha_i))/(B_tooth*(f_r*l));
       b_s:=(Constants.pi*(D + 2*delta_htr)/Q_s) - b_z;
       h_tr:=z_Q*(I_s/(a*J_s))/(b_s*K_u);

       // Trapezoid correction for size 2 machines
       if htrmodifflag==1 then
         h_tr:=AuxiliaryFunc.htrmodif(Q_s,h_tr,b_s);
       end if;
       h_z:=h_tr + delta_htr;

       // STEP 5.4: Magnetic iron working points
       // Saturated:
       H_yoke:=Modelica.Math.Vectors.interpolate(Mat_s[:,1],Mat_s[:,2],B_yoke);
       mu_r_yoke:=B_yoke/(H_yoke*Constants.mue_0);
       H_tooth:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_tooth);
       mu_r_tooth:=B_tooth/(H_tooth*Constants.mue_0);
       // Non saturated (for reluctances)
        mu_r_yoke2:=B_yoke/(Constants.mue_0*Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_yoke/3));
        mu_r_tooth2:=B_tooth/(Constants.mue_0*Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_tooth*4/5));

       // STEP 5.5: Tooth losses compensation
       B_tooth_real:=B_tooth;
       B_err:=1;
       real_counter:=1;

       // Real values based on initials and new calculated geometry (b_z)
       while (B_err>0.0001 and real_counter<30) loop
         // For tooth
         B_tooth_real_old:=B_tooth_real;
         H_tooth_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_tooth_real);
         B_tooth_real:=B_tooth - Constants.mue_0*H_tooth_real*(l_eff*t_u/(f_r*(l - n_v*b_v)*b_z) - 1);
         mu_r_tooth:=B_tooth_real/(H_tooth_real*Constants.mue_0);
         // While conditions
         B_err:=abs(B_tooth_real - B_tooth_real_old);
         real_counter:=real_counter + 1;
       end while;

       // STEP 5.6: Recalcualte Bg

       K_c:=t_u/(t_u - (b_1*b_1/g)/(5 + b_1/g));

       // Airgap Magnetic Voltage
       t_p_m:=(D - g)*Constants.pi/(2*p);
       l_g:=t_p_m*mcf_PM;
       R_g:=K_c*g/(Constants.mue_0*t_p_m*l_eff);
       U_g:=flux_g*R_g;

       // Stator Magnetic Voltage
       b_yR:=1/2*Constants.pi*(D + 2*h_z + h_y)/(2*p);
       U_stator:=H_tooth_real*h_z + H_yoke*b_yR;
       K_sat:=(U_stator)/U_g;

       // Limiter to the interpolation data
       if K_sat > 1.655 then
         K_sat:=1.655;
       end if;

       // New alpha_i value and evaluation
       alphai_new:=Modelica.Math.Vectors.interpolate(
           Ksat_alpha[:, 1],
           Ksat_alpha[:, 2],
           K_sat);

       stator_counter:=stator_counter + 1;

       // STEP 6: Geometry calculation for rotor validation
       D_os:=(D + 2*h_y + 2*h_z);
       D_or:=(D - 2*g);

       // Output structure
       motorIPM.stator.case_type:=case_type;
       motorIPM.stator.Q_s:=Q_s;
       motorIPM.stator.D:=D;
       motorIPM.stator.I_s:=I_s;
       motorIPM.stator.J_s:=J_s;
       motorIPM.stator.win.a:=a;
       motorIPM.stator.win.N_ph:=N_ph;
       motorIPM.stator.win.K_w:=K_w;
       motorIPM.stator.win.K_uhc:=K_uhc;
       motorIPM.stator.win.K_u:=K_u;
       motorIPM.stator.win.coilspan:=coilspan;
       motorIPM.stator.win.S_cond:=h_y*b_s*K_u/z_Q;
       motorIPM.stator.dim.K_c:=K_c;
       motorIPM.stator.dim.D_os:=D_os;
       motorIPM.stator.dim.D_is:=D;
       motorIPM.stator.dim.l:=l;
       motorIPM.stator.dim.l_eff:=l_eff;
       motorIPM.stator.dim.X:=X;
       motorIPM.stator.dim.h_z:=h_z;
       motorIPM.stator.dim.b_z:=b_z;
       motorIPM.stator.dim.h_tr:=h_tr;
       motorIPM.stator.dim.b_s:=b_s;
       motorIPM.stator.dim.h_y:=h_y;
       motorIPM.stator.dim.b_1:=b_1;
       motorIPM.stator.dim.delta_htr:=delta_htr;

     end while;

     Modelica.Utilities.Streams.print("**** STATOR GEOMETRY SUCCESSFULLY CALCULATED ****   final alphai: "+Modelica.Math.Vectors.toString(vector(alphai_new)),"");

     // Data preparation after stator calculation
     if l_shaft==0 then
       l_shaft:=3*l;
     end if;

    //  if D_shaft==0 then
    //    D_shaft:=AuxiliaryFunc.round(D/2 + 3);
    //  end if;

     // Rotor Geometry
     // Rotor geometry is based on predefined value of flux_g, so the dimensions of PM should match to obtain this desired flug_g value

     flux_g:=B_g*alpha_i*t_p*l;

     for rotor_counter in 1:integer(rotor_limiter) loop
       RotorGeo:=AuxiliaryFunc.RotorShape(
         Shape,
         D - 2*g,
         D_shaft_internal,
         p,
         h_PM,
         alpha_v,
         mcf_PM,
         SlotAir,
         DeltaD,
         D_hub,
         Embedded);

       // Local assignments to shorten formulas
       shapeflag:=RotorGeo.shape;
       failflag:=RotorGeo.fail;
       failpath:=RotorGeo.failpath;
       K_fc:=RotorGeo.K_fc;
       l_PM_max:=RotorGeo.dim.l_PM_max;
       l_PM:=RotorGeo.dim.l_PM;
       actualPMSlotAir:=RotorGeo.dim.actualPMSlotAir;
       h_Tb:=RotorGeo.dim.h_Tb;
       l_Tb:=RotorGeo.dim.l_Tb;
       h_Tf:=RotorGeo.dim.h_Tf;
       l_Tf:=RotorGeo.dim.l_Tf;
       h_Bb:=RotorGeo.dim.h_Bb;
       l_Bb:=RotorGeo.dim.l_Bb;
       h_Hub:=RotorGeo.dim.h_Hub;
       l_Hub:=RotorGeo.dim.l_Hub;
       h_fe_first:=RotorGeo.dim.h_fe_first;
       l_fe_first:=RotorGeo.dim.l_fe_first;
       l_fe_last:=RotorGeo.dim.l_fe_last;
       l_web:=RotorGeo.dim.l_web;

       // Rotor variables assignment
       RotorGeo.dim.D_or:=D_or;
       RotorGeo.dim.D_ir:=D_shaft_internal;
       RotorGeo.dim.D_shaft:=D_shaft_internal;
       RotorGeo.dim.D_shaftB:=D_shaft;
       RotorGeo.dim.D_hub:=D_hub;

       motorIPM.typee:="IPM"+possibleshapes[(RotorGeo.shape + 1), 1];
       motorIPM.classe:=shapeflag;

       // Rotor assignment to the main structure
       motorIPM.rotor:=RotorGeo;

       if RotorGeo.shape<>Shape then
         msg_handle :="**** ERROR: Generated geometry is not the desired one. Desired: "+possibleshapes[Shape + 1, 1]+" Generated: "+possibleshapes[RotorGeo.shape + 1, 1]+"  Please redefine Input Parameters associated with Rotor Geometry ****";
         Modelica.Utilities.Streams.print(msg_handle,"");
       end if;
       if K_fc*B_r>0.65*max(Mat_s[:, 1]) then
         msg_handle:="**** WARNING: High Flux Concentration Effects Detected ("+Modelica.Math.Vectors.toString(vector(K_fc*100)," ",3)+" %). Consider deep FEM analysis for this Geometry.";
         Modelica.Utilities.Streams.print(msg_handle,"");
       elseif K_fc*B_r>0.8*max(Mat_s[:, 1]) then
         msg_handle:="**** WARNING: Very High Flux Concentration Effects Detected ("+Modelica.Math.Vectors.toString(vector(K_fc*100)," ",3)+" %). Rotor may work Saturated. Permanent Demagnetization may be present. Results Cannot be considered Valid.";
         Modelica.Utilities.Streams.print(msg_handle,"");
       end if;

       // STEP XX: Working points of the reluctance path elements
       B_Tooth:=B_tooth*alpha_i;
       H_Tooth:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_Tooth);
       // Tooth saturated
       MMF_STo:=H_Tooth*(h_z + h_y/2);

       B_Yoke:=B_yoke*alpha_i;
       H_Yoke:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_Yoke);
       // Yoke saturated
       MMF_Yoke:=H_yoke*b_yR/4;
       MMF_g:=B_g/Constants.mue_0*K_c*g;
       // All the MMF from the neutral point to rotor exit
       Sum_MMF:=MMF_STo + MMF_Yoke + MMF_g;

       if l_Tb==0 then  // length set to 0 when path is not considered
         H_Tb:=0;
       else
         H_Tb:=Sum_MMF/l_Tb;
       end if;
       if l_Tf==0 then // length set to 0 when path is not considered
         H_Tf:=0;
       else
         H_Tf:=Sum_MMF/(1*l_Tf);
       end if;
       if l_Bb==0 then // length set to 0 when path is not considered
         H_Bb:=0;
       else
         H_Bb:=Sum_MMF/l_Bb;
       end if;
       if l_Hub==0 then  // length set to 0 when path is not considered
         H_Hub:=0;
       else
         H_Hub:=Sum_MMF/l_Hub;
       end if;

       // Corresponding flux densities depending on the material
       B_Tb:=Constants.mue_0*H_Tb;
       B_Bb:=Constants.mue_0*H_Bb;
       B_Hub:=Constants.mue_0*H_Hub;

       if H_Tf>=max(Mat_s[:, 2]) then
         B_Tf:=max(Mat_s[:, 1]);
       else
         B_Tf:=Modelica.Math.Vectors.interpolate(
           Mat_s[:, 2],
           Mat_s[:, 1],
           H_Tf);
       end if;

       // STEP XXX: Fluxes and permeances of reluctance path elements

       flux_Tb:=B_Tb*h_Tb*l;
       flux_Tf:=B_Tf*h_Tf*l;
       flux_Bb:=B_Bb*h_Bb*l;
       flux_Hub:=B_Hub*h_Hub*l;
       y_Tb:=2*flux_Tb/Sum_MMF;
       y_Tf:=2*Constants.mue_0*flux_Tf/Sum_MMF;
       y_Bb:=2*Constants.mue_0*flux_Bb/Sum_MMF;
       y_Hub:=2*flux_Hub/Sum_MMF;
       y_g:=1/R_g;
       y_T:=flux_g/MMF_STo;
       y_Y:=flux_g/MMF_Yoke;
       y_g_prima:=1/(1/y_g + 1/y_Y + 1/y_T);
       y_0:=y_g_prima + y_Tb + y_Tf + y_Bb + y_Hub;
       y_insu:=h_i/(Constants.mue_0*2*l_PM*l);
       y_Rotor_first:=1000000000000000000000000;
       y_serie:=1/(1/y_insu + 1/y_Rotor_first);
       y_TOTAL:=1/(1/y_0 + 1/y_serie);

       // All considered losses
       Sum_flux_loss:=flux_Tb + flux_Tf + flux_Bb + flux_Hub;
       Sum_flux:=flux_g + 2*Sum_flux_loss;
       MMF_PM:=Sum_MMF + Sum_flux*y_serie;

       // Limit alpha_v inside valid range for p
       if alpha_v*Constants.pi/180<=p/(2*Constants.pi) then
         roundalpha_v:=Constants.pi/(2*p);
       elseif (alpha_v*Constants.pi/180>p/(2*Constants.pi) and alpha_v*Constants.pi/180<Constants.pi) then
         roundalpha_v:=alpha_v*Constants.pi/180;
       elseif alpha_v*Constants.pi/180>=Constants.pi then
       roundalpha_v:=Constants.pi;
       end if;

       // Linear corelation between alpha_v and mue_r
       murr:=-3/2*mu_r_PM/(Constants.pi - Constants.pi/2/p)*roundalpha_v + mu_r_PM/2 + 3/2*Constants.pi*mu_r_PM/(
         Constants.pi - Constants.pi/2/p);

       // EMC1==Equivalent Br, EMC2==Equivalent Hc
       EMC1:=-sqrt(2*(-H_c)*B_r/murr/Constants.mue_0);
       EMC2:=sqrt(2*(-H_c)*B_r*murr*Constants.mue_0);
       // Verificaion of murr==mu_r_EMC
       mu_r_EMC:=-EMC2/EMC1/Constants.mue_0;
       // Equivalent h_PM
       h_PM_EMC:=mu_r_EMC/mu_r_PM*h_PM;
       l_PM_EMC:=mu_r_EMC/mu_r_PM*l_PM;

       // STEP 9

       y_PM:=Constants.mue_0*mu_r_PM*l_PM*l/(h_PM);
       flux_gen:=2*B_r*l_PM*l;
       flux_m_disp:=flux_gen - MMF_PM*y_PM;
       flux_pm_lost:=flux_gen - flux_m_disp;
       flux_pm2g:=flux_m_disp - 2*Sum_flux_loss;
       flux_RE:=(flux_m_disp - Sum_flux)/flux_m_disp;

       p_lf:=2*Sum_flux_loss/flux_pm2g;
       Omega:=0;

       // STEP XX: Break condition and conditions variation
       msg_handle:="Rotor_counter= "+Modelica.Math.Vectors.toString(vector(rotor_counter));
       Modelica.Utilities.Streams.print(msg_handle,"");
       evolutiondata[rotor_counter,1:10]:={rotor_counter,h_PM*1000,flux_gen,flux_g,flux_m_disp,flux_pm2g,
         flux_Tb*20,flux_Tf*20,flux_Bb*20,0};
       // Three conditions to stop
       // First condition: a good value is found, therefore a desired B_g is atainable
       if rotor_counter>=1+delta_rotor_counter then
         if (abs((flux_g-flux_pm2g)/flux_g)<=0.005 or flux_pm2g>=flux_g) then
           evolutiondata[rotor_counter,size(evolutiondata,2)]:=1;
         end if;
       end if;
       // Second: A maximum is found --> this will be the best relative result
       if rotor_counter>=3+delta_rotor_counter then
         if (evolutiondata[rotor_counter-1,6]-evolutiondata[rotor_counter-2,6]>=0 and evolutiondata[rotor_counter-1,6]-evolutiondata[rotor_counter,6]>=0) then
           evolutiondata[rotor_counter,size(evolutiondata,2)]:=2;
         end if;
       end if;
       if (evolutiondata[rotor_counter,size(evolutiondata,2)]==1 or evolutiondata[rotor_counter,size(evolutiondata,2)]==2) then
         if (evolutiondata[rotor_counter,size(evolutiondata,2)]==1) then
           msg_handle:="**** ROTOR GEOMETRY SUCCESSFULLY CALCULATED ****  final h_PM: " +
             Modelica.Math.Vectors.toString(vector(evolutiondata[rotor_counter, 2]));
           Modelica.Utilities.Streams.print(msg_handle,"");
         elseif evolutiondata[rotor_counter,size(evolutiondata,2)]==2 then
           msg_handle:="**** ROTOR GEOMETRY CALCULATION FAILED ****  "+RotorGeo.msg.failmessage;
           Modelica.Utilities.Streams.print(msg_handle,"");
         elseif evolutiondata[rotor_counter,size(evolutiondata,2)]==4 then
           msg_handle:="**** ERROR LESSER THAN DESIRED, SUCCESS ****    ";
         end if;
         break;
       end if;
       // Third: Generated geometry contains errors
       if (RotorGeo.fail<>0) then
         msg_handle:="**** ROTOR GEOMETRY CALCULATION FAILED, because of geometry error:" + RotorGeo.msg.failmessage;
         Modelica.Utilities.Streams.print(msg_handle,"");
         break;
       end if;
       h_PM:=h_PM + delta_h_PM;
     end for;  // end of rotor diemsioning based on constant B_G

     BEMF:=2*Constants.pi/sqrt(2)*K_w*f*N_ph*flux_g;

     // Output structure
     motorIPM.specs.evolutiondata:=evolutiondata;
     motorIPM.specs.hpmlimiter:=hpmlimiter;

     // Motor plate characteristics
     motorIPM.plate.RatedPower:=P_mec;
     motorIPM.plate.RatedCurrent:=I_s;
     motorIPM.plate.RatedVoltage:=U_ph;
     motorIPM.plate.RatedVoltageType:="Phase";
     motorIPM.plate.BEMF:=BEMF;
     motorIPM.plate.RatedSpeed:=n;
     motorIPM.plate.RatedSpeedRPM:=n_rpm;
     motorIPM.plate.RatedTorque:=T;
     motorIPM.plate.PowerFactor:=PF;
     motorIPM.plate.Efficiency:=eff;

     motorIPM.typee:="IPM " + possibleshapes[RotorGeo.shape + 1, 1];
     motorIPM.classe:=shapeflag;

     // Stator and Rotor Assembling

     motorIPM.rotor.dim.h_PM:=h_PM;
     motorIPM.rotor.dim.Omega:=Omega;
     motorIPM.rotor.phy.R_PM:=1/y_PM;
     motorIPM.rotor.dim.mcf_PM:=mcf_PM;
     motorIPM.airgap.phy.R_g:=R_g;
     motorIPM.airgap.phy.B_g:=B_g;
     motorIPM.airgap.phy.flux_g:=flux_g;
     motorIPM.stator.phy.p_lf:=p_lf;

     // MoI and Mass
      if l_extraH==0 or l_extraW==0 then
        l_extra:=floor((0.002 + l*0.3)*1000)/1000;
        l_extraH:=floor((l_extra*0.6)*1000)/1000;
        l_extraW:=l_extra - l_extraH;
      else
        l_extra:=l_extraW + l_extraH;
      end if;

      h_case:=floor(D_os*1.02*1000)/1000 + 0.003;
      if h_case-D_os<=0.005 then
        h_case:=D_os + 0.005;
      elseif h_case-D_os>=0.012 then
         h_case:=D_os + 0.012;
      end if;

      l_case_exp_void:=floor((l_extra/2 + l*0.1)*1000)/1000;
      l_case_exp_front:=floor(((h_case - D_os)/2*1.5)*1000)/1000;
      l_case_exp_rear:=floor((l*0.3)*1000)/1000;
      l_case:=l + 2*l_extra + 2*l_case_exp_void;
      l_total:=l_case + l_case_exp_front + l_case_exp_rear;
      h_total:=h_case/1000;

      if l_shaft_extra==0 then
        l_shaft_extra:=floor(l_case/3*1000)/1000000;
      end if;
      l_shaft:=l_total + l_shaft_extra;

      // Structure assignments (dimensions)
      motorIPM.casing.dim.l_case_exp_front:=l_case_exp_front;
      motorIPM.casing.dim.l_case_exp_rear:=l_case_exp_rear;
      motorIPM.casing.dim.l_case_exp_void:=l_case_exp_void;
      motorIPM.casing.dim.l_case:=l_case;
      motorIPM.casing.dim.h_case:=h_case;

      motorIPM.rotor.dim.l_shaft_extra:=l_shaft_extra;
      motorIPM.rotor.dim.l_shaft:=l_shaft;
      motorIPM.casing.l_total:=l_total;
      motorIPM.casing.h_total:=h_total;

      motorIPM.stator.win.dim.l_extraW:=l_extraW;
      motorIPM.stator.win.dim.l_extraH:=l_extraH;
      motorIPM.stator.win.dim.l_extra:=l_extra;

      // Structure assigment (densities)
      motorIPM.casing.dim.phy.ro_case:=ro_case;
      motorIPM.stator.dim.phy.ro_mst:=ro_mst;
      motorIPM.stator.dim.phy.ro_cu:=ro_cu;
      motorIPM.rotor.dim.phy.ro_mst:=ro_mst;
      motorIPM.rotor.dim.phy.ro_PM:=ro_PM;
      motorIPM.rotor.dim.phy.ro_hub:=ro_hub;
      motorIPM.rotor.dim.phy.ro_shaft:=ro_shaft;

      // Structure assigment (other properties)
      motorIPM.stator.dim.phy.mst.K_h:=K_h;
      motorIPM.stator.dim.phy.mst.K_e:=K_e;
      motorIPM.stator.dim.phy.mst.n_i:=n_i;

      // Masses and MOI calculations
      motorIPM:=AuxiliaryFunc.MAssandMOI(motorIPM);

      // Resistance
      // Temperature data assignment
      motorIPM.casing.dim.phy.t_aro:=t_aro;
      motorIPM.casing.dim.phy.t_amb:=t_amb;
      motorIPM.casing.dim.phy.Delta_t_work:=Delta_t_work;
      motorIPM.stator.win.phy.ro_t_aro:=ro_t_aro;
      motorIPM:=AuxiliaryFunc.Resistance_IPM(motorIPM);

      // Inductance
      motorIPM:=AuxiliaryFunc.Inductances(motorIPM);

      // Torque vs Speed
      //motorIPM:=AuxiliaryFunc.TorquevsSpeed(motorIPM);
      // Export motorIPM structure
      motorIPM.plate.RatedFrequency:=f;
      IPM_Structure.IPM_Export(motorIPM);
       pp:=p;
       Jr:=motorIPM.rotor.dim.I_R;
       Js:=motorIPM.casing.dim.I_S;
       iMax:=I_s;
       tauMax:=T;
       R:=motorIPM.stator.phy.R_s;
       TRef:=motorIPM.stator.phy.t_work;
       Alpha:=ro_t_aro;
       Ld:=motorIPM.stator.phy.L_dm;
       Lq:=motorIPM.stator.phy.L_qm;
       VsOpenCircuit:=BEMF;
       fn:=motorIPM.plate.RatedFrequency;
       nM:=motorIPM.stator.dim.mM;
       mT:=motorIPM.plate.M;

     annotation (
        Documentation(info="<html>

<head>
<title>General IPM Pre-sizing help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<h2><a name='_Toc414020031'></a><a name='_Toc402539784'></a><a
name='_Toc414020016'></a><a name='_Toc402539769'><span lang=EN-US>1.1<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US>IPM Table of inputs</span></a></h2>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Name</span></b></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Description</span></b></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border:solid windowtext 1.0pt;
  border-left:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Units</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>U_ph</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Phase Voltage</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>V</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>m</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of phases</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>a</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of parallel paths</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>p</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of pair poles</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>P_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated mechanical Power</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>W</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>n</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Speed</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>rpm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>T</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Torque</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Nm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>I_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Current</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A<sub>rms</sub></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>eff</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Efficiency</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>PF</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Power Factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_tooth</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Max Flux Density at Stator Tooth</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_yoke</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Max Flux Density at Yoke</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Max Flux Density at Airgap</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>J_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Current density at stator</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A<sub>rms</sub>/mm<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Airgap</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Q_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Stator Slots</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>n_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of ventilating ducts</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>b_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Ventilating Ducts width</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>F_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Lamination factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_u</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Slot Filling Factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_w</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Winding Factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>C_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Mechanical Constant</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>kWs/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>X</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Aspect Ratio</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>coilspan</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Coil span</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Material.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator/Rotor B-H material characteristic
  curve, losses data</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Magnet Remanent Flux</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>H_c</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Magnet Coercitive Force</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A/m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>mu_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Relative Permeability</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>alpha_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Angle for the magnets &quot;V&quot; aperture</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>deg</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>mcf_PM</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Magnet Cover Factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>h_i</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Insulator layer PM-Rotor thickness</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>b_1</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Slot Opening width</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>DeltaD</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Wedge between Magnet and Rotor Surface</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_mst</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Magnetic Steel Mass Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_PM</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Permanent Magnet Steel Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_cu</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Copper Steel Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_case</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Case Steel Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>D_shaft</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Diameter</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_shaft</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Length</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_amb</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Ambient Temperature</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_aro</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Reference Resistivity Temperature</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_t_aro</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Reference Resistivity</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>&#8486;·m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Delta_t_work</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Temperature Increase over ambient</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_extra</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Extra length on medium turn </span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>p_lf</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Magnet permeance leakage factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_web</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Web Length</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Omega</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Web Cover angle</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>deg</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>D_shaft</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Diameter</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_shaft</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Length</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>h_ch</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Head Coil </span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019788'></a><a name='_Toc402539852'></a><a
name='_Toc385255427'><span lang=EN-GB>Table </span></a><span
lang=EN-GB>33</span><span lang=EN-GB>. List of inputs: variable name,
description and units in IS</span></p>

<h2><span lang=EN-US>1.2<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Algorithm concept</span></h2>

<p class=MsoNormal><span lang=EN-US>Internal Permanent Magnet motor family (of
spoke, v-shape and planar geometries) pre-sizing is based on a split
stator/rotor algorithm. The link between both is the desired value of airgap
flux density (B_g_ini). Stator sizing is similar to IM machines, takes into
account real and apparent fluxes and saturation factor alpha_i. Rotor is divided
into two steps, first a geometrical validation of the intended rotor and then
magnetic circuit validation in order to increase magnet’s width. Rotor loop
stop code is rather more complex due to the possibility not to achieve the
desired airgap flux density and thus requires several conditions.</span></p>

<h2><span lang=EN-US>1.3<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Program Structure Explanation</span></h2>

<p class=MsoNormal><span lang=EN-US>This section covers only code part directly
related to sizing functions and thus how the sizing is performed.  The program
could be separated into 4 basic functions. The following figure (</span><span lang=EN-GB>Figure 1</span><span lang=EN-US>) shows its function blocks; please
use it to follow the explanation.</span></p>

<p class=MsoNormal><span lang=EN-US>First, depending on inputs values and sets,
lacking inputs and sizing flags. Generate inputs if their initial value is not
set, and prepare variable flags that will be required in further code. </span></p>

<p class=MsoNormal><span lang=EN-US>Second, to size stator imposing the required
B_g and the subsequent alpha_i</span><span lang=EN-GB> by means of a loop</span><span
lang=EN-US>. This process consist on first calculate number of conductors per
phase in series (N_ph) find the number of conductors per slot (z_Q) round them
and recalculate airgap flux (flux_g). This leads to basic stator geometry (of
tooth and yoke) which leads to magnetic drops in those parts. Then the
saturation factor is calculated and leads to a form factor coefficient
(alpha_i) that is set back the beginning of the loop. When differences between
consecutive form factors is less than a threshold loop breaks.</span></p>

<p class=MsoNormal><span lang=EN-US>Third, motor sizing and machine validation,
this is performed in another loop that requires a valid geometry plus the
desired airgap flux in order to stop successfully. Between the data obtained in
the previous step and the desired inputs rotorshape() creates a whole rotor. At
this point that function is able to evaluate if rotor is feasible or not also
graphical fail information is created that will be further used in the GUI. In
any case (feasible or not) magnetic circuit is evaluated by means of the
reluctance widths and lengths provided by rotor geometry plus stator data
previously calculated. Then permeabilities are obtained. Based on magnet
properties and volume, which IPM geometry has been selected, it is determined
if rotor flux density is achievable or not. Because of rotor geometry
complexities a matrix (evolutiondata) is used to evaluate sizing progression.
Then stop condition plays its role by breaking the loop based on: If geometry
is feasible or not, if a maximum in flux has been reached or if the desired
flux has been reached.   </span></p>

<p class=MsoNormal><span lang=EN-US>Fourth, once machine geometry has been
calculated several sub functions, each in charge of one magnitude end
pre-sizing algorithm: Masses and Inertias, Circuit Parameters (R, L_d, L_q</span><span
lang=EN-GB>)</span><span lang=EN-GB> </span><span lang=EN-US>and finally Torque
vs. Speed which requires information of masses and lumped parameters.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=548 height=841 src='General_IPM_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019884'></a><a name='_Toc402539893'></a><a
name='_Ref392232799'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Basic Structure and STEPs of code</span><span
lang=EN-GB>.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end IPM_Motor_Sizing;

    function SRM_Motor_Sizing "Sizing_SRMnew_23"
      import Modelica.Constants;
      import Modelica.SIunits;
      constant Real c=68E-6 "Cu Temperature Constant";
     parameter input Modelica.SIunits.Voltage U_ph(min=0)=300
        "Rated phase voltage"
      annotation (Dialog(group="Nominal parameters"));
     parameter input Integer m(min=1)=1
        "Number of simultaneously conducting phases"
     annotation (Dialog(group="Nominal parameters"));
     parameter input Integer N_s(min=2)=8 "Stator poles"
     annotation (Dialog(group="Nominal parameters"));
     parameter input Integer N_r(min=2)=6 "Rotor poles"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.Angle beta_s_ref(min=0,max=360)=18
        "Stator pole arc"
     annotation (Dialog(tab="Stator",group="Geometry"));
     parameter input SIunits.Angle beta_r_ref(min=0,max=360)=22
        "Rotor pole arc"
     annotation (Dialog(tab="Rotor"));
     parameter input Integer a(min=1,max=2)=1 "Parallel paths"
     annotation (Dialog(tab="Stator",group="Winding"));
     parameter input SIunits.Length g(min=0)=5e-4 "Airgap"
     annotation (Dialog(tab="Airgap"));
     parameter input Real f_r(min=0,max=1)=0.95 "Lamination factor"
     annotation (Dialog(tab="Stator",group="Winding"));
     parameter input SIunits.MagneticFluxDensity B_work(min=0)=1.75
        "Flux density seed value at magnetic steel working point"
     annotation (Dialog(tab="Stator",group="Magnetic circuit"));
     parameter input String Material_File="M235_35A.txt"
        "Stator Material B(H) characteristic"
     annotation (Dialog(tab="Stator",group="Material"));
     parameter input String PowerDshaftFile="PowerDshaft.txt"
        "IEC60034 Machine Power vs D_shaft"
        annotation (Dialog(tab="Rotor"));
     parameter input String AWGwiringsFile="AWGwirings.txt"
        "AWG wiring characteristics"
        annotation (Dialog(tab="Stator",group="Winding"));
     parameter input SIunits.Power P_mec(min=0)=3729 "Rated mechanical power"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.Conversions.NonSIunits.AngularVelocity_rpm n(min=0)=1500
        "Rated speed"
        annotation (Dialog(group="Nominal parameters"));
     parameter input Real eff(min=0,max=1)=0.88 "Desired efficiency"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.Current I_ph(min=0)=13 "Rated current"
     annotation (Dialog(group="Nominal parameters"));
     parameter input SIunits.CurrentDensity J_s(min=0)=5.5
        "Stator current density"
     annotation (Dialog(tab="Stator", group="Winding"));
     parameter input Real X(min=0)=2 "Aspect ratio"
     annotation (Dialog(tab="Stator",group="Geometry"));
     parameter input Real A_s(min=0)=13000 "Electrical loading"
     annotation (Dialog(tab="Stator"));
     parameter input Real K_d(min=0,max=1)=1 "Duty cycle"
     annotation (Dialog(tab="Stator"));
     parameter input Real K_1=Constants.pi*Constants.pi/120 "Constant #1"
     annotation (Dialog(tab="Stator"));
     parameter input Real K_2(min=0,max=1)=0.8265 "Constant #2 (1-Lu/La)"
     annotation (Dialog(tab="Stator"));
     parameter input Real b_seg(min=0,max=1)=0.006
        "Spacing between consecutive coils"
     annotation (Dialog(tab="Stator"));
     parameter input Real coil_security_coefficient(min=0)=0.2
        "Coil security coefficient"
     annotation (Dialog(tab="Stator"));
     parameter input Real BRatio_sy(min=0,max=1)=0.525
        "Flux density stator pole to yoke ratio"
     annotation (Dialog(tab="Stator"));
     parameter input Real BRatio_ryp(min=0,max=1)=0.7
        "Flux density rotor pole to yoke ratio"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_aro(min=0)=20
        "Referency resistivity temperature"
        annotation (Dialog(tab="Stator"));
     parameter input SIunits.Resistivity ro_t_aro(min=0)=0 "Cu Resistivity"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_amb(min=0)=35
        "Ambient temperature"
        annotation (Dialog(tab="Stator"));
     parameter input SIunits.Conversions.NonSIunits.Temperature_degC Delta_t_work(min=0)=55
        "Temperature increase relative to ambient"
        annotation (Dialog(tab="Stator"));
     parameter input SIunits.Length l_shaft_ref(min=0)=0 "Shaft length"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Length l_shaft_extra_ref(min=0)=50/1000
        "Shaft extra length"
     annotation (Dialog(tab="Rotor"));
     parameter input Integer case_type(min=1,max=2)=1
        "Casing type 1-circular, 2-square solid"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Density ro_mst(min=0)=7460
        "Magnetic steel density"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Density ro_PM(min=0)=7400 "Magnet density"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Density ro_cu(min=0)=8750 "Cu density"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Density ro_case(min=0)=7850
        "Metal cabinet density"
     annotation (Dialog(tab="Stator"));
     parameter input SIunits.Density ro_hub(min=0)=7460
        "Hub insulation steel density"
     annotation (Dialog(tab="Rotor"));
     parameter input SIunits.Density ro_shaft(min=0)=7460 "Shaft steel density"
     annotation (Dialog(tab="Rotor"));

    protected
     parameter SIunits.Angle beta_s=beta_s_ref*Constants.pi/180
        "Stator pole arc";
     parameter SIunits.Angle beta_r=beta_r_ref*Constants.pi/180
        "Rotor pole arc";
     Real Allowed_diff=0.02 "Allowed current density error";
     Real PF=0 "Power factor (not used";
     Real E=0 "Back electromotive force (not used)";
     SRM_Structure.motorSRM motorSRM "Structure with whole motor info";
     SRM_Structure.motorSRM motorLOWER "Structure with whole motor info";
     SRM_Structure.motorSRM motorHIGHER "Structure with whole motor info";
     SRM_Structure.motorSRM motorSRM_Good "Structure with whole motor info";
     parameter Real[:,:] Mat=DataFiles.readCSVmatrix(Material_File)
        "Material B(H) characteristic";
     parameter Real[:,4] PowerDshaft=DataFiles.readCSVmatrix(PowerDshaftFile);
     parameter SIunits.Power[:] PowerD=PowerDshaft[:,1];
     parameter Real[:] D_shaftD=PowerDshaft[:,4];
     parameter Real[:,:] AWGwirings=DataFiles.readCSVmatrix(AWGwiringsFile);
     Integer[2] Feasible;
     SIunits.Length D_shaft;
     SIunits.Length D "Bore diameter (inner stator diameter)";
     SIunits.MagneticFluxDensity B_sp;
     Integer B_solution_reached=0 "Flux density solution reach flag";
     Integer B_counter=0 "Flux density counter";
     Integer B_counter_limit=15 "Flux density limit";
     SIunits.Length l "Length";
     SIunits.Length h_sp "Stator pole height";
     Integer J_solution_reached=0 "Current Density Solution Reach Flag";
     Integer J_counter=0 "Current density counter";
     Integer J_counter_limit=60 "Current density counter limit";
     SIunits.CurrentDensity J_motorLOWER=0
        "Current density of the below-limit solution";
     SIunits.CurrentDensity J_motorHIGHER=999
        "Current density of the above-limit solution";
     Integer lower_last_attempt=0 "Flag for below-limit solution";
     Integer higher_last_attempt=0 "Flag for above-limit solution";
     SIunits.MagneticFluxDensity B_sy "Stator yoke flux density";
     SIunits.MagneticFluxDensity B_ry "Rotor yoke flux density";
     SIunits.Length b_sp "Stator pole width";
     SIunits.Length b_rp "Rotor pole width";
     SIunits.Length w_g "Airgap width";
     SIunits.MagneticFluxDensity B_rp "Rotor pole flux density";
     SIunits.MagneticFluxDensity B_g "Airgap flux density";
     SIunits.Length b_sy "Stator yoke width";
     SIunits.Length b_ry "Rotor yoke width";
     SIunits.Length l_ry "Rotor yoke length";
     SIunits.Length l_rp "Rotor pole length";
     SIunits.Length l_g "Airgap length";
     SIunits.Length l_sp "Stator pole length";
     SIunits.Length l_sy "Stator yoke length";
     SIunits.Length D_ms "Outer stator pole diameter";
     SIunits.Length alpha "Half of stator pole pitch";
     SIunits.Length h_seg "Coil security margin radial shifting";
     SIunits.Length x_p2 "Point2 x-coord";
     SIunits.Length y_p2 "Point2 y-coord";
     SIunits.Length b_sc_max "Stator coil width";
     SIunits.Angle gamma "Outer pole-area upper extreme half-angle";
     SIunits.Length P1_shifting "Point1 shifting";
     SIunits.Length h_sc "Stator coil height";
     SIunits.MagneticFieldStrength H_sy "H field in stator yoke";
     SIunits.MagneticFieldStrength H_sp "H field in stator pole";
     SIunits.MagneticFieldStrength H_rp "H field in rotor pole";
     SIunits.MagneticFieldStrength H_ry "H field in rotor yoke";
     SIunits.MagneticFieldStrength H_g "Field in the airgap";
     SIunits.Permeability mu_sy "Stator yoke relative permeability";
     SIunits.Permeability mu_sp "Stator pole relative permeability";
     SIunits.Permeability mu_rp "Rotor pole relative permeability";
     SIunits.Permeability mu_ry "Rotor yoke relative permeability";
     SIunits.Permeability mu_g "Airgap relative permeability";
     SIunits.Reluctance R_sy "Stator yoke reluctance";
     SIunits.Reluctance R_sp "Stator pole reluctance";
     SIunits.Reluctance R_rp "Rotor pole reluctance";
     SIunits.Reluctance R_ry "Rotor yoke reluctance";
     SIunits.Reluctance R_g "Airgap reluctance";
     SIunits.MagnetomotiveForce FMM_fmm "FMM total by fmm";
     SIunits.MagneticFlux Flux_a "Stator pole total flux";
     SIunits.Reluctance R_serie "In-series aligned reluctance";
     SIunits.Reluctance R_tot "Total aligned reluctance";
     SIunits.Reluctance RR_tot "Total aligned reluctance";
     SIunits.MagnetomotiveForce FMM_rel "FMM by reluctance method";
     Real N_ph "Number of conductors per phase";
     Integer N_pc "Rounded number of conductors per pole";
     Real N_ph_r "Rounded number of conductors per phase";
     AuxiliaryFunc.packr hexa_data;
     AuxiliaryFunc.packr quadra_data;
     AuxiliaryFunc.packr Good_wiring;
     SIunits.Resistivity Re_s_cond_u "Stator conductor resistance per meter";
     Real C_sc "Number of conductors per semicoil";
     SIunits.Length D_cond "Conductor diameter";
     Real Gauge_st "Conductor AWG Gauge";
     Real K_f "Filling factor of the assigned area";
     Real K_f_real "Filling factor by the actual used area";
     SIunits.Length h_sc_real "Actual h_sc used";
     SIunits.Length b_sc_real "Actual b_sc used";
     Real[:,:] centers "Center location";
     Real[:,:] Semi_coil_points "Semi coil drawing points";
     Real[:,:] Semi_coil_real_points "Real semi coil drawing points";
     SIunits.CurrentDensity J_round_real "Rounded current density";
     AuxiliaryFunc.res_output Resistance "Resistance structure";
     SIunits.Resistance R_sc "Coil resistance";
     SIunits.Length l_coil "Coil wire length";
     SIunits.Conversions.NonSIunits.Temperature_degC t_work
        "Cu working temperature";
     SIunits.Resistivity ro_work "Resistivity at work temperature";
     SIunits.Resistivity ro_work2 "Resistivity at work temperature";
     SIunits.Resistance R_sc_t_aro "Coil resistance at ambient temperature";
     SIunits.Resistance R_ph "Phase resistance";
     SIunits.Length D_os "Diameter outer stator";
     SIunits.Length D_or "Diameter outer rotor";
     SIunits.Length D_ir "Diameter inner rotor";
     SIunits.Length h_rp "Rotor pole height";
     SIunits.Length l_extra "Extra head coil length";
     SIunits.Length h_case "Square case height";
     SIunits.Length l_case_exp_void "Internal margin length";
     SIunits.Length l_case_exp_front "Extra case front length";
     SIunits.Length l_case_exp_rear "Extra case rear length";
     SIunits.Length l_case "Square case length";
     SIunits.Length l_total "Total case length";
     SIunits.Length h_total "Total case height";
     SIunits.Length l_shaft_extra=l_shaft_extra_ref "External shaft length";
     SIunits.Length l_shaft=l_shaft_ref "Total shaft length";
     SIunits.Length h_sp_temp "Temporal value of stator pole height";
     Real lower_diff "Current density lower per unit difference";
     Real higher_diff "Current density lower per unit difference";
     SIunits.MagneticFlux Flux_real "Rreal working flux";
     SIunits.MagneticFluxDensity B_work_real "Real working flux density";
     Real B_err "Flux density relative error";
     SIunits.Power P_mec_real "Real mechanical power";
     Real P_err "Power density relative error";
     Real J_err "Current density relative error";
     SIunits.Inductance L_alig "Aligned inductance";
     SIunits.Inductance L_unalig "Unaligned inductance";
     String error_msg "Error message";
     SIunits.Angle alpha_seg "Coil security margin angle";
     SIunits.Length D_shaft_internal "Internal shaft";

    algorithm
      assert(U_ph>0,"U_ph input value outside of valid range (0,inf)");
      assert(I_ph>0,"I_ph input value outside of valid range (0,inf)");
      assert(m >=1,"m input value outside of valid range <1,inf)");
      assert(N_s >=2,"N_s input value outside of valid range <2,inf)");
      assert(N_r >=2,"N_r input value outside of valid range <2,inf)");
      assert(beta_s_ref >0 and beta_s_ref <360,"beta_s_ref input value outside of valid range (0,360)");
      assert(beta_r_ref >0 and beta_r_ref <360,"beta_r_ref input value outside of valid range (0,360)");
      assert(a >=1 and a <=2,"a input value outside of valid range <1,2>");
      assert(g>0,"g input value outside of valid range (0,inf)");
      assert(f_r >0 and a <=1,"f_r input value outside of valid range (0,1>");
      assert(B_work >0,"B_work input value outside of valid range (0,inf)");
      assert(P_mec >0,"P_mec input value outside of valid range (0,inf)");
      assert(n >0,"n input value outside of valid range (0,inf)");
      assert(eff >0 and eff <=1,"eff input value outside of valid range (0,1>");
      assert(J_s >0,"J_s input value outside of valid range (0,inf)");
      assert(X >0,"X input value outside of valid range (0,inf)");
      assert(A_s >0,"A_s input value outside of valid range (0,inf)");
      assert(K_d >0 and K_d <=1," K_d input value outside of valid range (0,1>");
      assert(K_2 >0 and K_2 <1," K_2 input value outside of valid range (0,1)");
      assert(b_seg >0,"b_seg input value outside of valid range (0,inf)");
      assert(coil_security_coefficient >0,"coil_security_coefficient input value outside of valid range (0,inf)");
      assert(BRatio_sy >0 and BRatio_sy <=1," BRatio_sy input value outside of valid range (0,1>");
      assert(BRatio_ryp >0 and BRatio_ryp <=1," BRatio_ryp input value outside of valid range (0,1>");
      assert(t_aro >=0,"t_aro input value outside of valid range <0,inf)");
      assert(ro_t_aro >=0,"ro_t_aro input value outside of valid range <0,inf)");
      assert(t_amb >=0,"t_amb input value outside of valid range <0,inf)");
      assert(Delta_t_work >=0,"Delta_t_work input value outside of valid range <0,inf)");
      assert(l_shaft >=0,"l_shaft input value outside of valid range <0,inf)");
      assert(l_shaft_extra >=0,"l_shaft_extra input value outside of valid range <0,inf)");
      assert(case_type >=1 and case_type<=2,"case_type input value outside of valid range <1,2>");
      assert(ro_mst >0,"ro_mst input value outside of valid range (0,inf)");
      assert(ro_PM >0,"ro_PM input value outside of valid range (0,inf)");
      assert(ro_cu >0,"ro_cu input value outside of valid range (0,inf)");
      assert(ro_case >0,"ro_case input value outside of valid range (0,inf)");
      assert(ro_hub >0,"ro_hub input value outside of valid range (0,inf)");
      assert(ro_shaft >0,"ro_shaft input value outside of valid range (0,inf)");

      error_msg:="none";
      Feasible:=AuxiliaryFunc.feasibleregion(beta_s,beta_r,N_s,N_r,0);
      (D_shaft, D_shaft_internal):=AuxiliaryFunc.PowerDShaft(P_mec,0);

       // Condition: desired working flux density is assigned at stator pole
       B_sp:=B_work;
       // Diameter loop
       // Seed values
       D:=(P_mec/(eff*K_1*K_2*K_d*B_sp*X*n*A_s))^(1/3);
       B_solution_reached:=0;
       B_counter:=0;
       B_counter_limit:=15;

       Modelica.Utilities.Streams.print("D_shaft="+Modelica.Math.Vectors.toString(vector(D_shaft)),"");
       Modelica.Utilities.Streams.print("D_shaft_internal="+Modelica.Math.Vectors.toString(vector(D_shaft_internal)),"");
       Modelica.Utilities.Streams.print("B_sp="+Modelica.Math.Vectors.toString(vector(B_sp)),"");
       Modelica.Utilities.Streams.print("D="+Modelica.Math.Vectors.toString(vector(D)),"");

       while (B_solution_reached==0 and B_counter<B_counter_limit) loop
         l:=(X^2*P_mec/(eff*K_1*K_2*K_d*B_sp*n*A_s))^(1/3);
         h_sp:=D/2;
         // Stator pole height (h_sp) loop controlling variables
         J_solution_reached:=0;
         J_counter:=0;
         J_counter_limit:=60;
         J_motorLOWER:=0;
         J_motorHIGHER:=999;
         lower_last_attempt:=0;
         higher_last_attempt:=0;
         while (J_solution_reached==0 and J_counter<J_counter_limit) loop
           B_sp:=B_work;
           B_sy:=BRatio_sy*B_sp;
           B_ry:=BRatio_ryp*B_sp;
           b_sp:=2*(D/2*sin(beta_s/2));
           b_rp:=2*((D/2 - g)*sin(beta_r/2));
           w_g:=(D - g)/2*(beta_s + beta_r)/2;
           B_rp:=b_sp/b_rp*B_sp;
           B_g:=b_sp/w_g*B_sp;
           b_sy:=B_sp/(2*B_sy)*b_sp;
           b_ry:=B_rp/(2*B_ry)*b_rp;
           // Length of reluctance path arc+projection
           l_ry:=Constants.pi*(D_shaft + b_ry)/2;
           l_rp:=D/2 - (D_shaft + b_ry)/2 - g;
           l_g:=g;
           l_sp:=b_sy/2 + h_sp;
           l_sy:=Constants.pi*(D/2 + h_sp + b_sy/2);
           // Coil max dimensions
           D_ms:=D + 2*h_sp;
           alpha:=2*Constants.pi/(2*N_s);
           h_seg:=b_seg/sin(alpha);
           x_p2:=D/2 + coil_security_coefficient*h_sp;
           y_p2:=tan(alpha)*x_p2 - b_seg/cos(alpha);
           b_sc_max:=y_p2 - b_sp/2;
           gamma:=(b_sc_max + b_sp/2)/(D_ms/2);
           P1_shifting:=D_ms/2*(1 - cos(gamma));
           h_sc:=h_sp - coil_security_coefficient*h_sp - P1_shifting;

           // Check coil geometry coherency
           if y_p2 < b_sp/2 then  // incoherent geometry (geometry is not physically possible)
             error_msg:="*** ERROR: IMPOSSIBLE GEOMETRY. Coil not allocable because an excess of b_seg or stator pole angle. Program will be aborted in this iteration.";
             Modelica.Utilities.Streams.print(error_msg,"");
             b_sc_max:=abs(b_sc_max);
             h_sc:=abs(h_sc);
             J_solution_reached:=1;
             B_solution_reached:=1;
             Feasible[2]:=-1;
           else // possible unallocable geometry (geometry may be physically possible but does not meet designer requirements)
             alpha_seg:=Constants.pi/N_s - atan(y_p2/x_p2);
             Feasible:=AuxiliaryFunc.feasibleregion(beta_s,beta_r,N_s,N_r,alpha_seg);

              if Feasible[2]==0 then
                J_solution_reached:=1;
                B_solution_reached:=1;
                error_msg:="*** ERROR: UNALLOCABLE GEOMETRY. Coil not allocable because an excess of b_seg or stator pole angle. Program will be aborted in this iteration.";
                Modelica.Utilities.Streams.print(error_msg,"");
              end if;
           end if;

           // Magnetic circuit
           H_sy:=Modelica.Math.Vectors.interpolate(Mat[:, 1],Mat[:, 2],B_sy);
           H_sp:=Modelica.Math.Vectors.interpolate(
            Mat[:, 1],
            Mat[:, 2],
            B_sp);
           H_rp:=Modelica.Math.Vectors.interpolate(
            Mat[:, 1],
            Mat[:, 2],
            B_rp);
           H_ry:=Modelica.Math.Vectors.interpolate(
            Mat[:, 1],
            Mat[:, 2],
            B_ry);

           H_g:=B_g/Constants.mue_0;

           // Relative permeabilities
           mu_sy:=B_sy/H_sy;
           mu_sp:=B_sp/H_sp;
           mu_rp:=B_rp/H_rp;
           mu_ry:=B_ry/H_ry;
           mu_g:=Constants.mue_0;

           // Reluctances
           R_sy:=l_sy/(mu_sy*(b_sy*l));
           R_sp:=l_sp/(mu_sp*(b_sp*l));
           R_rp:=l_rp/(mu_rp*(b_rp*l));
           R_ry:=l_ry/(mu_ry*(b_ry*l));
           R_g:=g/(mu_g*(w_g*l));

           // FMM
           FMM_fmm:=2*H_sp*l_sp + 2*H_rp*l_rp + 2*H_g*l_g + H_ry*l_ry + H_sy*l_sy;
           // by reluctance
           Flux_a:=B_work*b_sp*l;
           R_serie:=2*R_sp + 2*R_rp + 2*R_g + 0.5*R_ry;
           R_tot:=R_serie + 0.5*R_sy;
           FMM_rel:=Flux_a*R_tot;
           N_ph:=FMM_fmm/(I_ph/a);
           N_pc:=AuxiliaryFunc.round(N_ph/2);
           N_ph_r:=N_pc*2;

           // Number of conductors

           // Normalized diameters data
           for ii in 1:1:size(AWGwirings,1) loop
             // In hexa_pack configuration
             hexa_data:=AuxiliaryFunc.hexapack(h_sc,b_sc_max,AWGwirings[ii, 2]/1000);
             // In quadra_pack configuration
             quadra_data:=AuxiliaryFunc.quadrapack(h_sc,b_sc_max,AWGwirings[ii, 2]/1000);

             if hexa_data.C_sc>quadra_data.C_sc then
               Good_wiring:=hexa_data;
             elseif hexa_data.C_sc<=quadra_data.C_sc then
               Good_wiring:=quadra_data;
             end if;

             if Good_wiring.C_sc>=N_pc then
               Re_s_cond_u:=AWGwirings[ii, 2]*1000;
               Good_wiring.Gauge_st:=ii;
               C_sc:=Good_wiring.C_sc;
               D_cond:=Good_wiring.D_cond;
               Gauge_st:=Good_wiring.Gauge_st;
               K_f:=Good_wiring.K_f;
               K_f_real:=Good_wiring.K_f_real;
               h_sc_real:=Good_wiring.dim.h_sc_real;
               b_sc_real:=Good_wiring.dim.b_sc_real;
               centers:=Good_wiring.draw.centers;
               Semi_coil_points:=Good_wiring.draw.Semi_coil_points;
               Semi_coil_real_points:=Good_wiring.draw.Semi_coil_real_points;
               break;
             end if;
           end for;

           // N_ph correction (per area)

           J_round_real:=N_pc*I_ph/(h_sc_real*b_sc_real)/1000000;
           Resistance:=AuxiliaryFunc.Resistance_SRM(
                l,
                b_sp,
                t_amb,
                Delta_t_work,
                t_aro,
                ro_t_aro,
                Good_wiring);
           R_sc:=Resistance.R_sc;
           l_coil:=Resistance.l_coil;
           t_work:=Resistance.t_work;
           ro_work:=Resistance.ro_work;
           R_sc_t_aro:=Resistance.R_sc_t_aro;

           R_ph:=R_sc*(2/a);
           D_os:=D + 2*h_sp + 2*b_sy;
           D_or:=D - 2*g;
           D_ir:=D_shaft + 2*b_ry;
           h_rp:=D/2 - g - b_ry - D_shaft/2;
           l_extra:=b_sc_max + D_cond/2;
           h_case:=floor(D_os*1.02*1000)/1000 + 0.003;

           if h_case-D_os<=0.005 then
             h_case:=D_os + 0.005;
           elseif h_case-D_os >=0.012 then
             h_case:=D_os + 0.012;
           end if;

           l_case_exp_void:=floor((l_extra/2 + l*0.1)*1000)/1000;
           l_case_exp_front:=floor(((h_case - D_os)/2*1.5)*1000)/1000;
           l_case_exp_rear:=floor((l*0.3)*1000)/1000;
           l_case:=l + 2*l_extra + 2*l_case_exp_void;
           l_total:=l_case + l_case_exp_front + l_case_exp_rear;
           h_total:=h_case;

           if l_shaft_extra==0 then
             l_shaft_extra:=floor(l_case/3*1000)/1000000;
           end if;
           l_shaft:=l_total + l_shaft_extra;

           // SRM structure
           motorSRM.m:=m;
           motorSRM.q:=integer(N_s/2);
           motorSRM.g:=g;
           motorSRM.RatedPower:=P_mec;
           motorSRM.classe:=5;

           motorSRM.plate.RatedPower:=P_mec;
           motorSRM.plate.RatedCurrent:=I_ph;
           motorSRM.plate.RatedVoltage:=U_ph;
           motorSRM.plate.RatedVoltageType:="Phase";
           motorSRM.plate.BEMF:=E;
           motorSRM.plate.RatedSpeedRPM:=n;
           motorSRM.plate.Torque:=P_mec/(n*Constants.pi/30);
           motorSRM.plate.PowerFactor:=PF;
           motorSRM.plate.Efficiency:=eff;
           motorSRM.plate.M:=0;
           motorSRM.plate.TmM:=0;

           motorSRM.specs.f_r:=f_r;
           motorSRM.specs.X:=X;
           motorSRM.specs.Kd:=K_d;
           motorSRM.specs.K1:=K_1;
           motorSRM.specs.K2:=K_2;
           motorSRM.specs.A_load:=A_s;
           motorSRM.specs.coil_security_coefficient:=coil_security_coefficient;
           motorSRM.specs.RR_tot:=R_tot;
           motorSRM.specs.Feasible:=Feasible;

           motorSRM.specs.J_counter:=J_counter;
           motorSRM.stator.case_type:=1;
           motorSRM.stator.N_s:=N_s;
           motorSRM.stator.D:=D;
           motorSRM.stator.I_s:=I_ph;
           motorSRM.stator.J_s_desired:=J_s;
           motorSRM.stator.J_s_real:=J_round_real;
           motorSRM.stator.dim.beta_s:=beta_s;
           motorSRM.stator.dim.D_os:=D_os;
           motorSRM.stator.dim.D_is:=D;
           motorSRM.stator.dim.b_sp:=b_sp;
           motorSRM.stator.dim.h_sp:=h_sp;
           motorSRM.stator.dim.b_sy:=b_sy;
           motorSRM.stator.dim.l:=l;

           motorSRM.stator.win.a:=a;
           motorSRM.stator.win.N_ph:=N_ph_r;
           motorSRM.stator.win.K_f:=K_f;
           motorSRM.stator.win.K_f_real:=K_f_real;
           motorSRM.stator.win.dim.b_sc:=b_sc_max;
           motorSRM.stator.win.dim.h_sc:=h_sc;
           motorSRM.stator.win.dim.b_seg:=b_seg;
           motorSRM.stator.win.dim.D_cond:=D_cond;
           motorSRM.stator.phy.R_sc_t_aro:=R_sc_t_aro;
           motorSRM.stator.phy.t_work:=t_work;
           motorSRM.stator.phy.R_ph:=R_ph;
           motorSRM.stator.win.draw.Semi_coil_points:=Semi_coil_points;
           motorSRM.stator.win.draw.Semi_coil_real_points:=Semi_coil_real_points;
           motorSRM.stator.win.draw.Semi_coil_centers:=centers;

           motorSRM.rotor.shape:=5;
           motorSRM.rotor.fail:=0;
           motorSRM.rotor.failpath:=0;
           motorSRM.rotor.msg:=error_msg;
           motorSRM.rotor.dim.N_r:=N_r;
           motorSRM.rotor.dim.beta_r:=beta_r;
           motorSRM.rotor.dim.D_or:=D_or;
           motorSRM.rotor.dim.D_ir:=D_ir;
           motorSRM.rotor.dim.D_shaft:=D_shaft;
           motorSRM.rotor.dim.D_shaftB:=D_ir;
           motorSRM.rotor.dim.D_hub:=D_ir;
           motorSRM.rotor.dim.b_rp:=b_rp;
           motorSRM.rotor.dim.h_rp:=h_rp;
           motorSRM.rotor.dim.b_ry:=b_ry;

           motorSRM.casing.dim.l_case_exp_front:=l_case_exp_front;
           motorSRM.casing.dim.l_case_exp_rear:=l_case_exp_rear;
           motorSRM.casing.dim.l_case_exp_void:=l_case_exp_void;
           motorSRM.casing.dim.l_case:=l_case;
           motorSRM.casing.dim.h_case:=h_case;
           motorSRM.rotor.dim.l_shaft_extra:=l_shaft_extra;
           motorSRM.rotor.dim.l_shaft:=l_shaft;
           motorSRM.casing.l_total:=l_total;
           motorSRM.casing.h_total:=h_total;
           motorSRM.stator.win.dim.l_extra:=l_extra;
           motorSRM.stator.win.dim.l_extraH:=b_sc_max;
           motorSRM.stator.win.dim.l_extraW:=D_cond/2;
           motorSRM.casing.dim.phy.Delta_t_work:=Delta_t_work;
           motorSRM.casing.dim.phy.t_amb:=t_amb;
           motorSRM.casing.dim.phy.t_aro:=t_aro;
           motorSRM.stator.dim.phy.ro_t_aro:=ro_t_aro;
           motorSRM.casing.dim.phy.ro_case:=ro_case;
           motorSRM.stator.dim.phy.ro_mst:=ro_mst;
           motorSRM.stator.dim.phy.ro_cu:=ro_cu;
           motorSRM.rotor.dim.phy.ro_mst:=ro_mst;
           motorSRM.rotor.dim.phy.ro_shaft:=ro_shaft;

           motorSRM.stator.phy.L_u:=99999;
           motorSRM.stator.phy.L_a:=99999;

           motorSRM.rotor.dim.M_mst_R:=99999;
           motorSRM.rotor.dim.M_R:=99999;
           motorSRM.rotor.dim.I_R:=99999;
           motorSRM.plate.M:=99999;
           motorSRM.plate.TmM:=99999;
           motorSRM.stator.dim.mM:=99999;
           motorSRM.stator.dim.cM:=99999;
           motorSRM.casing.dim.M_S:=99999;
           motorSRM.casing.dim.I_S:=99999;

           // Case below current density target
           if J_round_real < J_s then
             motorLOWER:=motorSRM;
             J_motorLOWER:=J_round_real;

             if J_round_real*2.1<J_s then // far from the solution
               h_sp_temp:=AuxiliaryFunc.round(h_sp*1000*0.75)/1000;
             else
               h_sp_temp:=AuxiliaryFunc.round(h_sp*1000*0.99)/1000;
             end if;

             if h_sp_temp==h_sp then
               h_sp:=h_sp - 0.001; // minimum decrease of 1mm
               lower_last_attempt:=1;
             else
               h_sp:=h_sp_temp;
               lower_last_attempt:=0;
             end if;
           end if;

           // Case above current density target
           if J_round_real>J_s then
             motorHIGHER:=motorSRM;
             J_motorHIGHER:=J_round_real;
             if J_round_real>2.1*J_s then
               h_sp_temp:=AuxiliaryFunc.round(h_sp*1000/0.75)/1000;
             else
               h_sp_temp:=AuxiliaryFunc.round(h_sp*1000*1.01)/1000;
             end if;

             if h_sp_temp==h_sp then
               h_sp:=h_sp + 0.001;
                                // minimum incrase of 1mm
               higher_last_attempt:=1;
             else
               h_sp:=h_sp_temp;
               higher_last_attempt:=0;
             end if;
           end if;

           // Stop condition in case of abnormal geometry
           if J_solution_reached==1 and B_solution_reached==1 then
             motorSRM_Good:=motorSRM;
           end if;

           // Stop Condition
           // First: at least iterated twice
           // Second: selection of closest machine to the desired current density
           // Third: difference must be less than Allowed_diff

           if J_counter>=3 then
             lower_diff:=abs(J_motorLOWER - J_s)/J_s;
             higher_diff:=abs(J_motorHIGHER - J_s)/J_s;

             if lower_diff < Allowed_diff and higher_diff<Allowed_diff then
               if lower_diff<higher_diff then
                 motorSRM_Good:=motorLOWER;
                 J_solution_reached:=1;
                else
                 motorSRM_Good:=motorHIGHER;
                 J_solution_reached:=1;
                end if;
              elseif lower_diff<Allowed_diff then
               motorSRM_Good:=motorLOWER;
               J_solution_reached:=1;
              elseif higher_diff<Allowed_diff then
               motorSRM_Good:=motorHIGHER;
               J_solution_reached:=1;
              elseif lower_last_attempt*higher_last_attempt==1 then
               if lower_diff<higher_diff then
                 motorSRM_Good:=motorLOWER;
                 J_solution_reached:=1;
               else
                 motorSRM_Good:=motorHIGHER;
                 J_solution_reached:=1;
               end if;
             end if;
           end if;

           J_counter:=J_counter + 1;
         end while;
         N_ph_r:=motorSRM_Good.stator.win.N_ph;
         J_round_real:=motorSRM_Good.stator.J_s_real;
         RR_tot:=motorSRM_Good.specs.RR_tot;
         b_sp:=motorSRM_Good.stator.dim.b_sp;
         h_sp:=motorSRM_Good.stator.dim.h_sp;
         D:=motorSRM_Good.stator.D;
         D_os:=motorSRM_Good.stator.dim.D_os;
         h_rp:=motorSRM_Good.rotor.dim.h_rp;
         D_cond:=motorSRM.stator.win.dim.D_cond;

         Flux_real:=(2/a)*(N_ph_r/2)*I_ph/R_tot;
         B_work_real:=Flux_real/(b_sp*l);
         B_err:=(B_work_real - B_work)/B_work;
         P_mec_real:=(eff*K_1*K_2*K_d*B_work_real*X*n*A_s)*D^3;
         P_err:=(P_mec_real - P_mec)/P_mec;
         J_err:=(J_round_real - J_s)/J_s;

         if abs(B_err)>0.05 then
           if B_work_real>B_work then
             D:=D - 0.003;
           elseif B_work_real<B_work then
             D:=D + 0.003;
           end if;
         else
           B_solution_reached:=1;
         end if;
       end while;

       // Inductances
       L_alig:=N_ph_r*Flux_real/I_ph;
       L_unalig:=(1 - K_2)*L_alig;
       motorSRM_Good.stator.phy.L_u:=L_unalig;
       motorSRM_Good.stator.phy.L_a:=L_alig;

       // Masses and moments of inertia
       motorSRM_Good:=AuxiliaryFunc.MassandMOISRM(motorSRM_Good);

       SRM_Structure.SRM_Export(motorSRM_Good);
      annotation (
        Documentation(info="<html>

<head>
<title>General SRM Pre-sizing help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<h2><a name='_Toc414020113'></a><a name='_Toc411951245'></a><a
name='_Toc414020102'></a><a name='_Toc411951234'><span lang=EN-US>1.1<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US>Table of Inputs</span></a></h2>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Name</span></b></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Description</span></b></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Units</span></b></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Type</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>U_ph</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Phase Voltage</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>V</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><b><span lang=EN-US>tt</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>I_ph</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Phase Current</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>I</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><b><span lang=EN-US>tt</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>q</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Phases</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>m</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Phases conducting Simultaneously</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>N_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Stator Poles</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>N_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Rotor Poles</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>b_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Pole Angle</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>degree</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>b_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Pole Angle</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>degree</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>a</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Parallel Paths</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Airgap length</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>f_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Lamination factor</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_work</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Flux Density Seed Value at Mag Steel Working
  Point</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><b><span lang=EN-US>tt</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>P_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Mechanical Power</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>W</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><b><span lang=EN-US>tt</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>n</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Speed</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>rpm</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>eff</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>efficiency</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>J_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Current Density at Stator</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A<sub>rms</sub>/mm<sup>2</sup></span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>tt</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>X</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Aspect Ratio</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>A_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Electrical Loading</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A/m</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_d</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>duty cycle</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_1</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Inductance Constant 1</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_2</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Inductance Constant 2</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>b_seg</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Spacing Between Consecutive Coils</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>BRatio_sy</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Flux Density Stator Pole to Yoke Ratio</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>BRatio_ryp</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Flux Density Rotor Pole to Yoke Ratio</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 style='width:90.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_case</span></p>
  </td>
  <td width=282 style='width:211.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>&nbsp;Housing Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 style='width:90.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_mst</span></p>
  </td>
  <td width=282 style='width:211.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Magnetic Steel Density&nbsp;</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 style='width:90.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_cu</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Copper Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 style='width:90.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_mst</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Magnetic Steel Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 style='width:90.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_shaft</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Steel Density</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Kg/m<sup>3</sup></span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_shaft</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Length</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_shaft_extra</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft External Length</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_amb</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Ambient Temperature</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_aro</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Reference Resistivity Temperature</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_t_aro</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Reference Resistivity</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>&#8486;·m</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Delta_t_work</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Temperature Increase over ambient</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>case_type</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Flag for housing type</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Allowed_diff</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>% of J_s error allowed </span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><b><span lang=EN-US>tt</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>coil_security_coef</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Coil Area Security Coefficient</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>fix</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Magnetic_Steel.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>B-H curve of the magnetic steel</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>[T,A/m]</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>PowerDshaft.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>IEC60034 Machine Power vs. Shaft Diameter</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>n/a</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>AWGwirings.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>AWG gauges and properties table</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>n/a</span></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2 style='page-break-after:avoid'><span lang=EN-US>n/a</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019825'><span lang=EN-GB>Table </span></a><span lang=EN-GB>1</span><span lang=EN-GB>. Table of Inputs</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

<h2><span lang=EN-US>1.2<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Algorithm concept</span></h2>

<p class=MsoNormal><span lang=EN-US>The algorithm is based on dimensioning the
machine in order to fulfill stator pole flux density and stator current
density. All geometry is parametrized in order to depend on those two
variables.</span></p>

<p class=MsoNormal><span lang=EN-US>In every iteration one variable is </span><span
lang=EN-GB>changed (B_sp or J_s),</span><span lang=EN-US> geometry
recalculated, after that, magnetic circuit evaluated and a decision is made.
The decision depends on the difference between the current iteration values and
the target values of each of those two variables. Once the machine is finished
inductances are recalculated, then masses and moments of inertia and finally
its graphical representations are made.</span></p>

<h2><a name='_Toc414020114'></a><a name='_Toc411951246'><span lang=EN-US>1.3<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US>Program Structure Explanation</span></a></h2>

<p class=MsoNormal><span lang=EN-GB>Use </span><span
lang=EN-GB>Figure 1</span><span lang=EN-GB> to follow basic sizing explanation.
After inputs several extra requirements are calculated: The feasible region for
the stator and rotor angles, assignment of Shaft diameter and initial Bore
diameter value. </span></p>

<p class=MsoNormal><span lang=EN-GB>Once this data is generated the first loop
(Flux density, B_sp) variables are initialized including length and stator pole
length. B_work input is assigned to B_sp, being stator pole the most saturated
part in the circuit. By that assignment it is assured that no other parts will
work more saturated than stator pole.</span></p>

<p class=MsoNormal><span lang=EN-GB>Then the second loop is initialized (Stator
Current Density, </span><span class=CdigoenWordCar><span lang=EN-US
style='font-size:10.0pt;font-weight:normal'>J_s</span></span><span lang=EN-GB>).
This loop is inside the first one. In the Stator Current Density loop first
Flux Densities are evaluated based on input Ratios and geometry restrictions,
then geometry associated to those restrictions. After that geometry is known, reluctance
paths lengths are calculated and coil size is obtained. FMM of each part is
then calculated and given the stator current input a number of turns
calculated.</span></p>

<p class=MsoNormal><span lang=EN-GB>Those number of turns per phase (N_ph) and
the coil area (2·h_c·b_c) are evaluated in order to know the best distribution
of conductors (hexapack()</span><span class=CdigoenWordCar><span lang=EN-GB
style='font-size:10.0pt;font-weight:normal'> </span></span><span lang=EN-GB>and
quadrapack()) and which current density is obtained. In this phase also the
coil resistance is calculated. At this point all subsequent geometry in the
three axes can be calculated and a motorSRM structure is fulfilled. </span></p>

<p class=MsoNormal><span lang=EN-GB>Once this is done two last operations in
the loop are performed: first it is decided which will be next iteration stator
pole height (and hence coil length) depending on the current density. Second,
if at least three iterations have been performed the loop evaluates if the best
solution has been reached: first the two closest generated motors from above
and below the target value are compared. If any is inside the “considered
correct” (Target value ± Allowed Difference) range is selected as good, if not,
and the maximum iterations have been reached the best solution is selected. In
both cases the results return to the first loop.  This methodology always saves
the best two machines above and below the target requirements for J_s no matter
how many iterations are performed. </span></p>

<p class=MsoNormal><span lang=EN-GB>If the generated motor also fulfils the
B_sp requirements is considered as good and the second outer loop breaks. </span></p>

<p class=MsoNormal><span lang=EN-GB>For this machines real vs. target values
are calculated and introduced into the motor structure. Inductances are
calculated and finally Moments of Inertia and Masses.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=570 height=843 src='General_SRM_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019930'></a><a name='_Ref413083381'><span
lang=EN-GB>Figure </span></a><span lang=EN-GB>1</span><span lang=EN-GB>. Global
Sizing Algorithm explanation.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end SRM_Motor_Sizing;

    function SYN_Motor_Sizing "SizingNEW_SM25"
      import Modelica.Constants;
      import Modelica.SIunits;
      constant Real mu_air=Constants.mue_0*1.00000037 "Air permeability";
      constant Real c=68E-6 "Cu Temperature Constant";
      parameter input Integer generator(min=0,max=1)=1 "Generator/Machine flag"
      annotation (Dialog(group="Nominal parameters"));
      parameter input SIunits.Torque T(min=0)=159.1557 "Rated torque"
      annotation (Dialog(group="Nominal parameters"));
      parameter input
        Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm               n_rpm(min=0)=12000
        "Rated speed"
      annotation (Dialog(group="Nominal parameters"));
      parameter input SIunits.Voltage U_ph_s(min=0)=115.4339
        "Rated phase voltage"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real eff(min=0,max=1)=0.95 "Stator efficiency"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real PF(min=0,max=1)=0.95 "Power factor"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real C_mec_ini(min=0)=315 "Mechanical constant"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Boolean C_mec_family_flag=false "C_mec_family_flag"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real X(min=0)=0.7 "Form factor"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input SIunits.Length g(min=0)=0.000528 "Airgap length"
      annotation (Dialog(tab="Airgap"));
      parameter input Integer Q_s(min=2)=30 "Number of stator slots"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input Integer m(min=2)=3 "Number of phases"
      annotation (Dialog(group="Nominal parameters"));
      //parameter input SIunits.Current I_s_ref(min=0)=154.93 "Rated current"
      //annotation (Dialog(group="Nominal parameters"));
      parameter input SIunits.CurrentDensity J_s(min=0)=31e6
        "Stator current density"
      annotation (Dialog(tab="Stator", group="Winding"));
      parameter input Integer a_s(min=1)=1 "Number of parallel paths in stator"
      annotation (Dialog(tab="Stator", group="Winding"));
      parameter input SIunits.Current I_dc_r(min=0)=132.5309
        "Rated pole current"
      annotation (Dialog(tab="Rotor", group="Winding"));
      parameter input Real sigma_p(min=1)=1.0 "Polar dispersion coeficient"
      annotation (Dialog(tab="Rotor", group="Winding"));
      parameter input Real sigma_arc(min=1)=2 "Rotor coil area security factor"
      annotation (Dialog(tab="Rotor", group="Winding"));
      parameter input SIunits.CurrentDensity J_r(min=0)=31e6
        "Rotor current density"
      annotation (Dialog(tab="Rotor", group="Winding"));
      parameter input Integer p(min=2)=5 "Number of pole pairs"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real pcf(min=0,max=1)=0.7 "Pole cover factor"
      annotation (Dialog(tab="Rotor", group="Winding"));
      parameter input SIunits.MagneticFluxDensity B_g_ini(min=0)=1.225
        "Airgap max real flux density"
      annotation (Dialog(tab="Airgap"));
      parameter input SIunits.MagneticFluxDensity B_yoke_ini(min=0)=1.9
        "Stator yoke max real flux density"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input SIunits.MagneticFluxDensity B_tooth_ini(min=0)=1.9
        "Stator tooth max real flux density"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input SIunits.MagneticFluxDensity B_pole_ini(min=0)=1.9
        "Rotor pole max real flux density"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input SIunits.MagneticFluxDensity B_ryoke_limit(min=0)=1.6
        "Rotor yoke max real flux density"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input String Stator_Material_File="M235_35A_refined.txt"
        "Stator Material B(H) characteristic"
      annotation (Dialog(tab="Stator",group="Material"));
      parameter input String Rotor_Material_File="M235_35A_refined.txt"
        "Rotor Material B(H) characteristic"
      annotation (Dialog(tab="Rotor",group="Material"));
      parameter input Real f_r_s(min=0)=0.98 "Stator lamination factor"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Real K_u_s(min=0,max=1)=0.5378
        "Stator slot filling factor"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Real f_r_r(min=0)=0.98 "Rotor lamination factor"
      annotation (Dialog(tab="Rotor",group="Winding"));
      parameter input Real K_u_r(min=0,max=1)=0.3689
        "Rotor solot filling factor"
      annotation (Dialog(tab="Rotor",group="Winding"));
      parameter input SIunits.Length b_1(min=0)=0.00127 "Stator slot opening"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input SIunits.Angle gamma_ref(min=0,max=360)=12 "Skewing angle"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input Real K_uhc(min=0,max=1)=0.8
        "Stator head coil filling factor"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input SIunits.Angle alpha_rph_ref(min=0,max=90)=40
        "Pole head line angle"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input SIunits.Length D_shaft_ref(min=0)=0.055
        "Shaft external diameter"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input SIunits.Length D_shaft_internal_ref(min=0)=0
        "Shaft internal diameter"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input Real Internal_shaft_coefficient=1.1722
        "Ratio Between External and Internal Shafts"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input SIunits.Length l_shaft_ini(min=0)=0 "Shaft length"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input SIunits.Length l_shaft_extra_ref(min=0)=50/1000
        "Shaft extra length"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input Integer case_type(min=1,max=2)=1
        "1=Circular, 2=Square solid"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Density ro_mst(min=0)=7460
        "Magnetic steel density"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Density ro_cu(min=0)=8750 "Cu density"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Density ro_case(min=0)=7850
        "Metal cabinet density"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Density ro_shaft(min=0)=7460
        "Magnetic steel density"
      annotation (Dialog(tab="Stator"));
      parameter input Integer n_cs(min=0)=0 "Stator number of cooling ducts"
      annotation (Dialog(tab="Stator"));
      parameter input Integer e_cs_t(min=1,max=2)=1
        "Stator Wide/Type of cooling ducts 1=10mm, 2=15mm wide square"
        annotation (Dialog(tab="Stator"));
      parameter input Integer n_cr(min=0)=0 "Rotor number of cooling ducts"
      annotation (Dialog(tab="Rotor"));
      parameter input Integer e_cr_t(min=1,max=2)=1
        "Rotor Wide/Type of cooling ducts 1=10mm, 2=15mm wide squre"
      annotation (Dialog(tab="Rotor"));
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_aro(min=0)=20
        "Reference resistivity temperature"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Resistivity ro_t_aro(min=0)=1.68e-8
        "Cu resistivity"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_amb(min=0)=35
        "Ambient temperature"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC Delta_t_work(min=0)=215
        "Temperature increase relative to ambient"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Length l_extraH_ref(min=0)=0.008
        "Stator head coil longitudinal extra length"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input SIunits.Length l_extraW_ref(min=0)=0.015
        "Stator head coil winding longitudinal extra length"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input String PowerDshaftFile="PowerDshaft.txt"
        "IEC60034 Machine Power vs D_shaft"
      annotation (Dialog(tab="Rotor",group="Geometry"));
      parameter input String AWGwiringsFile="AWGwirings.txt"
        "AWG wiring characteristics"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input String Ksat_alpha_File="Ksat_alpha.txt"
        "Saturation factor characteristic"
      annotation (Dialog(tab="Stator",group="Material"));
      parameter input String K_M_seta_File="K_M_seta.txt"
        "K_M vs. pcf curve for interpolation for sine pole only"
      annotation (Dialog(tab="Rotor",group="Winding"));
      parameter input String K_F_seta_File="K_F_seta.txt"
        "K_F vs. pcf curve for interpolation for sine pole only"
      annotation (Dialog(tab="Rotor",group="Winding"));
      parameter input String ecd_10_File="ecd_10.txt"
        "Corrected distance by airgap for 10mm cooling channels"
      annotation (Dialog(tab="Airgap"));
      parameter input String ecd_15_File="ecd_15.txt"
        "Corrected distance by airgap for 15mm channels"
      annotation (Dialog(tab="Airgap"));
      parameter Real K_E_U_ratio(min=0)=1.05 "Per phase E/U modulus ratio"
      annotation (Dialog(tab="Stator"));
      parameter Real Internal_shaft_coeficient(min=1)=1.3
        "Ratio between external and internal shafts"
      annotation (Dialog(tab="Rotor"));

    protected
      SIunits.Angle gamma=gamma_ref*Constants.pi/180 "Skewing angle";
      SIunits.Angle alpha_rph=alpha_rph_ref*Constants.pi/180
        "Pole head line angle";
      SIunits.Length l_extraH=l_extraH_ref
        "Stator head coil longitudinal extra length";
      SIunits.Length l_extraW=l_extraW_ref
        "Stator head coil winding longitudinal extra length";
      SIunits.Length l_shaft_extra=l_shaft_extra_ref "Shaft extra length";
      SYN_Structure.motorSYN motorSYN "Structure with whole motor info";
      Real precision "Number of points per degree in rotor geometry";
      Real cooling_ducts_relative_position
        "Variable to determine the relative position between stator and rotor ducts";
      Real Allowed_diff "Allowewd current density error";
      Integer R_fe_consider "Consider or not R_fe in T.vs.S evaluation";
      Integer failflag "Fail information";
      parameter Real[:,:] Mat_s=DataFiles.readCSVmatrix(Stator_Material_File)
        "Stator Material B(H) characteristic";
      parameter Real[:,:] Mat_r=DataFiles.readCSVmatrix(Rotor_Material_File)
        "Rotor Material B(H) characteristic";
      parameter Real[:,:] Ksat_alpha=DataFiles.readCSVmatrix(Ksat_alpha_File)
        "Ksat_alpha(alpha) characteristic";
      parameter Real[:,4] PowerDshaft=DataFiles.readCSVmatrix(PowerDshaftFile);
      parameter SIunits.Power[:] PowerD=PowerDshaft[:,1];
      parameter Real[:] D_shaftD=PowerDshaft[:,4];
      parameter Real[:,:] AWGwirings=DataFiles.readCSVmatrix(AWGwiringsFile);
      parameter Real[:,:] K_M_seta=DataFiles.readCSVmatrix(K_M_seta_File);
      parameter Real[:,:] ecd_10=DataFiles.readCSVmatrix(ecd_10_File);
      parameter Real[:,:] ecd_15=DataFiles.readCSVmatrix(ecd_15_File);
      parameter Real[:,:] K_F_seta=DataFiles.readCSVmatrix(K_F_seta_File);
      SIunits.AngularVelocity n "Mechanical speed";
      SIunits.Frequency f "Electrical frequency";
      Real C_mec "Mechanical constant";
      SIunits.Power P_mec "Mechanical power";
      SIunits.Power P_e "Rated electrical power";
      Integer Q_r "Number of poles";
      SIunits.Angle t_P "Polar pitch";
      SIunits.Angle t_S "Slot pitch";
      Integer coilspan "Coilspan";
      Real q "Slots per pole per phase";
      Real K_w "Winding factor";
      SIunits.Current I_s "Rated current";
      SIunits.Length D_shaft=D_shaft_ref "Shaft external diameter";
      SIunits.Length D_shaft_internal=D_shaft_internal_ref
        "Shaft internal diameter";
      Real K_M "Relative amplitude factor";
      SIunits.Length delta_htr "height of teeth expansion";
      Integer htrmodifflag "Use of trapezoidal slot flag 0=no, 1=yes";
      SIunits.Length D "Bore diameter";
      SIunits.Length L_brut "Raw machine length";
      SIunits.Length t_s=D/2*t_S "Slot pitch";
      Real ecd_s[:,:] "Stator cooling ducts properties";
      SIunits.Length e_cs "Stator duct length";
      Real ecd_r[:,:] "Rotor cooloing ducts properties";
      SIunits.Length e_cr "Rotor duct length";
      SIunits.Length ed_s "Stator duct corrected length";
      SIunits.Length ed_r "Rotor duct corrected length";
      SIunits.Length L_delta "Cooling duct interpolated corrected length";
      Real K_L "Axial stator length correction factor";
      Real K_C "Carter fringing coefficient";
      Real K_F "Relative form factor";
      Real K_fe "Tooth linear correction factor";
      Real L_fe "Iron corrected for stator";
      Real L_p "Iron corrected for poles";
      Real K_p "Pole linear correction factor";
      Real L_ry "Iron corrected for rotor-yoke";
      Real K_ry "Rotor yoke linear correction factor";
      SIunits.Length t_p "Polar pitch";
      SIunits.Angle t_pcf "Pole cover factor angle";
      SIunits.Length b_p "Pole cover factor distance";
      SIunits.MagneticFluxDensity B_g_max_real "Airgap max real flux density";
      SIunits.MagneticFluxDensity B_g_max_sin
        "Airgap max sine FEM equivalent flux density";
      SIunits.MagneticFluxDensity B_st_max_real
        "Stator tooth max real flux density";
      SIunits.MagneticFluxDensity B_sy_max_real
        "Stator yoke max real flux density";
      SIunits.Voltage E_ph_s "Phase FEM";
      Real N_ph1 "Number of turns per phase not rounded";
      Integer z_Q "Number of conductors per slot";
      Real N_ph_s "Number of turns per phase integer";
      SIunits.MagneticFluxDensity B_g_max_sin_round
        "Airgap max sine FEM equivalent flux density";
      SIunits.MagneticFluxDensity B_g_stat_error
        "Relative error between target and final B_g";
      SIunits.MagneticFieldStrength H_st_max_real "Stator tooth field strength";
      SIunits.Length b_z "Stator tooth width @ D";
      SIunits.Length b_s "Stator slot width @ D";
      SIunits.Length h_y "Stator yoke width";
      SIunits.Length h_tr "Stator slot height";
      SIunits.Length h_z "Stator tooth height";
      SIunits.MagneticFieldStrength H_sy_max_real;
      SIunits.RelativePermeability mu_r_sy;
      SIunits.RelativePermeability mu_r_st;
      SIunits.MagneticFieldStrength H_g_max_real
        "Airgap max real field density";
      SIunits.MagnetomotiveForce U_g_max_real
        "Airgap max real magnetic voltage";
      SIunits.Length l_sy "Stator yoke path length";
      SIunits.MagnetomotiveForce U_s_max "Airgap magnetic voltage";
      Real K_sat "Saturation factor";
      Real alpha_i;
      SIunits.Length D_os "Outer stator diameter";
      SIunits.Length D_or "Outer rotor diameter";
      Integer B_counter "Flux density counter";
      Real B_counter_limit "Flux density limit";
      SIunits.MagneticFluxDensity B_rp_max_real
        "Desired flux density at rotor pole";
      SIunits.Length b_rp "Rotor pole width";
      SIunits.Length h_rp "Rotor pole height";
      Integer J_solution_reached "Current density solution reach flag";
      Integer J_counter "Current density counter";
      Integer J_counter_limit "Current density limit";
      SIunits.CurrentDensity J_motorLOWER
        "Current density of the below-limit solution";
      SIunits.CurrentDensity J_motorHIGHER
        "Current density of the above-limit solution";
      Integer lower_last_attempt "Flag for below limit solution";
      Integer higher_last_attempt "Flag for above limit solution";
      SIunits.Length h_rc "Rotor coil height";
      SIunits.Length g_pcf "Airgap thickness at pole limit";
      SIunits.Length h_pcf "Pole head 20 degrees thickness";
      SIunits.Length b_3 "Tangencial distance of point3";
      Real[1,2] Point1 "Point 1 coordinates";
      Real[1,2] Point3 "Point 3 coordinates";
      Real[1,2] Point33 "Point 3 coordinates";
      Real[1,2] Point2 "Point 3 coordinates";
      SIunits.Length h_0 "Circular head height";
      SIunits.Length D_ir_prima "Inner rotor diameter";
      SIunits.Length D_ir "Radial distance of point2";
      Real[1,2] Point22 "Point 22 coordinates";
      SIunits.Length dist_Point1_3 "Distance between P1,P3";
      SIunits.Angle beta "Pole cover angle";
      SIunits.Length r_ph "Radius of pole head circle";
      SIunits.Length b_rc_Distance_candidate1 "b_rc candidate 1";
      SIunits.Length b_rc_Distance_candidate2 "b_rc candidate 2";
      SIunits.Length b_rc_max;
      Real[1,2] Point333L "Point333L coordinates";
      SIunits.Length scoil_displacement_x "Semi coil area positioning x-coord";
      SIunits.Length scoil_displacement_y "Semi coil area positioning y-coord";
      SIunits.Length[1,2] semi_coil_center "Using MOI-COORDS";
      SIunits.Length b_ry "Rotor yoke width";
      SIunits.MagneticFluxDensity B_ry_max_real
        "Rotor yoke max real flux density";
      SIunits.Length l_st "Stator tooth path length";
      SIunits.Length l_g "Airgap path length";
      SIunits.Length l_rph "Rotor pole head path length";
      SIunits.Length l_rp "Rotor pole path length";
      SIunits.Length l_ry "Rotor yoke path length";
      SIunits.MagneticFieldStrength H_rph_max_real
        "Rotor head pole max real field density";
      SIunits.MagneticFieldStrength H_rp_max_real
        "Rotor pole max real field density";
      SIunits.MagneticFieldStrength H_ry_max_real
        "Rotor yoke max real field density";
      SIunits.Current FMM_fmm_real_max "Max FMM total real";
      Real N_ph_rpole "Number of turns per phase";
      Real N_pc_round_rpole "Rounded number of conductors/turns per pole";
      Real FMM_fmm_real_max_round "Rounded maax fmm total real";
      Real FMM_rel_error "FMM max relative error";
      SIunits.Resistance Re_s_cond_u "Stator conductor resistance per meter";
      AuxiliaryFunc.packr hexa_data;
      AuxiliaryFunc.packr quadra_data;
      AuxiliaryFunc.packr Good_wiring;
      Real C_rc "Number of conductors per area";
      SIunits.Length D_cond_r "Conductor diameter";
      Integer Gauge_rt "Conductor AWG Gauge";
      Real K_f_r "Fill factor";
      Real K_f_r_real "Real fill factor";
      Real h_rc_real "Actual occupied area";
      Real b_rc_real;
      Real[:,:] centers;
      Real[:,:] Semi_coil_points;
      Real[:,:] Semi_coil_real_points;
      Real gauge;
      SIunits.CurrentDensity J_round_real;
      AuxiliaryFunc.res_output Resistance "Resistance structure";
      SIunits.Resistance R_rc "Coil resistance";
      SIunits.Length l_coil "Coil wire length";
      SIunits.Conversions.NonSIunits.Temperature_degC t_work
        "Cu working temperature";
      SIunits.Resistivity ro_work "Resistivity at work temperature";
      SIunits.Resistance R_rc_t_aro "Coil resistance at ambient temperature";
      SIunits.Resistance R_rpole "Pole coil resistance";
      SIunits.Length l_extraR "Rotor extra headcoil length";
      SIunits.Length l_extraS "Stator extra headcoil length";
      SIunits.Length h_case "Square case height";
      SIunits.Length l_extra;
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case "Square case length";
      SIunits.Length l_total "Total case length";
      SIunits.Length h_total "Total case height";
      SIunits.Length l_shaft "Total shaft length";
      Real[200,2] rotorpolepoints;
      Real[90,2] rotorpolepoints_tempA;
      Real[4,2] rotorpolepoints_tempB;
      Real[90,2] rotorpolepoints_tempC;
      Real[1,2] rotorpolepoints_tempD;
      SIunits.Conversions.NonSIunits.Angle_deg top_superior_angle;
      Integer initial_for_point;
      Real[4,2] rotorpolepointsR;
      SIunits.Conversions.NonSIunits.Angle_deg initial_bottom_pole_angle;
      Real[1000,2] externalrotorpoints;
      Integer fail_elements;
      Real[4,2] fail_element1;
      Real[2,2] fail_element2;
      Real[5,2] semi_coil_points;
      Real[5,2] semi_coil_points_R;
      SIunits.Power P_elec_pole "Electrical power losses per pole";
      SIunits.Power P_elec_t "Electrical power used (stator+rotor)";
      SYN_Structure.motorSYN motorLOWER
        "Machine structure for comparison purpose";
      SYN_Structure.motorSYN motorHIGHER
        "Machine structure for comparison purpose";
      Real h_rp_temp;
      SYN_Structure.motorSYN motorSYN_Good;
      Real lower_diff "Current density lower per unit difference";
      Real higher_diff "Current density higher per unit difference";
      SIunits.Length pole_height "Rotor pole height";
      Real eff_real "Actual efficiency";
      Integer l_matrixA;
      Integer l_for=0;
      Integer l_matrixC;
      Integer l_matrixD;
      Real[400,2] rotorpolepoints_POLE;

    algorithm
       assert(generator==0 or generator==1,"generator input value outside of valid range {0,1}");
       assert(T > 0,"T input value outside of valid range (0,inf)");
       assert(n_rpm > 0,"n_rpm input value outside of valid range (0,inf)");
       assert(U_ph_s > 0,"U_ph_s input value outside of valid range (0,inf)");
       assert(eff > 0 and eff <=1,"eff input value outside of valid range (0,1>");
       assert(PF > 0 and PF <=1,"eff input value outside of valid range (0,1>");
       assert(C_mec_ini > 0,"C_mec_ini input value outside of valid range (0,inf)");
       assert(X > 0,"X input value outside of valid range (0,inf)");
       assert(g > 0,"g input value outside of valid range (0,inf)");
       assert(Q_s >= 2,"Q_s input value outside of valid range <2,inf)");
       assert(m >= 2,"m input value outside of valid range (0,inf)");
       //assert(I_s_ref > 0,"I_s_ref input value outside of valid range (0,inf)");
       assert(J_s > 0,"J_s input value outside of valid range (0,inf)");
       assert(a_s >= 1,"a_s input value outside of valid range <1,inf)");
       assert(I_dc_r > 0,"I_dc_r input value outside of valid range (0,inf)");
       assert(sigma_p >= 1,"sigma_p input value outside of valid range <1,inf)");
       assert(sigma_arc >= 1,"sigma_arc input value outside of valid range <1,inf)");
       assert(J_r > 0,"J_r input value outside of valid range (0,inf)");
       assert(p >= 2,"p input value outside of valid range <2,inf)");
       assert(pcf > 0 and pcf <=1,"pcf input value outside of valid range (0,1>");
       assert(B_g_ini > 0,"B_g_ini input value outside of valid range (0,inf)");
       assert(B_yoke_ini > 0,"B_yoke_ini input value outside of valid range (0,inf)");
       assert(B_tooth_ini > 0,"B_tooth_ini input value outside of valid range (0,inf)");
       assert(B_pole_ini > 0,"B_pole_ini input value outside of valid range (0,inf)");
       assert(B_ryoke_limit > 0,"B_ryoke_limit input value outside of valid range (0,inf)");
       assert(f_r_s > 0,"f_r_s input value outside of valid range (0,inf)");
       assert(K_u_s > 0 and eff <=1,"K_u_s input value outside of valid range (0,1>");
       assert(f_r_r > 0,"f_r_r input value outside of valid range (0,inf)");
       assert(K_u_r > 0 and eff <=1,"K_u_r input value outside of valid range (0,1>");
       assert(b_1 > 0,"b_1 input value outside of valid range (0,inf)");
       assert(gamma_ref >= 0 and gamma_ref <=360,"gamma_ref input value outside of valid range <0,360>");
       assert(K_uhc > 0 and K_uhc <=1,"K_uhc input value outside of valid range (0,1>");
       assert(alpha_rph > 0 and alpha_rph <=90,"alpha_rph_ref input value outside of valid range (0,90>");
       assert(D_shaft >= 0,"D_shaft input value outside of valid range <0,inf)");
       assert(D_shaft_internal >= 0,"D_shaft_internal input value outside of valid range <0,inf)");
       assert(l_shaft_ini >= 0,"l_shaft_ini input value outside of valid range <0,inf)");
       assert(l_shaft_extra >= 0,"l_shaft_extra input value outside of valid range <0,inf)");
       assert(case_type==1 or case_type==2,"case_type input value outside of valid range {1,2}");
       assert(ro_mst > 0,"ro_mst input value outside of valid range (0,inf)");
       assert(ro_cu > 0,"ro_cu input value outside of valid range (0,inf)");
       assert(ro_case > 0,"ro_case input value outside of valid range (0,inf)");
       assert(ro_shaft > 0,"ro_shaft input value outside of valid range (0,inf)");
       assert(n_cs >= 0,"n_cs input value outside of valid range <0,inf)");
       assert(e_cs_t==1 or e_cs_t==2,"e_cs_t input value outside of valid range {1,2}");
       assert(n_cr >= 0,"n_cr input value outside of valid range <0,inf)");
       assert(e_cr_t==1 or e_cr_t==2,"e_cr_t input value outside of valid range {1,2}");
       assert(t_aro >= 0,"t_aro input value outside of valid range <0,inf)");
       assert(ro_t_aro > 0,"ro_t_aro input value outside of valid range (0,inf)");
       assert(t_amb >= 0,"t_amb input value outside of valid range <0,inf)");
       assert(Delta_t_work > 0,"Delta_t_work input value outside of valid range (0,inf)");
       assert(l_extraH >= 0,"l_extraH input value outside of valid range <0,inf)");
       assert(l_extraW >= 0,"l_extraW input value outside of valid range <0,inf)");
       assert(K_E_U_ratio > 0,"K_E_U_ratio input value outside of valid range (0,inf)");
       assert(Internal_shaft_coeficient > 1,"Internal_shaft_coeficient input value outside of valid range (1,inf)");

       precision:=1;
       cooling_ducts_relative_position:=99999;
       Allowed_diff:=0.05;
       R_fe_consider:=0;
       failflag:=0;

       // STEP 1: Data preparation
       if pcf>0.8 or pcf<0.5 then
         Modelica.Utilities.Streams.print("*** ERROR: Pole Cover Factor must be between 0.5 and 0.8 in order to use this program","");
         break;
       end if;
       n:=n_rpm/60;
       f:=n*p;
       P_mec:=T*(2*Constants.pi*n);

       if generator==0 then
         P_e:=P_mec/eff;
         I_s:=P_e/(3*U_ph_s*PF);
       elseif generator==1 then
         P_e:=P_mec*eff;
         I_s:=P_e/(3*U_ph_s*PF);
       end if;

       Q_r:=2*p;
       t_P:=2*Constants.pi/Q_r;
       t_S:=2*Constants.pi/Q_s;
       coilspan:=integer(Q_s/p/2);
       q:=Q_s/(2*p*m);

       K_w:=(AuxiliaryFunc.windingf(Q_s,p,m,coilspan,gamma,1))[1];
       K_w:=0.82699;
       if K_w<0 then
         Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be negative, please verify input data: Q_s, p, m","");
         K_w:=abs(K_w);
       elseif K_w==1 then
         Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be >1, please verify input data: Q_s, p, m","");
         K_w:=1;
       end if;

       D_shaft:=AuxiliaryFunc.PowerDShaft(P_mec, Internal_shaft_coefficient);

       if D_shaft_internal==0 then
         D_shaft_internal:=D_shaft*Internal_shaft_coefficient;
         Modelica.Utilities.Streams.print("*** WARNING: D_shaft could not be properly selected Maximun Value as default'","");
       end if;

       if g==0 then
         //g:=t_p*q/(3*K_M*B_g_max_sin);
       end if;

       // Machine size considerations

       if P_mec < 10000 then
         delta_htr:=0.001;
         htrmodifflag:=0;
       elseif P_mec>10000 and P_mec < 25000 then
         delta_htr:=0.0015;
         htrmodifflag:=1;
       elseif P_mec>=25000 then
         delta_htr:=0.002;
         htrmodifflag:=1;
       end if;

       C_mec:=C_mec_ini*1000;
       if C_mec==0 and C_mec_family_flag==false then
         C_mec:=AuxiliaryFunc.CmecCALC(p, P_mec);
         C_mec:=C_mec*1000;
       elseif C_mec_family_flag==true then
         C_mec:=Modelica.Math.Vectors.interpolate(
          (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 1],
          (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 2],
          P_mec/(2*p));
          C_mec:=C_mec*1000;
       end if;

       D:=(P_mec/(C_mec*X*n))^0.3333;
       L_brut:=D*X;
       t_s:=D/2*t_S;
       if B_ryoke_limit==0 then
    //     B_ryoke_limit:=1.4;
       end if;

       // STEP 2: Machine's geometrical factors

       K_M:=Modelica.Math.Vectors.interpolate(K_M_seta[:, 1],K_M_seta[:, 2],pcf);
       if n_cs==n_cr then
         Modelica.Utilities.Streams.print("--- Warning: Aligned cooling ducts selected (Equal number Stator and Rotor)","");
         cooling_ducts_relative_position:=1;
       else
         cooling_ducts_relative_position:=0;
       end if;

       if e_cs_t==1 then
         ecd_s:=ecd_10/1000;
         e_cs:=0.01;
       else
         ecd_s:=ecd_15/1000;
         e_cs:=0.015;
       end if;

       if e_cr_t==1 then
         ecd_r:=ecd_10/1000;
         e_cr:=0.01;
       else
         ecd_r:=ecd_15/1000;
         e_cr:=0.015;
       end if;

       ed_s:=(cooling_ducts_relative_position*Modelica.Math.Vectors.interpolate(ecd_s[:, 1],ecd_s[:, 2],g) + (1 - cooling_ducts_relative_position)*Modelica.Math.Vectors.interpolate(ecd_s[:, 1],ecd_s[:, 3],g));
       ed_r:=(cooling_ducts_relative_position*Modelica.Math.Vectors.interpolate(
        ecd_r[:, 1],
        ecd_r[:, 2],
        g) + (1 - cooling_ducts_relative_position)*Modelica.Math.Vectors.interpolate(
        ecd_r[:, 1],
        ecd_r[:, 3],
        g));

        L_delta:=L_brut - (n_cs*ed_s + n_cr*ed_r);
        if L_delta<0 then
          Modelica.Utilities.Streams.print("*** ERROR: Cooling ducts length bigger than Total raw length (L_brut), verify D or X [PROGRAM ABORTED]","");
          break;
        end if;
        K_L:=L_brut/L_delta;
        K_C:=t_s/(t_s - (b_1*b_1/g)/(5 + b_1/g));
        K_F:=Modelica.Math.Vectors.interpolate(K_F_seta[:, 1],K_F_seta[:, 2],pcf);
        L_fe:=f_r_s*(L_brut-(n_cs*e_cs));
        K_fe:=L_brut/L_fe;
        L_p:=f_r_r*(L_brut -(n_cr*e_cr));
        K_p:=L_brut/L_p;
        L_ry:=L_brut*f_r_r;
        K_ry:=L_brut/L_ry;
        t_p:=(D/2 - g)*t_P;
        t_pcf:=pcf*t_P;
        b_p:=D/2*t_pcf;

        // STEP 3: Caluclation code - stator

        // STEP 3.1: Airgap
        B_g_max_real:=B_g_ini;
        B_g_max_sin:=B_g_max_real/(K_M*K_L*K_C);
        B_st_max_real:=B_tooth_ini;
        B_sy_max_real:=B_yoke_ini;

        // STEP 3.2: Stator

        E_ph_s:=K_E_U_ratio*U_ph_s;
        N_ph1:=Constants.pi/sqrt(2)*E_ph_s/(2*Constants.pi*f*K_w*t_p*L_brut*(K_F/(Constants.pi/2))*B_g_max_sin);
        N_ph_s:=AuxiliaryFunc.round_Nph(N_ph1,p,q);
        z_Q:=AuxiliaryFunc.round(2*a_s*m*N_ph1/Q_s);

        B_g_max_sin_round:=Constants.pi/sqrt(2)*E_ph_s/(a_s*2*Constants.pi*f*K_w*t_p*L_brut*(K_F/(Constants.pi
        /2))*N_ph_s);
        B_g_stat_error:=(B_g_max_sin_round - B_g_max_sin)/B_g_max_sin*100;

        // Stator geometry (based on rounded B_g_max_sin_round)
        H_st_max_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_st_max_real);
        b_z:=t_s*(K_M*K_fe*B_g_max_sin_round - mu_air*(K_fe + 1)*H_st_max_real)/B_st_max_real;
        b_s:=(Constants.pi*(D + 2*delta_htr)/Q_s) - b_z;
        h_y:=0.5*K_F*(K_fe*D/p)*B_g_max_sin_round/B_sy_max_real;
        h_tr:=z_Q*(I_s/(a_s*J_s))/(b_s*K_u_s);

        // Trapezoid correction for size 2 machines
        if htrmodifflag==1 then
          h_tr:=AuxiliaryFunc.htrmodif(Q_s,h_tr,b_s);
        end if;
        h_z:=h_tr + delta_htr;

        // Iron working points
        H_sy_max_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_sy_max_real);
        mu_r_sy:=B_sy_max_real/(H_sy_max_real*Constants.mue_0);
        mu_r_st:=B_st_max_real/(H_st_max_real*Constants.mue_0);

        H_g_max_real:=B_g_max_sin_round*(K_M*K_L*K_C)/mu_air;
        U_g_max_real:=g*H_g_max_real;

        l_sy:=1/2*Constants.pi*(D + 2*h_z + h_y)/(2*p);
        U_s_max:=H_sy_max_real*h_z + H_st_max_real*l_sy;
        K_sat:=(U_s_max + U_g_max_real)/U_g_max_real;

        alpha_i:=Modelica.Math.Vectors.interpolate(Ksat_alpha[:,1],Ksat_alpha[:,2],K_sat);
        D_os:=(D + 2*h_y + 2*h_z);
        D_or:=(D - 2*g);

        motorSYN.p:=p;
        motorSYN.m:=m;
        motorSYN.g:=g;
        motorSYN.RatedPower:=P_mec;
        motorSYN.classe:=-1;

        motorSYN.stator.case_type:=case_type;
        motorSYN.stator.Q_s:=Q_s;
        motorSYN.stator.D:=D;
        motorSYN.stator.I_s:=I_s;
        motorSYN.stator.J_s:=J_s;
        motorSYN.stator.win.a:=a_s;
        motorSYN.stator.win.N_ph:=N_ph_s;
        motorSYN.stator.win.K_w:=K_w;
        motorSYN.stator.win.K_uhc:=K_uhc;
        motorSYN.stator.win.K_u:=K_u_s;
        motorSYN.stator.win.coilspan:=coilspan;
        motorSYN.stator.win.S_cond:=h_y*b_s*K_u_s/z_Q;
        motorSYN.stator.dim.K_c:=K_C;
        motorSYN.stator.dim.D_os:=D_os;
        motorSYN.stator.dim.D_is:=D;
        motorSYN.stator.dim.l:=L_brut;
        motorSYN.stator.dim.X:=X;
        motorSYN.stator.dim.h_z:=h_z;
        motorSYN.stator.dim.b_z:=b_z;
        motorSYN.stator.dim.h_tr:=h_tr;
        motorSYN.stator.dim.b_s:=b_s;
        motorSYN.stator.dim.h_y:=h_y;
        motorSYN.stator.dim.b_1:=b_1;
        motorSYN.stator.dim.delta_htr:=delta_htr;

        B_counter:=0;
        B_counter_limit:=3;
        B_rp_max_real:=B_pole_ini;
        b_rp:=K_F*sigma_p*K_p*D/p*B_g_max_sin_round/B_rp_max_real;
        h_rp:=b_rp/4;

        J_solution_reached:=0;
        J_counter:=1;
        J_counter_limit:=25;
        J_motorLOWER:=0;
        J_motorHIGHER:=999;

        lower_last_attempt:=0;
        higher_last_attempt:=0;

        while (J_solution_reached==0 and J_counter<J_counter_limit) loop
          h_rc:=h_rp*1/sqrt(sigma_arc);
          g_pcf:=g/cos(t_pcf/2)*2;
          h_pcf:=tan(alpha_rph)*(b_p - b_rp)/2;
          b_3:=(D/2 - g_pcf)*sin(t_pcf/2);

          Point1:=[D/2 - g,0];
          Point3:=AuxiliaryFunc.Rotation([D/2 - g_pcf,0], -t_pcf/2);
          Point33:=[Point3[1, 1] - h_pcf,Point3[1, 2]];
          Point2:=[Point3[1, 1] - h_pcf,-b_rp/2];
          h_0:=Point1[1, 1] - Point3[1, 1];
          D_ir_prima:=D - 2*(g + h_0 + h_pcf + h_rp);
          D_ir:=2*sqrt((D_ir_prima/2)^2 + (b_rp/2)^2);
          Point22:=[D_ir_prima/2,-b_rp/2];
          dist_Point1_3:=AuxiliaryFunc.cartdistance(transpose(Point1), transpose(
          Point3));
          beta:=Constants.pi - 2*acos(sqrt(dist_Point1_3^2 - b_3^2)/dist_Point1_3);
          r_ph:=1/(1 - cos(beta))*sqrt(dist_Point1_3^2 - b_3^2);

          b_rc_Distance_candidate1:=tan(t_P/2)*D_ir_prima - abs(Point22[1, 2]);
          b_rc_Distance_candidate2:=abs(Point3[1, 2]) - abs(Point22[1, 2]);
          b_rc_max:=min(b_rc_Distance_candidate1, b_rc_Distance_candidate2);
          Point333L:=[0.5*D - (g + h_0 + h_pcf + h_rc),-(b_rp/2 + b_rc_max)];

          scoil_displacement_x:=0.5*D - (g + h_0 + h_pcf + h_rc);
          scoil_displacement_y:=b_rp/2;
          semi_coil_center:=[0.5*D - (g + h_0 + h_pcf + h_rc/2),0.5*(b_rp +b_rc_max)];

          // Pole limit check
          if Point33[1,2]<-tan(t_P/2)*Point33[1,1] then
            failflag:=1;
            Modelica.Utilities.Streams.print("- Warning: Pole Limit, Surpased by Head Base, pcf too high. [NOT OK]","");
          else
            Modelica.Utilities.Streams.print("-- Pole Limit: Not Surpased by Head Base [OK]","");
          end if;

          // Pole length limit check (may not be applicable by b_ry restriction
           if Point22[1,1]<Point22[1,2]/(-tan(t_P/2)) then
             failflag:=2;
             Modelica.Utilities.Streams.print("- Warning: Pole Length, Not Allocable by Inner Diameter [NOT OK]","");
           else
             Modelica.Utilities.Streams.print("- Pole Length: Allocable [OK]","");
           end if;

           // Coil allocation check
           if abs(Point333L[1,2])>=abs(tan(t_P/2)*Point333L[1,1]) then
             failflag:=3;
             Modelica.Utilities.Streams.print("- Warning: Coil Area, Not Allocable by its Outer Bottom Corner [NOT OK]","");
           else
             Modelica.Utilities.Streams.print("-- Coil Area: Allocable [OK]","");
           end if;

           pole_height:=h_0 + h_pcf + h_rp;

           // Magnetic circuit check

           // Six elements: stator yoke, stator tooth, airgap, rotor head, rotor pole, rotor yoke

           // Yoke flux density max real:
           b_ry:=(D_ir - D_shaft_internal)/2;
           B_ry_max_real:=sigma_p*K_F*K_ry/2*D/(p*b_ry)*B_g_max_sin_round;

           // Path lengths:
           l_st:=h_z;
           l_g:=g*(K_M*K_L*K_C);
           l_rph:=h_0 + h_pcf;
           l_rp:=h_rp;
           l_ry:=b_ry/2 + (D_ir - 2*b_ry)*Constants.pi/(2*p)/2;

           // Field density (real values have to be used)

           H_rph_max_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_g_max_real);
           H_rp_max_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_rp_max_real);
           H_ry_max_real:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_ry_max_real);

           // Heights (solution by FMM)
           FMM_fmm_real_max:=H_sy_max_real*l_sy + H_st_max_real*l_st + H_g_max_real*l_g +
           H_rph_max_real*l_rph + H_rp_max_real*l_rp + H_ry_max_real*l_ry;

           N_ph_rpole:=FMM_fmm_real_max/(I_dc_r);
           N_pc_round_rpole:=AuxiliaryFunc.round(N_ph_rpole);
           FMM_fmm_real_max_round:=N_pc_round_rpole*I_dc_r;
           FMM_rel_error:=(FMM_fmm_real_max_round - FMM_fmm_real_max)/FMM_fmm_real_max;

           // Number of conductors

           // Normalized diameters data
           for ii in 1:1:size(AWGwirings,1)-18 loop    // from the biggest to the smallest section
             if ii==size(AWGwirings,1)-18 then
               Modelica.Utilities.Streams.print("** Warning: Even the smallest wiring cannot fit, using smallest as Good **","");
               Re_s_cond_u:=AWGwirings[ii - 1, 2]*1000;
               Good_wiring.Gauge_st:=ii - 1;
               C_rc:=Good_wiring.C_sc;
               D_cond_r:=Good_wiring.D_cond;
               Gauge_rt:=Good_wiring.Gauge_st;
               K_f_r:=Good_wiring.K_f;
               K_f_r_real:=Good_wiring.K_f_real;
               h_rc_real:=Good_wiring.dim.h_sc_real;
               b_rc_real:=Good_wiring.dim.b_sc_real;
               centers:=Good_wiring.draw.centers;
               Semi_coil_points:=Good_wiring.draw.Semi_coil_points;
               Semi_coil_real_points:=Good_wiring.draw.Semi_coil_real_points;
               gauge:=ii - 1;
               break;
             end if;

             hexa_data:=AuxiliaryFunc.hexapack(h_rc,b_rc_max,AWGwirings[ii,2]/1000);
             quadra_data:=AuxiliaryFunc.quadrapack(h_rc,b_rc_max,AWGwirings[ii,2]/1000);

             if hexa_data.C_sc>quadra_data.C_sc then
               Good_wiring:=hexa_data;
             else
               Good_wiring:=quadra_data;
             end if;

             if Good_wiring.C_sc>=N_pc_round_rpole then
               Re_s_cond_u:=AWGwirings[ii, 2]*1000;
               Good_wiring.Gauge_st:=ii;
               C_rc:=Good_wiring.C_sc;
               D_cond_r:=Good_wiring.D_cond;
               Gauge_rt:=Good_wiring.Gauge_st;
               K_f_r:=Good_wiring.K_f;
               K_f_r_real:=Good_wiring.K_f_real;
               h_rc_real:=Good_wiring.dim.h_sc_real;
               b_rc_real:=Good_wiring.dim.b_sc_real;
               centers:=Good_wiring.draw.centers;
               Semi_coil_points:=Good_wiring.draw.Semi_coil_points;
               Semi_coil_real_points:=Good_wiring.draw.Semi_coil_real_points;
               gauge:=ii;
               break;
             end if;
           end for;

           J_round_real:=N_pc_round_rpole*I_dc_r/(h_rc_real*b_rc_real);

           // Coil resistance
           Resistance:=AuxiliaryFunc.Resistance_SRM(
              L_brut,
              b_rp,
              t_amb,
              Delta_t_work,
              t_aro,
              ro_t_aro,
              Good_wiring);
           R_rc:=Resistance.R_sc;
           l_coil:=Resistance.l_coil;
           t_work:=Resistance.t_work;
           ro_work:=Resistance.ro_work;
           R_rc_t_aro:=Resistance.R_sc_t_aro;

           R_rpole:=R_rc;
           l_extraR:=b_rc_max + D_cond_r/2;
           if l_extraH==0 or l_extraW==0 then
             l_extraS:=1/3*L_brut;
             l_extraH:=1/3*l_extra;
             l_extraW:=l_extra - l_extraH;
           else
             l_extraS:=l_extraH + l_extraW;
           end if;

           if l_extraR<=l_extraS then
             l_extra:=l_extraS;
           else
             l_extra:=l_extraR;
           end if;

           h_case:=floor(D_os*1.02*1000)/1000 + 0.003;

           if (h_case-D_os)<=0.005 then
             h_case:=D_os + 0.005;
           elseif (h_case-D_os)>=0.012 then
             h_case:=D_os + 0.012;
           end if;
           l_case_exp_void:=floor((l_extra/2 + L_brut*0.1)*1000)/1000;
           l_case_exp_front:=floor(((h_case - D_os)/2*1.5)*1000)/1000;
           l_case_exp_rear:=floor((L_brut*0.3)*1000)/1000;
           l_case:=L_brut + 2*l_extra + 2*l_case_exp_void;
           l_total:=l_case + l_case_exp_front + l_case_exp_rear;
           h_total:=h_case;

           if l_shaft_extra==0 then
             l_shaft_extra:=floor(l_case/3*1000)/1000000;
           end if;

           if (l_shaft_ini<>0 and l_shaft_ini>=l_total+l_shaft_extra) then
             l_shaft:=l_shaft_ini;
           elseif l_shaft_ini<l_total+l_shaft_extra then
             l_shaft:=l_total + l_shaft_extra;
             Modelica.Utilities.Streams.print("- Warning: desired l_shaft is too short: Auto assigned a larger one [mm]:","");
           elseif l_shaft_ini==0 then
             l_shaft:=l_total+l_shaft_extra;
           end if;

           // Rotor pole points
           rotorpolepoints:=zeros(200,2);

           // + Superior Pole Arc
           top_superior_angle:=floor(beta*180/Constants.pi);

           for i in 1:1:integer(top_superior_angle+1) loop
             rotorpolepoints_tempA[i,:]:=vector([D_or/2 - r_ph,0] + r_ph*[cos((i - 1)*Constants.pi/180),-sin((i - 1)
            *Constants.pi/180)]);
            l_matrixA:=i;
           end for;

           rotorpolepoints_tempB[1,:]:=vector(Point3);
           rotorpolepoints_tempB[2,:]:=vector(Point33);
           rotorpolepoints_tempB[3,:]:=vector(Point2);
           rotorpolepoints_tempB[4,:]:=vector(Point22);

           // + Bottom pole arc
           initial_bottom_pole_angle:=asin(b_rp/D_ir)/Constants.pi*180;
           initial_for_point:=1;

           for i in 1:1:integer(floor(t_P/2/Constants.pi*180)) loop
             l_matrixC:=i;
             if i+initial_bottom_pole_angle>=floor(t_P/2/Constants.pi*180) then
               l_matrixC:=i - 1;
               break;
             end if;
             rotorpolepoints_tempC[i,:]:=vector(D_ir/2*[cos((i + initial_bottom_pole_angle)*Constants.pi/180),-sin((i +
            initial_bottom_pole_angle)*Constants.pi/180)]);
           end for;
           // Final point (now rotorpolepoints contains all points of half of a pole
           rotorpolepoints_tempD[1,1:2]:=vector(D_ir/2*[cos(t_P/2),-sin(t_P/2)]);
           // Symmetry of one pole, points ordered (now rotorpolepoints contains all points of a pole)

           rotorpolepoints[1:l_matrixA,:]:=rotorpolepoints_tempA[1:l_matrixA,:];
           rotorpolepoints[l_matrixA+1:l_matrixA+4,:]:=rotorpolepoints_tempB[1:4, :];
           rotorpolepoints[l_matrixA+5:l_matrixA+4+l_matrixC,:]:=rotorpolepoints_tempC[1:l_matrixC, :];
           rotorpolepoints[l_matrixA+5+l_matrixC,:]:=rotorpolepoints_tempD[1,:];

           rotorpolepoints_POLE[1:l_matrixA+5+l_matrixC,:]:=[Modelica.Math.Matrices.flipUpDown(rotorpolepoints[1:l_matrixA+5+l_matrixC,:])];
           rotorpolepoints_POLE[l_matrixA+6+l_matrixC:2*(l_matrixA+5+l_matrixC),:]:=Modelica.Math.Matrices.flipUpDown(AuxiliaryFunc.Reflection(rotorpolepoints[1:l_matrixA+5+l_matrixC,:], 0));

           // Auxiliary variable is used to define the whole external rotor contour
           externalrotorpoints[1:2*(l_matrixA+5+l_matrixC),:]:=rotorpolepoints_POLE[1:2*(l_matrixA+5+l_matrixC),:];

           l_for:=2*(l_matrixA + 5 + l_matrixC);

            for i in 1:1:Q_r-1 loop
              Modelica.Utilities.Streams.print("i: "+Modelica.Math.Vectors.toString((vector(i))),"");
              externalrotorpoints[(i*l_for+1):(i*l_for+l_for),:]:=[Modelica.Math.Matrices.flipUpDown(AuxiliaryFunc.Rotation(rotorpolepoints_POLE[1:l_for,:], i*2*Constants.pi/Q_r))];
            end for;
           //Modelica.Utilities.Streams.print("externalrotorpoints: "+Modelica.Math.Matrices.toString((matrix(externalrotorpoints[:,1]))),"");
           //Modelica.Utilities.Streams.print("externalrotorpoints: "+Modelica.Math.Matrices.toString((matrix(externalrotorpoints[:,2]))),"");
           //Modelica.Utilities.Streams.print(Modelica.Math.Vectors.toString((vector(externalrotorpoints[:,1]))),"kurwa.txt");

           //Modelica.Utilities.Streams.print("Modelica.Math.Vectors.toString((vector(l_matrixA))),"");

           // Fail element points
           if failflag==1 then // Pole limit
             motorSYN.rotor.draw.fail_elements:=1;
             motorSYN.rotor.draw.fail.fail_element1:=[Point3; Point33; Point2]*1000;
           elseif failflag==2 then // Diameter limit
             motorSYN.rotor.draw.fail_elements:=1;
             motorSYN.rotor.draw.fail.fail_element1:=[Point2; Point22]*1000;
           elseif failflag==3 then  // Coil area limit
             motorSYN.rotor.draw.fail_elements:=2;
             semi_coil_points:=[-h_rc/2,-b_rc_max/2; h_rc/2,-b_rc_max/2; h_rc/2,b_rc_max/2; -h_rc/2,b_rc_max/2; -h_rc/2,-b_rc_max/2];

             semi_coil_points_R[:,1]:=semi_coil_points[:, 1].+semi_coil_center[1,1];
             semi_coil_points_R[:,2]:=semi_coil_points[:, 2].+ semi_coil_center[1, 2];

             motorSYN.rotor.draw.fail.fail_element1:=semi_coil_points_R*1000;
             motorSYN.rotor.draw.fail.fail_element2:=[0,0; D/2*cos(t_P/2),D/2*sin(t_P/2)]*1000;
           else // No problems
             motorSYN.rotor.draw.fail_elements:=0;
             motorSYN.rotor.draw.fail.fail_element1[1,:]:={99999,99999};
           end if;

           // Efficiency
           if generator==0 then
             P_elec_pole:=I_dc_r^2*R_rpole;
             P_elec_t:=P_e + 2*p*P_elec_pole;
           elseif generator==1 then
             P_elec_pole:=I_dc_r^2*R_rpole;
             P_elec_t:=P_e;
             eff_real:=P_elec_t/(P_mec + 2*p*P_elec_pole);
           end if;

           // STEP 5: Synchronous Structure
           motorSYN.m:=m;
           motorSYN.p:=p;
           motorSYN.g:=g;
           motorSYN.RatedPower:=P_mec;
           motorSYN.classe:=7;

           // Function Flags
           motorSYN.flags.R_fe_consider:=R_fe_consider;

           // Plate
           motorSYN.plate.RatedPower:=P_mec;
           motorSYN.plate.RatedCurrent:=I_s;
           motorSYN.plate.RatedVoltage:=U_ph_s;
           motorSYN.plate.RatedVoltageType:="Phase";
           motorSYN.plate.BEMF:=U_ph_s;
           motorSYN.plate.RatedCurrentInductor:=I_dc_r;
           motorSYN.plate.RatedVoltageInductor:=I_dc_r*R_rpole;
           motorSYN.plate.RatedSpeedRPM:=n_rpm;
           motorSYN.plate.Torque:=T;
           motorSYN.plate.PowerFactor:=PF;
           motorSYN.plate.Efficiency:=eff;
           motorSYN.plate.M:=0;
           motorSYN.plate.TmM:=0;

           // Specifications
           motorSYN.specs.C_mec:=C_mec;
           motorSYN.specs.TargetFluxDensities.B_g_ini:=B_g_ini;
           motorSYN.specs.TargetFluxDensities.B_yoke_ini:=B_yoke_ini;
           motorSYN.specs.TargetFluxDensities.B_tooth_ini:=B_tooth_ini;
           motorSYN.specs.TargetFluxDensities.B_pole_ini:=B_pole_ini;
           motorSYN.specs.TargetFluxDensities.B_ryoke_limit:=B_ryoke_limit;
           motorSYN.specs.J_counter:=J_counter;

           motorSYN.airgap.K_F:=K_F;
           motorSYN.airgap.K_M:=K_M;
           motorSYN.airgap.B_g_max_sin_round:=B_g_max_sin_round;
           motorSYN.airgap.B_g_max_real_round:=B_g_max_sin_round*(K_M*K_L*K_C);

           // Stator
           motorSYN.stator.case_type:=1;
           motorSYN.stator.Q_s:=Q_s;
           motorSYN.stator.D:=D;
           motorSYN.stator.I_s:=I_s;
           motorSYN.stator.J_s:=J_s;
           motorSYN.stator.a:=a_s;

           motorSYN.stator.J_s_desired:=J_s;
           motorSYN.stator.J_s_real:=J_s;

           motorSYN.stator.dim.D_os:=D_os;
           motorSYN.stator.dim.D_is:=D;
           motorSYN.stator.dim.X:=X;
           motorSYN.stator.dim.h_z:=h_z;
           motorSYN.stator.dim.b_z:=b_z;
           motorSYN.stator.dim.h_tr:=h_tr;
           motorSYN.stator.dim.delta_htr:=delta_htr;
           motorSYN.stator.dim.b_s:=b_s;
           motorSYN.stator.dim.h_y:=h_y;
           motorSYN.stator.dim.b_1:=b_1;
           motorSYN.stator.dim.l:=L_brut;
           motorSYN.stator.dim.K_c:=K_C;
           motorSYN.stator.dim.K_fe:=K_fe;
           motorSYN.stator.dim.K_u:=K_u_s;
           motorSYN.stator.dim.f_r_s:=f_r_s;
           motorSYN.stator.cool.n_cs:=n_cs;
           motorSYN.stator.cool.e_cs_t:=e_cs_t;

           // Stator winding
           motorSYN.stator.win.a:=a_s;
           motorSYN.stator.win.N_ph:=N_ph_s;
           motorSYN.stator.win.K_w:=K_w;

           motorSYN.stator.win.K_w:=K_w;
           motorSYN.stator.win.K_uhc:=K_uhc;
           motorSYN.stator.win.coilspan:=coilspan;
           motorSYN.stator.win.z_Q:=z_Q;
           motorSYN.stator.win.S_cond:=h_z*b_s*K_u_s/z_Q;
           motorSYN.stator.win.dim.D_cond_s:=99999;

           // Rotor
           motorSYN.rotor.shape:=70;
           motorSYN.rotor.fail:=failflag;
           motorSYN.rotor.failpath:=0;
           motorSYN.rotor.msg:="None";
           motorSYN.rotor.I_cd_r:=I_dc_r;
           motorSYN.rotor.J_r:=J_round_real;
           motorSYN.rotor.a_r:=1;
           motorSYN.rotor.specs.sigma_p:=sigma_p;
           motorSYN.rotor.specs.sigma_arc:=sigma_arc;

           motorSYN.rotor.dim.D_or:=D_or;
           motorSYN.rotor.dim.D_ir:=D_ir;
           motorSYN.rotor.dim.D_shaft:=D_shaft;
           motorSYN.rotor.dim.D_shaft_internal:=D_shaft_internal;
           motorSYN.rotor.dim.D_hub:=D_ir;
           motorSYN.rotor.dim.b_rp:=b_rp;
           motorSYN.rotor.dim.b_ry:=b_ry;
           motorSYN.rotor.dim.h_rp:=h_rp;
           motorSYN.rotor.dim.pcf:=pcf;
           motorSYN.rotor.dim.K_p:=K_p;
           motorSYN.rotor.dim.K_ry:=K_ry;
           motorSYN.rotor.dim.K_u:=K_f_r;
           motorSYN.rotor.dim.f_r:=f_r_r;
           motorSYN.rotor.cool.n_cs:=n_cs;
           motorSYN.rotor.cool.e_cs_t:=e_cs_t;
           motorSYN.rotor.dim.pole_height:=pole_height;

           // Rotor winding
           motorSYN.rotor.win.N_ph:=N_pc_round_rpole;
           motorSYN.rotor.win.K_f_r:=K_f_r;
           motorSYN.rotor.win.K_f_r_real:=K_f_r_real;
           motorSYN.rotor.win.dim.b_rc:=b_rc_max;
           motorSYN.rotor.win.dim.h_rc:=h_rc;
           motorSYN.rotor.win.dim.D_cond_r:=D_cond_r;
           motorSYN.rotor.phy.R_rc_t_aro:=R_rc_t_aro;
           motorSYN.rotor.phy.t_work:=t_work;
           motorSYN.rotor.phy.R_ph:=R_rpole*2*p;

           motorSYN.rotor.win.draw.Semi_coil_points:=Semi_coil_points;
           motorSYN.rotor.win.draw.Semi_coil_real_points:=Semi_coil_real_points;
           motorSYN.rotor.win.draw.Semi_coil_centers:=centers;
           motorSYN.rotor.win.draw.scoil_displacement_x:=scoil_displacement_x;
           motorSYN.rotor.win.draw.scoil_displacement_y:=scoil_displacement_y;

           // Extra Geometry
           motorSYN.rotor.geo.externalrotorpoints:=externalrotorpoints;
           motorSYN.rotor.geo.rotorpolepointsR:=rotorpolepointsR;
           motorSYN.rotor.geo.rotorpolepoints:=rotorpolepoints;
           motorSYN.rotor.geo.beta:=beta;
           motorSYN.rotor.geo.r_ph:=r_ph;
           motorSYN.rotor.geo.g_pcf:=g_pcf;
           motorSYN.rotor.geo.t_pcf:=t_pcf;
           motorSYN.rotor.geo.h_0:=h_0;
           motorSYN.rotor.geo.h_pcf:=h_pcf;
           motorSYN.rotor.geo.h_rp:=h_rp;
           motorSYN.rotor.geo.semi_coil_center:=semi_coil_center;

           // Rotor Fail Elements
           //motorSYN.rotor.draw.fail:=motorSYN.rotor.draw.fail;
           motorSYN.rotor.draw.fail_elements:=fail_elements;

           // Housing
           motorSYN.casing.dim.l_case_exp_front:=l_case_exp_front;
           motorSYN.casing.dim.l_case_exp_rear:=l_case_exp_rear;
           motorSYN.casing.dim.l_case_exp_void:=l_case_exp_void;
           motorSYN.casing.dim.l_case:=l_case;
           motorSYN.casing.dim.h_case:=h_case;

           motorSYN.rotor.dim.l_shaft_extra:=l_shaft_extra;
           motorSYN.rotor.dim.l_shaft:=l_shaft;
           motorSYN.casing.l_total:=l_total;
           motorSYN.casing.h_total:=h_total;

           motorSYN.stator.win.dim.l_extra:=l_extra;
           motorSYN.stator.win.dim.l_extraH:=l_extraH;
           motorSYN.stator.win.dim.l_extraW:=l_extraW;

           motorSYN.casing.dim.phy.Delta_t_work:=Delta_t_work;
           motorSYN.casing.dim.phy.t_amb:=t_amb;
           motorSYN.casing.dim.phy.t_aro:=t_aro;
           motorSYN.stator.win.phy.ro_t_aro:=ro_t_aro;

           // Densities (of all parts)
           motorSYN.casing.dim.phy.ro_case:=ro_case;
           motorSYN.stator.dim.phy.ro_mst:=ro_mst;
           motorSYN.stator.dim.phy.ro_cu:=ro_cu;
           motorSYN.rotor.dim.phy.ro_mst:=ro_mst;
           motorSYN.rotor.dim.phy.ro_cu:=ro_cu;
           motorSYN.rotor.dim.phy.ro_shaft:=ro_shaft;

           // Stator pole increase/decrease

           // Case below current density target
           if J_round_real<J_r then
             motorLOWER:=motorSYN; // whole machine saved to compare later
             J_motorLOWER:=J_round_real;

             // Stator pole reduction, at two rates
             if J_round_real*2.1<J_r then// far from the solution
               h_rp_temp:=AuxiliaryFunc.round(h_rp*1000*0.75)/1000;
             else
               h_rp_temp:=AuxiliaryFunc.round(h_rp*1000*0.99)/1000;
             end if;
             if h_rp_temp==h_rp then
               h_rp:=h_rp - 0.001;
               lower_last_attempt:=1;
             else
               h_rp:=h_rp_temp;
               lower_last_attempt:=0;
             end if;
           end if;

           // Case above current density target
           if J_round_real>J_r then
             motorHIGHER:=motorSYN;
             J_motorHIGHER:=J_round_real;
             if J_round_real>2.1*J_r then
               h_rp_temp:=AuxiliaryFunc.round(h_rp*1000/0.75)/1000;
             else
               h_rp_temp:=AuxiliaryFunc.round(h_rp*1000*1.01)/1000;
             end if;
             if h_rp_temp==h_rp then
               h_rp:=h_rp + 0.001;
               higher_last_attempt:=1;
             else
               h_rp:=h_rp_temp;
               higher_last_attempt:=0;
             end if;
           end if;

           // STEP 6: Stop Condition

           // Extra stop conditions
           // Sixth condition: flux density at rotor's yoke must not surpass target value

           if failflag<>0 then
             motorSYN.rotor.msg:="*** ERROR: Error in Geometry, check Plot Sections, last generated motor accepted as GOOD";
             Modelica.Utilities.Streams.print(motorSYN.rotor.msg,"");
             break;
           end if;
           if B_ryoke_limit<=B_ry_max_real then
             motorSYN.rotor.msg:="*** ERROR: Exceed Flux Density allowed for Rotor Yoke, last generated motor accepted as GOOD";
             Modelica.Utilities.Streams.print(motorSYN.rotor.msg,"");
             motorSYN_Good:=motorSYN;
             break;
           end if;

           // Basic stop conditions
           // First condition: at least iterated twice
           // Second condition: closest machine to the desired current density
           // Third condition: difference lower than allowed_diff
           // Fourth condition: iteration limit reached

           if J_counter>=3 or J_counter==J_counter_limit-1 then
             lower_diff:=abs(J_motorLOWER - J_r)/J_r;
             higher_diff:=abs(J_motorHIGHER - J_r)/J_r;
             if J_counter==J_counter_limit-1 then
               Modelica.Utilities.Streams.print("*** ERROR: Maximum number of iterations reached, selected the best available solution as GOOD","");
               motorSYN_Good:=motorSYN;
             end if;

             // If both solutions have an error lower the allowed differenc, take the smallest
             if lower_diff<Allowed_diff and higher_diff<Allowed_diff then
               if lower_diff<higher_diff then
                 motorSYN_Good:=motorLOWER;
                 J_solution_reached:=1;
               else
                 motorSYN_Good:=motorHIGHER;
                 J_solution_reached:=1;
               end if;
             // If only the lower approximation meets the Allowed_difference
             elseif lower_diff<Allowed_diff then
               motorSYN_Good:=motorLOWER;
               J_solution_reached:=1;
             // If only the upper approximation meets the Allowed_difference
             elseif higher_diff<Allowed_diff then
               motorSYN_Good:=motorHIGHER;
               J_solution_reached:=1;
             // If none of them have met the Allowed_difference, but have reached the +/- region
             elseif lower_last_attempt*higher_last_attempt==1 then
               if lower_diff<higher_diff then
                 motorSYN_Good:=motorLOWER;
                 J_solution_reached:=1;
               else
                 motorSYN_Good:=motorHIGHER;
                 J_solution_reached:=1;
               end if; // of lower difference

             end if;
           end if;  // if at least 3 iterations have been performed
           J_counter:=J_counter + 1;
        end while;

        // Stator phase resistance
        motorSYN_Good:=AuxiliaryFunc.Resistance_SYN(motorSYN_Good);

        // STEP 7: Masses and moments of inertia
        motorSYN_Good:=AuxiliaryFunc.MassandMOISYN(motorSYN_Good);
        SYN_Structure.SYN_Export(motorSYN_Good);

    annotation (
        Documentation(info="<html>

<head>
<title>General Synchronous Pre-sizing help files</title>
<style>

</style>

</head>

<body lang=ES>

<div class=WordSection1>

<h2><a name='_Toc414020152'><span lang=EN-US>1.1<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Table of Inputs</span></a></h2>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=161 valign=top style='width:120.8pt;border:solid windowtext 1.0pt;
  background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Name</span></b></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Description</span></b></p>
  </td>
  <td width=68 valign=top style='width:50.95pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US>Units</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>T</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Torque</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Nm</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>n_rpm</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Speed in RPM</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>rpm</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>U_ph_s</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Phase Voltage</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>V<sub>RMS</sub></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>eff</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Efficiency</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>PF</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Power Factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>C_mec</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Mechanical Constant</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>kW/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>X</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Form Factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>g</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Airgap Length</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Q_s</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Stator Slots</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>m</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Phases</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>I_s</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Stator Current</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>I<sub>RMS</sub></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>J_s</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Stator Current Density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A/m<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>a_s</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Stator Parallel Paths</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>I_dc_r</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Rated Current</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>sigma_p</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Polar leakage Coefficient</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>sigma_arc</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Coil Area Security Factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>J_r</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rated Rotor Current Density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>A/m<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>p</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Pole Pairs</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>pcf</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Pole Cover Factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_g_ini</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Airgap MAX Real flux density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_yoke_ini</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Yoke MAX real flux Density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_tooth_ini</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Tooth MAX  real Flux density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_pole_ini</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Pole MAX Real flux density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>B_ryoke_limit</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Yoke MAX real flux density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>f_r_r</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Lamination factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_u_r</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor slot filling factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>b_1</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Slot Opening</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>gamma</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Skewing Angle</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>degree</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_uhc</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Head Coil filling factor</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>alpha_rph</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Pole head Line Angle</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>degree</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>D_shaft</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>External Shaft Diameter</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>D_shaft_internal</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Internal Shaft Diameter</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_shaft_ini</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft length if desired (total)</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_shaft_extra</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>External Shaft Extra Length (from housing)</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_mst</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Magnetic Steel density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_cu</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Winding Copper Density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_case</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Metal cabinet density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_shaft</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Shaft Steel density</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>kg/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>n_cs</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Stator Ventilation Ducts</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>e_cs_t</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Wide/Type of Cooling Ducts 1 for 
  10mm, 2 for 15mm</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>n_cr</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Number of Rotor Ventilation Ducts</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>e_cr_t</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Rotor Wide/Type of Cooling Ducts 1 for 
  10mm, 2 for 15mm</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_aro</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Reference Resistivity Temperature</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ro_t_aro</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Reference Resistivity</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>&#8486;·m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_amb</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Ambient Temperature</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Delta_t_work</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Temperature Increase over ambient</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>°C</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_extraH</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Head Coil extra length</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>l_extraW</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Stator Head Coil end winding extra length</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>case_type</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>Type
  of housing shape</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>generator</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>generator/motor
  flag consideration</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>PowerDshaft.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>IEC60034 Machine Power vs. Shaft Diameter</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>AWGwirings.txt                  </span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>AWG gauges and properties table</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_M_seta.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>K_M per pole cover factor curve</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_F_seta.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>K_F per pole cover factor curve</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ecd_10.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>Equivalent length by airgap length curve for 10mm</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>ecd_15.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>Equivalent length by airgap length curve for 15mm</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Rotor_MagSteel.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>B-H curve of the magnetic steel</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Stator_MagSteel.txt</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>B-H curve of the magnetic steel</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>n/a</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>K_E_U_ratio</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>Phase Voltage to Back EMF ratio</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=161 valign=bottom style='width:120.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Internal_shaft_coeficient</span></p>
  </td>
  <td width=282 valign=bottom style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Calibri','sans-serif';
  color:black'>External to Internal Shaft Ratio</span></p>
  </td>
  <td width=68 valign=bottom style='width:50.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span style='font-family:'Calibri','sans-serif';color:black'>-</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

<h2><span lang=EN-US>1.2<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Algorithm concept</span></h2>

<p class=MsoNormal><span lang=EN-US>Algorithm is based on the generation of
both magnetic parts (stator and rotor) each one able to provide the desired
airgap flux density (</span><span class=CdigoenWordCar><span lang=EN-US
style='font-family:'Calibri','sans-serif''>B_g_ini</span></span><span
lang=EN-US>). Stator will fulfill that condition by creating a slot able to
contain the required turns, taking into account apparent and real flux
densities in its parts and the different shape flux density takes along stator
path. Rotor is based on a similar principle. With an iterative process which
expands pole height (by means of </span><span class=CdigoenWordCar><span
lang=EN-US style='font-family:'Calibri','sans-serif''>h_rp</span></span><span
lang=EN-US> increases) solution is reach when its shape is able to fulfill the </span><span
class=CdigoenWordCar><span lang=EN-US style='font-family:'Calibri','sans-serif''>B_g_ini</span></span><span
lang=EN-US> condition and contain the necessary turns per pole at desired
current density (</span><span class=CdigoenWordCar><span lang=EN-US
style='font-family:'Calibri','sans-serif''>J_r</span></span><span lang=EN-GB>)</span><span
lang=EN-GB>, as it was done in SRM with stator dimensioning.</span></p>

<h2><a name='_Toc414020153'><span lang=EN-US>1.3<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Program Structure Explanation</span></a></h2>

<p class=MsoNormal><span lang=EN-US>Use </span><span
lang=EN-GB>Figure 1</span><span lang=EN-US> to follow basic sizing explanation.
After inputs have been made, first, if a variable has not been introduced it is
estimated and, second, when this minimal information has been generated other
associated variables are obtained: ducts correction and lengths and form
coefficients. </span></p>

<p class=MsoNormal><span lang=EN-US>After that pre-calculation stage is done
stator dimensioning process starts. By means of Back EMF and airgap flux
density slot size is determined and, around it, the whole stator. A new airgap
flux (</span><span class=CdigoenWordCar><span lang=EN-US style='font-family:
'Calibri','sans-serif''>B_g_max_sin_round</span></span><span lang=EN-US>) is
generated based on the round number of conductors able to fit slot. </span></p>

<p class=MsoNormal><span lang=EN-US>Then the rotor dimensioning process starts.
This process consists of a loop based on rotors current density very similar to
the one performed for SRM. Based on a seed value of pole height (</span><span
class=CdigoenWordCar><span lang=EN-US style='font-family:'Calibri','sans-serif''>h_rp</span></span><span
lang=EN-US>) all rotor dimensions are calculated and among them its possible
maximum viable coil size. Three fail geometry conditions are evaluated and a
flag variable (</span><span class=CdigoenWordCar><span lang=EN-US
style='font-family:'Calibri','sans-serif''>rotor.fail</span></span><span
lang=EN-US>) is raised if an error occurs. When this has been done, and based
on a leakage factor (</span><span class=CdigoenWordCar><span lang=EN-US
style='font-family:'Calibri','sans-serif''>sigma_p</span></span><span
lang=EN-US>) and the aforesaid new airgap flux density, the magnetic circuit is
evaluated and the number of required turns obtained. This number of turns is
introduced in a second inner loop where current density is evaluated. This
current density must comply with the desired value for rotor (<b>J_r</b></span><span
lang=EN-GB>)</span><span lang=EN-US> within an allowed threshold. All the
machine information is saved, including, in case of a geometrical error the
fail information to be shown in the graphical representations). Then if the
current density can’t be fitted a new higher or lower value for </span><span
class=CdigoenWordCar><span lang=EN-US style='font-family:'Calibri','sans-serif''>h_rp</span></span><span
lang=EN-US> is generated. After this new value for the pole height is generated
the program decides if a better solution can be found depending on several
factors: if the coil is viable, if the threshold for flux density in yoke has
been reached, and if the iterations are leading to a dead end. Based on those
the best solution among the generated is selected as good (</span><span
class=CdigoenWordCar><span lang=EN-US style='font-family:'Calibri','sans-serif''>motorSYN_Good</span></span><span
lang=EN-US> structure).</span></p>

<p class=MsoNormal><span lang=EN-US>By means of this selected motor then, the
subsequent resistance and moments of inertia and masses functions are applied
in order to obtain the final pre-design data which is saved inside the
structure and pre-sizing ends. </span></p>

<p class=MsoNormal><span lang=EN-US>Note that, no matter if the solution
reached is within the parameters a machine is generated, this is to allow the
designer either by the very data or by console messages evaluate what gone
wrong. </span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=604 height=854
src='General_Synchronous_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019945'></a><a name='_Ref411871569'><span
lang=EN-GB>Figure </span></a><span lang=EN-GB>1</span><span lang=EN-GB>. Global
Sizing Algorithm explanation.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end SYN_Motor_Sizing;

    function SMPMSM_Motor_Sizing "new_SMPMSM_12_FINAL"
      import Modelica.Constants;
      import Modelica.SIunits;
      parameter input Modelica.SIunits.Voltage U_ph_ref=346/sqrt(3)
        "Rated phase voltage"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real K_E_U_ratio=0.99 "Per phase E/U modulus ratio"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer m=3 "Number of phases"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Modelica.SIunits.Torque T=2.3 "Rated torque"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Modelica.SIunits.Current I_s=2.9 "Rated stator current"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer a=1 "Parallel paths"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Real PF=0.98986 "Power factor"
      annotation (Dialog(group="Nominal parameters"));
      parameter input
        Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm               n_rpm=6000
        "Rated Speed"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real C_mec_ini=72.76 "Desired mechanical constant"
      annotation (Dialog(enable=C_mec_ini == true, group="Mechanical constant"));
      parameter input Boolean C_mec_family_flag=false "C_mec family flag"
      annotation (Dialog(group="Mechanical constant",__Dymola_compact=true, __Dymola_descriptionLabel=true),choices(__Dymola_checkBox=true));

      parameter input Modelica.SIunits.CurrentDensity J_s=3.576*10^6
        "Current density"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Integer p=3 "Number of pole pairs"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_g_ini=0.738
        "Maximum airgap flux density"
      annotation (Dialog(tab="Airgap"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_yoke_ini=1.217
        "Maximum stator yoke flux density initial value"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_tooth_ini=1.792
        "Maximum stator tooth flux density initial value"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input Integer Bg_type=0
        "Type of magnetization; 0-Squarewave // Radial magnetization, enables rotor skew; 1-Sinewave // Parallel magnetization, disables rotor skew"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));

      parameter input Real X=1.923 "Aspect ratio"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input Modelica.SIunits.Length g_ref=0.0007
        "Desired air gap length"
      annotation (Dialog(tab="Airgap"));
      parameter input Integer Q_s=18 "Number of stator slots"
      annotation (Dialog(tab="Stator", group="Geometry"));
      parameter input SIunits.Length b_1=0.0022 "Stator slot opening"
      annotation (Dialog(tab="Stator", group="Geometry"));
      parameter input Integer n_c=0 "Stator number of cooling ducts"
      annotation (Dialog(tab="Stator", group="Ventilation"));
      parameter input Integer e_c=0 "Stator wide/type of cooling ducts"
      annotation (Dialog(tab="Stator", group="Ventilation"));
      parameter input Real f_r_s(min=0.9, max=0.99)=0.98
        "Stator lamination factor"
      annotation (Dialog(tab="Stator", group="Winding"), Evaluate=true);

      parameter input Real K_u=0.47 "Stator slot filling factor"
      annotation (Dialog(tab="Stator", group="Winding"));

      parameter Real K_uhc=0.8 "Stator head coil filling factor";
      parameter input Modelica.SIunits.Conversions.NonSIunits.Angle_deg gamma=20
        "Skewing angle"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input String Stator_Material_File="STEEL_ABB8Cr_EXT.txt"
        "Stator Material B(H) characteristic"
      annotation(Dialog(tab="Stator",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Stator Material B(H) characteristic file")));
      parameter input Real mcf_PM=0.835 "Magnet cover factor"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input Modelica.SIunits.Length l_t=5e-4
        "Thickness of insulation layer between PMs and rotor"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input Integer hole_rotor=1
        "Rotor with or without holes, 0-No holes, 1-Holes in rotor"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input SIunits.MagneticFluxDensity B_r_yoke_ini=1.954
        "Rotor yoke max flux density"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input Real hcf_ref=0 "Hole cover factor"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input Real f_r_r(min=0.9, max=0.99)=0.98
        "Rotor lamination factor"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input Integer R_fe_consider_ref=0
        "1-consider R_fe effect, 0-do not consider R_fe efffect"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input Real K_h=50 "Magnetic steel hysteresis constant"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input Real K_e=0.05 "Magnetic steel eddy constant"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input Real n_i=1.6 "Steinmetz Constant"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input Integer magnet_no=7
        "Magnet number from database PMDB1.txt"
      annotation (Dialog(tab="Rotor", group="Magnetic"));
      parameter input SIunits.Length D_shaft_ref=18/1000
        "Shaft external diameter"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input SIunits.Length D_shaft_internal_ref=20/1000
        "Shaft internal diameter"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input Real Internal_shaft_coefficient=1.3
        "Ratio between external and internal shafts"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input SIunits.Length l_shaft_ini=0 "Shaft length"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input SIunits.Length l_shaft_extra_ref=50/1000
        "Shaft external length"
      annotation (Dialog(tab="Rotor", group="Geometry"));
      parameter input Integer case_type=1 "1-Circular, 2-Square solid"
      annotation (Dialog(tab="Rotor"));

      parameter input SIunits.Density ro_mst=7460 "Magnetic steel density"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Density ro_cu=8750 "Cu density"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Density ro_case=7850 "Metal cabinet density"
      annotation (Dialog(tab="Rotor"));
      parameter input SIunits.Density ro_shaft=7460 "Shaft steel density"
      annotation (Dialog(tab="Rotor"));
      parameter input SIunits.Density ro_PM=7400 "Magnet density"
      annotation (Dialog(tab="Rotor"));

      parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_aro=20
        "Reference resistivity temperature"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Resistivity ro_t_aro(min=0)=1.68e-8
        "Cu Resistivity"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_amb(min=0)=30
        "Ambient temperature"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC Delta_t_work(min=0)=55
        "Temperature increase relative to ambient"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Length l_extraH_ref=0.008
        "Stator head coil longitudinal extra length"
      annotation (Dialog(tab="Stator"));
      parameter input SIunits.Length l_extraW_ref=0.020
        "Stator head coil winding longitudinal length"
      annotation (Dialog(tab="Stator"));

    protected
      Integer sizing_iteration_limit=50
        "Limits the number of iterations for sizing";
      Integer magnetic_circuit_limit=100
        "Limits the number of magnetic circuit iterations";
      Integer stator_iteration_limit=10
        "Limits the number of stator iterations for sizing";
      Real Flux_slot_loss_thresshold=0.005
        "Allowed relative error of flux slot loos p.u.";
      Real h_PM_thresshold=0.005 "Allowed relative error of h_PM p.u.";
      Real h_PM_main=0.001 "Magnet height seed value";
      SIunits.MagneticFlux Flux_slot_loss_t_p_main=0.001
        "Seed value for stator losses";
      parameter Real[:] Mat_B_s=(DataFiles.readCSVmatrix(Stator_Material_File))[:,1]
        "B field for interpolation";
      parameter Real[:] Mat_H_s=(DataFiles.readCSVmatrix(Stator_Material_File))[:,2]
        "H field for interpolation";
      Real[3] Mat_pm "Permanent magnet properties";
      SIunits.MagneticFluxDensity B_r "Remanence magnetization of PM";
      SIunits.MagneticFieldStrength H_c "Coercive field of PM";
      SIunits.RelativePermeability mu_r_PM "Relative permeability of PM";
      SIunits.Permeability mu_air=Constants.mue_0*1.00000037
        "Permeability of air";
      SIunits.Angle xi=0
        "Angle betweeen current density and flux density at airgap";
      Modelica.SIunits.Conversions.NonSIunits.Angle_deg delta=0
        "Torque/current angle at rated conditions (default 0 for SPMSM)";
      Integer ReducedInputs_parameters=0 "0-Motor, 1-Generator mode";
      AuxiliaryFunc.ReducedInputsr ElectricalData "Reduced inputs";
      SIunits.Power P_mec "Mechanical power";
      SIunits.Voltage E_ph "Back EMF at rated conditions";
      SIunits.Power P_elec "Electrical power";
      Real eff "Efficiency";
      SIunits.Frequency n_Hz "Mechanical rated spped in Hz";
      SIunits.Frequency f "Electrical frequency";
      Real C_mec "Mechanical constant";
      Real hcf=hcf_ref "Hole cover factor";
      Integer R_fe_consider=R_fe_consider_ref
        "1-consider R_fe effect, 0-do not consider R_fe efffect";
      SIunits.Length delta_htr "Height of teeth expansion";
      SIunits.Length delta_bz_3 "Extra tooth width";
      Real q "Slot per pole per phase";
      Integer coilspan "Coilspan";
      Real K_w "Winding factor";
      SIunits.Length D_shaft=D_shaft_ref "Shaft external diameter";
      SIunits.Length D_shaft_internal=D_shaft_internal_ref
        "Shaft internal diameter";
      SIunits.Length D_g "Bore diameter";
      SIunits.Length D_or "Diameter of outer rotor";
      SIunits.Length D_is "Diameter of inner stator";
      SIunits.Length l_eff "Effective length";
      SIunits.Length l_r "Rotor mensurable length";
      SIunits.Length l_s "Stator mensurable length";
      SIunits.Length l_r_mag "Rotor magnetic length";
      SIunits.Length l_s_mag "Stator magnetic length";
      SIunits.Length g=g_ref "Air gap length";
      Integer Q_r "Number of poles";
      SIunits.Angle t_P "Polar pitch";
      SIunits.Angle t_S "Slot pitch";
      SIunits.Length t_p "Polar pitch length";
      SIunits.Length t_s "Slot pitch";
      SIunits.Length l_PM_ini "Magnet length";
      Real Cover_ini "Magnetic flux density wave cover";
      Real k_f "Magnetic flux density wave form factor";
      SIunits.MagneticFlux Flux_g_ini_t_p "Total flux per pole at airgap";
      SIunits.MagneticFluxDensity B_g_max_sin
        "First harmonic of the Fourier series of the airgap flux density";
      Real C_3 "Machine constant";
      Real A_load "Electrical loading";
      Real K_E_U_ratio2 "Updated Per phase E/U modulus ratio";
      SIunits.MagneticFlux Flux_slot_loss_t_p;
      SIunits.MagneticFlux Flux_mag_ini_t_p
        "Initial number of turns based on desired magnetizing flux {Pyrhonen}";
      Real N_ph1_Corr "Number of turns per phase";
      Real N_ph_round "Rounded number of turns per phase";
      Real z_Q "Number of conductors per slot";
      SIunits.MagneticFlux Flux_mag_t_p;
      SIunits.MagneticFlux Flux_mag_t_s "Magnetizing flux per stator pitch";
      SIunits.Length b_z "Tooth width {Pyrhonen}";
      SIunits.Area A_slot "Slot area";
      SIunits.Area S_cond "Conductor area";
      SIunits.Length D_cond "Conductor diameter";
      AuxiliaryFunc.tooth_dimr tooth_3 "Tooth geometrical information";
      SIunits.Length b_sB "Slot width at bottom (arc)";
      SIunits.Length b_sT "Slot width at top (arc)";
      SIunits.Length b_zT "Tooth width at top (arc)";
      SIunits.Length D_ms "Stator medium diameter";
      SIunits.Length b_zB;
      Real d1_prop "d1 measure proportion to d3";
      Real d2_prop "d2 measure proportion to d3";
      SIunits.Length d1 "Tooth square shoe height";
      SIunits.Length d2 "Tooth trapezoid shoe height";
      SIunits.Length d3 "d3 tooth dimension (seed value)";
      Real A_cu_rel_error
        "Slot copper area relative eroor of the best solution";
      SIunits.Length h_y "Stator yoke height";
      SIunits.Length D_os "Diameter of outer stator";
      Real K_C "Carter factor";
      SIunits.Length g_eff "Effective airgap";
      Modelica.SIunits.MagneticFieldStrength H_yoke
        "Yoke magnetic field strength";
      Modelica.SIunits.RelativePermeability mu_r_yoke
        "Yoke relative permeability";
      Modelica.SIunits.MagneticFieldStrength H_tooth
        "Tooth magnetic field strength";
      Modelica.SIunits.RelativePermeability mu_r_tooth
        "Tooth relative permeability";
      SIunits.Reluctance R_loss_stator_slot "One tooth-tooth leak reluctance";
      SIunits.Reluctance R_tooth "One tooth (t_S) reluctance";
      SIunits.Reluctance R_stator_yoke "One stator yoke (t_P) reluctance";
      SIunits.MagneticFlux Flux_slot_loss_t_p_new "Slot loss flux";
      Real Flux_slot_loss_rel_error "Flux slot loss relative error";
      SIunits.MagnetomotiveForce MMF_stator_cond;
      SIunits.Length h_PM "Magnetic circuit magnet's height";
      SIunits.Reluctance R_PM "One magnet reluctance";
      SIunits.Reluctance R_loss_insulator_PM
        "One magnet's insulator reluctance";
      SIunits.Reluctance R_g "One polar pitch airgap reluctance";
      SIunits.Reluctance R_loss_inter_PM "One PM-PM leak reluctance";
      SIunits.MagnetomotiveForce MMF_rotor "Maximum magnetic voltage drop";
      SIunits.MagneticFlux Flux_leak_t_p "Inter permanent magnet flux leak";
      SIunits.MagneticFlux Flux_r_yoke "Total flux at rotor yoke";
      SIunits.Length h_rot_space "Rotor magnetic space";
      SIunits.MagneticFluxDensity B_r_yoke
        "Final rotor yoke maximum flux density value";
      SIunits.Length h_ry "Rotor yoke height";
      SIunits.Length h_rh "Rotor hole height";
      SIunits.MagneticFieldStrength H_r_rotor "Field strength at rotor yoke";
      SIunits.RelativePermeability mu_r_rotor
        "Relative permeability at rotor yoke";
      SIunits.Reluctance R_rotor_yoke "One rotor yoke (t_P) reluctance";
      Real h_PM_rel_error "Magnetic loop magnet's height relative error";
      Real p_lf "Pole leakage factor";
      SIunits.MagnetomotiveForce MMF_magnet "MMF suffered by magnet";
      SIunits.Length h_PM_new;
      Real h_PM_main_rel_error "Sizing loop magnet's height relative error";
      SIunits.Length l_extraS "Stator extra head coil length";
      SIunits.Length l_extra "Housing extra head coil length";
      SIunits.Length h_case "Square case height";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case "Square case length";
      SIunits.Length l_total "Total case length";
      SIunits.Length h_total "Total case height";
      SIunits.Length l_extraH=l_extraH_ref
        "Stator head coil longitudinal extra length";
      SIunits.Length l_extraW=l_extraW_ref
        "Stator head coil winding longitudinal length";
      SIunits.Length l_shaft_extra=l_shaft_extra_ref "Shaft external length";
      SIunits.Length l_shaft "Total shaft length";
      SMPMSM_Structure.motorSMPMSM motorSMPMSM "Motor data structure";
      SIunits.Length g_eff_Inductances "Effective airgap";
      SIunits.Inductance L_d "d-axis inductance";
      SIunits.Inductance L_ph "Phase inductance";
      Real K_f;
      SIunits.Resistance R_s "Phase resistance";
      SIunits.Voltage U_ph=U_ph_ref "Rated phase voltage";

    algorithm
      // STEP 1: Data preparation
      Mat_pm:=AuxiliaryFunc.MAGNETdb(1, magnet_no);
      B_r:=Mat_pm[1];
      H_c:=Mat_pm[2];
      mu_r_PM:=Mat_pm[3];
      //B_r:=1.18;
      //H_c:=935000;
      //mu_r_PM:=1.03;

      ElectricalData:=AuxiliaryFunc.ReducedInputs(U_ph,I_s,PF,K_E_U_ratio,delta,T,n_rpm,m,p,ReducedInputs_parameters);
      K_E_U_ratio2:=ElectricalData.K_E_U_ratio;
      E_ph:=ElectricalData.E_ph;
      P_mec:=ElectricalData.P_mec;
      P_elec:=ElectricalData.P_elec;
      eff:=ElectricalData.eff;

      n_Hz:=n_rpm/60;
      f:=n_Hz*p;

      // New C_mec for all machines using C_mec
      C_mec:=C_mec_ini*1000;
      if (C_mec==0 and C_mec_family_flag==false) then    // using default curves
        C_mec:=AuxiliaryFunc.CmecCALC(p, P_mec);
        C_mec:=C_mec*1000;
      elseif (C_mec_family_flag==true) then
        C_mec:=Modelica.Math.Vectors.interpolate(
          (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 1],
          (DataFiles.readCSVmatrix("CmecTQ_family.txt"))[:, 2],
          P_mec/(2*p));
          C_mec:=C_mec*1000;
      end if;

      // Checks

      if
        (R_fe_consider==1 and (K_e==0 or K_h==0 or n_i==0)) then
        Modelica.Utilities.Streams.print("SM *** ERROR: losses coefficients missing: R_fe_consider reset to 0. Please introduce all losses inputs: K_e, K_h and n_i","");
        R_fe_consider:=0;
      end if;

      if (hcf==0 and hole_rotor==1) then
        hcf:=0.85;
      else
        hcf:=0;
      end if;
      delta_htr:=0;
      delta_bz_3:=0.0005*(P_mec/30000) + 0.0005;
      q:=Q_s/(2*p*m);
      coilspan:=integer(Q_s/p/2);

      // Winding factor
      K_w:=(AuxiliaryFunc.windingf(Q_s,p,m,coilspan,gamma,1))[1];

      if (K_w<0) then
        Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be negative, please verify input data: Q_s, p, m","");
        K_w:=abs(K_w);
      elseif (K_w>1) then
        Modelica.Utilities.Streams.print("*** ERROR: Winding factor of first harmonic cannot be >1, please verify input data: Q_s, p, m. K_w set at 1","");
        K_w:=1;
      end if;

      // External D_shaft is not introduced (==0) as input

      D_shaft:=AuxiliaryFunc.PowerDShaft(P_mec, Internal_shaft_coefficient);
      if (D_shaft_internal==0) then
        D_shaft_internal:=D_shaft*Internal_shaft_coefficient;
      end if;

      // Initial airgap flux density

      // We consider D (bore diameter) as "D_g in the middle of the airgap" for all bore diameter instances
      D_g:=(P_mec/(C_mec*X*n_Hz))^0.3333333;
      D_or:=D_g - g;
      D_is:=D_g + g;
      l_eff:=D_g*X;
      l_r:=l_eff - 2*g;
      l_s:=l_eff - 2*g;
      l_r_mag:=l_r*f_r_r;
      l_s_mag:=(l_s - n_c*e_c)*f_r_s;

      Q_r:=2*p;
      t_P:=2*Constants.pi/Q_r;
      t_S:=2*Constants.pi/Q_s;
      t_p:=t_P*D_g/2;
      t_s:=t_S*D_g/2;

      //l_PM_ini:=(D_g/2 - g - h_PM_main)*Constants.pi/p*mcf_PM;

      for j in 1:sizing_iteration_limit loop      // Sizing starting for
        l_PM_ini:=(D_g/2 - g - h_PM_main)*Constants.pi/p*mcf_PM;
        Cover_ini:=l_PM_ini/t_p;
        if (Bg_type==0) then
          k_f:=1/sqrt(Cover_ini);
          Flux_g_ini_t_p:=l_PM_ini*l_eff*B_g_ini;
          B_g_max_sin:=4/Constants.pi*cos((1 - mcf_PM)*Constants.pi/2)*B_g_ini;
          C_3:=C_mec/(eff*PF/K_E_U_ratio2);
          A_load:=C_3/(Constants.pi*Constants.pi/sqrt(2)*K_w*B_g_max_sin);
        else
          k_f:=Constants.pi/(2*sqrt(2));
          Flux_g_ini_t_p:=l_PM_ini*l_eff*2/Constants.pi*B_g_ini;
          C_3:=C_mec/(eff*PF/K_E_U_ratio2);
          A_load:=C_3/(Constants.pi*Constants.pi/sqrt(2)*K_w*B_g_ini);
        end if;
        // Defining flux of the number of turns is the magnetizing flux
        // Considering there are two leaks, stator slot and inter-magnets, there are 3 different
        // fluxes from the PM flux to the magnetizing flux

        // Differentiation between magnetizing flux and airgap flux

        Flux_slot_loss_t_p:=Flux_slot_loss_t_p_main;

        for k in 1:stator_iteration_limit loop      // Stator iteration for
          Flux_mag_ini_t_p:=Flux_g_ini_t_p - Flux_slot_loss_t_p;
          N_ph1_Corr:=E_ph/(4*f*k_f*K_w*Flux_mag_ini_t_p);
          N_ph_round:=AuxiliaryFunc.round_Nph(N_ph1_Corr,p,q);
          z_Q:=2*a*m*N_ph_round/Q_s;

          // Once rounded N_ph, two situation can happen:
          // 1. fixed E and therefore lower B, change B requires revaluation of available flux and therefore h_PM, C_mec
          // 2. fixed B and therefore increase E, change E may affect K_E_U ratio and therefore C_mec as well
          // This change may look insignificant for high number of turns per phase but it is not if the number is low

          // Stator Geometry
          Flux_mag_t_p:=E_ph/(4*f*k_f*K_w*N_ph_round);
          Flux_mag_t_s:=t_S/t_P*Flux_mag_t_p;
          Flux_mag_t_p:=Flux_mag_t_p;

          // Stator tooth
          b_z:=Flux_mag_t_s/(l_s_mag*B_tooth_ini) + delta_bz_3;
          if (b_z>=t_S*0.95*D_is/2) then
            Modelica.Utilities.Streams.print("*** ERROR: b_z occupies more than 95% of stator pitch, increase B_tooth or check ventilation ducts geometry","");
          end if;
          A_slot:=z_Q*I_s/(a*J_s*K_u);
          S_cond:=A_slot*K_u/z_Q;
          D_cond:=sqrt(4*S_cond/Constants.pi);

          // Tooth dimensioning based on D_is and b_z restriction
          // This function makes a constant pitch teeth based on D_is and b_z restrictions
          // Tip proportion is based 10% for the constant region and 5% for the union
          tooth_3:=AuxiliaryFunc.tooth_dim(A_slot,t_S,t_S,D_is,b_z,0);
          b_sB:=tooth_3.b_sB;
          b_sT:=tooth_3.b_sT;
          b_zT:=tooth_3.b_zT;
          b_zB:=b_z;
          D_ms:=tooth_3.D_ms;
          d1_prop:=tooth_3.d1_prop;
          d2_prop:=tooth_3.d2_prop;
          d1:=tooth_3.d1;
          d2:=tooth_3.d2;
          d3:=tooth_3.d3;
          A_cu_rel_error:=tooth_3.A_cu_rel_error;

          // Stator yoke
          h_y:=Flux_mag_t_p/2/(l_s_mag*B_yoke_ini) + delta_bz_3;
          D_os:=D_ms + 2*h_y;

          // Magnetic circuit calculation
          K_C:=AuxiliaryFunc.carter_factor(b_1,g,t_S*D_is/2);
          g_eff:=g*K_C;

          // Real vs apparent flux density on stator tooth not used instead a new reluctance in stator is added in worst case scenario
          H_yoke:=Modelica.Math.Vectors.interpolate(Mat_B_s,Mat_H_s,B_yoke_ini);
          mu_r_yoke:=B_yoke_ini/(H_yoke*Constants.mue_0);
          H_tooth:=Modelica.Math.Vectors.interpolate(Mat_B_s,Mat_H_s,B_tooth_ini);
          mu_r_tooth:=B_tooth_ini/(H_tooth*Constants.mue_0);

          // Stator reluctance
          R_loss_stator_slot:=AuxiliaryFunc.Reluctance(d3,(b_sT + b_sB)/2,l_eff,Constants.mue_0);
          R_tooth:=AuxiliaryFunc.Reluctance(d1 + d2 + d3 + h_y/2,(b_zT + b_zB)/2,l_s_mag,Constants.mue_0*mu_r_tooth);
          R_stator_yoke:=AuxiliaryFunc.Reluctance((t_P - t_S)*(D_ms + h_y)/2,h_y,l_s_mag,Constants.mue_0*mu_r_yoke);
          MMF_stator_cond:=Flux_mag_t_p/2*(2/(m*q)*R_tooth + 0.5*R_stator_yoke + 2/(m*q)*R_tooth);
          Flux_slot_loss_t_p_new:=MMF_stator_cond/(2/(m*q)*R_loss_stator_slot);
          Flux_slot_loss_rel_error:=(Flux_slot_loss_t_p - Flux_slot_loss_t_p_new)/Flux_slot_loss_t_p;
          Flux_slot_loss_t_p:=Flux_slot_loss_t_p_new;
          if (abs(Flux_slot_loss_rel_error)<=Flux_slot_loss_thresshold) then
            break;
          end if;
        end for;

        h_PM:=h_PM_main;

        // Magnetic circuit
        for i in 1:magnetic_circuit_limit loop
          // Rotor reluctances
          R_PM:=AuxiliaryFunc.Reluctance(h_PM,t_P*(D_or/2 - h_PM),l_r,Constants.mue_0*mu_r_PM);
          R_loss_insulator_PM:=AuxiliaryFunc.Reluctance(l_t,t_P*(D_or/2 - h_PM),l_r,Constants.mue_0);
          R_g:=AuxiliaryFunc.Reluctance(g_eff,t_P*D_g/2,l_eff,Constants.mue_0);
          R_loss_inter_PM:=AuxiliaryFunc.Reluctance(Constants.pi,log(1 + Constants.pi*g_eff/(t_p*(1 - mcf_PM))),l_eff,Constants.mue_0);
          R_loss_stator_slot:=AuxiliaryFunc.Reluctance(d3,(b_sT + b_sB)/2,l_eff,Constants.mue_0);
          R_tooth:=AuxiliaryFunc.Reluctance(d1 + d2 + d3 + h_y/2,(b_zT + b_zB)/2,l_s_mag,Constants.mue_0*mu_r_tooth);
          R_stator_yoke:=AuxiliaryFunc.Reluctance((t_P - t_S)*(D_ms + h_y)/2,h_y,l_s_mag,Constants.mue_0*mu_r_yoke);
          MMF_rotor:=(Flux_mag_t_p + Flux_slot_loss_t_p)/2*2*R_g + Flux_mag_t_p/2*(2/(m*q)*R_tooth + 0.5
            *R_stator_yoke + 2/(m*q)*R_tooth) + (Flux_mag_t_p + Flux_slot_loss_t_p)/2*2*R_g;
          Flux_leak_t_p:=MMF_rotor/(R_loss_inter_PM/2);

          // Rotor yoke: until total flux is known it is not possible to determine mu_r_rotor and therefore its reluctance
          // Rotor yoke:
          Flux_r_yoke:=(Flux_leak_t_p + Flux_g_ini_t_p)/2;
          h_rot_space:=abs(D_or/2 - h_PM - D_shaft_internal/2);
          if (hole_rotor==1) then      // if rotor requires holes
            B_r_yoke:=B_r_yoke_ini;
            h_ry:=Flux_r_yoke/(l_r*B_r_yoke);
            if (h_rot_space>=2.5*h_ry) then  // if there is enough space in rotor to allocate hole
              h_rh:=(h_rot_space - h_ry)/2;
            elseif (h_rot_space>h_ry) then   // if space is tight
              h_rh:=(h_rot_space - h_ry)/2;
            else                             // if there is not enough space
              h_ry:=h_rot_space;
              h_rh:=0;
              // Flux density is imposed by geometry
              B_r_yoke:=Flux_r_yoke/(l_r*h_ry);
            end if;
          else                       // if rotor doesn't have holes
            h_ry:=h_rot_space;
            h_rh:=0;
            // Flux density imposed by geometry
            B_r_yoke:=Flux_r_yoke/(l_r*h_ry);
          end if;
          if (B_r_yoke>max(Mat_B_s)) then
            B_r_yoke:=max(Mat_B_s);
          end if;

          H_r_rotor:=Modelica.Math.Vectors.interpolate(Mat_B_s,Mat_H_s,B_r_yoke);
          mu_r_rotor:=B_r_yoke/(H_r_rotor*Constants.mue_0);

          // Rotor reluctances
          R_rotor_yoke:=AuxiliaryFunc.Reluctance(t_P*(D_or - h_ry/2)/2,h_ry,l_r,Constants.mue_0*mu_r_rotor);
          MMF_magnet:=MMF_rotor + (Flux_leak_t_p + Flux_g_ini_t_p)/2*(2*R_loss_insulator_PM+2*R_PM + 2
            *0.5*R_rotor_yoke + 2*R_PM + 2*R_loss_insulator_PM);
          h_PM_new:=MMF_magnet/(2*H_c);
          h_PM_rel_error:=abs((h_PM - h_PM_new)/h_PM);
          p_lf:=Flux_leak_t_p/Flux_mag_t_p;
          h_PM:=h_PM_new;

          if (i==magnetic_circuit_limit) then
            Modelica.Utilities.Streams.print("ERROR: Magnetic Circuit do not converge, please revise inputs feasibility","");
          elseif (h_PM_rel_error<=h_PM_thresshold) then
            break;
          end if;
      //               Modelica.Utilities.Streams.print("H_c"+Modelica.Math.Vectors.toString((vector(H_c))),"");
      //               Modelica.Utilities.Streams.print("h_PM_main"+Modelica.Math.Vectors.toString((vector(h_PM_main))),"");
      // Modelica.Utilities.Streams.print("h_PM"+Modelica.Math.Vectors.toString((vector(h_PM))),"");
      // Modelica.Utilities.Streams.print("R_PM"+Modelica.Math.Vectors.toString((vector(R_PM))),"");
      // Modelica.Utilities.Streams.print("R_loss_insulator_PM"+Modelica.Math.Vectors.toString((vector(R_loss_insulator_PM))),"");
      // Modelica.Utilities.Streams.print("R_g"+Modelica.Math.Vectors.toString((vector(R_g))),"");
      // Modelica.Utilities.Streams.print("R_loss_inter_PM"+Modelica.Math.Vectors.toString((vector(R_loss_inter_PM))),"");
      // Modelica.Utilities.Streams.print("R_loss_stator_slot"+Modelica.Math.Vectors.toString((vector(R_loss_stator_slot))),"");
      // Modelica.Utilities.Streams.print("R_tooth"+Modelica.Math.Vectors.toString((vector(R_tooth))),"");
      // Modelica.Utilities.Streams.print("R_stator_yoke"+Modelica.Math.Vectors.toString((vector(R_stator_yoke))),"");
      // Modelica.Utilities.Streams.print("MMF_rotor"+Modelica.Math.Vectors.toString((vector(MMF_rotor))),"");
      // Modelica.Utilities.Streams.print("Flux_leak_t_p"+Modelica.Math.Vectors.toString((vector(Flux_leak_t_p))),"");
      // Modelica.Utilities.Streams.print("Flux_r_yoke"+Modelica.Math.Vectors.toString((vector(Flux_r_yoke))),"");
      // Modelica.Utilities.Streams.print("h_rot_space"+Modelica.Math.Vectors.toString((vector(h_rot_space))),"");
      // Modelica.Utilities.Streams.print("z_Q"+Modelica.Math.Vectors.toString((vector(z_Q))),"");
      // Modelica.Utilities.Streams.print("Flux_mag_t_p"+Modelica.Math.Vectors.toString((vector(Flux_mag_t_p))),"");
      // Modelica.Utilities.Streams.print("Flux_mag_t_s"+Modelica.Math.Vectors.toString((vector(Flux_mag_t_s))),"");
      // Modelica.Utilities.Streams.print("Flux_mag_t_p"+Modelica.Math.Vectors.toString((vector(Flux_mag_t_p))),"");
      // Modelica.Utilities.Streams.print("B_r_yoke"+Modelica.Math.Vectors.toString((vector(B_r_yoke))),"");
      // Modelica.Utilities.Streams.print("h_ry"+Modelica.Math.Vectors.toString((vector(h_ry))),"");
      // Modelica.Utilities.Streams.print("h_rh"+Modelica.Math.Vectors.toString((vector(h_rh))),"");
      // Modelica.Utilities.Streams.print("H_r_rotor"+Modelica.Math.Vectors.toString((vector(H_r_rotor))),"");
      // Modelica.Utilities.Streams.print("mu_r_rotor"+Modelica.Math.Vectors.toString((vector(mu_r_rotor))),"");
      // Modelica.Utilities.Streams.print("R_rotor_yoke"+Modelica.Math.Vectors.toString((vector(R_rotor_yoke))),"");
      // Modelica.Utilities.Streams.print("MMF_magnet"+Modelica.Math.Vectors.toString((vector(MMF_magnet))),"");
      // Modelica.Utilities.Streams.print("h_PM_new"+Modelica.Math.Vectors.toString((vector(h_PM_new))),"");
      // Modelica.Utilities.Streams.print("h_PM_rel_error"+Modelica.Math.Vectors.toString((vector(h_PM_rel_error))),"");
      // Modelica.Utilities.Streams.print("p_lf"+Modelica.Math.Vectors.toString((vector(p_lf))),"");
      // Modelica.Utilities.Streams.print("h_PM"+Modelica.Math.Vectors.toString((vector(h_PM))),"");
      // Modelica.Utilities.Streams.print("h_PM_main_rel_error"+Modelica.Math.Vectors.toString((vector(h_PM_main_rel_error))),"");
        end for;

        h_PM_main_rel_error:=abs(((h_PM_main - h_PM)/h_PM_main));
        if (j==sizing_iteration_limit) then
          break;
        elseif (h_PM_main_rel_error<=h_PM_thresshold) then
          break;
        else
          // Iterated h_PM of magnetic circuit (h_PM) is assigned as sizing h_PM (h_PM_main) to reelaborate the machine
          h_PM_main:=h_PM;
        end if;
    //     Modelica.Utilities.Streams.print("Flux_mag_ini_t_p"+Modelica.Math.Vectors.toString((vector(Flux_mag_ini_t_p))),"");
    // Modelica.Utilities.Streams.print("N_ph1_Corr"+Modelica.Math.Vectors.toString((vector(N_ph1_Corr))),"");
    // Modelica.Utilities.Streams.print("N_ph_round"+Modelica.Math.Vectors.toString((vector(N_ph_round))),"");
    // Modelica.Utilities.Streams.print("z_Q"+Modelica.Math.Vectors.toString((vector(z_Q))),"");
    // Modelica.Utilities.Streams.print("Flux_mag_t_p"+Modelica.Math.Vectors.toString((vector(Flux_mag_t_p))),"");
    // Modelica.Utilities.Streams.print("Flux_mag_t_s"+Modelica.Math.Vectors.toString((vector(Flux_mag_t_s))),"");
    // Modelica.Utilities.Streams.print("Flux_mag_t_p"+Modelica.Math.Vectors.toString((vector(Flux_mag_t_p))),"");
    // Modelica.Utilities.Streams.print("b_z"+Modelica.Math.Vectors.toString((vector(b_z))),"");
    // Modelica.Utilities.Streams.print("A_slot"+Modelica.Math.Vectors.toString((vector(A_slot))),"");
    // Modelica.Utilities.Streams.print("S_cond"+Modelica.Math.Vectors.toString((vector(S_cond))),"");
    // Modelica.Utilities.Streams.print("D_cond"+Modelica.Math.Vectors.toString((vector(D_cond))),"");
    // Modelica.Utilities.Streams.print("b_sB"+Modelica.Math.Vectors.toString((vector(b_sB))),"");
    // Modelica.Utilities.Streams.print("b_sT"+Modelica.Math.Vectors.toString((vector(b_sT))),"");
    // Modelica.Utilities.Streams.print("b_zT"+Modelica.Math.Vectors.toString((vector(b_zT))),"");
    // Modelica.Utilities.Streams.print("b_zB"+Modelica.Math.Vectors.toString((vector(b_zB))),"");
    // Modelica.Utilities.Streams.print("D_ms"+Modelica.Math.Vectors.toString((vector(D_ms))),"");
    // Modelica.Utilities.Streams.print("d1_prop"+Modelica.Math.Vectors.toString((vector(d1_prop))),"");
    // Modelica.Utilities.Streams.print("d2_prop"+Modelica.Math.Vectors.toString((vector(d2_prop))),"");
    // Modelica.Utilities.Streams.print("d1"+Modelica.Math.Vectors.toString((vector(d1))),"");
    // Modelica.Utilities.Streams.print("d2"+Modelica.Math.Vectors.toString((vector(d2))),"");
    // Modelica.Utilities.Streams.print("d3"+Modelica.Math.Vectors.toString((vector(d3))),"");
    // Modelica.Utilities.Streams.print("A_cu_rel_error"+Modelica.Math.Vectors.toString((vector(A_cu_rel_error))),"");
    // Modelica.Utilities.Streams.print("h_y"+Modelica.Math.Vectors.toString((vector(h_y))),"");
    // Modelica.Utilities.Streams.print("D_os"+Modelica.Math.Vectors.toString((vector(D_os))),"");
    // Modelica.Utilities.Streams.print("K_C"+Modelica.Math.Vectors.toString((vector(K_C))),"");
    // Modelica.Utilities.Streams.print("g_eff"+Modelica.Math.Vectors.toString((vector(g_eff))),"");
    // Modelica.Utilities.Streams.print("H_yoke"+Modelica.Math.Vectors.toString((vector(H_yoke))),"");
    // Modelica.Utilities.Streams.print("mu_r_yoke"+Modelica.Math.Vectors.toString((vector(mu_r_yoke))),"");
    // Modelica.Utilities.Streams.print("H_tooth"+Modelica.Math.Vectors.toString((vector(H_tooth))),"");
    // Modelica.Utilities.Streams.print("mu_r_tooth"+Modelica.Math.Vectors.toString((vector(mu_r_tooth))),"");
    // Modelica.Utilities.Streams.print("R_loss_stator_slot"+Modelica.Math.Vectors.toString((vector(R_loss_stator_slot))),"");
    // Modelica.Utilities.Streams.print("R_tooth"+Modelica.Math.Vectors.toString((vector(R_tooth))),"");
    // Modelica.Utilities.Streams.print("R_stator_yoke"+Modelica.Math.Vectors.toString((vector(R_stator_yoke))),"");
    // Modelica.Utilities.Streams.print("MMF_stator_cond"+Modelica.Math.Vectors.toString((vector(MMF_stator_cond))),"");
    // Modelica.Utilities.Streams.print("Flux_slot_loss_t_p_new"+Modelica.Math.Vectors.toString((vector(Flux_slot_loss_t_p_new))),"");
    // Modelica.Utilities.Streams.print("Flux_slot_loss_rel_error"+Modelica.Math.Vectors.toString((vector(Flux_slot_loss_rel_error))),"");
    // Modelica.Utilities.Streams.print("Flux_slot_loss_t_p"+Modelica.Math.Vectors.toString((vector(Flux_slot_loss_t_p))),"");

      end for;

      // Lengths
      if (l_extraH==0) then
        l_extraS:=1/3*l_eff;
        l_extraH:=1/3*l_extra;
        l_extraW:=l_extra - l_extraH;
      else
        l_extraS:=l_extraH + l_extraW;
      end if;
      l_extra:=l_extraS;
      h_case:=floor(D_os*1.02*1000)/1000 + 0.003;
      if ((h_case-D_os)<=0.005) then
        h_case:=D_os + 0.005;
      elseif ((h_case-D_os)>=0.012) then
        h_case:=D_os + 0.012;
      end if;

      l_case_exp_void:=floor((l_extra/2 + l_eff*0.1)*1000)/1000;
      l_case_exp_front:=floor(((h_case - D_os)/2*1.5)*1000)/1000;
      l_case_exp_rear:=floor((l_eff*0.3)*1000)/1000;
      l_case:=l_eff + 2*l_extra + 2*l_case_exp_void;
      l_total:=l_case + l_case_exp_front + l_case_exp_rear;
      h_total:=h_case;
      if (l_shaft_extra==0) then
        l_shaft_extra:=floor(l_case/3*1000)/1000000;
      end if;

      // if the desired l_shaft is bigger than required
      if (l_shaft_ini<>0 and l_shaft_ini>=l_total+l_shaft_extra) then
        l_shaft:=l_shaft_ini;
      // if the assigned l_shaft is shorter than required
      elseif (l_shaft_ini<l_total+l_shaft_extra) then
        l_shaft:=l_total + l_shaft_extra;
        Modelica.Utilities.Streams.print("SM - Warning: desired l_shaft is too short: Auto assigned a larger one [mm]","");
      // or if its 0
      elseif (l_shaft_ini==0) then
        l_shaft:=l_total+l_shaft_extra;
      end if;

      Modelica.Utilities.Streams.print("R_PM"+Modelica.Math.Vectors.toString((vector(R_PM))),"");
      // Structure construction for graphical representation

      // Basic
      motorSMPMSM.m:=m;
      motorSMPMSM.p:=p;
      motorSMPMSM.g:=g;
      motorSMPMSM.RatedPower:=P_mec;
      motorSMPMSM.classe:=81;
      // Plate
      motorSMPMSM.plate.RatedPower:=P_mec;
      motorSMPMSM.plate.RatedCurrent:=I_s;
      motorSMPMSM.plate.RatedVoltage:=U_ph;
      motorSMPMSM.plate.RatedVoltageType:="Phase";
      motorSMPMSM.plate.BEMF:=E_ph;
      motorSMPMSM.plate.RatedSpeedRPM:=n_rpm;
      motorSMPMSM.plate.RatedSpeed:=f;
      motorSMPMSM.plate.Torque:=T;
      motorSMPMSM.plate.PowerFactor:=PF;
      motorSMPMSM.plate.Efficiency:=eff;
      motorSMPMSM.plate.M:=99999;
      motorSMPMSM.plate.TmM:=99999;
      // Specifications
      motorSMPMSM.specs.f_r:=99999;
      motorSMPMSM.specs.X:=X;
      motorSMPMSM.specs.C_mec:=C_mec;
      motorSMPMSM.specs.A_load:=A_load;

      motorSMPMSM.specs.TargetRatedVoltage:=U_ph;

      motorSMPMSM.specs.flux_final:=(Flux_leak_t_p + Flux_mag_t_p)/2;

      motorSMPMSM.specs.TargetFluxDensities.B_g_ini:=B_g_ini;
      motorSMPMSM.specs.TargetFluxDensities.B_yoke_ini:=B_yoke_ini;
      motorSMPMSM.specs.TargetFluxDensities.B_tooth_ini:=B_tooth_ini;
      motorSMPMSM.specs.TargetFluxDensities.B_r_yoke_ini:=B_r_yoke_ini;

      motorSMPMSM.flags.R_fe_consider:=R_fe_consider;

      // Airgap
      motorSMPMSM.airgap.phy.B_g:=B_g_max_sin;
      motorSMPMSM.airgap.phy.flux_g:=Flux_g_ini_t_p;
      motorSMPMSM.airgap.phy.R_g:=R_g;
      motorSMPMSM.airgap.phy.flux_g_final:=(Flux_leak_t_p + Flux_mag_t_p)/2;

      // Stator
      motorSMPMSM.stator.case_type:=1;
      motorSMPMSM.stator.Q_s:=Q_s;
      motorSMPMSM.stator.D:=D_is;
      motorSMPMSM.stator.I_s:=I_s;
      motorSMPMSM.stator.J_s_desired:=J_s;

      motorSMPMSM.stator.dim.tau_s:=t_s;
      motorSMPMSM.stator.dim.D_os:=D_os;
      motorSMPMSM.stator.dim.D_is:=D_is;
      motorSMPMSM.stator.dim.h_tr:=d1 + d2 + d3;
      motorSMPMSM.stator.dim.h_z:=d3;
      motorSMPMSM.stator.dim.b_s:=b_sB;
      motorSMPMSM.stator.dim.b_z:=b_z;
      motorSMPMSM.stator.dim.h_y:=h_y;
      motorSMPMSM.stator.dim.b_1:=b_1;
      motorSMPMSM.stator.dim.delta_htr:=d1 + d2;
      motorSMPMSM.stator.dim.l:=l_s;
      motorSMPMSM.stator.dim.f_r_s:=f_r_s;
      motorSMPMSM.stator.dim.K_c:=K_C;

      // New Stator Entries
      motorSMPMSM.stator.dim.b_sB:=tooth_3.b_sB;
      motorSMPMSM.stator.dim.b_sT:=tooth_3.b_sT;
      motorSMPMSM.stator.dim.b_zT:=tooth_3.b_zT;
      motorSMPMSM.stator.dim.b_zB:=b_z;
      motorSMPMSM.stator.dim.D_ms:=tooth_3.D_ms;
      motorSMPMSM.stator.dim.d1_prop:=tooth_3.d1_prop;
      motorSMPMSM.stator.dim.d2_prop:=tooth_3.d2_prop;
      motorSMPMSM.stator.dim.d1:=tooth_3.d1;
      motorSMPMSM.stator.dim.d2:=tooth_3.d2;
      motorSMPMSM.stator.dim.d3:=tooth_3.d3;
      motorSMPMSM.stator.dim.phy.mst.K_h:=K_h;
      motorSMPMSM.stator.dim.phy.mst.K_e:=K_e;
      motorSMPMSM.stator.dim.phy.mst.n_i:=n_i;

      // Stator Winding
      motorSMPMSM.stator.win.a:=a;
      motorSMPMSM.stator.win.N_ph:=N_ph_round;
      motorSMPMSM.stator.win.K_w:=K_w;
      motorSMPMSM.stator.win.K_u:=K_u;
      motorSMPMSM.stator.win.K_uhc:=K_uhc;
      motorSMPMSM.stator.win.S_cond:=S_cond;
      motorSMPMSM.stator.win.coilspan:=coilspan;
      motorSMPMSM.stator.win.dim.D_cond:=D_cond;
      motorSMPMSM.stator.phy.R_sc_t_aro:=99999;
      motorSMPMSM.stator.phy.t_work:=99999;
      motorSMPMSM.stator.phy.R_s:=99999;
      motorSMPMSM.stator.phy.p_lf:=p_lf;
      motorSMPMSM.stator.win.phy.ro_t_aro:=ro_t_aro;

      // Rotor
      motorSMPMSM.rotor.shape:=8;
      motorSMPMSM.rotor.fail:=0;
      motorSMPMSM.rotor.failpath:=0;
      motorSMPMSM.rotor.msg:="None";

      motorSMPMSM.rotor.dim.tau_r:=t_p;
      motorSMPMSM.rotor.dim.D_or:=D_or;
      motorSMPMSM.rotor.dim.D_ir:=D_shaft_internal;
      motorSMPMSM.rotor.dim.D_shaft_internal:=D_shaft_internal;
      motorSMPMSM.rotor.dim.D_shaft:=D_shaft;
      motorSMPMSM.rotor.dim.D_shaftB:=D_shaft;
      motorSMPMSM.rotor.dim.D_hub:=D_shaft_internal;
      motorSMPMSM.rotor.dim.h_PM:=h_PM;
      motorSMPMSM.rotor.dim.l_PM:=l_PM_ini;
      motorSMPMSM.rotor.dim.mcf_PM:=mcf_PM;
      motorSMPMSM.rotor.dim.l:=l_r;
      motorSMPMSM.rotor.dim.f_r:=f_r_r;
      motorSMPMSM.rotor.dim.DeltaD:=0;
      motorSMPMSM.rotor.dim.Cover:=Cover_ini;
      motorSMPMSM.rotor.dim.h_ry:=h_ry;
      motorSMPMSM.rotor.dim.h_rh:=h_rh;
      motorSMPMSM.rotor.dim.hcf:=hcf;

      motorSMPMSM.rotor.draw.hole_rotor:=hole_rotor;
      motorSMPMSM.rotor.draw.magnet_style:=1;
      motorSMPMSM.rotor.draw.magnetSlotPM_style:=1;
      motorSMPMSM.rotor.draw.magneticSteel_style:=1;
      motorSMPMSM.rotor.draw.rotorHub_style:=1;
      motorSMPMSM.rotor.draw.shaft_style:=1;
      motorSMPMSM.rotor.draw.fail_elements:=99999;

      motorSMPMSM.rotor.dim.l_web:=0;
      motorSMPMSM.rotor.dim.Omega:=0;

      motorSMPMSM.rotor.phy.R_PM:=R_PM;
      motorSMPMSM.rotor.phy.B_r_yoke:=B_r_yoke;

      // Housing
      motorSMPMSM.casing.dim.l_case_exp_front:=l_case_exp_front;
      motorSMPMSM.casing.dim.l_case_exp_rear:=l_case_exp_rear;
      motorSMPMSM.casing.dim.l_case_exp_void:=l_case_exp_void;
      motorSMPMSM.casing.dim.l_case:=l_case;
      motorSMPMSM.casing.dim.h_case:=h_case;

      motorSMPMSM.rotor.dim.l_shaft_extra:=l_shaft_extra;
      motorSMPMSM.rotor.dim.l_shaft:=l_shaft;
      motorSMPMSM.casing.l_total:=l_total;
      motorSMPMSM.casing.h_total:=h_total;

      motorSMPMSM.stator.win.dim.l_extra:=l_extra;
      motorSMPMSM.stator.win.dim.l_extraH:=l_extraH;
      motorSMPMSM.stator.win.dim.l_extraW:=l_extraW;

      motorSMPMSM.casing.dim.phy.Delta_t_work:=Delta_t_work;
      motorSMPMSM.casing.dim.phy.t_amb:=t_amb;
      motorSMPMSM.casing.dim.phy.t_aro:=t_aro;

      // Densities (of all parts)
      motorSMPMSM.casing.dim.phy.ro_case:=ro_case;
      motorSMPMSM.stator.dim.phy.ro_mst:=ro_mst;
      motorSMPMSM.stator.dim.phy.ro_cu:=ro_cu;
      motorSMPMSM.rotor.dim.phy.ro_mst:=ro_mst;
      motorSMPMSM.rotor.dim.phy.ro_PM:=ro_PM;
      motorSMPMSM.rotor.dim.phy.ro_shaft:=ro_shaft;

      // Added later (contains 99999 == ERROR)
      motorSMPMSM.stator.phy.L_d:=99999;
      motorSMPMSM.stator.phy.L_q:=99999;

      // Added by MassandMOI (contains 99999 == ERROR)
      motorSMPMSM.rotor.dim.M_mst_R:=99999;
      motorSMPMSM.rotor.dim.M_R:=99999;
      motorSMPMSM.rotor.dim.I_R:=99999;

      motorSMPMSM.plate.M:=99999;
      motorSMPMSM.plate.TmM:=99999;

      motorSMPMSM.stator.dim.mM:=99999;
      motorSMPMSM.stator.dim.cM:=99999;

      motorSMPMSM.casing.dim.M_S:=99999;
      motorSMPMSM.casing.dim.I_S:=99999;

      // Phase resistance

      motorSMPMSM:=AuxiliaryFunc.Resistance_SMPMSM(motorSMPMSM);

      // Inductances

      g_eff_Inductances:=K_C*g + h_PM/mu_r_PM;
      K_f:=1.11/(1/sqrt(Cover_ini));
      L_d:=K_f*m/2*4/Constants.pi*K_f*Constants.mue_0/(2*p)*t_p/g_eff_Inductances*l_eff*(K_w*
        N_ph_round)^2;
      L_ph:=L_d*3/2;

      R_s:=motorSMPMSM.stator.phy.R_s;
      U_ph:=sqrt((E_ph + R_s*I_s)^2 + (L_ph*(2*Constants.pi*f)*I_s)^2);

      // Final assignments
      motorSMPMSM.plate.RatedVoltage:=U_ph;
      motorSMPMSM.plate.L_ph:=L_ph;
      motorSMPMSM.stator.phy.L_dm:=L_d;
      motorSMPMSM.stator.phy.L_qm:=L_d;

      // Masses and Moments of Inertia:
      motorSMPMSM:=AuxiliaryFunc.MassandMOISMPMSM(motorSMPMSM);

      motorSMPMSM.specs.TSeff_lut:=99999;
      motorSMPMSM.specs.MTPA_lut:=99999;

      SMPMSM_Structure.SMPMSM_Export(motorSMPMSM);

    annotation (
        Documentation(info="<html>

<head>

<title>General SMPMSM Pre-sizing help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<h2><span lang=EN-US>1.1<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Table of inputs</span></h2>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Name</span></b></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Description</span></b></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Units</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>U_ph</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Phase Voltage</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>V</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>m</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of phases</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>a</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of parallel paths</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>p</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of pair poles</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>P_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  mechanical Power</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>W</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>n</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Speed</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>rpm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>f</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Frequency</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Hz</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>T</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Torque</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Nm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>I_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Current</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A<sub>rms</sub></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>eff</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Efficiency</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>PF</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Power
  Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_tooth</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Stator Tooth</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_yoke</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Yoke</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Airgap</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>J_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Current
  density at stator</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A<sub>rms</sub>/mm<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Airgap</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>Q_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of Stator Slots</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>n_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of ventilating ducts</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>b_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Ventilating
  Ducts width</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>F_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Lamination
  factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>K_u</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Slot
  Filling Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>K_w</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Winding
  Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>C_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Mechanical
  Constant</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>kWs/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>X</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Aspect
  Ratio</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>coilspan</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Coil
  Span</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>Material.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Stator/Rotor
  B-H material characteristic curve</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T-A/m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Magnet
  Remanent Flux</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>H_c</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Magnet
  Coercitive Force</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A/m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>mu_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Relative
  Permeability</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>mcf_PM</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Magnet
  Cover Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption align=center style='text-align:center'><a
name='_Toc414019771'></a><a name='_Toc385322413'></a><a name='_Toc385318064'></a><a
name='_Ref413740534'>Table </a>1. SMPMSM List of inputs: variable name,
description and units in IS</p>

<h2><span lang=EN-US>1.2<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Algorithm concept</span></h2>

<p class=MsoNormal><span lang=EN-US>Algorithm is based on the generation of
both magnetic parts (stator and rotor) each one able to provide the desired
airgap flux density (</span>B_g_ini<span lang=EN-US>)</span>. Stator sizing is
generated similarly to IM machine. Mechanical constant is used to obtain the
bore diameter. Using airgap flux density and phase voltage the number of turns
is determined. In this moment a loop is opened recalculating stator and
dimensions first and then magnetic voltage drops. To find the permeabilities in
order to solve a single pole circuity in d-axis position based on machines symmetry.
 Then the loop gradually increase or decrease the magnet thickness (radial
height) in order to provide the required input flux density in airgap and
taking into account the stator MMF forces previously calculated.</p>

<h2><span lang=EN-US>1.3<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Program Structure Explanation</span></h2>

<p class=MsoNormal>Please follow Figure 1 for a better comprehention. After
input data has been performed several pre-sizing data is calculated: the number
of slots per pole (q) and the winding factor (k_w), this last one by means of
an auxiliary funcions. Then Initial values for airgap flux density (B_g_ini) and
magnet height (h_PM) are set. At this point depending on machine power htrmodifflag
is set. And efficiency (eff), mechanical constant (C_mec), and airgap (g) are
calculated if it input was 0. </p>

<p class=MsoNormal>Mechanical constant (C_mec) and electrical loading (A_load) provides
first values for diameter and length. Once this has been determined pitches for
stator and rotor (t_u, t_p respectively) are also set as well as magnet cover
length (l_p). This leads to obtain the value of the desired flux at the airgap
and the number of turns per phase in series (N_ph), value that is immediately rounded
and provides the number of conductors per slot (z_Q).</p>

<p class=MsoNormal>With this initial information the main loop is started. In
each iteration magnet lengt (l_p), calculated at half of magnets thickness
(radial height, h_PM) will determine the current airgap flux density and
therefore the airgap flux (flux_g). This one is used to size stator tooth,
slots and yoke. Slot shape compensation is is performed at this point by means
of htrmodif() function. Then field of each part is calculated and its
permeabilities (mu based on desired input flux densities. Once this is done the
final step is to calculate circuit reluctance and check if magnet is able to
sustain the desired flux. Finally, depending on the new value of flux density
magnet’s thickness (h_PM) is modified. More magnet thicknes if flux density is
less than required or less magnet if flux density is avobe the desired value.</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><img
width=310 height=973 src='General_SMPMSM_archivos/image001.png'></p>

<p class=MsoCaption align=center style='text-align:center'>Figure 1. Global Sizing Algorithm explanation.</p>

</div>

</body>

</html>
"));
    end SMPMSM_Motor_Sizing;

    function PMSM_Motor_Sizing_Simple
      import Modelica.Constants;
      parameter input Modelica.SIunits.Voltage U_ph=400/sqrt(3)
        "Rated phase voltage"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer m=3 "Number of phases"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Integer p=3 "Number of pole pairs"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Modelica.SIunits.Power P_mec_ref=1450
        "Desired rated mechanical power"
      annotation (Dialog(group="Nominal parameters"));
      parameter input
        Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm               n=6000
        "Rated Speed"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real eff=0.95 "Efficiency"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Real PF=0.92 "Power factor"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Modelica.SIunits.Torque T=2.3 "Rated torque"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Modelica.SIunits.Current I_s=2.9 "Rated stator current"
      annotation (Dialog(group="Nominal parameters"));
      parameter input Boolean C_mec_flag=true "C_mec manual"
      annotation (Dialog(group="Mechanical constant",__Dymola_compact=true, __Dymola_descriptionLabel=true),choices(__Dymola_checkBox=true));
      parameter input Real C_mec_ref=68 "Desired mechanical constant"
      annotation (Dialog(enable=C_mec_manual == true, group="Mechanical constant"));
      parameter input Modelica.SIunits.CurrentDensity J_s=5.2*10^6
        "Current density"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Boolean g_ref_flag=true "g manual"
      annotation (Dialog(tab="Airgap",__Dymola_compact=true, __Dymola_descriptionLabel=true),choices(__Dymola_checkBox=true));
      parameter input Modelica.SIunits.Length g_ref=0.0007
        "Desired air gap length"
      annotation (Dialog(tab="Airgap"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_g_ini=0.738
        "Maximum airgap flux density"
      annotation (Dialog(tab="Airgap"));
      parameter input String Stator_Material_File="STEEL_ABB8Cr.txt"
        "Stator Material B(H) characteristic"
      annotation(Dialog(tab="Stator",group="Material",
      __Dymola_descriptionLabel = true,
      __Dymola_loadSelector(filter="Text files (*.txt);;CSV files (*.csv)",caption="Open Stator Material B(H) characteristic file")));
      parameter input Modelica.SIunits.MagneticFluxDensity B_yoke=1.217
        "Maximum stator yoke flux density"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input Modelica.SIunits.MagneticFluxDensity B_tooth=1.792
        "Maximum stator tooth flux density"
      annotation (Dialog(tab="Stator",group="Magnetic circuit"));
      parameter input Integer a=1 "Parallel paths"
      annotation (Dialog(tab="Stator",group="Winding"));
      parameter input Real X=1.923 "Aspect ratio"
      annotation (Dialog(tab="Stator",group="Geometry"));
      parameter input Integer Q_s=18 "Number of stator slots"
      annotation (Dialog(tab="Stator", group="Geometry"));
      parameter input Integer n_v=0 "Number of ventilation ducts"
      annotation (Dialog(tab="Stator", group="Ventilation"));
      parameter input Modelica.SIunits.Length b_v=0 "Ventilation duct width"
      annotation (Dialog(tab="Stator", group="Ventilation"));
      parameter input Real f_r(min=0.9, max=0.99)=0.98
        "Stator lamination factor"
      annotation (Dialog(tab="Stator", group="Winding"), Evaluate=true);
      parameter input Real K_u=0.47 "Stator slot filling factor"
      annotation (Dialog(tab="Stator", group="Winding"));
      parameter input Integer coilspan=3 "Coil span"
      annotation (Dialog(tab="Stator", group="Winding"));
      parameter input Modelica.SIunits.Conversions.NonSIunits.Angle_deg gamma=0
        "Skewing angle"
      annotation (Dialog(tab="Rotor"));
      parameter input Real mc_p=0.86 "Magnet cover factor"
      annotation (Dialog(tab="Rotor"));
      parameter input Modelica.SIunits.Height h_PM_ref=0.003
        "Desired magnet height"
      annotation (Dialog(tab="Rotor"));
      parameter input Modelica.SIunits.Length l_t=5e-4
        "Thickness of insulation layer between PMs and rotor"
      annotation (Dialog(tab="Rotor"));

      output Modelica.SIunits.MagneticFlux flux_g "Air gap flux"
      annotation (Dialog(tab="Outputs", group="Magnetic circuit"));
      output Modelica.SIunits.MagneticFluxDensity B_g_new
        "Recalculated air gap flux density"
      annotation (Dialog(tab="Outputs", group="Magnetic circuit"));
      output Modelica.SIunits.Power P_mec "Rated mechanical power"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Modelica.SIunits.Voltage E "Back EMF"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Real A_load=0 "Electric loading"
      annotation (Dialog(tab="Outputs", group="Winding"));
      output Real C_mec=C_mec_ref "Mechanical constant"
      annotation (Dialog(tab="Outputs", group="Nominal parameters"));
      output Real N_ph "Number of turns per phase"
      annotation (Dialog(tab="Outputs", group="Winding"));
      output Modelica.SIunits.Diameter D_os "Stator outer diameter"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Diameter D_or "Rotor outer diameter"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Diameter D "Bore diameter"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Length l "Stack length"
      annotation (Dialog(tab="Outputs", group="Machine geometry"));
      output Modelica.SIunits.Length g "Air gap length"
      annotation (Dialog(tab="Outputs", group="Machine geometry"));
      output Modelica.SIunits.Height h_z "Stator tooth height"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Height h_tr "Stator slot height"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Length b_s "Stator slot width"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Length b_z "Stator tooth width"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Height h_y "Stator yoke height"
      annotation (Dialog(tab="Outputs", group="Stator geometry"));
      output Modelica.SIunits.Length l_p "Magnet length"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));
      output Modelica.SIunits.Height h_PM "Magnet height"
      annotation (Dialog(tab="Outputs", group="Rotor geometry"));

    protected
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm k=60
        "Mechanical frequency scaling factor";
      parameter Modelica.SIunits.Frequency f=n/k*p "Rated electrical frequency";
      parameter Real q=Q_s/(2*p*m) "Slots per pole per phase ratio";
      parameter Real[:,:] Mat_s=DataFiles.readCSVmatrix(Stator_Material_File)
        "Stator material B(H) characteristic";
      Real[3] Mat_pm "Permanent magnet properties";
      Real C "Apparent machine constant";
      Modelica.SIunits.MagneticFluxDensity B_r "Remanence magnetizationof PM";
      Modelica.SIunits.MagneticFieldStrength H_c "Coercive field of PM";
      Modelica.SIunits.RelativePermeability mu_r_pm
        "Relative permeability of PM";
      Real K_w "Winding factor";
      Modelica.SIunits.Height delta_htr "Height of teeth expansion";
      Boolean htrmodifflag "Trapezoidal slot flag";
      Modelica.SIunits.MagneticFlux sigma "Sigma";
      Modelica.SIunits.Length l_eff "Effective length";
      Modelica.SIunits.Length t_u "Stator slot pitch";
      Modelica.SIunits.Length t_p "Polar pitch";
      Real z_Q "Number of conductors in slot";
      Modelica.SIunits.MagneticFieldStrength H_yoke
        "Yoke magnetic field strength";
      Modelica.SIunits.RelativePermeability mu_r_yoke
        "Yoke relative permeability";
      Modelica.SIunits.MagneticFieldStrength H_tooth
        "Tooth magnetic field strength";
      Modelica.SIunits.RelativePermeability mu_r_tooth
        "Tooth relative permeability";
      Modelica.SIunits.Reluctance R_g
        "Air gap reluctance between PM and stator";
      Modelica.SIunits.Reluctance R_g2 "Air gap reluctance between two magnets";
      Modelica.SIunits.Reluctance R_pm "Permanent magnet reluctance";
      Modelica.SIunits.Length b_y "Yoke width path in reluctance network";
      Modelica.SIunits.Reluctance R_fe "Reluctance of Iron";
    algorithm
      // STEP 1.1: Data preparation
      P_mec:=P_mec_ref;
      Mat_pm:=AuxiliaryFunc.MAGNETdb(1, 7);
      B_r:=Mat_pm[1];
      H_c:=Mat_pm[2];
      mu_r_pm:=Mat_pm[3];
      K_w:=(AuxiliaryFunc.windingf(Q_s,p,m,coilspan,0,1))[1];
      h_PM:=h_PM_ref;

      // STEP 2: Variable input calculations
      if (P_mec<10000) then
        delta_htr:=0.001;
        htrmodifflag:=false;
      elseif (P_mec>=10000) then
        delta_htr:=0.002;
        htrmodifflag:=true;
      end if;
      if (C_mec_flag==false) then
        C_mec:=AuxiliaryFunc.CmecCALC(p, P_mec);
      end if;
      C_mec:=C_mec*1000;
      if (g_ref_flag==false) then
        g:=(0.18 + 0.006*P_mec^0.4)/1000;
      else
        g:=g_ref;
      end if;

      // STEP 3: Obtaining A_load & Basic geometry
      C:=C_mec/(PF*eff);
      A_load:=C*sqrt(2)/(Constants.pi*Constants.pi*K_w*B_g_ini);
      sigma:=2/Constants.pi*B_g_ini*A_load;
      D:=(4*T/(2*sigma*Constants.pi*X))^0.3333;
      l_eff:=D*X;
      l:=l_eff - 2*g;

      // STEP 4: Magnet
      t_u:=Constants.pi*D/Q_s;
      t_p:=Constants.pi*(D - 2*g - h_PM)/(2*p);
      l_p:=mc_p*Constants.pi*(D - 2*g - h_PM)/(2*p);

      // STEP 5: Back-EMF and N_ph
      flux_g:=B_g_ini*l_p*l_eff;
      N_ph:=1.05*U_ph*sqrt(2)/(2*Constants.pi*f*K_w*flux_g);
      z_Q:=AuxiliaryFunc.round(2*a*m*N_ph/Q_s);
      N_ph:=z_Q*Q_s/(2*a*m);
      flux_g:=0.95*U_ph*sqrt(2)/(2*Constants.pi*f*K_w*N_ph);

      for i in 1:1000 loop
        // STEP 6: New Bg and magnet geometry
        l_p:=mc_p*Constants.pi*(D - 2*g - h_PM)/(2*p);
        B_g_new:=flux_g/(l_p*l_eff);

        // STEP 7: Stator geometry
        h_y:=flux_g/(B_yoke*2*f_r*(l - n_v*b_v));
        b_z:=(l_eff*t_u*B_g_new)/(B_tooth*f_r*(l - n_v*b_v));
        b_s:=t_u - b_z;
        h_tr:=z_Q*(I_s/(a*J_s))/(b_s*K_u);
        if (htrmodifflag==true) then
          h_tr:=AuxiliaryFunc.htrmodif(
            Q_s,
            h_tr,
            b_s);
        end if;
        h_z:=h_tr + delta_htr;

        // STEP 8.1: Saturation Point
        H_yoke:=Modelica.Math.Vectors.interpolate(Mat_s[:,1],Mat_s[:,2],B_yoke);
        mu_r_yoke:=B_yoke/(H_yoke*Constants.mue_0);
        H_tooth:=Modelica.Math.Vectors.interpolate(Mat_s[:, 1],Mat_s[:, 2],B_tooth);
        mu_r_tooth:=B_tooth/(H_tooth*Constants.mue_0);

        // STEP 8.2 Magnet height and Bg convergence
        R_g:=g/(Constants.mue_0*t_p*l_eff);
        R_g2:=h_PM/(Constants.mue_0*(t_p - l_p)*l_eff);
        R_pm:=h_PM/(Constants.mue_0*mu_r_pm*l_p*l) + l_t/(Constants.mue_0*l_p*l);
        b_y:=Constants.pi*D/(4*p);
        R_fe:=b_y/(2*Constants.mue_0*mu_r_yoke*l*h_y) + h_z/(Constants.mue_0*mu_r_tooth*l*b_z);

        // STEP 8.3
        B_g_new:=H_c*h_PM*(1/(l_p*l_eff))/(R_pm + R_fe + R_g + (R_pm/R_g2)*(R_g + R_fe));
        if (abs(B_g_new-B_g_ini) <= 0.005) then
          break;
        end if;
        if (B_g_new > B_g_ini) then
          h_PM:=h_PM/1.005;
        else
          h_PM:=h_PM*1.005;
        end if;
      end for;

      // STEP 9
      flux_g:=B_g_new*l_p*l_eff;
      P_mec:=C_mec*D*D*l_eff*n/k;
      E:=0.95*2*Constants.pi*f*K_w*N_ph*flux_g/sqrt(2);
      A_load:=C*sqrt(2)/(Constants.pi*Constants.pi*K_w*B_g_new);
      D_os:=(D + 2*h_y + 2*h_z);
      D_or:=(D - 2*g - 2*h_PM);

      DataFiles.writeCSVmatrix("MotorSPM_Structure.csv",{"B_g_new","flux_g","E","P_mec","C_mec","A_load","N_ph","D","D_os","b_z","b_s","h_y","h_z","h_tr","D_or","l_p","h_PM","l","g"},[B_g_new,flux_g,E,P_mec,C_mec,A_load,N_ph,D,D_os,b_z,b_s,h_y,h_z,h_tr,D_or,l_p,h_PM,l,g],",");

    //    Modelica.Utilities.Streams.print("B_g_new="+Modelica.Math.Vectors.toString(vector(B_g_new)),"");
    //    Modelica.Utilities.Streams.print("flux_g="+Modelica.Math.Vectors.toString(vector(flux_g)),"");
    //    Modelica.Utilities.Streams.print("E="+Modelica.Math.Vectors.toString(vector(E)),"");
    //
    //    Modelica.Utilities.Streams.print("P_mec="+Modelica.Math.Vectors.toString(vector(P_mec)),"");
    //    Modelica.Utilities.Streams.print("C_mec="+Modelica.Math.Vectors.toString(vector(C_mec)),"");
    //    Modelica.Utilities.Streams.print("A_load="+Modelica.Math.Vectors.toString(vector(A_load)),"");
    //    Modelica.Utilities.Streams.print("N_ph="+Modelica.Math.Vectors.toString(vector(N_ph)),"");
    //
    //    Modelica.Utilities.Streams.print("D="+Modelica.Math.Vectors.toString(vector(D*1000)),"");
    //    Modelica.Utilities.Streams.print("D_os="+Modelica.Math.Vectors.toString(vector(D_os*1000)),"");
    //    Modelica.Utilities.Streams.print("b_z="+Modelica.Math.Vectors.toString(vector(b_z*1000)),"");
    //    Modelica.Utilities.Streams.print("b_s="+Modelica.Math.Vectors.toString(vector(b_s*1000)),"");
    //    Modelica.Utilities.Streams.print("h_y="+Modelica.Math.Vectors.toString(vector(h_y*1000)),"");
    //    Modelica.Utilities.Streams.print("h_z="+Modelica.Math.Vectors.toString(vector(h_z*1000)),"");
    //    Modelica.Utilities.Streams.print("h_tr="+Modelica.Math.Vectors.toString(vector(h_tr*1000)),"");
    //    Modelica.Utilities.Streams.print("D_or="+Modelica.Math.Vectors.toString(vector(D_or*1000)),"");
    //
    //    Modelica.Utilities.Streams.print("l_p="+Modelica.Math.Vectors.toString(vector(l_p*1000)),"");
    //    Modelica.Utilities.Streams.print("h_PM="+Modelica.Math.Vectors.toString(vector(h_PM*1000)),"");
    //
    //    Modelica.Utilities.Streams.print("l="+Modelica.Math.Vectors.toString(vector(l*1000)),"");
    //    Modelica.Utilities.Streams.print("g="+Modelica.Math.Vectors.toString(vector(g*1000)),"");

    annotation (
        Documentation(info="<html>

<head>

<title>General SMPMSM Pre-sizing help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<h2><span lang=EN-US>1.1<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Table of inputs</span></h2>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Name</span></b></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border:solid windowtext 1.0pt;
  border-left:none;background:#8DB3E2;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Description</span></b></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border:solid windowtext 1.0pt;
  border-left:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Units</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>U_ph</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Phase Voltage</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>V</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>m</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of phases</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>a</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of parallel paths</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>p</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of pair poles</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>P_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  mechanical Power</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>W</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>n</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Speed</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>rpm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>f</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Frequency</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Hz</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>T</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Torque</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Nm</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>I_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Rated
  Current</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A<sub>rms</sub></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>eff</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Efficiency</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>PF</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Power
  Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_tooth</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Stator Tooth</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_yoke</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Yoke</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Max
  Flux Density at Airgap</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>J_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Current
  density at stator</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A<sub>rms</sub>/mm<sup>2</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>g</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Airgap</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>Q_s</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of Stator Slots</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>n_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Number
  of ventilating ducts</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>b_v</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Ventilating
  Ducts width</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>F_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Lamination
  factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>K_u</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Slot
  Filling Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>K_w</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Winding
  Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>C_mec</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Mechanical
  Constant</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>kWs/m<sup>3</sup></span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>X</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Aspect
  Ratio</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>coilspan</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Coil
  Span</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>Material.txt</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Stator/Rotor
  B-H material characteristic curve</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T-A/m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>B_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Magnet
  Remanent Flux</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>T</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>H_c</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Magnet
  Coercitive Force</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>A/m</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>mu_r</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Relative
  Permeability</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
 <tr>
  <td width=120 valign=top style='width:90.05pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla><span lang=EN-US>mcf_PM</span></p>
  </td>
  <td width=282 valign=top style='width:211.8pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>Magnet
  Cover Factor</span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US style='font-family:'Times New Roman','serif''>-</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption align=center style='text-align:center'><a
name='_Toc414019771'></a><a name='_Toc385322413'></a><a name='_Toc385318064'></a><a
name='_Ref413740534'>Table </a>1. SMPMSM List of inputs: variable name,
description and units in IS</p>

<h2><span lang=EN-US>1.2<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Algorithm concept</span></h2>

<p class=MsoNormal><span lang=EN-US>Algorithm is based on the generation of
both magnetic parts (stator and rotor) each one able to provide the desired
airgap flux density (</span>B_g_ini<span lang=EN-US>)</span>. Stator sizing is
generated similarly to IM machine. Mechanical constant is used to obtain the
bore diameter. Using airgap flux density and phase voltage the number of turns
is determined. In this moment a loop is opened recalculating stator and
dimensions first and then magnetic voltage drops. To find the permeabilities in
order to solve a single pole circuity in d-axis position based on machines symmetry.
 Then the loop gradually increase or decrease the magnet thickness (radial
height) in order to provide the required input flux density in airgap and
taking into account the stator MMF forces previously calculated.</p>

<h2><span lang=EN-US>1.3<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Program Structure Explanation</span></h2>

<p class=MsoNormal>Please follow Figure 1 for a better comprehention. After
input data has been performed several pre-sizing data is calculated: the number
of slots per pole (q) and the winding factor (k_w), this last one by means of
an auxiliary funcions. Then Initial values for airgap flux density (B_g_ini) and
magnet height (h_PM) are set. At this point depending on machine power htrmodifflag
is set. And efficiency (eff), mechanical constant (C_mec), and airgap (g) are
calculated if it input was 0. </p>

<p class=MsoNormal>Mechanical constant (C_mec) and electrical loading (A_load) provides
first values for diameter and length. Once this has been determined pitches for
stator and rotor (t_u, t_p respectively) are also set as well as magnet cover
length (l_p). This leads to obtain the value of the desired flux at the airgap
and the number of turns per phase in series (N_ph), value that is immediately rounded
and provides the number of conductors per slot (z_Q).</p>

<p class=MsoNormal>With this initial information the main loop is started. In
each iteration magnet lengt (l_p), calculated at half of magnets thickness
(radial height, h_PM) will determine the current airgap flux density and
therefore the airgap flux (flux_g). This one is used to size stator tooth,
slots and yoke. Slot shape compensation is is performed at this point by means
of htrmodif() function. Then field of each part is calculated and its
permeabilities (mu based on desired input flux densities. Once this is done the
final step is to calculate circuit reluctance and check if magnet is able to
sustain the desired flux. Finally, depending on the new value of flux density
magnet’s thickness (h_PM) is modified. More magnet thicknes if flux density is
less than required or less magnet if flux density is avobe the desired value.</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><img
width=310 height=973 src='General_SMPMSM_archivos/image001.png'></p>

<p class=MsoCaption align=center style='text-align:center'>Figure 1. Global Sizing Algorithm explanation.</p>

</div>

</body>

</html>
"));
    end PMSM_Motor_Sizing_Simple;
  end PredesignAlgorithms;

  package Demos
    function IPM_Demo_Sizing_IPM_23
      extends Sizing.PredesignAlgorithms.IPM_Motor_Sizing(
        alpha_v(min=0,max=360)=160.21,
        Shape(min=0,max=2)=1,
        U_ph(min=0)=161,
        m(min=0)=3,
        p=4,
        n_rpm=1200,
        PF=0.973,
        T=400,
        I_s=200/sqrt(2),
        C_mec_ini=925,
        C_mec_family_flag=false,
        J_s=8.5*10^6,
        g=0.0006,
        B_g_ini=1.05,
        Stator_Material_File="M270_35A.txt",
        Ksat_alpha_File="Ksat_alpha.txt",
        B_yoke=1.41,
        B_tooth=1.58,
        a=1,
        X=0.411,
        Q_s=48,
        f_r=0.95,
        K_u=0.95,
        gamma=0,
        b_1=2.5e-3,
        D_shaft_internal_ref=0.112,
        Internal_shaft_coefficient=1.3,
        D_shaftB_ref=0.055,
        D_hub_ref=0.120,
        l_shaft_ref=0,
        l_shaft_extra_ref=50/1000,
        case_type=1,
        B_r=1.2,
        H_c=-0.95e6,
        mu_r_PM=1.0052,
        mcf_PM=0.82688,
        h_i=0.0002,
        DeltaD=0.001,
        SlotAir=0.001,
        ro_mst=7460,
        ro_PM=7400,
        ro_cu=8750,
        ro_case=7850,
        ro_hub=7460,
        ro_shaft=7460,
        K_h=50,
        K_e=0.05,
        n_i=1.6,
        R_fe_consider=0,
        t_aro=20,
        ro_t_aro=1.68E-8,
        t_amb=35,
        Delta_t_work=55,
        K_uhc=0.8,
        l_extraH_ref=0.008,
        l_extraW_ref=0.015,
        Embedded(min=0,max=1)=0);

    end IPM_Demo_Sizing_IPM_23;
  end Demos;

  package IPM_Structure "Structure of IPM motor parameters"
    record motorIPM "Record of Motor"
      import Modelica.SIunits;

      SIunits.Power RatedPower "Rated power";
      motorIPM_airgap airgap;
      motorIPM_casing casing;
      Integer classe "Motor class";
      motorIPM_flags flags;
      SIunits.Length g "Airgap length";
      Integer m "Number of phases";
      Integer p "Number of pole pairs";
      motorIPM_plate plate;
      motorIPM_rotor rotor;
      motorIPM_specs specs;
      motorIPM_stator stator;
      String typee "Motor type";
    end motorIPM;

    record motorIPM_airgap "Airgap record"
      airgap_phy phy;
    end motorIPM_airgap;

    record airgap_phy
      import Modelica.SIunits;
      SIunits.Reluctance R_g "Airgap reluctance";
      SIunits.MagneticFluxDensity B_g "Airgap flux density";
      SIunits.MagneticFlux flux_g "Airgap flux";
    end airgap_phy;

    record motorIPM_casing
      import Modelica.SIunits;
      casing_dim dim;
      SIunits.Length l_total "Total case length";
      SIunits.Length h_total "Total case height";
    end motorIPM_casing;

    record casing_dim
      import Modelica.SIunits;
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      dim_phy phy;
      SIunits.Mass M_S "Stator mass";
      SIunits.MomentOfInertia I_S "Stator Yoke Ring Moment of Inertia";

    end casing_dim;

    record dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Temperature t_aro "Reference resistivity temperature";
      SIunits.Temperature t_amb "Ambient temperature";
      SIunits.Temperature Delta_t_work
        "Temperature increase relative to ambient";
    end dim_phy;

    record motorIPM_flags
      import Modelica.SIunits;
      Integer R_fe_consider;
    end motorIPM_flags;

    record motorIPM_plate
      import Modelica.SIunits;
      SIunits.Power RatedPower "Rated power";
      SIunits.Current RatedCurrent "Rated current";
      SIunits.Voltage RatedVoltage "Rated voltage";
      String RatedVoltageType "Rated voltage type: phase, line";
      SIunits.Voltage BEMF "Back EMF at rated speed, no load RMS";
      SIunits.Frequency RatedSpeed "Rated speed";
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm RatedSpeedRPM
        "Rated speed in rpm";
      SIunits.Torque RatedTorque "Rated torque";
      Real PowerFactor "Power factor";
      Real Efficiency "Efficiency";
      SIunits.Mass M "Total mass";
      SIunits.Mass TmM "Total magnetic mass";
      SIunits.Frequency RatedFrequency;
    end motorIPM_plate;

    record motorIPM_rotor
      import Modelica.SIunits;
      Integer shape "Shape type";
      Integer fail "Flag indicating if it's possible to calculate shape";
      Integer failpath
        "Flag indicating if it's possible to calculate path using ellipse";
      Real K_fc "Factor of flux concentration";
      rotor_msg msg;
      rotor_dim dim;
      rotor_pos pos;
      rotor_draw draw;
      rotor_phy phy;
    end motorIPM_rotor;

    record rotor_msg
      import Modelica.SIunits;
      String failmessage "Fail message";
    end rotor_msg;

    record rotor_dim
      import Modelica.SIunits;
      SIunits.Length l_PM_max "Maximum nagent allowe length for the h_PM";
      SIunits.Length h_PM "Magnet thickness";
      SIunits.Length l_PM "Magnet's length for h_PM";
      SIunits.Length DeltaD "Distance between magnet pod and rotor surface";
      SIunits.Length actualPMSlotAir "PMSlotAir after Slot Creation";
      SIunits.Length h_Tb "Top barrier reluctance height";
      SIunits.Length l_Tb "Top barrier reluctance length";
      SIunits.Length h_Tf "Top iron reluctance height";
      SIunits.Length l_Tf "Top iron reluctance length";
      SIunits.Length h_Bb "Bottom air barrier reluctance height";
      SIunits.Length l_Bb "Bottom air barrier reluctance length";
      SIunits.Length h_Hub "Bottom Non-Magnetic Iron Hub Reluctance height";
      SIunits.Length l_Hub "Bottom Non-Magnetic Iron Hub Reluctance length";
      SIunits.Length l_fe_first "First Rotor  Iron Reluctance length";
      SIunits.Length h_fe_first "First Rotor  Iron Reluctance height";
      SIunits.Length l_fe_last "Last Rotor Iron Reluctance length";
      SIunits.Length l_web "Web length Between Consecutive Pole Magnets";
      SIunits.Length D_or "rotor outer diameter";
      SIunits.Length D_ir "rotor inner diameter";
      SIunits.Length D_shaft "Shaft internal diameter";
      SIunits.Length D_shaftB "Shaft external diameter";
      SIunits.Length D_hub "Rotor magnet hub barrier diameter";
      Real Omega "Web angle cover factor";
      Real mcf_PM "Magnet cover factor";
      SIunits.Length l_shaft_extra "Shaft extra length";
      SIunits.Length l_shaft "Total shaft length";
      rotor_dim_phy phy;
      SIunits.Mass M_mst_R "Rotor magnetic steel mass";
      SIunits.Mass M_R "Total rotor mass";
      SIunits.MomentOfInertia I_R "Rotor Inertia";

    end rotor_dim;

    record rotor_dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_mst "Magnetic steel density";
      SIunits.Density ro_PM "Magnet density";
      SIunits.Density ro_hub "Hub insulation steel density";
      SIunits.Density ro_shaft "Shaft steel density";
    end rotor_dim_phy;

    record rotor_pos
      import Modelica.SIunits;
      Real[2,1] TopAirCDG "Top air area in XX YY coords";
      SIunits.Area TopAirArea "Top air area";
      Real[2,1] MagnetCDG "Magnet center coords in XX YY";
      SIunits.Area MagnetArea "Magnet area";
      Real[2,1] BottomAirCDG "Bottom air cords in XX YY";
      SIunits.Area BottomAirArea "Bottom air area";
      SIunits.Area CartdrigeArea "Total magnet slot into the steel area";
    end rotor_pos;

    record rotor_draw
      import Modelica.SIunits;
      Real[:,:] rotor_Hub_p "Rotor hub";
      Integer magnetSlotPM_style "Magnet slot style";
      Integer magneticSteel_style "Magnetic steel style";
      Integer magnet_style "Magnet style";
      Integer rotorHub_style "Rotor hub style";
      Integer shaft_style "Shaft style";
      Integer fail_elements "Fail elements";
      Integer rotorSteel_style "Rotor steel style";
      Real[:,:] magnetSlotPM_p "Magnet slot points";
      Real[:,:] magnet_p "Magnet points";
      Real[:,:] magnet_pN "Magnet points";
      Real[:,:] magnet_pS "Magnet points";
      rotor_draw_fail fail;

    end rotor_draw;

    record rotor_draw_fail
      import Modelica.SIunits;

      Real[:,:] fail_element1;
      Real[:,:] fail_element2;
    end rotor_draw_fail;

    record rotor_phy
      import Modelica.SIunits;
      SIunits.Reluctance R_PM "PM reluctance";
    end rotor_phy;

    record motorIPM_specs
      import Modelica.SIunits;
      Real f_r "Lamination factor";
      Real[:,:] evolutiondata;
      SIunits.Length hpmlimiter "Maximum allowed magnet thickness";
      Real[:,:,:] TSeff_lut "Torque vs Speed vs Efficiency table";
      Real[:,:] MTPA_lut "Maximum torque per ampere";

    end motorIPM_specs;

    record motorIPM_stator
      import Modelica.SIunits;
      Integer case_type "Case type";
      Integer Q_s "Number of stator slots";
      Real D;
      SIunits.Current I_s "Rated current";
      SIunits.CurrentDensity J_s "Stator current density";
      stator_win win;
      stator_dim dim;
      stator_phy phy;
    end motorIPM_stator;

    record stator_win
      import Modelica.SIunits;
      Integer a "Parallel paths";
      Real N_ph "Number of turns per phase, integer";
      Real K_w "Winding factor";
      Real K_uhc "Head Coil filling factor";
      Real K_u "Slot filling factor";
      Integer coilspan "Coilspan";
      SIunits.Area S_cond "Conductor approximate cross section";
      win_dim dim;
      win_phy phy;
    end stator_win;

    record win_dim
      import Modelica.SIunits;
      SIunits.Length l_extraW "Head Coil winding longitudinal extra length";
      SIunits.Length l_extraH "Head Coil longitudinal extra length";
      SIunits.Length l_extra "Head coil length";
    end win_dim;

    record win_phy
      import Modelica.SIunits;
      SIunits.Resistivity ro_t_aro "Cu resistivity";
    end win_phy;

    record stator_dim
      import Modelica.SIunits;
      Real K_c "Carter fringing coefficient";
      SIunits.Length D_os "Outer stator diameter";
      SIunits.Length D_is "Inner stator diameter";
      SIunits.Length l "Length";
      SIunits.Length l_eff "Effective length";
      Real X "Aspect ratio";
      SIunits.Length h_z "Stator tooth height";
      SIunits.Length b_z "Stator tooth width";
      SIunits.Length h_tr "Stator slot height";
      SIunits.Length b_s "Stator slot width";
      SIunits.Length h_y "Stator yoke height";
      SIunits.Length b_1 "Stator slot opening";
      SIunits.Length delta_htr "Height of teeth expansion";
      stator_dim_phy phy;
      SIunits.Mass mM "Stator magnetic mass";
      SIunits.Mass cM "Total copper mass";
      SIunits.Length l_turn;
    end stator_dim;

    record stator_dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_mst "Magnetic Steel density";
      SIunits.Density ro_cu "Cu density";
      stator_dim_phy_mst mst;
      SIunits.Resistivity ro_work "Resistivity at work temperature";

    end stator_dim_phy;

    record stator_dim_phy_mst
      import Modelica.SIunits;
      Real K_h "Magnetic Steel Hysteresis Constant";
      Real K_e "Magnetic Steel Eddy Constant";
      Real n_i "Steinmetz Constant";
    end stator_dim_phy_mst;

    record stator_phy
      import Modelica.SIunits;
      SIunits.Permeance p_lf "Magnet permeance leakage factor";
      SIunits.Length l_turn "Average turn length";
      SIunits.Resistance R_s "Phase resistance";
      SIunits.Temperature t_work "Cu working temperature";
      SIunits.Inductance L_m0
        "Surface Magnet Synchornous Magnetizing Equivalent Inductance [H]";
      SIunits.Inductance L_qm "Magnetizing inductance in q";
      SIunits.Inductance L_dm "Magnetizing inductance in d";

    end stator_phy;

    function IPM_Export "Exports structure to mat rows&columns"
     parameter input IPM_Structure.motorIPM motorIPM;

    algorithm
      // RatedPower
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_RatedPower"},[motorIPM.RatedPower],false);

      // airgap
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_airgap_phy_R_g"},[motorIPM.airgap.phy.R_g],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_airgap_phy_B_g"},[motorIPM.airgap.phy.B_g],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_airgap_phy_flux_g"},[motorIPM.airgap.phy.flux_g],true);

      // casing
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_l_case_exp_front"},[motorIPM.casing.dim.l_case_exp_front],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_l_case_exp_rear"},[motorIPM.casing.dim.l_case_exp_rear],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_l_case_exp_void"},[motorIPM.casing.dim.l_case_exp_void],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_l_case"},[motorIPM.casing.dim.l_case],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_h_case"},[motorIPM.casing.dim.h_case],true);

      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_phy_ro_case"},[motorIPM.casing.dim.phy.ro_case],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_phy_t_aro"},[motorIPM.casing.dim.phy.t_aro],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_phy_t_amb"},[motorIPM.casing.dim.phy.t_amb],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_phy_Delta_t_work"},[motorIPM.casing.dim.phy.Delta_t_work],true);

      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_M_S"},[motorIPM.casing.dim.M_S],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_dim_I_S"},[motorIPM.casing.dim.I_S],true);

      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_l_total"},[motorIPM.casing.l_total],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_casing_h_total"},[motorIPM.casing.h_total],true);
      // class
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_classe"},[motorIPM.classe],true);

      // flags
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_flags_R_fe_consider"},[motorIPM.flags.R_fe_consider],true);

      // g
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_g"},[motorIPM.g],true);

      // m
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_m"},[motorIPM.m],true);

      // p
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_p"},[motorIPM.p],true);

      // plate
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedPower"},[motorIPM.plate.RatedPower],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedCurrent"},[motorIPM.plate.RatedCurrent],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedVoltage"},[motorIPM.plate.RatedVoltage],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_BEMF"},[motorIPM.plate.BEMF],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedSpeed"},[motorIPM.plate.RatedSpeed],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedSpeedRPM"},[motorIPM.plate.RatedSpeedRPM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedTorque"},[motorIPM.plate.RatedTorque],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_PowerFactor"},[motorIPM.plate.PowerFactor],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_Efficiency"},[motorIPM.plate.Efficiency],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_M"},[motorIPM.plate.M],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_TmM"},[motorIPM.plate.TmM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_plate_RatedFrequency"},[motorIPM.plate.RatedFrequency],true);

      // rotor
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_shape"},[motorIPM.rotor.shape],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_fail"},[motorIPM.rotor.fail],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_failpath"},[motorIPM.rotor.failpath],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_K_fc"},[motorIPM.rotor.K_fc],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_PM_max"},[motorIPM.rotor.dim.l_PM_max],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_h_PM"},[motorIPM.rotor.dim.h_PM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_PM"},[motorIPM.rotor.dim.l_PM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_DeltaD"},[motorIPM.rotor.dim.DeltaD],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_actualPMSlotAir"},[motorIPM.rotor.dim.actualPMSlotAir],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_h_Tb"},[motorIPM.rotor.dim.h_Tb],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_Tb"},[motorIPM.rotor.dim.l_Tb],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_h_Tf"},[motorIPM.rotor.dim.h_Tf],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_Tf"},[motorIPM.rotor.dim.l_Tf],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_h_Bb"},[motorIPM.rotor.dim.h_Bb],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_Bb"},[motorIPM.rotor.dim.l_Bb],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_h_Hub"},[motorIPM.rotor.dim.h_Hub],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_Hub"},[motorIPM.rotor.dim.l_Hub],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_fe_first"},[motorIPM.rotor.dim.l_fe_first],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_h_fe_first"},[motorIPM.rotor.dim.h_fe_first],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_fe_last"},[motorIPM.rotor.dim.l_fe_last],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_web"},[motorIPM.rotor.dim.l_web],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_D_or"},[motorIPM.rotor.dim.D_or],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_D_ir"},[motorIPM.rotor.dim.D_ir],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_D_shaft"},[motorIPM.rotor.dim.D_shaft],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_D_shaftB"},[motorIPM.rotor.dim.D_shaftB],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_D_hub"},[motorIPM.rotor.dim.D_hub],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_Omega"},[motorIPM.rotor.dim.Omega],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_mcf_PM"},[motorIPM.rotor.dim.mcf_PM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_shaft_extra"},[motorIPM.rotor.dim.l_shaft_extra],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_l_shaft"},[motorIPM.rotor.dim.l_shaft],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_phy_ro_mst"},[motorIPM.rotor.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_phy_ro_PM"},[motorIPM.rotor.dim.phy.ro_PM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_phy_ro_hub"},[motorIPM.rotor.dim.phy.ro_hub],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_phy_ro_shaft"},[motorIPM.rotor.dim.phy.ro_shaft],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_M_mst_R"},[motorIPM.rotor.dim.M_mst_R],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_M_R"},[motorIPM.rotor.dim.M_R],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_dim_I_R"},[motorIPM.rotor.dim.I_R],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_TopAirCDG"},[motorIPM.rotor.pos.TopAirCDG],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_TopAirArea"},[motorIPM.rotor.pos.TopAirArea],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_MagnetCDG"},[motorIPM.rotor.pos.MagnetCDG],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_MagnetArea"},[motorIPM.rotor.pos.MagnetArea],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_BottomAirCDG"},[motorIPM.rotor.pos.BottomAirCDG],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_BottomAirArea"},[motorIPM.rotor.pos.BottomAirArea],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_pos_CartdrigeArea"},[motorIPM.rotor.pos.CartdrigeArea],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_rotor_Hub_p"},[motorIPM.rotor.draw.rotor_Hub_p],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magnetSlotPM_style"},[motorIPM.rotor.draw.magnetSlotPM_style],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magneticSteel_style"},[motorIPM.rotor.draw.magneticSteel_style],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magnet_style"},[motorIPM.rotor.draw.magnet_style],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_rotorHub_style"},[motorIPM.rotor.draw.rotorHub_style],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_shaft_style"},[motorIPM.rotor.draw.shaft_style],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_fail_elements"},[motorIPM.rotor.draw.fail_elements],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_rotorSteel_style"},[motorIPM.rotor.draw.rotorSteel_style],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magnetSlotPM_p"},[motorIPM.rotor.draw.magnetSlotPM_p],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magnet_p"},[motorIPM.rotor.draw.magnet_p],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magnet_pN"},[motorIPM.rotor.draw.magnet_pN],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_magnet_pS"},[motorIPM.rotor.draw.magnet_pS],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_phy_R_PM"},[motorIPM.rotor.phy.R_PM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_fail_fail_element1"},[motorIPM.rotor.draw.fail.fail_element1],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_rotor_draw_fail_fail_element2"},[motorIPM.rotor.draw.fail.fail_element2],true);

      // specs
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_specs_f_r"},[motorIPM.specs.f_r],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_specs_evolutiondata"},[motorIPM.specs.evolutiondata],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_specs_hpmlimiter"},[motorIPM.specs.hpmlimiter],true);
      //DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_specs_TSeff_lut"},[motorIPM.specs.TSeff_lut[:,:,:]],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_specs_MTPA_lut"},[motorIPM.specs.MTPA_lut],true);

      // stator
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_case_type"},[motorIPM.stator.case_type],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_Q_s"},[motorIPM.stator.Q_s],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_D"},[motorIPM.stator.D],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_I_s"},[motorIPM.stator.I_s],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_J_s"},[motorIPM.stator.J_s],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_a"},[motorIPM.stator.win.a],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_N_ph"},[motorIPM.stator.win.N_ph],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_K_w"},[motorIPM.stator.win.K_w],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_K_uhc"},[motorIPM.stator.win.K_uhc],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_K_u"},[motorIPM.stator.win.K_u],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_coilspan"},[motorIPM.stator.win.coilspan],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_S_cond"},[motorIPM.stator.win.S_cond],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_dim_l_extraW"},[motorIPM.stator.win.dim.l_extraW],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_dim_l_extraH"},[motorIPM.stator.win.dim.l_extraH],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_dim_l_extra"},[motorIPM.stator.win.dim.l_extra],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_win_phy_ro_t_aro"},[motorIPM.stator.win.phy.ro_t_aro],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_K_c"},[motorIPM.stator.dim.K_c],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_D_os"},[motorIPM.stator.dim.D_os],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_D_is"},[motorIPM.stator.dim.D_is],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_l"},[motorIPM.stator.dim.l],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_l_eff"},[motorIPM.stator.dim.l_eff],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_X"},[motorIPM.stator.dim.X],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_h_z"},[motorIPM.stator.dim.h_z],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_b_z"},[motorIPM.stator.dim.b_z],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_h_tr"},[motorIPM.stator.dim.h_tr],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_b_s"},[motorIPM.stator.dim.b_s],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_h_y"},[motorIPM.stator.dim.h_y],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_b_1"},[motorIPM.stator.dim.b_1],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_delta_htr"},[motorIPM.stator.dim.delta_htr],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_phy_ro_mst"},[motorIPM.stator.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_phy_ro_cu"},[motorIPM.stator.dim.phy.ro_cu],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_phy_mst_K_h"},[motorIPM.stator.dim.phy.mst.K_h],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_phy_mst_K_e"},[motorIPM.stator.dim.phy.mst.K_e],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_phy_mst_n_i"},[motorIPM.stator.dim.phy.mst.n_i],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_phy_ro_work"},[motorIPM.stator.dim.phy.ro_work],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_mM"},[motorIPM.stator.dim.mM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_cM"},[motorIPM.stator.dim.cM],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_dim_l_turn"},[motorIPM.stator.dim.l_turn],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_phy_p_lf"},[motorIPM.stator.phy.p_lf],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_phy_R_s"},[motorIPM.stator.phy.R_s],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_phy_t_work"},[motorIPM.stator.phy.t_work],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_phy_L_m0"},[motorIPM.stator.phy.L_m0],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_phy_L_qm"},[motorIPM.stator.phy.L_qm],true);
      DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_stator_phy_L_dm"},[motorIPM.stator.phy.L_dm],true);

      // typee

      // Export strings
      Modelica.Utilities.Files.remove("MotorIPM_Structure.txt");
      Modelica.Utilities.Streams.print("motorIPM_plate_RatedVoltageType","MotorIPM_Structure.txt");
      Modelica.Utilities.Streams.print(motorIPM.plate.RatedVoltageType,"MotorIPM_Structure.txt");

      //DataFiles.writeMATmatrix("MotorIPM_Structure.mat",motorIPM.rotor.msg.failmessage,[1],true);
      Modelica.Utilities.Streams.print("motorIPM_rotor_msg_failmessage","MotorIPM_Structure.txt");
      Modelica.Utilities.Streams.print(motorIPM.rotor.msg.failmessage,"MotorIPM_Structure.txt");
      //DataFiles.writeMATmatrix("MotorIPM_Structure.mat",{"motorIPM_typee"},[motorIPM.typee],true);
      Modelica.Utilities.Streams.print("motorIPM_typee","MotorIPM_Structure.txt");
      Modelica.Utilities.Streams.print(motorIPM.typee,"MotorIPM_Structure.txt");

    end IPM_Export;
  end IPM_Structure;

  package SRM_Structure "Structure of SRM motor parameters"
    record motorSRM "Record of Motor"
      import Modelica.SIunits;

      Integer m "Number of simultaneously conducting phases";
      Integer q "Number of phases";
      SIunits.Length g "Airgap";
      SIunits.Power RatedPower "Rated power";
      Integer classe "Motor class";
      motorSRM_plate plate;
      motorSRM_specs specs;
      motorSRM_stator stator;
      motorSRM_rotor rotor;
      motorSRM_casing casing;
    end motorSRM;

    record motorSRM_plate
      import Modelica.SIunits;
      SIunits.Power RatedPower "Rated power";
      SIunits.Current RatedCurrent "Rated current";
      SIunits.Voltage RatedVoltage "Rated voltage";
      String RatedVoltageType "Rated voltage type: phase, line";
      //SIunits.Conversions.NonSIunits.AngularVelocity_rpm RatedSpeed "Rated speed";
      SIunits.Voltage BEMF "Back electromotive force";
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm RatedSpeedRPM
        "Rated speed in rpm";
      SIunits.Torque Torque "Rated torque";
      Real PowerFactor "Power factor";
      Real Efficiency "Efficiency";
      SIunits.Mass M "Total mass";
      SIunits.Mass TmM "Total magnetic mass";
      //SIunits.Frequency RatedFrequency;
    end motorSRM_plate;

    record motorSRM_specs
      import Modelica.SIunits;
      Real f_r "Lamination factor";
      Real X "Aspect ratio";
      Real Kd "Duty cycle";
      Real K1 "Constant #1";
      Real K2 "Constant #2";
      Real A_load "Electrical loading";
      Real coil_security_coefficient "Coil security coeficient";
      SIunits.Reluctance RR_tot "Total aligned reluctance";
      Integer J_counter "Current density counter";
      Integer[2] Feasible "Feasible geometry info";

    end motorSRM_specs;

    record motorSRM_casing
      import Modelica.SIunits;
      casing_dim dim;
      SIunits.Length l_total "Total case length";
      SIunits.Length h_total "Total case height";
    end motorSRM_casing;

    record casing_dim
      import Modelica.SIunits;
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      dim_phy phy;
      SIunits.Mass M_S "Stator mass";
      SIunits.MomentOfInertia I_S "Stator Yoke Ring Moment of Inertia";

    end casing_dim;

    record dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Temperature t_aro "Reference resistivity temperature";
      SIunits.Temperature t_amb "Ambient temperature";
      SIunits.Temperature Delta_t_work
        "Temperature increase relative to ambient";
    end dim_phy;

    record motorSRM_rotor
      import Modelica.SIunits;
      Integer shape "Shape type";
      Integer fail "Flag indicating if it's possible to calculate shape";
      Integer failpath
        "Flag indicating if it's possible to calculate path using ellipse";
      String msg "Message";
      rotor_dim dim;

    end motorSRM_rotor;

    record rotor_dim
      import Modelica.SIunits;
      Integer N_r "Rotor pole";
      SIunits.Angle beta_r "Rotor pole arc";
      SIunits.Length D_or "Diameter outer rotor";
      SIunits.Length D_ir "Diameter inner rotor";
      SIunits.Length D_shaft;
      SIunits.Length D_shaftB;
      SIunits.Length D_hub;
      SIunits.Length b_rp "Rotor pole width";
      SIunits.Length h_rp "Rotor pole height";
      SIunits.Length b_ry "Rotor yoke width";
      SIunits.Length l_shaft_extra "External shaft length";
      SIunits.Length l_shaft "Total shaft length";
      rotor_dim_phy phy;
      SIunits.Mass M_mst_R "Rotor's magnetic steel mass";
      SIunits.Mass M_R "Rotor Mass";
      SIunits.MomentOfInertia I_R "Rotor inertia";

    end rotor_dim;

    record rotor_dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_mst "Magnetic steel density";
      SIunits.Density ro_shaft "Shaft steel density";
    end rotor_dim_phy;

    record motorSRM_stator
      import Modelica.SIunits;
      Integer case_type "Case type";
      Integer N_s "Stator poles";
      SIunits.Length D "Bore diameter (inner stator diameter)";
      SIunits.Current I_s "Rated current";
      SIunits.CurrentDensity J_s_desired "Stator current density";
      SIunits.CurrentDensity J_s_real "Rounded current density";
      stator_win win;
      stator_dim dim;
      stator_phy phy;
    end motorSRM_stator;

    record stator_win
      import Modelica.SIunits;
      Integer a "Parallel paths";
      Real N_ph "Number of turns per phase, integer";
      Real K_f "Filling factor of the assigned area";
      Real K_f_real "Filling factor by the actual used area";
      win_dim dim;
      win_draw draw;
    end stator_win;

    record win_dim
      import Modelica.SIunits;
      SIunits.Length b_sc "Stator coil width";
      SIunits.Length h_sc "Stator coil height";
      Real b_seg "Spacing between consecutive coils";
      SIunits.Length D_cond "Conductor diameter";
      SIunits.Length l_extra "Extra head coil length";
      SIunits.Length l_extraW "Head Coil winding longitudinal extra length";
      SIunits.Length l_extraH "Head Coil longitudinal extra length";
    end win_dim;

    record win_draw
      import Modelica.SIunits;
      Real[:,:] Semi_coil_points "Semi coil drawing points";
      Real[:,:] Semi_coil_real_points "Real semi coil drawing points";
      Real[:,:] Semi_coil_centers "Center location";

    end win_draw;

    record stator_dim
      import Modelica.SIunits;
      SIunits.Angle beta_s "Stator pole arc";
      SIunits.Length D_os "Diameter outer stator";
      SIunits.Length D_is "Bore diameter (inner stator diameter)";
      SIunits.Length b_sp "Stator pole width";
      SIunits.Length h_sp "Stator pole height";
      SIunits.Length b_sy "Stator yoke width";
      SIunits.Length l "Length";
      stator_dim_phy phy;
      SIunits.Mass mM "Stator magnetic mass";
      SIunits.Mass cM "Total copper mass";

    end stator_dim;

    record stator_dim_phy
      import Modelica.SIunits;
      SIunits.Resistivity ro_t_aro "Cu Resistivity";
      SIunits.Density ro_mst "Magnetic steel density";
      SIunits.Density ro_cu "Cu density";

    end stator_dim_phy;

    record stator_phy
      import Modelica.SIunits;
      SIunits.Resistance R_sc_t_aro "Coil resistance at ambient temperature";
      SIunits.Conversions.NonSIunits.Temperature_degC t_work
        "Cu working temperature";
      SIunits.Resistance R_ph "Phase resistance";
      SIunits.Inductance L_a "Aligned inductance";
      SIunits.Inductance L_u "Unaligned inductance";

    end stator_phy;

    function SRM_Export "Exports structure to mat rows&columns"
      import Sizing;
     parameter input Sizing.SRM_Structure.motorSRM motorSRM;

    algorithm
      // RatedPower
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_RatedPower"},[motorSRM.RatedPower],false);

      // casing

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_phy_Delta_t_work"},[motorSRM.casing.dim.phy.Delta_t_work],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_phy_t_amb"},[motorSRM.casing.dim.phy.t_amb],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_phy_t_aro"},[motorSRM.casing.dim.phy.t_aro],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_phy_ro_case"},[motorSRM.casing.dim.phy.ro_case],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_l_case_exp_front"},[motorSRM.casing.dim.l_case_exp_front],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_l_case_exp_rear"},[motorSRM.casing.dim.l_case_exp_rear],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_l_case_exp_void"},[motorSRM.casing.dim.l_case_exp_void],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_l_case"},[motorSRM.casing.dim.l_case],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_h_case"},[motorSRM.casing.dim.h_case],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_M_S"},[motorSRM.casing.dim.M_S],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_dim_I_S"},[motorSRM.casing.dim.I_S],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_l_total"},[motorSRM.casing.l_total],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_casing_h_total"},[motorSRM.casing.h_total],true);

      // class
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_classe"},[motorSRM.classe],true);

      // g
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_g"},[motorSRM.g],true);

      // m
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_m"},[motorSRM.m],true);

      // plate
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_RatedPower"},[motorSRM.plate.RatedPower],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_RatedCurrent"},[motorSRM.plate.RatedCurrent],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_RatedVoltage"},[motorSRM.plate.RatedVoltage],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_BEMF"},[motorSRM.plate.BEMF],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_RatedSpeedRPM"},[motorSRM.plate.RatedSpeedRPM],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_Torque"},[motorSRM.plate.Torque],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_PowerFactor"},[motorSRM.plate.PowerFactor],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_Efficiency"},[motorSRM.plate.Efficiency],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_M"},[motorSRM.plate.M],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_plate_TmM"},[motorSRM.plate.TmM],true);

      // q
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_q"},[motorSRM.q],true);

      // rotor
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_shape"},[motorSRM.rotor.shape],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_fail"},[motorSRM.rotor.fail],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_failpath"},[motorSRM.rotor.failpath],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_N_r"},[motorSRM.rotor.dim.N_r],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_beta_r"},[motorSRM.rotor.dim.beta_r],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_D_or"},[motorSRM.rotor.dim.D_or],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_D_ir"},[motorSRM.rotor.dim.D_ir],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_D_shaft"},[motorSRM.rotor.dim.D_shaft],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_D_shaftB"},[motorSRM.rotor.dim.D_shaftB],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_D_hub"},[motorSRM.rotor.dim.D_hub],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_b_rp"},[motorSRM.rotor.dim.b_rp],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_h_rp"},[motorSRM.rotor.dim.h_rp],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_b_ry"},[motorSRM.rotor.dim.b_ry],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_l_shaft_extra"},[motorSRM.rotor.dim.l_shaft_extra],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_l_shaft"},[motorSRM.rotor.dim.l_shaft],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_phy_ro_mst"},[motorSRM.rotor.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_phy_ro_shaft"},[motorSRM.rotor.dim.phy.ro_shaft],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_M_mst_R"},[motorSRM.rotor.dim.M_mst_R],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_M_R"},[motorSRM.rotor.dim.M_R],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_rotor_dim_I_R"},[motorSRM.rotor.dim.I_R],true);

      // specs
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_f_r"},[motorSRM.specs.f_r],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_X"},[motorSRM.specs.X],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_Kd"},[motorSRM.specs.Kd],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_K1"},[motorSRM.specs.K1],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_K2"},[motorSRM.specs.K2],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_A_load"},[motorSRM.specs.A_load],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_coil_security_coefficient"},[motorSRM.specs.coil_security_coefficient],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_RR_tot"},[motorSRM.specs.RR_tot],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_specs_J_counter"},[motorSRM.specs.J_counter],true);

      // stator
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_case_type"},[motorSRM.stator.case_type],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_N_s"},[motorSRM.stator.N_s],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_D"},[motorSRM.stator.D],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_I_s"},[motorSRM.stator.I_s],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_J_s_desired"},[motorSRM.stator.J_s_desired],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_J_s_real"},[motorSRM.stator.J_s_real],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_beta_s"},[motorSRM.stator.dim.beta_s],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_D_os"},[motorSRM.stator.dim.D_os],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_D_is"},[motorSRM.stator.dim.D_is],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_b_sp"},[motorSRM.stator.dim.b_sp],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_h_sp"},[motorSRM.stator.dim.h_sp],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_b_sy"},[motorSRM.stator.dim.b_sy],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_l"},[motorSRM.stator.dim.l],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_phy_ro_t_aro"},[motorSRM.stator.dim.phy.ro_t_aro],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_phy_ro_mst"},[motorSRM.stator.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_phy_ro_cu"},[motorSRM.stator.dim.phy.ro_cu],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_mM"},[motorSRM.stator.dim.mM],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_dim_cM"},[motorSRM.stator.dim.cM],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_a"},[motorSRM.stator.win.a],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_N_ph"},[motorSRM.stator.win.N_ph],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_K_f"},[motorSRM.stator.win.K_f],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_K_f_real"},[motorSRM.stator.win.K_f_real],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_b_sc"},[motorSRM.stator.win.dim.b_sc],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_h_sc"},[motorSRM.stator.win.dim.h_sc],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_b_seg"},[motorSRM.stator.win.dim.b_seg],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_D_cond"},[motorSRM.stator.win.dim.D_cond],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_l_extra"},[motorSRM.stator.win.dim.l_extra],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_l_extraH"},[motorSRM.stator.win.dim.l_extraH],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_dim_l_extraW"},[motorSRM.stator.win.dim.l_extraW],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_draw_Semi_coil_points"},[motorSRM.stator.win.draw.Semi_coil_points],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_draw_Semi_coil_real_points"},[motorSRM.stator.win.draw.Semi_coil_real_points],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_win_draw_Semi_coil_centers"},[motorSRM.stator.win.draw.Semi_coil_centers],true);

      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_phy_R_sc_t_aro"},[motorSRM.stator.phy.R_sc_t_aro],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_phy_t_work"},[motorSRM.stator.phy.t_work],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_phy_R_ph"},[motorSRM.stator.phy.R_ph],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_phy_L_u"},[motorSRM.stator.phy.L_u],true);
      DataFiles.writeMATmatrix("MotorSRM_Structure.mat",{"motorSRM_stator_phy_L_a"},[motorSRM.stator.phy.L_a],true);

      // Export strings
      Modelica.Utilities.Files.remove("MotorSRM_Structure.txt");
      Modelica.Utilities.Streams.print("motorSRM_plate_RatedVoltageType","MotorSRM_Structure.txt");
      Modelica.Utilities.Streams.print(motorSRM.plate.RatedVoltageType,"MotorSRM_Structure.txt");

      //DataFiles.writeMATmatrix("MotorSRM_Structure.mat",motorSRM.rotor.msg.failmessage,[1],true);
      Modelica.Utilities.Streams.print("motorSRM_rotor_msg","MotorSRM_Structure.txt");
      Modelica.Utilities.Streams.print(motorSRM.rotor.msg,"MotorSRM_Structure.txt");

    end SRM_Export;
  end SRM_Structure;

  package SYN_Structure "Structure of SRM motor parameters"
    record motorSYN "Record of Motor"
      import Modelica.SIunits;

      Integer m "Number of phases";
      Integer p "Number of pole pairs";
      SIunits.Length g "Airgap";
      SIunits.Power RatedPower "Rated power";
      Integer classe "Motor class";
      motorSYN_flags flags;
      motorSYN_plate plate;
      motorSYN_specs specs;
      motorSYN_stator stator;
      motorSYN_rotor rotor;
      motorSYN_casing casing;
      motorSYN_airgap airgap;
    end motorSYN;

    record motorSYN_flags
      import Modelica.SIunits;
      Integer R_fe_consider "Consider or not R_fe in T.vs.S evaluation";
    end motorSYN_flags;

    record motorSYN_plate
      import Modelica.SIunits;
      SIunits.Power RatedPower "Rated power";
      SIunits.Current RatedCurrent "Rated current";
      SIunits.Voltage RatedVoltage "Rated voltage";
      String RatedVoltageType "Rated voltage type: phase, line";
      //SIunits.Conversions.NonSIunits.AngularVelocity_rpm RatedSpeed "Rated speed";
      SIunits.Voltage BEMF "Back electromotive force";
      SIunits.Current RatedCurrentInductor;
      SIunits.Voltage RatedVoltageInductor;
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm RatedSpeedRPM
        "Rated speed in rpm";
      SIunits.Torque Torque "Rated torque";
      Real PowerFactor "Power factor";
      Real Efficiency "Efficiency";
      SIunits.Mass M "Total mass";
      SIunits.Mass TmM "Total magnetic mass";
      SIunits.MomentOfInertia I_Total "Total moment of inertia";
      //SIunits.Frequency RatedFrequency;

    end motorSYN_plate;

    record motorSYN_airgap
      import Modelica.SIunits;
      Real K_F "Relative form factor";
      Real K_M "Relative amplitude factor";
      SIunits.MagneticFluxDensity B_g_max_sin_round
        "Airgap max sine FEM equivalent flux density";
      SIunits.MagneticFluxDensity B_g_max_real_round;
    end motorSYN_airgap;

    record motorSYN_specs
      import Modelica.SIunits;
      Real C_mec "Mechanical constant";
      Integer J_counter "Current density counter";
      specs_TargetFluxDensities TargetFluxDensities;

    end motorSYN_specs;

    record specs_TargetFluxDensities
      import Modelica.SIunits;
      SIunits.MagneticFluxDensity B_g_ini "Airgap max real flux density";
      SIunits.MagneticFluxDensity B_yoke_ini
        "Stator yoke max real flux density";
      SIunits.MagneticFluxDensity B_tooth_ini
        "Stator tooth max real flux density";
      SIunits.MagneticFluxDensity B_pole_ini "Rotor pole max real flux density";
      SIunits.MagneticFluxDensity B_ryoke_limit
        "Rotor yoke max real flux density";

    end specs_TargetFluxDensities;

    record motorSYN_casing
      import Modelica.SIunits;
      casing_dim dim;
      SIunits.Length l_total "Total case length";
      SIunits.Length h_total "Total case height";
    end motorSYN_casing;

    record casing_dim
      import Modelica.SIunits;
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      dim_phy phy;
      SIunits.Mass M_S "Stator mass";
      SIunits.MomentOfInertia I_S "Stator Yoke Ring Moment of Inertia";

    end casing_dim;

    record dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Temperature t_aro "Reference resistivity temperature";
      SIunits.Temperature t_amb "Ambient temperature";
      SIunits.Temperature Delta_t_work
        "Temperature increase relative to ambient";
    end dim_phy;

    record motorSYN_rotor
      import Modelica.SIunits;
      Integer shape "Shape type";
      Integer fail "Flag indicating if it's possible to calculate shape";
      Integer failpath
        "Flag indicating if it's possible to calculate path using ellipse";
      String msg "Message";
      SIunits.Current I_cd_r "Rated pole current";
      SIunits.CurrentDensity J_r "Rotor current density";
      Integer a_r;

      rotor_specs specs;
      rotor_dim dim;
      rotor_cool cool;
      rotor_win win;
      rotor_phy phy;
      rotor_geo geo;
      rotor_draw draw;

    end motorSYN_rotor;

    record rotor_specs
      import Modelica.SIunits;
      Real sigma_p "Polar dispersion coeficient";
      Real sigma_arc "Rotor coil area security factor";

    end rotor_specs;

    record rotor_dim
      import Modelica.SIunits;

      SIunits.Length D_or "Diameter outer rotor";
      SIunits.Length D_ir "Diameter inner rotor";
      SIunits.Length D_shaft;
      SIunits.Length D_shaft_internal;
      SIunits.Length D_hub;
      SIunits.Length b_rp "Rotor pole width";
      SIunits.Length b_ry "Rotor yoke width";
      SIunits.Length h_rp "Rotor pole height";
      Real pcf "Pole cover factor";
      Real K_p "Pole linear correction factor";
      Real K_ry "Rotor yoke linear correction factor";
      Real K_u "Tooth linear correction factor";
      Real f_r "Stator lamination factor";
      SIunits.Length pole_height "Rotor pole height";
      SIunits.Length l_shaft_extra "External shaft length";
      SIunits.Length l_shaft "Total shaft length";
      rotor_dim_phy phy;
      SIunits.Mass M_mst_R "Rotor's magnetic steel mass";
      SIunits.Mass cM "Rotor copper mass";
      SIunits.Mass M_R "Rotor Mass";
      SIunits.MomentOfInertia I_R "Rotor inertia";

    end rotor_dim;

    record rotor_dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_mst "Magnetic steel density";
      SIunits.Density ro_cu "Copper density";
      SIunits.Density ro_shaft "Shaft steel density";
    end rotor_dim_phy;

    record rotor_cool
      import Modelica.SIunits;
      Integer n_cs "Stator number of cooling ducts";
      Integer e_cs_t
        "Stator wide/type of cooling ducts 1=10mm wide square, 2=15mm wide square";

    end rotor_cool;

    record rotor_win
      import Modelica.SIunits;
      Real N_ph "Number of turns per phase integer";
      Real K_f_r;
      Real K_f_r_real;
      rotor_win_dim dim;
      rotor_win_draw draw;
    end rotor_win;

    record rotor_win_dim
      import Modelica.SIunits;
      SIunits.Length b_rc;
      SIunits.Length h_rc "Rotor coil height";
      SIunits.Length D_cond_r "Conductor diameter";

    end rotor_win_dim;

    record rotor_win_draw
      import Modelica.SIunits;
      Real[:,:] Semi_coil_points;
      Real[:,:] Semi_coil_real_points;
      Real[:,:] Semi_coil_centers;
      SIunits.Length scoil_displacement_x "Semi coil area positioning x-coord";
      SIunits.Length scoil_displacement_y "Semi coil area positioning y-coord";
    end rotor_win_draw;

    record rotor_phy
      import Modelica.SIunits;

      SIunits.Resistance R_rc_t_aro "Coil resistance at ambient temperature";
      SIunits.Conversions.NonSIunits.Temperature_degC t_work
        "Cu working temperature";
      SIunits.Resistance R_ph "Pole coil resistance";
    end rotor_phy;

    record rotor_geo
      import Modelica.SIunits;
      Real[:,2] externalrotorpoints;
      Real[4,2] rotorpolepointsR;
      Real[:,2] rotorpolepoints;
      SIunits.Angle beta "Pole cover angle";
      SIunits.Length r_ph "Radius of pole head circle";
      SIunits.Length g_pcf "Airgap thickness at pole limit";
      SIunits.Angle t_pcf "Pole cover factor angle";
      SIunits.Length h_0 "Circular head height";
      SIunits.Length h_pcf "Pole head 20 degrees thickness";
      SIunits.Length h_rp "Rotor pole height";
      SIunits.Length[1,2] semi_coil_center "Using MOI-COORDS";

    end rotor_geo;

    record rotor_draw
      import Modelica.SIunits;
      rotor_draw_fail fail;
      Integer fail_elements;
    end rotor_draw;

    record rotor_draw_fail
      import Modelica.SIunits;
      Real[4,2] fail_element1;
      Real[2,2] fail_element2;
    end rotor_draw_fail;

    record motorSYN_stator
      import Modelica.SIunits;
      Integer case_type "Case type";
      Integer Q_s "Number of stator slots";
      SIunits.Length D "Bore diameter (inner stator diameter)";
      SIunits.Current I_s "Rated current";
      SIunits.CurrentDensity J_s "Stator current density";
      SIunits.CurrentDensity J_s_desired "Stator current density";
      SIunits.CurrentDensity J_s_real "Rounded current density";
      Integer a "Parallel paths";

      stator_win win;
      stator_dim dim;
      stator_cool cool;
      stator_phy phy;
    end motorSYN_stator;

    record stator_win
      import Modelica.SIunits;
      Integer a "Parallel paths";
      Real N_ph "Number of turns per phase, integer";
      Real K_w "Winding factor";
      Real K_uhc "Stator head coil filling factor";
      Real K_u "Stator slot filling factor";
      Integer coilspan "Coilspan";
      Integer z_Q "Number of conductors per slot";
      Real S_cond "Conductor cross section";
      stator_win_dim dim;
      stator_win_phy phy;
    end stator_win;

    record stator_win_dim
      import Modelica.SIunits;
      SIunits.Length D_cond_s "Conductor diameter";
      SIunits.Length l_extra "Extra head coil length";
      SIunits.Length l_extraW "Head Coil winding longitudinal extra length";
      SIunits.Length l_extraH "Head Coil longitudinal extra length";
    end stator_win_dim;

    record stator_win_phy
      import Modelica.SIunits;
      SIunits.Resistivity ro_t_aro "Cu resistivity";
    end stator_win_phy;

    record stator_dim
      import Modelica.SIunits;

      Real K_c "Carter fringing coefficient";
      SIunits.Length D_os "Diameter outer stator";
      SIunits.Length D_is "Bore diameter (inner stator diameter)";
      SIunits.Length l "Length";
      Real X "Form factor";
      SIunits.Length h_z "Stator tooth height";
      SIunits.Length b_z "Stator tooth width @ D";
      SIunits.Length h_tr "Stator slot height";
      SIunits.Length b_s "Stator slot width @ D";
      SIunits.Length h_y "Stator yoke width";
      SIunits.Length b_1 "Stator slot opening";
      SIunits.Length delta_htr "height of teeth expansion";
      Real K_fe "Tooth linear correction factor";
      Real K_u "Stator slot filling factor";
      Real f_r_s "Stator lamination factor";
      Real l_turn;
      SIunits.Mass mM "Stator magnetic mass";
      SIunits.Mass cM "Total copper mass";
      SIunits.MomentOfInertia I_S "Stator moment of inertia";
      SIunits.Mass M_S "Stator mass";

      stator_dim_phy phy;

    end stator_dim;

    record stator_dim_phy
      import Modelica.SIunits;
      SIunits.Resistivity ro_t_aro "Cu Resistivity";
      SIunits.Density ro_mst "Magnetic steel density";
      SIunits.Density ro_cu "Cu density";
      SIunits.Resistivity ro_work "Resistivity at working temperature";

    end stator_dim_phy;

    record stator_cool
      import Modelica.SIunits;
      Integer n_cs "Stator number of cooling ducts";
      Integer e_cs_t
        "Stator Wide/Type of cooling ducts 1=10mm, 2=15mm wide square";
    end stator_cool;

    record stator_phy
      import Modelica.SIunits;
      SIunits.Resistance R_s "Stator resistance";
      SIunits.Conversions.NonSIunits.Temperature_degC t_work
        "Working temperature";

    end stator_phy;

    function SYN_Export "Exports structure to mat rows&columns"
      import Sizing;
     parameter input Sizing.SYN_Structure.motorSYN MotorSYN;

    algorithm
      // RatedPower
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_RatedPower"},[MotorSYN.RatedPower],false);

      // airgap
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_airgap_K_F"},[MotorSYN.airgap.K_F],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_airgap_K_M"},[MotorSYN.airgap.K_M],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_airgap_B_g_max_sin_round"},[MotorSYN.airgap.B_g_max_sin_round],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_airgap_B_g_max_real_round"},[MotorSYN.airgap.B_g_max_real_round],true);

      // casing

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_l_case_exp_front"},[MotorSYN.casing.dim.l_case_exp_front],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_l_case_exp_rear"},[MotorSYN.casing.dim.l_case_exp_rear],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_l_case_exp_void"},[MotorSYN.casing.dim.l_case_exp_void],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_l_case"},[MotorSYN.casing.dim.l_case],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_h_case"},[MotorSYN.casing.dim.h_case],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_phy_Delta_t_work"},[MotorSYN.casing.dim.phy.Delta_t_work],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_phy_t_amb"},[MotorSYN.casing.dim.phy.t_amb],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_phy_t_aro"},[MotorSYN.casing.dim.phy.t_aro],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_phy_ro_case"},[MotorSYN.casing.dim.phy.ro_case],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_M_S"},[MotorSYN.casing.dim.M_S],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_dim_I_S"},[MotorSYN.casing.dim.I_S],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_l_total"},[MotorSYN.casing.l_total],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_casing_h_total"},[MotorSYN.casing.h_total],true);

      // class
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_classe"},[MotorSYN.classe],true);

      // flags
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_flags_R_fe_consider"},[MotorSYN.flags.R_fe_consider],true);

      // g
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_g"},[MotorSYN.g],true);

      // m
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_m"},[MotorSYN.m],true);

      // p
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_p"},[MotorSYN.p],true);

      // plate
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_RatedPower"},[MotorSYN.plate.RatedPower],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_RatedCurrent"},[MotorSYN.plate.RatedCurrent],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_RatedVoltage"},[MotorSYN.plate.RatedVoltage],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_BEMF"},[MotorSYN.plate.BEMF],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_RatedCurrentInductor"},[MotorSYN.plate.RatedCurrentInductor],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_RatedVoltageInductor"},[MotorSYN.plate.RatedVoltageInductor],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_RatedSpeedRPM"},[MotorSYN.plate.RatedSpeedRPM],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_Torque"},[MotorSYN.plate.Torque],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_PowerFactor"},[MotorSYN.plate.PowerFactor],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_Efficiency"},[MotorSYN.plate.Efficiency],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_M"},[MotorSYN.plate.M],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_TmM"},[MotorSYN.plate.TmM],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_plate_I_Total"},[MotorSYN.plate.I_Total],true);

      // rotor
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_shape"},[MotorSYN.rotor.shape],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_fail"},[MotorSYN.rotor.fail],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_failpath"},[MotorSYN.rotor.failpath],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_I_cd_r"},[MotorSYN.rotor.I_cd_r],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_J_r"},[MotorSYN.rotor.J_r],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_a_r"},[MotorSYN.rotor.a_r],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_specs_sigma_p"},[MotorSYN.rotor.specs.sigma_p],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_specs_sigma_arc"},[MotorSYN.rotor.specs.sigma_arc],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_D_or"},[MotorSYN.rotor.dim.D_or],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_D_ir"},[MotorSYN.rotor.dim.D_ir],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_D_shaft"},[MotorSYN.rotor.dim.D_shaft],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_D_shaft_internal"},[MotorSYN.rotor.dim.D_shaft_internal],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_D_hub"},[MotorSYN.rotor.dim.D_hub],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_b_rp"},[MotorSYN.rotor.dim.b_rp],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_b_ry"},[MotorSYN.rotor.dim.b_ry],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_h_rp"},[MotorSYN.rotor.dim.h_rp],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_pcf"},[MotorSYN.rotor.dim.pcf],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_K_p"},[MotorSYN.rotor.dim.K_p],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_K_ry"},[MotorSYN.rotor.dim.K_ry],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_K_u"},[MotorSYN.rotor.dim.K_u],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_f_r"},[MotorSYN.rotor.dim.f_r],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_l_shaft_extra"},[MotorSYN.rotor.dim.l_shaft_extra],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_l_shaft"},[MotorSYN.rotor.dim.l_shaft],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_phy_ro_mst"},[MotorSYN.rotor.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_phy_ro_cu"},[MotorSYN.rotor.dim.phy.ro_cu],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_phy_ro_shaft"},[MotorSYN.rotor.dim.phy.ro_shaft],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_M_mst_R"},[MotorSYN.rotor.dim.M_mst_R],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_cM"},[MotorSYN.rotor.dim.cM],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_M_R"},[MotorSYN.rotor.dim.M_R],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_I_R"},[MotorSYN.rotor.dim.I_R],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_cool_n_cs"},[MotorSYN.rotor.cool.n_cs],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_cool_e_cs_t"},[MotorSYN.rotor.cool.e_cs_t],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_dim_pole_height"},[MotorSYN.rotor.dim.pole_height],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_N_ph"},[MotorSYN.rotor.win.N_ph],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_K_f_r"},[MotorSYN.rotor.win.K_f_r],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_K_f_r_real"},[MotorSYN.rotor.win.K_f_r_real],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_dim_b_rc"},[MotorSYN.rotor.win.dim.b_rc],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_dim_h_rc"},[MotorSYN.rotor.win.dim.h_rc],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_dim_D_cond_r"},[MotorSYN.rotor.win.dim.D_cond_r],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_draw_Semi_coil_points"},[MotorSYN.rotor.win.draw.Semi_coil_points],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_draw_Semi_coil_real_points"},[MotorSYN.rotor.win.draw.Semi_coil_real_points],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_draw_Semi_coil_centers"},[MotorSYN.rotor.win.draw.Semi_coil_centers],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_draw_scoil_displacement_x"},[MotorSYN.rotor.win.draw.scoil_displacement_x],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_win_draw_scoil_displacement_y"},[MotorSYN.rotor.win.draw.scoil_displacement_y],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_phy_R_rc_t_aro"},[MotorSYN.rotor.phy.R_rc_t_aro],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_phy_t_work"},[MotorSYN.rotor.phy.t_work],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_phy_R_ph"},[MotorSYN.rotor.phy.R_ph],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_externalrotorpoints"},[MotorSYN.rotor.geo.externalrotorpoints],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_rotorpolepointsR"},[MotorSYN.rotor.geo.rotorpolepointsR],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_rotorpolepoints"},[MotorSYN.rotor.geo.rotorpolepoints],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_beta"},[MotorSYN.rotor.geo.beta],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_r_ph"},[MotorSYN.rotor.geo.r_ph],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_g_pcf"},[MotorSYN.rotor.geo.g_pcf],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_t_pcf"},[MotorSYN.rotor.geo.t_pcf],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_h_0"},[MotorSYN.rotor.geo.h_0],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_h_pcf"},[MotorSYN.rotor.geo.h_pcf],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_h_rp"},[MotorSYN.rotor.geo.h_rp],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_geo_semi_coil_center"},[MotorSYN.rotor.geo.semi_coil_center],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_draw_fail_fail_element1"},[MotorSYN.rotor.draw.fail.fail_element1],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_draw_fail_fail_element2"},[MotorSYN.rotor.draw.fail.fail_element2],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_rotor_draw_fail_elements"},[MotorSYN.rotor.draw.fail_elements],true);

      // specs
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_C_mec"},[MotorSYN.specs.C_mec],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_TargetFluxDensities_B_g_ini"},[MotorSYN.specs.TargetFluxDensities.B_g_ini],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_TargetFluxDensities_B_yoke_ini"},[MotorSYN.specs.TargetFluxDensities.B_yoke_ini],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_TargetFluxDensities_B_tooth_ini"},[MotorSYN.specs.TargetFluxDensities.B_tooth_ini],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_TargetFluxDensities_B_pole_ini"},[MotorSYN.specs.TargetFluxDensities.B_pole_ini],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_TargetFluxDensities_B_ryoke_limit"},[MotorSYN.specs.TargetFluxDensities.B_ryoke_limit],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_specs_J_counter"},[MotorSYN.specs.J_counter],true);

      // stator
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_case_type"},[MotorSYN.stator.case_type],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_Q_s"},[MotorSYN.stator.Q_s],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_D"},[MotorSYN.stator.D],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_I_s"},[MotorSYN.stator.I_s],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_J_s"},[MotorSYN.stator.J_s],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_a"},[MotorSYN.stator.win.a],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_N_ph"},[MotorSYN.stator.win.N_ph],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_K_w"},[MotorSYN.stator.win.K_w],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_K_uhc"},[MotorSYN.stator.win.K_uhc],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_K_u"},[MotorSYN.stator.win.K_u],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_coilspan"},[MotorSYN.stator.win.coilspan],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_S_cond"},[MotorSYN.stator.win.S_cond],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_z_Q"},[MotorSYN.stator.win.z_Q],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_dim_D_cond_s"},[MotorSYN.stator.win.dim.D_cond_s],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_dim_l_extra"},[MotorSYN.stator.win.dim.l_extra],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_dim_l_extraH"},[MotorSYN.stator.win.dim.l_extraH],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_dim_l_extraW"},[MotorSYN.stator.win.dim.l_extraW],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_win_phy_ro_t_aro"},[MotorSYN.stator.win.phy.ro_t_aro],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_K_c"},[MotorSYN.stator.dim.K_c],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_D_os"},[MotorSYN.stator.dim.D_os],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_D_is"},[MotorSYN.stator.dim.D_is],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_l"},[MotorSYN.stator.dim.l],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_X"},[MotorSYN.stator.dim.X],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_h_z"},[MotorSYN.stator.dim.h_z],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_b_z"},[MotorSYN.stator.dim.b_z],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_h_tr"},[MotorSYN.stator.dim.h_tr],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_b_s"},[MotorSYN.stator.dim.b_s],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_h_y"},[MotorSYN.stator.dim.h_y],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_b_1"},[MotorSYN.stator.dim.b_1],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_delta_htr"},[MotorSYN.stator.dim.delta_htr],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_K_fe"},[MotorSYN.stator.dim.K_fe],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_K_u"},[MotorSYN.stator.dim.K_u],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_f_r_s"},[MotorSYN.stator.dim.f_r_s],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_phy_ro_mst"},[MotorSYN.stator.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_phy_ro_cu"},[MotorSYN.stator.dim.phy.ro_cu],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_phy_ro_work"},[MotorSYN.stator.dim.phy.ro_work],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_l_turn"},[MotorSYN.stator.dim.l_turn],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_mM"},[MotorSYN.stator.dim.mM],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_cM"},[MotorSYN.stator.dim.cM],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_I_S"},[MotorSYN.stator.dim.I_S],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_dim_M_S"},[MotorSYN.stator.dim.M_S],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_a"},[MotorSYN.stator.a],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_J_s_desired"},[MotorSYN.stator.J_s_desired],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_J_s_real"},[MotorSYN.stator.J_s_real],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_cool_n_cs"},[MotorSYN.stator.cool.n_cs],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_cool_e_cs_t"},[MotorSYN.stator.cool.e_cs_t],true);

      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_phy_R_s"},[MotorSYN.stator.phy.R_s],true);
      DataFiles.writeMATmatrix("MotorSYN_Structure.mat",{"MotorSYN_stator_phy_t_work"},[MotorSYN.stator.phy.t_work],true);

      // Export strings
      Modelica.Utilities.Files.remove("MotorSYN_Structure.txt");
      Modelica.Utilities.Streams.print("MotorSYN_plate_RatedVoltageType","MotorSYN_Structure.txt");
      Modelica.Utilities.Streams.print(MotorSYN.plate.RatedVoltageType,"MotorSYN_Structure.txt");

      //DataFiles.writeMATmatrix("MotorSYN_Structure.mat",MotorSYN.rotor.msg.failmessage,[1],true);
      Modelica.Utilities.Streams.print("MotorSYN_rotor_msg","MotorSYN_Structure.txt");
      Modelica.Utilities.Streams.print(MotorSYN.rotor.msg,"MotorSYN_Structure.txt");

    end SYN_Export;
  end SYN_Structure;

  package SMPMSM_Structure

    record motorSMPMSM
      import Modelica.SIunits;
      Integer m "Number of phases";
      Integer p "Number of pole pairs";
      SIunits.Length g "Airgap";
      SIunits.Power RatedPower "Rated power";
      Integer classe "Motor class";
      motorSPMSM_plate plate;
      motorSPMSM_specs specs;
      motorSPMSM_flags flags;
      motorSPMSM_airgap airgap;
      motorSPMSM_stator stator;
      motorSPMSM_rotor rotor;
      motorSPMSM_casing casing;

    end motorSMPMSM;

    record motorSPMSM_plate
      import Modelica.SIunits;
      SIunits.Power RatedPower "Rated power";
      SIunits.Current RatedCurrent "Rated current";
      SIunits.Voltage RatedVoltage "Rated voltage";
      String RatedVoltageType "Rated voltage type: phase, line";
      SIunits.Voltage BEMF "Back electromotive force";
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm RatedSpeedRPM
        "Rated speed in rpm";
      SIunits.Frequency RatedSpeed "Rated speed in Hz";
      SIunits.Torque Torque "Rated torque";
      Real PowerFactor "Power factor";
      Real Efficiency "Efficiency";
      SIunits.Mass M "Total mass";
      SIunits.Mass TmM "Total magnetic mass";
      SIunits.Inductance L_ph "Phase inductance";
      SIunits.MomentOfInertia I_Total "Total moment of inertia";

    end motorSPMSM_plate;

    record motorSPMSM_specs
      import Modelica.SIunits;

      Real f_r;
      Real X "Aspec ratio";
      Real C_mec "Mechanical constant";
      Real A_load "Electrical loading";
      SIunits.Voltage TargetRatedVoltage "Rated voltage";
      SIunits.MagneticFlux flux_final "Final flux";
      Real TSeff_lut;
      Real MTPA_lut;
      specs_TargetFluxDensities TargetFluxDensities;

    end motorSPMSM_specs;

    record specs_TargetFluxDensities
      import Modelica.SIunits;
      SIunits.MagneticFluxDensity B_g_ini "Maximum airgap flux density";
      SIunits.MagneticFluxDensity B_yoke_ini "Maximum stator yoke flux density";
      SIunits.MagneticFluxDensity B_tooth_ini
        "Maximum stator tooth flux density";
      SIunits.MagneticFluxDensity B_r_yoke_ini "Rotor yoke max flux density";

    end specs_TargetFluxDensities;

    record motorSPMSM_flags
      Integer R_fe_consider
        "1-consider R_fe effect, 0-do not consider R_fe efffect";
    end motorSPMSM_flags;

    record motorSPMSM_airgap
      airgap_phy phy;
    end motorSPMSM_airgap;

    record airgap_phy
      import Modelica.SIunits;
      SIunits.MagneticFluxDensity B_g
        "First harmonic of the Fourier series of the airgap flux density";
      SIunits.MagneticFlux flux_g "Total flux per pole at airgap";
      SIunits.Reluctance R_g "One polar pitch airgap reluctance";
      SIunits.MagneticFlux flux_g_final "Final flux";
    end airgap_phy;

    record motorSPMSM_stator
      import Modelica.SIunits;
      Integer case_type "1-Circular, 2-Square solid";
      Integer Q_s "Number of stator slots";
      SIunits.Length D "Diameter of inner stator";
      Modelica.SIunits.Current I_s "Rated stator current";
      Modelica.SIunits.CurrentDensity J_s_desired "Current density";
      stator_dim dim;
      stator_win win;
      stator_phy phy;

    end motorSPMSM_stator;

    record stator_dim
      import Modelica.SIunits;
      SIunits.Length tau_s "Slot pitch length";
      SIunits.Length D_os "Diameter of outer stator";
      SIunits.Length D_is "Diameter of inner stator";
      SIunits.Length h_tr;
      SIunits.Length h_z;
      SIunits.Length b_s "Slot width at bottom (arc)";
      SIunits.Length b_z "Tooth width";
      SIunits.Length h_y "Stator yoke height";
      SIunits.Length b_1 "Stator slot opening";
      SIunits.Length delta_htr;
      SIunits.Length l "Stator mensurable length";
      Real f_r_s "Stator lamination factor";
      Real K_c "Carter factor";
      SIunits.Length b_sB "Slot width at bottom (arc)";
      SIunits.Length b_sT "Slot width at top (arc)";
      SIunits.Length b_zT "Tooth width at top (arc)";
      SIunits.Length b_zB "Tooth width";
      SIunits.Length D_ms "Stator medium diameter";
      Real d1_prop "d1 measure proportion to d3";
      Real d2_prop "d2 measure proportion to d3";
      SIunits.Length d1 "Tooth square shoe height";
      SIunits.Length d2 "Tooth trapezoid shoe height";
      SIunits.Length d3 "d3 tooth dimension (seed value)";
      SIunits.Mass mM "Stator magnetic mass";
      SIunits.Mass cM "Total copper mass";
      SIunits.Length l_turn "Average Turn Length";
      SIunits.MomentOfInertia I_S "Stator total moment of inertia";
      SIunits.Mass M_S "Stator mass";
      stator_dim_phy phy;

    end stator_dim;

    record stator_dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_mst "Magnetic steel density";
      SIunits.Density ro_cu "Cu density";
      SIunits.Resistivity ro_work "Resistivity at Work Temperature";
      stator_dim_phy_mst mst;
    end stator_dim_phy;

    record stator_dim_phy_mst
      Real K_h=50 "Magnetic steel hysteresis constant";
      Real K_e=0.05 "Magnetic steel eddy constant";
      Real n_i=1.6 "Steinmetz Constant";
    end stator_dim_phy_mst;

    record stator_win
      import Modelica.SIunits;
      Integer a "Parallel paths";
      Real N_ph "Rounded number of turns per phase";
      Real K_w "Winding factor";
      Real K_u "Stator slot filling factor";
      Real K_uhc "Stator head coil filling factor";
      SIunits.Area S_cond "Conductor area";
      Integer coilspan "Coilspan";
      stator_win_phy phy;
      stator_win_dim dim;

    end stator_win;

    record stator_win_phy
      import Modelica.SIunits;
      SIunits.Resistivity ro_t_aro "Cu Resistivity";
    end stator_win_phy;

    record stator_win_dim
      import Modelica.SIunits;
      SIunits.Length l_extra "Housing extra head coil length";
      SIunits.Length l_extraH "Stator head coil longitudinal extra length";
      SIunits.Length l_extraW "Stator head coil winding longitudinal length";
      SIunits.Length D_cond "Conductor diameter";
    end stator_win_dim;

    record stator_phy
      import Modelica.SIunits;
      Real R_sc_t_aro;
      Real t_work;
      Real R_s;
      Real p_lf "Pole leakage factor";
      SIunits.Inductance L_d "Magnetizing inductance in d";
      SIunits.Inductance L_q "Magnetizing inductance in q";
      SIunits.Inductance L_dm "Magnetizing inductance in d";
      SIunits.Inductance L_qm "Magnetizing inductance in q";

    end stator_phy;

    record motorSPMSM_rotor
      Integer shape;
      Integer fail;
      Integer failpath;
      String msg;
      rotor_dim dim;
      rotor_draw draw;
      rotor_phy phy;

    end motorSPMSM_rotor;

    record rotor_dim
      import Modelica.SIunits;
      SIunits.Length tau_r "Polar pitch";
      SIunits.Length D_or "Diameter of outer rotor";
      SIunits.Length D_ir "Shaft internal diameter";
      SIunits.Length D_shaft_internal "Shaft internal diameter";
      SIunits.Length D_shaft "Shaft external diameter";
      SIunits.Length D_shaftB "Shaft external diameter";
      SIunits.Length D_hub "Rotor magnet hub barrier diameter";
      SIunits.Length h_PM "Magnetic circuit magnet's height";
      SIunits.Length l_PM "Magnet length";
      Real mcf_PM "Magnet cover factor";
      SIunits.Length l "Rotor mensurable length";
      Real f_r "Rotor lamination factor";
      SIunits.Length DeltaD "Distance between magnet pod and rotor surface";
      Real Cover "Magnetic flux density wave cover";
      SIunits.Length h_ry "Rotor yoke height";
      SIunits.Length h_rh "Rotor hole height";
      Real hcf "Hole cover factor";
      SIunits.Length l_web "Web length Between Consecutive Pole Magnets";
      Real Omega "Web angle cover factor";
      SIunits.Length l_shaft_extra "Shaft external length";
      SIunits.Length l_shaft "Total shaft length";
      SIunits.Mass M_mst_R "Rotor magnetic steel mass";
      SIunits.Mass M_R "Total rotor mass";
      SIunits.MomentOfInertia I_R "Rotor Inertia";
      SIunits.Mass M_PM "Total rotor magnets mass";
      rotor_dim_phy phy;

    end rotor_dim;

    record rotor_dim_phy
      import Modelica.SIunits;
      SIunits.Density ro_PM "Magnet density";
      SIunits.Density ro_shaft "Shaft steel density";
      SIunits.Density ro_mst "Magnetic steel density";

    end rotor_dim_phy;

    record rotor_draw
      Integer hole_rotor=1
        "Rotor with or without holes, 0-No holes, 1-Holes in rotor";
      Integer magnet_style;
      Integer magnetSlotPM_style;
      Integer magneticSteel_style;
      Integer rotorHub_style;
      Integer shaft_style;
      Integer fail_elements;

    end rotor_draw;

    record rotor_phy
      import Modelica.SIunits;
      SIunits.Reluctance R_PM "One magnet reluctance";
      SIunits.MagneticFluxDensity B_r_yoke
        "Final rotor yoke maximum flux density value";

    end rotor_phy;

    record motorSPMSM_casing
      import Modelica.SIunits;
      SIunits.Length l_total "Total case length";
      SIunits.Length h_total "Total case height";
      casing_dim dim;
    end motorSPMSM_casing;

    record casing_dim
      import Modelica.SIunits;
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      SIunits.Mass M_S "Stator mass";
      SIunits.MomentOfInertia I_S "Stator Yoke Ring Moment of Inertia";
      casing_dim_phy phy;

    end casing_dim;

    record casing_dim_phy
      import Modelica.SIunits;
      SIunits.Conversions.NonSIunits.Temperature_degC Delta_t_work
        "Temperature increase relative to ambient";
      SIunits.Conversions.NonSIunits.Temperature_degC t_amb
        "Ambient temperature";
      SIunits.Conversions.NonSIunits.Temperature_degC t_aro
        "Reference resistivity temperature";
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Density ro_PM "Magnet density";
      SIunits.Density ro_shaft "Shaft steel density";

    end casing_dim_phy;

    function SMPMSM_Export "Exports structure to mat rows&columns"
     parameter input SMPMSM_Structure.motorSMPMSM motorSMPMSM;

    algorithm
      // m
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_m"},[motorSMPMSM.m],false);

      // p
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_p"},[motorSMPMSM.p],true);

      // g
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_g"},[motorSMPMSM.g],true);

      // RatedPower
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_RatedPower"},[motorSMPMSM.RatedPower],true);

      // class
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_classe"},[motorSMPMSM.classe],true);

      // plate

      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_RatedPower"},[motorSMPMSM.plate.RatedPower],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_RatedCurrent"},[motorSMPMSM.plate.RatedCurrent],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_RatedVoltage"},[motorSMPMSM.plate.RatedVoltage],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_BEMF"},[motorSMPMSM.plate.BEMF],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_RatedSpeedRPM"},[motorSMPMSM.plate.RatedSpeedRPM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_RatedSpeed"},[motorSMPMSM.plate.RatedSpeed],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_Torque"},[motorSMPMSM.plate.Torque],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_PowerFactor"},[motorSMPMSM.plate.PowerFactor],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_Efficiency"},[motorSMPMSM.plate.Efficiency],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_M"},[motorSMPMSM.plate.M],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_TmM"},[motorSMPMSM.plate.TmM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_L_ph"},[motorSMPMSM.plate.L_ph],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_plate_I_Total"},[motorSMPMSM.plate.I_Total],true);

      // specs

      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_f_r"},[motorSMPMSM.specs.f_r],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_X"},[motorSMPMSM.specs.X],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_C_mec"},[motorSMPMSM.specs.C_mec],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_A_load"},[motorSMPMSM.specs.A_load],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_TargetRatedVoltage"},[motorSMPMSM.specs.TargetRatedVoltage],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_flux_final"},[motorSMPMSM.specs.flux_final],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_TargetFluxDensities_B_g_ini"},[motorSMPMSM.specs.TargetFluxDensities.B_g_ini],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_TargetFluxDensities_B_yoke_ini"},[motorSMPMSM.specs.TargetFluxDensities.B_yoke_ini],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_TargetFluxDensities_B_tooth_ini"},[motorSMPMSM.specs.TargetFluxDensities.B_tooth_ini],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_TargetFluxDensities_B_r_yoke_ini"},[motorSMPMSM.specs.TargetFluxDensities.B_r_yoke_ini],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_TSeff_lut"},[motorSMPMSM.specs.TSeff_lut],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_MTPA_lut"},[motorSMPMSM.specs.MTPA_lut],true);
      //DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_specs_MTPV_lut"},[motorSMPMSM.specs.MTPV_lut],true);

      // flags
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_flags_R_fe_consider"},[motorSMPMSM.flags.R_fe_consider],true);

      // airgap
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_airgap_phy_B_g"},[motorSMPMSM.airgap.phy.B_g],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_airgap_phy_flux_g"},[motorSMPMSM.airgap.phy.flux_g],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_airgap_phy_R_g"},[motorSMPMSM.airgap.phy.R_g],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_airgap_phy_flux_g_final"},[motorSMPMSM.airgap.phy.flux_g_final],true);

      // stator
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_case_type"},[motorSMPMSM.stator.case_type],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_Q_s"},[motorSMPMSM.stator.Q_s],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_D"},[motorSMPMSM.stator.D],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_I_s"},[motorSMPMSM.stator.I_s],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_J_s_desired"},[motorSMPMSM.stator.J_s_desired],true);

      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_tau_s"},[motorSMPMSM.stator.dim.tau_s],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_D_os"},[motorSMPMSM.stator.dim.D_os],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_D_is"},[motorSMPMSM.stator.dim.D_is],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_h_tr"},[motorSMPMSM.stator.dim.h_tr],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_h_z"},[motorSMPMSM.stator.dim.h_z],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_s"},[motorSMPMSM.stator.dim.b_s],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_z"},[motorSMPMSM.stator.dim.b_z],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_h_y"},[motorSMPMSM.stator.dim.h_y],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_1"},[motorSMPMSM.stator.dim.b_1],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_delta_htr"},[motorSMPMSM.stator.dim.delta_htr],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_l"},[motorSMPMSM.stator.dim.l],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_f_r_s"},[motorSMPMSM.stator.dim.f_r_s],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_K_c"},[motorSMPMSM.stator.dim.K_c],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_sB"},[motorSMPMSM.stator.dim.b_sB],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_sT"},[motorSMPMSM.stator.dim.b_sT],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_zT"},[motorSMPMSM.stator.dim.b_zT],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_b_zB"},[motorSMPMSM.stator.dim.b_zB],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_D_ms"},[motorSMPMSM.stator.dim.D_ms],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_d1_prop"},[motorSMPMSM.stator.dim.d1_prop],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_d2_prop"},[motorSMPMSM.stator.dim.d2_prop],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_d1"},[motorSMPMSM.stator.dim.d1],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_d2"},[motorSMPMSM.stator.dim.d2],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_d3"},[motorSMPMSM.stator.dim.d3],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_phy_mst_K_h"},[motorSMPMSM.stator.dim.phy.mst.K_h],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_phy_mst_K_e"},[motorSMPMSM.stator.dim.phy.mst.K_e],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_phy_mst_n_i"},[motorSMPMSM.stator.dim.phy.mst.n_i],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_phy_ro_mst"},[motorSMPMSM.stator.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_phy_ro_cu"},[motorSMPMSM.stator.dim.phy.ro_cu],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_phy_ro_work"},[motorSMPMSM.stator.dim.phy.ro_work],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_mM"},[motorSMPMSM.stator.dim.mM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_cM"},[motorSMPMSM.stator.dim.cM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_l_turn"},[motorSMPMSM.stator.dim.l_turn],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_I_S"},[motorSMPMSM.stator.dim.I_S],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_dim_M_S"},[motorSMPMSM.stator.dim.M_S],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_a"},[motorSMPMSM.stator.win.a],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_N_ph"},[motorSMPMSM.stator.win.N_ph],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_K_w"},[motorSMPMSM.stator.win.K_w],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_K_u"},[motorSMPMSM.stator.win.K_u],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_K_uhc"},[motorSMPMSM.stator.win.K_uhc],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_S_cond"},[motorSMPMSM.stator.win.S_cond],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_coilspan"},[motorSMPMSM.stator.win.coilspan],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_dim_D_cond"},[motorSMPMSM.stator.win.dim.D_cond],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_dim_l_extra"},[motorSMPMSM.stator.win.dim.l_extra],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_dim_l_extraH"},[motorSMPMSM.stator.win.dim.l_extraH],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_dim_l_extraW"},[motorSMPMSM.stator.win.dim.l_extraW],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_win_phy_ro_t_aro"},[motorSMPMSM.stator.win.phy.ro_t_aro],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_R_sc_t_aro"},[motorSMPMSM.stator.phy.R_sc_t_aro],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_t_work"},[motorSMPMSM.stator.phy.t_work],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_R_s"},[motorSMPMSM.stator.phy.R_s],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_p_lf"},[motorSMPMSM.stator.phy.p_lf],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_L_d"},[motorSMPMSM.stator.phy.L_d],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_L_q"},[motorSMPMSM.stator.phy.L_q],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_L_dm"},[motorSMPMSM.stator.phy.L_dm],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_stator_phy_L_qm"},[motorSMPMSM.stator.phy.L_qm],true);

      // rotor
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_shape"},[motorSMPMSM.rotor.shape],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_fail"},[motorSMPMSM.rotor.fail],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_failpath"},[motorSMPMSM.rotor.failpath],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_tau_r"},[motorSMPMSM.rotor.dim.tau_r],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_D_or"},[motorSMPMSM.rotor.dim.D_or],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_D_ir"},[motorSMPMSM.rotor.dim.D_ir],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_D_shaft_internal"},[motorSMPMSM.rotor.dim.D_shaft_internal],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_D_shaft"},[motorSMPMSM.rotor.dim.D_shaft],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_D_shaftB"},[motorSMPMSM.rotor.dim.D_shaftB],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_D_hub"},[motorSMPMSM.rotor.dim.D_hub],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_h_PM"},[motorSMPMSM.rotor.dim.h_PM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_l_PM"},[motorSMPMSM.rotor.dim.l_PM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_mcf_PM"},[motorSMPMSM.rotor.dim.mcf_PM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_l"},[motorSMPMSM.rotor.dim.l],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_f_r"},[motorSMPMSM.rotor.dim.f_r],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_DeltaD"},[motorSMPMSM.rotor.dim.DeltaD],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_Cover"},[motorSMPMSM.rotor.dim.Cover],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_h_ry"},[motorSMPMSM.rotor.dim.h_ry],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_h_rh"},[motorSMPMSM.rotor.dim.h_rh],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_hcf"},[motorSMPMSM.rotor.dim.hcf],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_l_web"},[motorSMPMSM.rotor.dim.l_web],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_Omega"},[motorSMPMSM.rotor.dim.Omega],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_l_shaft_extra"},[motorSMPMSM.rotor.dim.l_shaft_extra],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_l_shaft"},[motorSMPMSM.rotor.dim.l_shaft],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_phy_ro_mst"},[motorSMPMSM.rotor.dim.phy.ro_mst],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_phy_ro_PM"},[motorSMPMSM.rotor.dim.phy.ro_PM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_phy_ro_shaft"},[motorSMPMSM.rotor.dim.phy.ro_shaft],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_M_mst_R"},[motorSMPMSM.rotor.dim.M_mst_R],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_M_R"},[motorSMPMSM.rotor.dim.M_R],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_I_R"},[motorSMPMSM.rotor.dim.I_R],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_dim_M_PM"},[motorSMPMSM.rotor.dim.M_PM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_hole_rotor"},[motorSMPMSM.rotor.draw.hole_rotor],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_magnet_style"},[motorSMPMSM.rotor.draw.magnet_style],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_magnetSlotPM_style"},[motorSMPMSM.rotor.draw.magnetSlotPM_style],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_magneticSteel_style"},[motorSMPMSM.rotor.draw.magneticSteel_style],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_rotorHub_style"},[motorSMPMSM.rotor.draw.rotorHub_style],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_shaft_style"},[motorSMPMSM.rotor.draw.shaft_style],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_draw_fail_elements"},[motorSMPMSM.rotor.draw.fail_elements],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_phy_R_PM"},[motorSMPMSM.rotor.phy.R_PM],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_rotor_phy_B_r_yoke"},[motorSMPMSM.rotor.phy.B_r_yoke],true);

      // casing
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_l_case_exp_front"},[motorSMPMSM.casing.dim.l_case_exp_front],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_l_case_exp_rear"},[motorSMPMSM.casing.dim.l_case_exp_rear],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_l_case_exp_void"},[motorSMPMSM.casing.dim.l_case_exp_void],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_l_case"},[motorSMPMSM.casing.dim.l_case],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_h_case"},[motorSMPMSM.casing.dim.h_case],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_phy_Delta_t_work"},[motorSMPMSM.casing.dim.phy.Delta_t_work],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_phy_t_amb"},[motorSMPMSM.casing.dim.phy.t_amb],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_phy_t_aro"},[motorSMPMSM.casing.dim.phy.t_aro],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_phy_ro_case"},[motorSMPMSM.casing.dim.phy.ro_case],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_M_S"},[motorSMPMSM.casing.dim.M_S],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_dim_I_S"},[motorSMPMSM.casing.dim.I_S],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_l_total"},[motorSMPMSM.casing.l_total],true);
      DataFiles.writeMATmatrix("motorSMPMSM_Structure.mat",{"motorSMPMSM_casing_h_total"},[motorSMPMSM.casing.h_total],true);

      // Export strings
      Modelica.Utilities.Files.remove("motorSMPMSM_Structure.txt");
      Modelica.Utilities.Streams.print("motorSMPMSM_plate_RatedVoltageType","motorSMPMSM_Structure.txt");
      Modelica.Utilities.Streams.print(motorSMPMSM.plate.RatedVoltageType,"motorSMPMSM_Structure.txt");

      Modelica.Utilities.Streams.print("motorSMPMSM_rotor_msg","motorSMPMSM_Structure.txt");
      Modelica.Utilities.Streams.print(motorSMPMSM.rotor.msg,"motorSMPMSM_Structure.txt");

    end SMPMSM_Export;
  end SMPMSM_Structure;

  package AuxiliaryFunc
    function cartdistance "Returns distance between two points in cart coords"
     input Real[2,1] point1;
     input Real[2,1] point2;
     output Real d "distance";
    algorithm
     d:=sqrt((point1[1, 1] - point2[1, 1])^2 + (point1[2, 1] - point2[2, 1])^2);
      annotation (
        Documentation(info="<html>

<head>

<title>cartdistance() help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNoSpacing><b>Cartdistance()</b> returns the distance between two
points in cart coords.</p>

<p class=MsoNoSpacing>distance = cartdistance( first point, second point)</p>

<p class=MsoNoSpacing>points must be 2x1 matrix i.e. [x;y]. If one point is 0,
just type 0.</p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>"));
    end cartdistance;

    function carter_factor
      "carter_factor returns stator/rotor carter factor based on geometry"
      import Modelica.SIunits;
      import Modelica.Constants;
      parameter input SIunits.Length opening "Slot opening";
      parameter input SIunits.Length airgap "Airgap length";
      parameter input SIunits.Length pitch;
      output Real K_C "Carter factor";

    protected
      SIunits.Length b_1=opening "Slot opening";
      SIunits.Length g=airgap "Airgap";
      SIunits.Length tau=pitch "Pitch";
      Real kappa;
    algorithm
      kappa:=2/Constants.pi*(atan(b_1/(2*g)) - 2*g/b_1*log(sqrt(1 + (b_1/(2*g))^2)));
      K_C:=tau/(tau - kappa*b_1);

    end carter_factor;

    function CmecCALC "CMEC interpolation"
      import Modelica.Constants;
      parameter input Integer p "Number of pole pairs";
      parameter input Modelica.SIunits.Power P_mec "Rated mechanical power";
      output Real Cmecc "Mechanical constant";
    protected
      parameter Real[:,:] CmecCALCv=DataFiles.readCSVmatrix("CmecCALCv.txt");
      parameter Real[:] Powpol=CmecCALCv[:,1] "Power per pole";
      parameter Real[:] nofP1=CmecCALCv[:,2] "Cmec values for number of P=1";
      parameter Real[:] nofP2=CmecCALCv[:,3] "Cmec values for number of P=2";
      parameter Real[:] nofP6=CmecCALCv[:,4] "Cmec values for number of P=6";
      parameter Real[2] nofPOLES={2,6};
      Real[2] CmecLIN "C_mec for interpolation when p<6";
      Real Powpol_act "Power per pole value for the actual motor";

    algorithm
      // Power per pole calculation
      Powpol_act:=P_mec/2/p/1000;
      if (Powpol_act<0.01) then
        Powpol_act:=1;
      elseif (Powpol_act>10000) then
        Powpol_act:=10000;
      end if;
      // C_mec calculation
      if (p==1) then
        Cmecc:=Modelica.Math.Vectors.interpolate(
            Powpol,
            nofP1,
            Powpol_act);
      elseif (p==2) then
        Cmecc:=Modelica.Math.Vectors.interpolate(
            Powpol,
            nofP2,
            Powpol_act);
      elseif (p==6) then
        Cmecc:=Modelica.Math.Vectors.interpolate(
            Powpol,
            nofP6,
            Powpol_act);
      elseif (p<6) then
        CmecLIN[1]:=Modelica.Math.Vectors.interpolate(
            Powpol,
            nofP2,
            Powpol_act);
        CmecLIN[2]:=Modelica.Math.Vectors.interpolate(
            Powpol,
            nofP6,
            Powpol_act);
        Cmecc:=Modelica.Math.Vectors.interpolate(
            nofPOLES,
            CmecLIN,
            p);
      end if;

    end CmecCALC;

    function hexapack
      "This function obtains a number of conductors per area, given a diameter following a hexagonal pattern"
      import Modelica.SIunits;
      import Modelica.Constants;
      parameter input SIunits.Length h_sc "Stator coil height";
      parameter input SIunits.Length b_sc "Stator coil width";
      parameter input SIunits.Length D_cond "Conductor diameter";
      output packr hexpack;
    protected
      Real pila_n;
      Real n;
      Real pila_m;
      Real m;
      Real me;
      Real b_sc_real;
      Real h_sc_real "Actual occupied area";
      Real C_sc "Number of conductors per area";
      Real fill "Fill factor";
      Real fill_real "Real fill factor";
      Real[:,:] centers;
      Integer centers_counter=1;
      Real[:,:] limit;
      Real[:,:] limit_real;

    algorithm
      pila_n:=floor((h_sc - D_cond)/(D_cond*cos(Constants.pi/6)));
      n:=1 + pila_n;
      pila_m:=floor(b_sc/D_cond);
      if b_sc>=pila_m*D_cond+D_cond/2 then
        m:=floor(b_sc/D_cond);
        me:=m;
        // Actual occupied area
        b_sc_real:=m*D_cond + D_cond/2;
      else
        m:=floor(b_sc/D_cond);
        me:=m - 1;
        // Actual occupied area
        b_sc_real:=m*D_cond;
      end if;
      C_sc:=abs(floor(n/2)*(m + me) + rem(n, 2)*(m));
      h_sc_real:=D_cond + (n - 1)*D_cond*cos(Constants.pi/6);
      if C_sc==0 then
        fill:=0;
        fill_real:=0;
      else
        fill:=(C_sc*Constants.pi*(D_cond/2)^2)/(h_sc*b_sc);
        fill_real:=(C_sc*Constants.pi*(D_cond/2)^2)/(h_sc_real*b_sc_real);
      end if;

      // Centers positioning
      centers:=zeros(integer(C_sc), 2);

      for j in 1:1:n loop // for each column
        if rem(j,2)==0 then // even columns
          for i in 1:1:me loop
            centers[centers_counter,:]:={D_cond/2 + D_cond*cos(Constants.pi/6)*(j - 1),D_cond*(i)};
            centers_counter:=centers_counter + 1;
          end for;
        else // odd columns
          for i in 1:1:m loop
            centers[centers_counter,:]:={D_cond/2 + D_cond*cos(Constants.pi/6)*(j - 1),D_cond/2 + D_cond*(i - 1)};
            centers_counter:=centers_counter + 1;
          end for;
        end if;
      end for;

      // Check
      limit:=[0,0; h_sc,0; h_sc,b_sc; 0,b_sc; 0,0];
      limit_real:=[0,0; h_sc_real,0; h_sc_real,b_sc_real; 0,b_sc_real; 0,0];

      hexpack.C_sc:=C_sc;
      hexpack.D_cond:=D_cond;
      hexpack.K_f:=fill;
      hexpack.K_f_real:=fill_real;
      hexpack.dim.h_sc_real:=h_sc_real;
      hexpack.dim.b_sc_real:=b_sc_real;
      hexpack.draw.centers:=centers;
      hexpack.draw.Semi_coil_points:=limit;
      hexpack.draw.Semi_coil_real_points:=limit_real;
    annotation (
        Documentation(info="<html>

<head>
<title>hexapack() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b>hexapack() </b>performs the following operations:</p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Calculate the number of conductors able to fit
in the area.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Calculate the actual area occupied by the
conductors.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Calculate the real filling factor (by the actual
area) and the filling factor taking into account all the viable allocable area.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-GB>-<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Return the actual area position and the relative
position of each conductor’s centre.</span></p>

<p class=MsoNormal align=center style='text-align:center'><img width=261
height=202 id='Imagen 30' src='hexapack_archivos/image001.jpg'></p>

</div>

</body>

</html>
"));
    end hexapack;

    record packr "Record for output of hexapack function"
      import Modelica.SIunits;
      Integer Gauge_st "Conductor AWG Gauge";
      Real C_sc "Number of conductors per area";
      SIunits.Length D_cond "Conductor diameter";
      Real K_f "Fill factor";
      Real K_f_real "Real fill factor";
      pack_dim dim;
      pack_draw draw;
    end packr;

    record pack_dim "Record for output of hexapack function"
      Real b_sc_real;
      Real h_sc_real "Actual occupied area";
    end pack_dim;

    record pack_draw "Record for output of hexapack function"
      Real[:,:] centers;
      Real[:,:] Semi_coil_points;
      Real[:,:] Semi_coil_real_points;
    end pack_draw;

    function htrmodif
      "HTRMODIF recalculates htr based on squared slot to htr based on trapezoidal slot"
      import Modelica.Constants;
      import Modelica.SIunits;
      input Integer Qs "Number of stator slots";
      input SIunits.Height htr "Slot height";
      input SIunits.Length bs "Slot width";
      output SIunits.Height htrmod "Modified slot height";
    protected
      parameter Real alpha_m=Constants.pi/Qs "Half of stator pitch";
      parameter SIunits.Length a=bs/2 "Half of bs";
    algorithm
      htrmod:=(-a + sqrt(a*a + 2*a*htr*tan(alpha_m)))/tan(alpha_m);
      annotation (
        Documentation(info="<html>

<head>
<title>htrmodif() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>htrmodif()</span></b><span lang=EN-GB> This</span><span
lang=EN-GB> </span><span lang=EN-US>method is based on the idea of extending
the D circle curve to a straight line. Figure 1 shows the example for v-shape
machine, but works swell with any other machine following the same stator
slotting principle. </span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=602 height=223 src='htrmodif_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019887'></a><a name='_Toc402539896'><span
lang=EN-GB>Figure </span></a><span lang=EN-GB>1</span><span lang=EN-GB>. Geometry
“expansion” for slot/tooth calculation</span></p>

<p class=MsoNormal><span lang=EN-US>So slots on rotor and stator become
rectangles:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=67 height=17 src='htrmodif_archivos/image002.png'></span></p>

<p class=MsoNormal><span lang=EN-US>To compensate compression effect of
diameters bigger than </span><span class=CdigoenWordCar><span lang=EN-GB
style='font-size:10.0pt'>D</span></span><span lang=EN-GB> </span><span
lang=EN-GB>in stator, just after width is calculated, function <b>htrmodif()</b></span><span
lang=EN-GB style='color:#7030A0'> </span><span lang=EN-GB>is applied to correct
form factor to a trapezoid. This function uses pitch and original height and
width to calculate a trapezoid of the same</span><span lang=EN-US> base and
total area which leads to a new height.</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=194 height=35 src='htrmodif_archivos/image003.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Where index</span><span lang=EN-US> </span><b><span
lang=EN-GB>i</span></b><span lang=EN-US> is valid for stator (S</span><span
lang=EN-GB>),  <b>htrmodifflag</b></span><span lang=EN-GB> will determine if
this compensation is used or not, depending on machine power. This
determination is different for every machine therefore <b>htrmodifflag</b> is
set outside the function.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>"));
    end htrmodif;

    function MAGNETdb "MAGNETdb return properties of selected permanent magnet"
      import Modelica.Constants;
      parameter input Integer family "PM material family";
      parameter input Integer number "Number from database";
      output Real[3] PM_prop "Properties of selected PM";
    protected
      parameter Real[:,:] PMDB1=DataFiles.readCSVmatrix("PMDB1.txt");
    algorithm
        PM_prop[1]:=PMDB1[number, 3];// B_r
        PM_prop[2]:=PMDB1[number, 4];// H_c
        PM_prop[3]:=PMDB1[number, 5];// mue_r
    end MAGNETdb;

    function ellipse "Finds an ellipse from five points and returns its center"
                     //ellipse5_1
    /* General conical equation: A*x^2+B*x*y+C*y^2+D*x+E*y+F=0
 To reduce one parameter can be set as:
                           A/F*x^2+B/F*x*y+C/F*y^2+D/F*x+E/F*y+1=0

 Then it is solved as a linear system where: A/F, B/F, ... is unknown:

    | x_1^2   x_1*y_1   y_1^2  ... |      |A/F|     |-F/F| (-F/F = -1)
    | x_2^2   x_2*y_1   y_2^2  ... |      |B/F|     |-F/F|
    | x_3^2   x_3*y_1   y_3^2  ... |  x   |C/F|  =  |-F/F| 
    | x_4^2   x_4*y_1   y_4^2  ... |      |D/F|     |-F/F|
    | x_5^2   x_5*y_1   y_5^2  ... |      |E/F|     |-F/F|

            Am[5x5]                        [5x1]      [5x1]
*/
     import Modelica.Constants;
     input Real[:,:] c=[81.293447914668720,26.362064480004243;87.445767853761080,28.361286844957778;88.210491886375500,28.661326228276370;87.414866154323590,28.456392496587295;81.262546215231230,26.457170131633760]
        "5x2 matrix containing 5 point [x_i y_i in a row] if size(5,2) else vector with coefficients A,B,C,D,E,F";
     output ellipser ellipse;

    protected
      constant Integer constant_1=1 "F";
      Real[5,5] Am "Am matrix";
      Real[6,1] Cm "Cm matrix";
      Real[3,3] A_q "A_q matrix";
      Real[2,1] bb "Eigen values";
      Real[2,2] Aa "Matrix with eigenvectors";
      Real eigen_handle[2,2] "Calculation handle matrix";
      Real a1;
      Integer mineigenpos;
      Integer maxeigenpos;

    algorithm
      ellipse.degenerated:=0;
      ellipse.center:=[0;0];
      ellipse.axisx:=0;
      ellipse.axisy:=0;
      ellipse.angle:=0;
      ellipse.length:=0;
      if size(c,1)==5 and size(c,2)==2 then
        Am:=[c[:, 1] .^ 2,c[:, 1] .* c[:, 2],c[:, 2] .^ 2,c[:, 1],c[:, 2]];  // Construction of Am matrix
        Cm[1:5,:]:=Modelica.Math.Matrices.solve2(Am, -constant_1*ones(5, 1));
        Cm:=[Cm[1:5,1];constant_1];
      elseif size(c,1)==1 and size(c,2)==6 then
        Cm:=transpose(c);
      end if;
      A_q:=[Cm[1, 1],Cm[2, 1]/2,Cm[4, 1]/2;Cm[2, 1]/2,Cm[3, 1],Cm[5, 1]/2;Cm[4, 1]/2,Cm[5, 1]/2,Cm[6,
        1]];

      if (Modelica.Math.Matrices.det(A_q)==0) then
        Modelica.Utilities.Streams.print("**** ERROR: Degenerated Conic Detected, program aborted","");
        ellipse.degenerated:=1;
        break;
      end if;
      if (Modelica.Math.Matrices.det(A_q[1:2,1:2])>0) then
        //Modelica.Utilities.Streams.print("Ellipse confirmed","");
      elseif (Modelica.Math.Matrices.det(A_q[1:2,1:2])<0) then
        Modelica.Utilities.Streams.print("*** ERROR: hyperbola","");
        ellipse.degenerated:=3;
      else
        Modelica.Utilities.Streams.print("*** ERROR: parabola","");
      end if;

      ellipse.center[1:2,1]:=Modelica.Math.Matrices.inv(A_q[1:2, 1:2])*(-A_q[1:2, 3]);
      (eigen_handle,):=Modelica.Math.Matrices.eigenValues(A_q[1:2, 1:2]);
      eigen_handle:=Modelica.Math.Matrices.flipUpDown(eigen_handle);
      bb[:,1]:=eigen_handle[1:2, 1];
      (,Aa):=Modelica.Math.Matrices.eigenValues(A_q[1:2, 1:2]);
      Aa:=Modelica.Math.Matrices.flipUpDown(Aa);
      Aa[1,2]:=-Aa[1, 2];
      Aa[2,1]:=-Aa[2, 1];
      ellipse.axisx:=1/sqrt(bb[1, 1]/(-Modelica.Math.Matrices.det(A_q)/Modelica.Math.Matrices.det(
        A_q[1:2, 1:2])));
      ellipse.axisy:=1/sqrt(bb[2, 1]/(-Modelica.Math.Matrices.det(A_q)/Modelica.Math.Matrices.det(
        A_q[1:2, 1:2])));
      if (ellipse.axisy >= ellipse.axisx) then
        a1:=ellipse.axisy;
        ellipse.axisy:=ellipse.axisx;
        ellipse.axisx:=a1;
      end if;
      (,mineigenpos):=minvector(abs(bb[:,1]));  // min eigenval position
      maxeigenpos:=3 - mineigenpos;             // max eigenval position

      // Ellispe angle is defined by the angle between the GLOBAL X axis and the
      // axis of the longest elipse.axis (belongs to the min eigen_value)
      ellipse.angle:=atan(Aa[2, mineigenpos]/Aa[1, mineigenpos]);

      // Ellipse lenght (perimeter):
      ellipse.length:=Constants.pi*(3*(ellipse.axisx + ellipse.axisy) + sqrt((3*ellipse.axisx +
        ellipse.axisy)*(ellipse.axisx + 3*ellipse.axisy)));

      annotation (
        Documentation(info="<html>

<head>
<title>ellipse help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><span lang=EN-GB>This function is able to find an ellipse by
five points and return its centre, axis and angle due respect global XY.</span></p>

<p class=MsoNormal><span lang=EN-GB>This function is able to find an ellipse by
six coefficients of its canonical equation</span><span lang=EN-GB
style='font-size:12.0pt;line-height:115%'> </span><span lang=EN-GB>and return
its centre, axis and angle due to respect global XY.</span></p>

<p class=MsoNormal><span lang=EN-GB>It can also plot it in any figure by giving
its figure number.</span><span lang=EN-GB style='font-size:12.0pt;line-height:
115%'> </span><span lang=EN-GB>It can also limit the plotting angles of the
ellipse.</span></p>

<p class=MsoNoSpacing><span lang=EN-GB>Input descriptions:</span></p>

<p class=MsoNoSpacing><span lang=EN-GB>c &lt;-- [5x2] matrix with five points</span></p>

<p class=MsoNoSpacing><span lang=EN-GB>              column 1: x coordinate. </span></p>

<p class=MsoNoSpacing><span lang=EN-GB>              column 2, y coordinate.</span></p>

<p class=MsoNoSpacing><span lang=EN-GB style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNoSpacing><span lang=EN-GB>c &lt;--[6x1] matrix with 5 coefficients.</span></p>

<p class=MsoNoSpacing><span lang=EN-GB>                 every coefficient is A,
B, C, D, E, F of canonical equation: </span></p>

<p class=MsoNoSpacing><span
style='font-size:11.0pt;line-height:115%;font-family:'Calibri','sans-serif''><img
width=257 height=19 src='ellipse_archivos/image001.png'></span></p>

<p class=MsoNormal><span lang=EN-GB style='font-size:12.0pt;line-height:115%'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:'Courier New''>&nbsp;</span></p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>
"));
    end ellipse;

    record ellipser "Record for output of ellipse function"
      Integer degenerated "Conic degeneration flag";
      Real[2,1] center "Center coordinates";
      Real axisx "long axis length";
      Real axisy "short axis length";
      Real angle "angle respect to x axis";
      Real length "Perimeter aproximation";
    end ellipser;

    function feasibleregion
      "This function tells if a couple of angles for SRM is feasible"
                            // feasibleregion2
      import Modelica.Constants;
      import Modelica.SIunits;

      input SIunits.Angle b_s;
      input SIunits.Angle b_r;
      input Integer N_s;
      input Integer N_r;
      input SIunits.Angle alpha_seg;
      output Integer[2] Feasible;
    protected
      Real q=N_s/2 "Stator phases";
      Integer condition1;
      Integer condition2;
      Integer condition3;
      Integer condition4;
      Integer condition5;
      SIunits.Angle stroke_angle=2*Constants.pi/(N_r*q) "Stroke angle";

    algorithm
      Modelica.Utilities.Streams.print("*** Start: feasibleregion ***","");
      // First condition
      if (b_s<=b_r) then
        condition1:=1;
      else
        condition1:=0;
      end if;

      // Second condition
      if b_s>=stroke_angle then
        condition2:=1;
      else condition2:=0;
      end if;

      // Third condition
      if 2*Constants.pi/N_r-b_r>=b_s then
        condition3:=1;
      else condition3:=0;
      end if;

      // Fourth condition
      if b_s<2*(Constants.pi/N_s-alpha_seg) then
        condition4:=1;
      else
        condition4:=0;
      end if;

      // Fifth condition
      if b_r<2*Constants.pi/N_r then
        condition5:=1;
      else
        condition5:=0;
      end if;

      Feasible[1]:=condition1*condition2*condition3;
      Feasible[2]:=condition4*condition5;
      Modelica.Utilities.Streams.print("*** End: feasibleregion ***","");
        annotation (
        Documentation(info="<html>

<head>

<title>feasibleregion() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b>feasibleregion()</b> The feasible region is the area in
the stator and polar angles space where the combination of both angles is
valid, but is not optimized. This region is the intersection of three magnetic conditions
that have to be fulfilled. Plus two physical constrains. Being:</p>

<p class=MsoNormal><span
style='font-size:11.0pt;line-height:115%;font-family:'Calibri','sans-serif''><img
width=43 height=35 src='feasibleregion_archivos/image001.png'></span></p>

<p class=MsoNormal><b>First condition</b>: The stator pole angle <strong><i><span
style='font-family:Symbol'>b</span></i></strong><i><sub>sp</sub></i> is usually
set smaller than the rotor pole angle <strong><i><span style='font-family:Symbol'>b</span></i></strong><i><sub>rp</sub></i>,
i.e. <span style='position:relative;top:7.0pt'><img width=52 height=21
src='feasibleregion_archivos/image002.png'></span>  </p>

<p class=MsoNormal><b>Second condition</b>: The effective torque region is
larger than the stroke angle <span style='position:relative;top:5.0pt'><img
width=90 height=19 src='feasibleregion_archivos/image003.png'></span> but
smaller than the stator pole angle <strong><i><span style='font-family:Symbol'>b</span></i></strong><i><sub>sp</sub></i>.
Therefore, to ensure starting capability at any initial position, it must be
accomplished<span style='position:relative;top:7.0pt'><img width=41 height=21
src='feasibleregion_archivos/image004.png'></span>.</p>

<p class=MsoNormal><b>Third condition</b>: The inductance profile of each
stator phase repeats every 360°/<i>N<sub>r</sub></i>. Within the zone in which
the stator and rotor pole arcs are not overlapped, the phase inductance remains
at its minimum unaligned value <i>L<sub>u</sub></i>. To avoid any overlap
between the stator and rotor poles in the unaligned position, the angle between
two adjacent rotor poles corners must be greater than the stator pole arc. This
condition is attained when<span style='position:relative;top:7.0pt'><img
width=111 height=21 src='feasibleregion_archivos/image005.png'></span>.</p>

<p class=MsoNormal><b>Fourth condition:</b> The sum of number of poles and
desired stator pole angle must be lesser than 2&#960;.</p>

<p class=MsoNormal><b>Fifth condition:</b> The sum of the number of poles and
desired rotor pole angle must be lesser than 2&#960;.</p>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=295 style='width:220.9pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;page-break-after:avoid'><span
  lang=EN-US style='font-size:10.0pt;font-family:'Times New Roman','serif''><img
  width=260 height=187 src='feasibleregion_archivos/image006.png'></span></p>
  </td>
  <td width=287 style='width:215.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;page-break-after:avoid'><span
  style='font-size:10.0pt;font-family:'Times New Roman','serif''><img
  width=240 height=183 id='Imagen 39' src='feasibleregion_archivos/image007.jpg'></span></p>
  </td>
 </tr>
</table>

<p class=MsoCaption><a name='_Toc414019931'></a><a name='_Ref413083516'><span
lang=EN-GB>Figure </span></a><span lang=EN-GB>1. Left: Stator/Rotor pole angle
feasible region representation for a 8/6 motor.</span><span lang=EN-GB> Right:
MAGMOLIB feasible region generated graphic: green triangle feasible region for
magnetic constrains. Red rectangle limits the physical limitations.</span></p>

<p class=MsoNormal>Feasible region returns two values in a vector called
feasible. The first element <strong><span style='font-family:'Calibri','sans-serif''>feasibe(1)==1</span></strong>
if the three magnetic conditions are met, and <strong><span style='font-family:
'Calibri','sans-serif''>feasibe(1)==0</span></strong> if only one of them is
not. <strong><span style='font-family:'Calibri','sans-serif''>feasibe(2)==1</span></strong>
if both geometrical conditions are met, and <strong><span style='font-family:
'Calibri','sans-serif''>feasibe(2)==0</span></strong> if any is not. This
second condition is used after before checking current density condition to
discard the machine.</p>

<p class=MsoNormal>After magnetic feasible region has been checked, then if no
shaft diameter has been set, one is automatically selected depending on
machines power based on normalized diameter per power.</p>

</div>

</body>

</html>"));
    end feasibleregion;

    function Inductances "Inductance Calculation Funcion, Inductances7"
      import Modelica.Constants;
      import Modelica.SIunits;

      parameter input IPM_Structure.motorIPM motorINPUT;
      output IPM_Structure.motorIPM motorOUTPUT;

      //output Real Lss[1,2] "DQ magnetizing inductances";
      //SIunits.Current I=141.4214 "Rated stator current";
      //Real p_lf_ref=0 "Magnet permeance leakage factor";
    protected
      SIunits.Diameter D=0.1877 "Stator inner diameter";
      Integer p=4 "Number of pole pairs";
      SIunits.Length l=0.0760 "Stack length";
      SIunits.Length g=6e-4 "Air gap length";
      Real K_c=1.1019 "Carter fringing coefficient";
      Real K_w=0.9659 "Winding factor";
      Real N_ph=104 "Number of turns per phase";
      SIunits.Reluctance R_g=9.28e4 "Air gap reluctance between PM and stator";
      SIunits.Reluctance R_PM=1.0549e6 "PM reluctance";
      //SIunits.Height h_PM_ref=0.0056 "Magnet thickness";
      Real alpha=0.8269 "Magnet cover factor";
      SIunits.Length l_web=0.0118 "Web Length between consecutive pole magnets";
      //Real Omega_ref=0 "Web angle cover factor";
      //SIunits.MagneticFlux flux_sat=0.0014 "Saturation flux difference";

      //Real Inductances[2,1] "Inductances";
      Real p_lf "Magnet permeance leakage factor";
      SIunits.Length g_eff "Basic Effective Airgap";
      Real lcf_web "Top Web Cover Factor";
      Real Omega "Web angle cover factor";
      SIunits.Reluctance R_gin "Airgap reluctance";
      SIunits.Permeance P_m "Permeance";
      Real k_alphad "D constant 1";
      Real k_1ad "D constant 2";
      Real k_1 "D constant 3";
      SIunits.Length g_eff_fd "d-axis Double Effective Airgap";
      Real Gamma_d "d-axis Effective Airgap Ratio";
      SIunits.Length g_eff_fq "q-axis Double Effective Airgap";
      Real Gamma_q "q-axis Effective Airgap Ratio";
      Real k_1aq "Q constants";
      SIunits.Inductance L_m0
        "Surface Magnet Synchornous Magnetizing Equivalent Inductance";
      SIunits.Inductance L_d "Direct Magnetizing Inductance";
      SIunits.Inductance L_q "Quadrature Magnetizing Inductance";

    algorithm
      p_lf:=motorINPUT.stator.phy.p_lf;
      D:=motorINPUT.stator.D;
      p:=motorINPUT.p;
      l:=motorINPUT.stator.dim.l;
      g:=motorINPUT.g;
      K_c:=motorINPUT.stator.dim.K_c;
      K_w:=motorINPUT.stator.win.K_w;
      N_ph:=motorINPUT.stator.win.N_ph;
      R_g:=motorINPUT.airgap.phy.R_g;
      R_PM:=motorINPUT.rotor.phy.R_PM;
      alpha:=motorINPUT.rotor.dim.mcf_PM;
      l_web:=motorINPUT.rotor.dim.l_web;
      Omega:=motorINPUT.rotor.dim.Omega;

      Modelica.Utilities.Streams.print("Start: Inductances","");
      if (p_lf==0) then
        p_lf:=0.1;   // Magnet permeance leakage factor
      end if;

      g_eff:=g*K_c;  // Basic Effective Airgap
      if (Omega==0) then
        lcf_web:=2*l_web/(D/2*2*p*Constants.pi)*Constants.pi; // Top Web Cover Factor
        Omega:=lcf_web;
      end if;
      R_gin:=2*p*g_eff/(Constants.mue_0*D*l*alpha*Constants.pi);// Airgap reluctance

      // Magnet permeance
      P_m:=1/R_PM*(1 + p_lf);

      // Magnetic potential after the magnet in the rotor
      // FOR DIRECT AXIS (d):
      k_alphad:=(sin(alpha*Constants.pi/2))/(alpha*Constants.pi/2); // D constant 1
      k_1ad:=alpha + sin(alpha*Constants.pi)/Constants.pi;          // D constant 2
      k_1:=4/Constants.pi*sin(alpha*Constants.pi/2);                // D constant 3
      g_eff_fd:=g_eff/(k_1ad - k_1*k_alphad/(1 + P_m*R_g));  // d-axis Double Effective Airgap
      Gamma_d:=g_eff/g_eff_fd;// d-axis Effective Airgap Ratio

      // FOR QUADRATURE AXIS (q):
      k_1aq:=alpha + Omega + (sin(Omega*Constants.pi) - sin(alpha*Constants.pi))/Constants.pi;
      g_eff_fq:=g_eff/k_1aq;    // q-axis Double Effective Airgap
      Gamma_q:=g_eff/g_eff_fq;  // q-axis Effective Airgap Ratio

      // Non salient pole magnetizing inductance
      L_m0:=3*Constants.mue_0*D*l/(Constants.pi*p^2*g_eff)*(K_w*N_ph)^2;// Surface Magnet Synchornous Magnetizing Equivalent Inductance
      L_d:=L_m0*Gamma_d;                           // Direct Magnetizing Inductance
      L_q:=L_m0*Gamma_q;                           // Quadrature Magnetizing Inductance

      //Lss[1,1]:=L_d;
      //Lss[1,2]:=L_q;

      motorOUTPUT:=motorINPUT;
      motorOUTPUT.stator.phy.L_m0:=L_m0;
      motorOUTPUT.stator.phy.L_qm:=L_q;
      motorOUTPUT.stator.phy.L_dm:=L_d;

      Modelica.Utilities.Streams.print("End: Inductances","");
      annotation (
        Documentation(info="<html>

<head>
<title>inductance() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>Inductance()</span></b><span lang=EN-GB>
</span><span lang=EN-US>calculates direct and quadrature inductances. Starting
from SMPMSM inductance value, with no saliency, airgap component of synchronous
inductance is:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=181 height=37 src='inductance_archivos/image001.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Where</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=68 height=17 src='inductance_archivos/image002.png'></span></p>

<p class=MsoNormal><span lang=EN-US>What is has been done is to </span><span
lang=EN-GB>correct L_m0 value</span><span lang=EN-GB> </span><span lang=EN-US>with
a corresponding new equivalent airgap that takes into account shape
discontinuities and thus:</span></p>

<div align=center>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='margin-left:76.3pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=161 style='width:120.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=51 height=38 src='inductance_archivos/image003.png'></span></p>
  </td>
  <td width=54 style='width:40.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:Wingdings'>à</span></p>
  </td>
  <td width=192 style='width:144.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=94 height=17 src='inductance_archivos/image004.png'></span></p>
  </td>
 </tr>
 <tr>
  <td width=161 style='width:120.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=50 height=39 src='inductance_archivos/image005.png'></span></p>
  </td>
  <td width=54 style='width:40.25pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span lang=EN-US
  style='font-size:10.0pt;font-family:Wingdings'>à</span></p>
  </td>
  <td width=192 style='width:144.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=94 height=17 src='inductance_archivos/image004.png'></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-US>This is only valid taking first harmonic
for inductance, which means, for d or q:</span></p>

<div align=center>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=153 valign=top style='width:115.1pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=97 height=17 src='inductance_archivos/image006.png'></span></p>
  </td>
  <td width=109 valign=top style='width:81.7pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:'Times New Roman','serif''>For
  </span><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
  position:relative;top:3.0pt'><img width=12 height=17
  src='inductance_archivos/image007.png'></span><span lang=EN-US
  style='font-size:10.0pt;font-family:'Times New Roman','serif''>and </span><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
  position:relative;top:4.5pt'><img width=25 height=19
  src='inductance_archivos/image008.png'></span></p>
  </td>
  <td width=38 valign=top style='width:1.0cm;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span lang=EN-US style='font-size:10.0pt;font-family:Wingdings'>à</span></p>
  </td>
  <td width=189 valign=top style='width:5.0cm;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=96 height=17 src='inductance_archivos/image009.png'></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-GB>Where X_delta corresponds</span><span
lang=EN-GB> </span><span lang=EN-US>to non-magnetizing inductance component.</span></p>

<p class=MsoNormal><span lang=EN-US>These equivalent airgaps have different
values for d and q positioning because of change in rotor geometry (see </span><span lang=EN-GB>Figure 1</span><span lang=EN-US>)</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=604 height=387 src='inductance_archivos/image010.png'></span></p>

<p class=MsoCaption><a name='_Toc414019906'></a><a name='_Toc402539914'></a><a
name='_Ref391897762'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Different inductance approximations given
motor geometry at a given instant. Green= active coil, Grey= Rotor, Blue=
stator</span></p>

<p class=MsoNormal><span lang=EN-GB>Taking only fundamental component of
armature-reaction flux. The flux expression can be obtained via Flux density:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=208 height=42 src='inductance_archivos/image011.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Where these limits (</span><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
position:relative;top:2.5pt'><img width=13 height=17
src='inductance_archivos/image012.png'></span><span lang=EN-GB>) are initial
and final points of magnet “slot” (without magnet) seen in cylindrical
coordinates. </span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=463 height=39 src='inductance_archivos/image013.png'></span></p>

<p class=MsoNormal><span lang=EN-US>This takes into account two potentials (</span><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
position:relative;top:3.0pt'><img width=37 height=17
src='inductance_archivos/image014.png'></span><span lang=EN-GB> and </span><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
position:relative;top:2.5pt'><img width=15 height=17
src='inductance_archivos/image015.png'></span><span lang=EN-US>). The
difference between both theta angles will measure in radians the percentage
marked by </span><span class=CdigoenWordCar><span lang=EN-US style='font-size:
10.0pt'>mcf_PM</span></span><span lang=EN-US> (alpha). This in radians is:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=195 height=33 src='inductance_archivos/image016.png'></span></p>

<p class=MsoNormal><span lang=EN-US>When both of theta angles are half of that
angle (meaning are positioned in d, q,-d,-q positions within a static
reference):</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=273 height=36 src='inductance_archivos/image017.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>From this equation the new relation between
g’ and g’’ can be found once two potential (limited to angles described are
found).</span></p>

<p class=MsoNormal><span lang=EN-GB>For d-axis case:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=260 height=67 src='inductance_archivos/image018.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>And for q-axis case:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=102 height=48 src='inductance_archivos/image019.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>If web must be taken into account
expression changes to:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=187 height=48 src='inductance_archivos/image020.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Where Omega</span><span lang=EN-GB> (&#8486;)
is the equivalent to magnet cover factor for rotor’s web. </span></p>

<p class=MsoNormal><span lang=EN-US>Note that denominators of these g’’
expressions are gamma factors used to correct sync inductance of equivalent (in
airgap) machine.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end Inductances;

    function Makerefe "Makes a 2D-turn matrix, angle in rad"
     input Modelica.SIunits.Angle angle "Rotation angle";
     output Real[2,1] A "Turn matrix";
    algorithm
      A:=[[cos(2*angle),sin(2*angle)]; [sin(2*angle),-cos(2*angle)]];
    annotation (
        Documentation(info="<html>

<head>
<title>makerefe() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>makerefe()</span></b><span lang=EN-GB>
function which performs the following operation:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=197 height=35 src='makerefe_archivos/image001.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Input a point in “a” base and input a
reflection axis (&#952;) in RADs </span><span lang=EN-GB style='font-family:
Wingdings'>à</span><span lang=EN-GB> outputs a point in “b” base.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end Makerefe;

    function Maketurn "Makes a 2D-turn matrix, angle in rad"
     input Modelica.SIunits.Angle angle "Rotation angle";
     output Real[2,2] A "Turn matrix";
    algorithm
      A:=[[cos(angle),-sin(angle)]; [sin(angle),cos(angle)]];
    annotation (
        Documentation(info="<html>

<head>
<title>maketurn() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>maketurn()</span></b><span lang=EN-GB>
function which performs the following operation:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=180 height=35 src='maketurn_archivos/image001.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Input a point in “a” base and input a turn
(&#952;) in RADs </span><span lang=EN-GB style='font-family:Wingdings'>à</span><span
lang=EN-GB> outputs a point in “b” base.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>"));
    end Maketurn;

    function maxvector
      "Returns minimum element and its index from input vector"
      import Modelica.Math.Vectors;
      input Real v[:] "Vector";
      output Real result "Element with the minimum value";
      output Integer index "v[index] has the minimum";
    protected
      Real v_i;
      Real res;
    algorithm
      if size(v,1) > 0 then
        res := v[1];
        index  := 1;
        for i in 2:size(v,1) loop
          v_i := v[i];
          if v_i > res then
            res := v_i;
            index := i;
          end if;
        end for;
        result :=v[index];
      else
        result := 0;
        index  := 0;
      end if;

    end maxvector;

    function minmatrix
      "Returns minimum element and its index from each matrix column"
      import Modelica.Math.Vectors;
      input Real m[:,:] "Matrix";
      output Real[size(m,2)] result
        "Elements with the minimum value per column";
      output Integer[size(m,2)] index
        "m[index] has the minimum value per column";
    protected
      Real m_i;
      Real res;
    algorithm
      if size(m,2) > 0 then

        for j in 1:size(m,2) loop
        res := m[1,j];
        index[j]  := 1;
        for i in 2:size(m,1) loop
          m_i := m[i,j];
          if m_i < res then
            res := m_i;
            index[j] := i;
          end if;
        end for;
        result[j] :=m[index[j],j];
        end for;

      else
        result := zeros(size(m,2));
        index  := zeros(size(m,2));
      end if;

    end minmatrix;

    function minvector
      "Returns minimum element and its index from input vector"
      import Modelica.Math.Vectors;
      input Real v[:] "Vector";
      output Real result "Element with the minimum value";
      output Integer index "v[index] has the minimum";
    protected
      Real v_i;
      Real res;
    algorithm
      if size(v,1) > 0 then
        res := v[1];
        index  := 1;
        for i in 2:size(v,1) loop
          v_i := v[i];
          if v_i < res then
            res := v_i;
            index := i;
          end if;
        end for;
        result :=v[index];
      else
        result := 0;
        index  := 0;
      end if;

    end minvector;

    function MOIMA "Obtains Area, Mass, Iz"
     /*
  MOIMA: This functions obtains MoI areas and Masses of polygonal set of
 points. This only works for simple polygons i.e. non self intersecting
 polygons.
 Sources of Area: http://en.wikipedia.org/wiki/Polygon
 Sources of MoI:  http://en.wikipedia.org/wiki/List_of_moments_of_inertia

 Geometrical conditions:
       - Polygon must not self intersect
       - Polygon must be closed (i.e. first and last poin must be the
           same)
       - Points must be defined as a matrix of [N+1,2] where N is the 
         number of points.
       - Points must be oriented clock or anticlockwise, but must be 
         consecutive (avoiding fake self intersects). 
         */
     import Modelica.Math.Vectors;
     import Modelica.SIunits;
     input Real[:,:] points "Points";
     input Real ro "ro";
     input Integer outputv "output type";
     output Real data "Output single data";
     //output MOIMAr data "Output record data";
    protected
      Real numer_MOIMA;
      Real denom_MOIMA;
      Real[:,:] pointsD;
      Real mass;
      Real area;
      Real I_z;
      Real AA_MOIMA;
      Real IX "X-axis kind of inertia";
      Real IY "Y-axis kind of inertia";
      Real Area "Total area verification";
      Real CX "Kind of polygon centroid x coordiante";
      Real CY "Kind of polygon centroid y coordinate";
      Real Sharedproduct;
      SIunits.MomentOfInertia IXx "X-axis moment of inertia per linear meter";
      SIunits.MomentOfInertia IYy "Y-axis moment of inertia per linear meter";
      SIunits.Length CXx "Polygon centroid x coordinate";
      SIunits.Length CYy "Polygon centorid y coordinate";

    algorithm
      //numer_MOIMA:=0;
      //denom_MOIMA:=0;

      pointsD:=[points[2:end, 1:end]; transpose(matrix(points[1, :]))];
      area:=abs(0.5*sum(points[:, 1].*pointsD[:, 2] - pointsD[:, 1].*points[:, 2]));
      mass:=area*ro;

      /* points matrix have 1 more point (repited) to close the
  polygon therefore the loop runs up to the number of points minus 1*/

    //   for i in 1:size(points,1)-1 loop
    //     AA_MOIMA:=(Vectors.norm(matrix(pointsD[i, :])*transpose(matrix(points[i, :]))))[1];
    //     numer_MOIMA:=numer_MOIMA + AA_MOIMA*(vector(pointsD[i, :])*vector(pointsD[i, :]) + vector(pointsD[i, :])*vector(points[i, :]) + vector(points[i, :])*vector(points[i, :]));
    //     denom_MOIMA:=denom_MOIMA + AA_MOIMA;
    //   end for;
    //     I_z:=1/6*mass*numer_MOIMA/denom_MOIMA;
      IX:=0;
      IY:=0;
      Area:=0;
      CX:=0;
      CY:=0;

      for i in 1:size(points,1)-1 loop
        Sharedproduct:=points[i, 1]*points[i + 1, 2] - points[i + 1, 1]*points[i, 2];
        Area:=Area + Sharedproduct;
        IX:=IX + (points[i, 2]^2 + points[i, 2]*points[i + 1, 2] + points[i + 1, 2]^2)*Sharedproduct;
        IY:=IY + (points[i, 1]^2 + points[i, 1]*points[i + 1, 1] + points[i + 1, 1]^2)*Sharedproduct;
        CX:=CX + (points[i, 1] + points[i + 1, 1])*Sharedproduct;
        CY:=CY + (points[i, 2] + points[i + 1, 2])*Sharedproduct;
      end for;

      IXx:=abs(IX*ro/12);
      IYy:=abs(IY*ro/12);
      //CXx:=CX/6/area;
      //CYy:=CY/6/area;
      I_z:=(IXx + IYy);

        if outputv==1 then
          data:=mass;
        elseif outputv==2 then
          data:=area;
        elseif outputv==3 then
          data:=I_z;
        end if;

    //     data.mass:=mass;
    //     data.area:=area;
    //     data.I_z:=I_z;
    annotation (
        Documentation(info="<html>

<head>
<title>MOIMA() Help File</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>MOIMA()</span></b><span lang=EN-GB>
function is able to deliver Area, and Mass and MoI per unit length given a
point sequence; this point sequence must comply with the following
specifications:</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Polygon must not self-intersect.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Polygon must be closed (i.e. first and last point
must be the same).</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Points must be defined as a matrix of [N+1,2] where
N is the number of points.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-GB>-<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Points must be oriented clock or anticlockwise,
but must be consecutive (avoiding fake self-intersections).</span></p>

<p class=MsoNormal><span lang=EN-GB>Theorem of perpendicular axis states that,
for an Euclidean space in Cartesian coordinates, being the directions
associated to each dimension; x(i), y(j), z(k) (were </span><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
position:relative;top:3.0pt'><img width=55 height=20
src='MOIMA_archivos/image001.png'></span><span lang=EN-GB> ), the associated
Moments of Inertia comply with:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=72 height=19 src='MOIMA_archivos/image002.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>For a general polygon complying with the
aforesaid conditions, and being y and x the coordinates of the N+1 points (N
different) referred to those axes, and </span><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif';
position:relative;top:3.0pt'><img width=8 height=17
src='MOIMA_archivos/image003.png'></span><span lang=EN-GB> the density
(considered constant):</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=352 height=49 src='MOIMA_archivos/image004.png'></span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=352 height=49 src='MOIMA_archivos/image005.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>The area of that polygon can be written as:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=209 height=49 src='MOIMA_archivos/image006.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>And thus, considering all volumes as 2-D
shapes extruded in the z </span><span lang=EN-GB>axis, <b>MOIMA()</b></span><span
lang=EN-GB> returns Area, as is in x-y plane, and Moment of Inertia and Mass
per linear meter in the z axis. </span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>"));
    end MOIMA;

    record MOIMAr "Record for output of MOIMA function"
      Real mass;
      Real area;
      Real I_z;
    end MOIMAr;

    function MAssandMOI "Calculates Mass and Moments of Inertia of motorIPM"
      import Modelica.SIunits;
      import Modelica.Constants;
      type VolumetricMomentOfInertia = Real (final quantity="Volumetric moment of Inertia", final unit="m5", min=0);
      parameter input IPM_Structure.motorIPM INPUTmotor;
      output IPM_Structure.motorIPM motorOUTPUT;

    protected
      Integer p;
      SIunits.Length D_os;
      SIunits.Length D_is;
      SIunits.Length D_or;
      SIunits.Length D_ir;
      SIunits.Length l;
      SIunits.Length D_shaft;
      SIunits.Length D_shaftB;
      SIunits.Length DeltaD;
      SIunits.Length h_PM;
      SIunits.Length l_PM;
      SIunits.Length h_z;
      SIunits.Length b_z;
      SIunits.Length h_tr;
      SIunits.Length b_s;
      SIunits.Length h_y;
      SIunits.Length delta_htr;
      SIunits.Length X;
      SIunits.Length l_extraH;
      SIunits.Length l_extraW;
      SIunits.Length l_extra;
      SIunits.Length l_case_exp_front;
      SIunits.Length l_case_exp_rear;
      SIunits.Length l_case_exp_void;
      SIunits.Length l_case;
      SIunits.Length h_case;
      SIunits.Length l_shaft;
      SIunits.Length l_total;
      SIunits.Length h_total;

      SIunits.Density ro_case;
      SIunits.Density ro_mstR;
      SIunits.Density ro_hub;
      SIunits.Density ro_PM;
      SIunits.Density ro_mstS;
      SIunits.Density ro_cu;
      SIunits.Density ro_shaft;
      Real f_r;
      Real K_uhc;
      Real K_u;
      Integer shape;
      Integer case_type;
      SIunits.Volume v_R_shaftB;
      SIunits.Volume v_R_shaftT;
      VolumetricMomentOfInertia Iv_R_shaftB;
      VolumetricMomentOfInertia Iv_R_shaftT;
      Real[:,:] hub_points
        "Set of points defining non magnetic hub volume per pole";
      SIunits.Volume vp_R_hub "Non magnetic hub volume per pole";
      SIunits.MomentOfInertia I_R_hub "Hub's moment of inertia per pole";
      SIunits.Volume vp_R_pod "Magnet's hole in magnetic steel volume per pole";
      SIunits.Volume vp_R_mst_ring "Magnetic steel volume per pole";
      SIunits.Volume vp_R_magnet "Permanent magnet volume per magnet";
      SIunits.MomentOfInertia I_R_magnet
        "Permanent magnet moment of inertia per pole";
      SIunits.MomentOfInertia I_R_pod "Air slot moment of inertia per pole";
      SIunits.MomentOfInertia I_R_mst_ring
        "Rotor's magnetic steel ring moment of inertia per pole";
      SIunits.Mass M_mst_R "Rotor's magnetic steel mass";
      SIunits.MomentOfInertia I_R "Rotor Inertia";
      SIunits.Mass M_R "Rotor Mass";
      SIunits.Volume v_S_exp "Stator tooth expansion volume";
      SIunits.Volume v_S_tAVG "Stator tooth ring volume";
      SIunits.Volume v_S_mst_yoke "Stator yoke ring volume";
      SIunits.Volume v_S_cu_headW "Stator coil head volume";
      SIunits.Volume v_S_cu_headH "Stator tooth ring volume";
      VolumetricMomentOfInertia Iv_S_exp
        "Stator tooth expansion volume inertia";
      VolumetricMomentOfInertia Iv_S_tAVG "Stator tooth ring volume inertia";
      VolumetricMomentOfInertia Iv_S_mst_yoke
        "Stator yoke ring volume of inertia";
      VolumetricMomentOfInertia Iv_S_cu_headW
        "Stator coil head volume of inertia";
      VolumetricMomentOfInertia Iv_S_cu_headH
        "Stator coil head volume of inertia";
      SIunits.Density ro_tAVG "Tooth mean density";
      SIunits.Density ro_cu_tooth "Tooth copper mean density";
      SIunits.Density ro_mst_tooth "Tooth magnetic steel mean density";
      SIunits.Density ro_cu_headW "Tooth widing filling density";
      SIunits.Density ro_cu_headH "Tooth extra head area compensated density";
      SIunits.Volume v_S_body "Stator body case volume";
      SIunits.Volume v_S_front "Stator front body case volume";
      SIunits.Volume v_S_rear "Stator rear body case volume";
      VolumetricMomentOfInertia Iv_S_body "Stator body case of volume inertia";
      VolumetricMomentOfInertia Iv_S_front
        "Stator front body of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear "Stator rear body of volume inertia";

      SIunits.MomentOfInertia I_S_mst_yoke "Stator yoke ring moment of inertia";
      SIunits.MomentOfInertia I_S_tAVG "Stator tooth ring moment of inertia";
      SIunits.MomentOfInertia I_S_exp
        "Stator tooth expansion moment of inertia";
      SIunits.MomentOfInertia I_S_cu_head "Stator coil head moment of inertia";
      SIunits.MomentOfInertia I_S_mstT "Stator magnetic moment of inertia";
      SIunits.MomentOfInertia I_S_body "Case moment of inertia";
      SIunits.MomentOfInertia I_S_front "Case front plate moment of inertia";
      SIunits.MomentOfInertia I_S_rear "Case rear plate moment of inertia";
      SIunits.MomentOfInertia I_S_case "Stator case moment of inertia";
      SIunits.MomentOfInertia I_S "Stator yoke ring moment of inertia";
      SIunits.Mass cM "Total copper mass";
      SIunits.Mass mM "Stator magnetic mass";
      SIunits.Mass M_S "Stator mass";
      SIunits.Mass M "Total mass";

    algorithm
      // STEP1: Internal function data gathering (from INPUTmotor)

      // Dimensions

      p:=INPUTmotor.p;
      D_os:=INPUTmotor.stator.dim.D_os;
      D_is:=INPUTmotor.stator.dim.D_is;
      D_or:=INPUTmotor.rotor.dim.D_or;
      D_ir:=INPUTmotor.rotor.dim.D_ir;
      l:=INPUTmotor.stator.dim.l;

      D_shaft:=INPUTmotor.rotor.dim.D_shaft;
      D_shaftB:=INPUTmotor.rotor.dim.D_shaftB;

      DeltaD:=INPUTmotor.rotor.dim.DeltaD;
      h_PM:=INPUTmotor.rotor.dim.h_PM;
      l_PM:=INPUTmotor.rotor.dim.l_PM;
      h_z:=INPUTmotor.stator.dim.h_z;
      b_z:=INPUTmotor.stator.dim.b_z;
      h_tr:=INPUTmotor.stator.dim.h_tr;
      b_s:=INPUTmotor.stator.dim.b_s;
      h_y:=INPUTmotor.stator.dim.h_y;
      delta_htr:=INPUTmotor.stator.dim.delta_htr;
      X:=INPUTmotor.stator.dim.X;

      l_extraH:=INPUTmotor.stator.win.dim.l_extraH;
      l_extraW:=INPUTmotor.stator.win.dim.l_extraW;
      l_extra:=INPUTmotor.stator.win.dim.l_extra;
      l_case_exp_front:=INPUTmotor.casing.dim.l_case_exp_front;
      l_case_exp_rear:=INPUTmotor.casing.dim.l_case_exp_rear;
      l_case_exp_void:=INPUTmotor.casing.dim.l_case_exp_void;
      l_case:=INPUTmotor.casing.dim.l_case;
      h_case:=INPUTmotor.casing.dim.h_case;
      l_shaft:=INPUTmotor.rotor.dim.l_shaft;

      l_total:=INPUTmotor.casing.l_total;
      h_total:=INPUTmotor.casing.h_total;

      // Densities
      ro_case:=INPUTmotor.casing.dim.phy.ro_case;
      ro_mstR:=INPUTmotor.rotor.dim.phy.ro_mst;
      ro_hub:=INPUTmotor.rotor.dim.phy.ro_hub;
      ro_PM:=INPUTmotor.rotor.dim.phy.ro_PM;
      ro_mstS:=INPUTmotor.stator.dim.phy.ro_mst;
      ro_cu:=INPUTmotor.stator.dim.phy.ro_cu;
      ro_shaft:=INPUTmotor.rotor.dim.phy.ro_shaft;

      f_r:=INPUTmotor.specs.f_r;
      K_uhc:=INPUTmotor.stator.win.K_uhc;
      K_u:=INPUTmotor.stator.win.K_u;

      // Flags
      shape:=INPUTmotor.rotor.shape;
      case_type:=INPUTmotor.stator.case_type;

      Modelica.Utilities.Streams.print("Start: MassandMOI","");

      // STEP 2: Rotor (Different for each machine)

      // Shared volumes (for masses)
      v_R_shaftB:=0.5*l_shaft*Constants.pi*(D_shaftB/2)^2;
      v_R_shaftT:=0.5*l*Constants.pi*((D_shaft/2)^2 - (D_shaftB/2)^2);

      // Rotor volume inertia (for inertias)
      Iv_R_shaftB:=v_R_shaftB*(D_shaftB/2)^2;
      Iv_R_shaftT:=v_R_shaftT*((D_shaftB/2)^2 + (D_shaft/2)^2);

      if shape==0 then   // Spoke
        hub_points:=[0,0; INPUTmotor.rotor.draw.rotor_Hub_p/1000; 0,0];
        vp_R_hub:=2*MOIMA(hub_points,ro_hub,2)*l - 1/(2*p)*l*Constants.pi*(D_shaft/2)^2;
        I_R_hub:=MOIMA(hub_points,ro_hub,3)*l + MOIMA(
        Reflection(hub_points, Constants.pi/2/p),ro_hub,3)*l - 1/(2*p)*0.5*l*Constants.pi*(D_shaft/2)^4;
      elseif shape==1 then    // V-shape
        vp_R_hub:=0;
        I_R_hub:=0;
      elseif shape==2 then    // Planar
        vp_R_hub:=0;
        I_R_hub:=0;
      else
        Modelica.Utilities.Streams.print("**** ERROR: Unknown Shape, unable to obtain Masses & MoIs, revise rotor.shape value","");
      end if;

      // Rotor volumes (for masses)
      vp_R_pod:=2*MOIMA(
        INPUTmotor.rotor.draw.magnetSlotPM_p/1000,
        ro_mstR,
        2)*l;
      vp_R_mst_ring:=0.5*l*Constants.pi/(2*p)*((D_or/2)^2 - (D_shaft/2)^2) - vp_R_pod - vp_R_hub;
      vp_R_magnet:=l*l_PM*h_PM;

      // Rotor moments of inertia (per pole)
      I_R_magnet:=MOIMA(INPUTmotor.rotor.draw.magnet_p/1000,ro_PM,3)*l + MOIMA(Reflection(INPUTmotor.rotor.draw.magnet_p/1000, Constants.pi/2/p),ro_PM,3)*l;
      I_R_pod:=MOIMA(
        INPUTmotor.rotor.draw.magnetSlotPM_p/1000,
        ro_mstR,
        3)*l + MOIMA(
        Reflection(INPUTmotor.rotor.draw.magnetSlotPM_p/1000, Constants.pi/2/p),
        ro_mstR,
        3)*l;
      I_R_mst_ring:=0.5*l*Constants.pi/(2*p)*((D_or/2)^4 - (D_ir/2)^4)*ro_mstR;

      // Total rotor magnetic steel mass
      M_mst_R:=ro_mstR*vp_R_mst_ring*2*p;
      I_R:=(Iv_R_shaftB + Iv_R_shaftT)*ro_shaft + 2*p*(I_R_mst_ring - I_R_pod + I_R_magnet+I_R_hub);
      M_R:=(v_R_shaftB + v_R_shaftT)*ro_shaft + 2*p*(ro_mstR*vp_R_mst_ring + ro_PM*vp_R_magnet + ro_hub*
        vp_R_hub);

      // STEP 3: Stator (shared for all machines)
      v_S_exp:=l*Constants.pi*((D_is/2 + delta_htr)^2 - (D_is/2)^2);
      v_S_tAVG:=l*Constants.pi*((D_os/2 - h_y)^2 - ((D_os/2 - h_y - h_tr))^2);
      v_S_mst_yoke:=l*Constants.pi*((D_os/2)^2 - ((D_os/2 - h_y))^2);

      v_S_cu_headW:=2*l_extraW*Constants.pi*((D_os/2 - h_y)^2 - (D_os/2 - h_y - h_tr)^2);
      v_S_cu_headH:=2*l_extraH*Constants.pi*((D_os/2 - h_y)^2 - ((D_os/2 - h_y - h_tr))^2);

      // Volume inertia (for inertias) mangetic stator
      Iv_S_exp:=0.5*v_S_exp*((D_is/2 + delta_htr)^2 + (D_is/2)^2);
      Iv_S_tAVG:=0.5*v_S_tAVG*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);
      Iv_S_mst_yoke:=0.5*v_S_mst_yoke*((D_os/2)^2 + (D_os/2 - h_y)^2);
      Iv_S_cu_headW:=0.5*v_S_cu_headW*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);
      Iv_S_cu_headH:=0.5*v_S_cu_headH*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);

      ro_tAVG:=(b_s*K_u*ro_cu + b_z*ro_mstS)/(b_s + b_z);
      ro_cu_tooth:=(b_s*K_u*ro_cu)/(b_s + b_z);
      ro_mst_tooth:=(b_z*ro_mstS)/(b_s + b_z);
      ro_cu_headW:=K_uhc*ro_cu;
      ro_cu_headH:=b_s*ro_cu/(b_s + b_z);

      // Two types of casing: 1-square, 2-circular
      // Volumes for mass case stator

      if case_type==1 then    // circular
        v_S_body:=l_case*Constants.pi*((h_case/2)^2 - (D_os/2)^2);
        v_S_front:=l_case_exp_front*Constants.pi*(h_case/2)^2;
        v_S_rear:=l_case_exp_rear*Constants.pi*(h_case/2)^2;
        Iv_S_body:=0.5*v_S_body*((h_case/2)^2 + (D_os/2)^2);
        Iv_S_front:=0.5*v_S_front*(h_case/2)^2;
        Iv_S_rear:=0.5*v_S_rear*(h_case/2)^2;
      elseif case_type==2 then     // solid squared
        v_S_body:=l_case*(h_case^2 - Constants.pi*(D_os/2)^2);
        v_S_front:=l_case_exp_front*h_case^2;
        v_S_rear:=l_case_exp_rear*h_case^2;
        Iv_S_body:=l_case*(1/6*h_case^4 - Constants.pi/2*(D_os/2)^4);
        Iv_S_front:=l_case_exp_front*1/6*h_case^4;
        Iv_S_rear:=l_case_exp_rear*1/6*h_case^4;
      end if;

      // Inertia magnetic stator
      I_S_mst_yoke:=Iv_S_mst_yoke*ro_mstS*f_r;
      I_S_tAVG:=Iv_S_tAVG*ro_tAVG;
      I_S_exp:=Iv_S_exp*ro_mstS*f_r;
      I_S_cu_head:=Iv_S_cu_headW*ro_cu_headW + Iv_S_cu_headH*ro_cu_headH;
      I_S_mstT:=I_S_mst_yoke + I_S_tAVG + I_S_exp + I_S_cu_head;

      // Inertia case stator
      I_S_body:=Iv_S_body*ro_case;
      I_S_front:=Iv_S_front*ro_case;
      I_S_rear:=Iv_S_rear*ro_case;

      I_S_case:=I_S_body + I_S_front + I_S_rear;

      // Total Stator inertia: case+magnetic+headcoil
      I_S:=I_S_case + I_S_mstT;

      // Total copper mass (stator)
      cM:=v_S_cu_headW*ro_cu_headW + v_S_cu_headH*ro_cu_headH + v_S_tAVG*
        ro_cu_tooth;

      // Total magnetic mass (stator)
      mM:=(v_S_exp + v_S_mst_yoke)*ro_mstS + v_S_tAVG*ro_mst_tooth;

      // Total stator mass: case+stator (av. ro)+winding (two parts)
      M_S:=(v_S_body + v_S_front + v_S_rear)*ro_case + (v_S_exp + v_S_mst_yoke)*
        ro_mstS*f_r + v_S_tAVG*ro_tAVG + v_S_cu_headW*ro_cu_headW + v_S_cu_headH*
        ro_cu_headH;
      M:=M_S + M_R;

      motorOUTPUT:=INPUTmotor;
      motorOUTPUT.rotor.dim.M_mst_R:=M_mst_R;
      motorOUTPUT.rotor.dim.M_R:=M_R;
      motorOUTPUT.rotor.dim.I_R:=I_R;

      motorOUTPUT.plate.M:=M;
      motorOUTPUT.plate.TmM:=mM + M_mst_R;

      motorOUTPUT.stator.dim.mM:=mM;
      motorOUTPUT.stator.dim.cM:=cM;

      motorOUTPUT.casing.dim.M_S:=M_S;
      motorOUTPUT.casing.dim.I_S:=I_S;

      Modelica.Utilities.Streams.print("End: MassandMOI","");

    end MAssandMOI;

    function MassandMOISMPMSM
      "MassandMOISMPMSMnew, calculates Mass and Moments of Inertia of motorSYN"
      import Modelica.SIunits;
      import Modelica.Constants;
      type VolumetricMomentOfInertia = Real (final quantity="Volumetric moment of Inertia", final unit="m5", min=0);
      parameter input SMPMSM_Structure.motorSMPMSM INPUTmotor;
      output SMPMSM_Structure.motorSMPMSM motorOUTPUT;

    protected
      Integer p "Number of pole pairs";
      SIunits.Length L_brut "Raw machine length";
      SIunits.Length h_z "Stator tooth height";
      SIunits.Length b_z "Stator tooth width @ D";
      SIunits.Length h_tr "Stator slot height";
      SIunits.Length b_s "Stator slot width @ D";
      SIunits.Length h_y "Stator yoke width";
      SIunits.Length delta_htr "height of teeth expansion";
      SIunits.Length D_os "Outer stator diameter";
      SIunits.Length D_is "Bore diameter";
      Real K_fe "Tooth linear correction factor";
      Real K_u_s "Stator slot filling factor";
      Real f_r_s "Stator lamination factor";
      Real K_uhc "Stator head coil filling factor";
      SIunits.Length D_ir "Radial distance of point2";
      SIunits.Length D_shaft "Shaft external diameter";
      SIunits.Length D_shaft_internal "Shaft internal diameter";
      SIunits.Length b_rp "Rotor pole width";
      Real[:,2] externalrotorpoints;
      SIunits.Length[1,2] semi_coil_center "Using MOI-COORDS";
      Real K_p "Pole linear correction factor";
      Real K_ry "Rotor yoke linear correction factor";
      Real K_u_r "Rotor solot filling factor";
      Real f_r_r "Rotor lamination factor";
      SIunits.Length b_rc;
      SIunits.Length D_cond_r "Conductor diameter";
      SIunits.Length l_extraH "Stator head coil longitudinal extra length";
      SIunits.Length l_extraW
        "Stator head coil winding longitudinal extra length";
      SIunits.Length l_extra;
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      SIunits.Length l_shaft "Total shaft length";
      SIunits.Length l_total "Total case length";
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Density ro_shaft "Magnetic steel density";
      SIunits.Density ro_mstR "Magnetic steel density";
      SIunits.Density ro_cuR "Cu density";
      SIunits.Density ro_mstS "Magnetic steel density";
      SIunits.Density ro_cuS "Cu density";
      Real shape;
      Integer case_type "1=Circular, 2=Square solid";
      SIunits.Length h_rc "Rotor coil height";
      Real Insulator_Steel_ro_ratio
        "Density ratio between steel and its 'glue'";
      Real Insulator_Copper_ro_ratio
        "Density ratio between copper and its insulator";
      Real headcoil_layers
        "Number of layers to be calculated in headcoil iteration";
      SIunits.Density ro_mstSye "Length-corrected stator yoke mass density";
      SIunits.Density ro_mstSte "Length-corrected stator tooth mass density";
      SIunits.Density ro_cuSe
        "'Insulation and fill factor'-corrected stator copper mass density";
      SIunits.Density ro_mstRe
        "Stack corrected rotor magnetic steel mass density";
      SIunits.Density ro_cuRe
        "'Insulation and fill factor'- corrected rotor copper mass density";
      SIunits.Density ro_cuRe2;
      SIunits.Density ro_cuRe3;
      SIunits.Density ro_cu_matR
        "Rotor copper material area-corrected mass density";
      SIunits.Length l_R_pole "Cooling ducts rotor pole corrected length";
      SIunits.Length l_R_yoke "Cooling ducts rotor yoke corrected length";
      SIunits.Volume v_R_shaftB "Shaft bottom volume";
      SIunits.Volume v_R_shaftT "Shaft top volume";
      Real[5,2] semi_coil_points;
      Real[5,2] semi_coil_points_R;
      Real[5,2] semi_coil_points_Rs;
      VolumetricMomentOfInertia Iv_R_shaftB
        "Shaft bottom kind of volume inertia";
      VolumetricMomentOfInertia Iv_R_shaftT "Shaft top kind of volume inertia";
      SIunits.Volume v_R_yoke "Shaft bottom volume";
      VolumetricMomentOfInertia Iv_R_yoke "Shaft top kind of volume inertia";
      SIunits.Volume v_R_poles "Magnetic steel points volume";
      SIunits.MomentOfInertia I_R_poles "Magnetic steel inertia";
      SIunits.Volume vp_R_semicoil "Stator semicoil volume per pole";
      SIunits.MomentOfInertia Ip_R_semicoil
        "Stator coil moment of inertia per pole";
      SIunits.Volume vp2_R_semicoil "Stator semicoil volume per pole";
      SIunits.MomentOfInertia Ip2_R_semicoil
        "Stator semicoil moment of inertia";
      SIunits.Length[1,2] headcoil_center "Head coil shape reposition";
      SIunits.Volume vp_R_headcoil "Head coil volume per pole per side";
      SIunits.MomentOfInertia Il_R_headcoil
        "Head coil moment of inertia per pole per side";
      SIunits.Length Deltaz "In Z-Axis raises of the head coil approximation";
      SIunits.Length z_i "Z center position of the head coil layer";
      SIunits.Length b_rc_i "In X-axis length of headcoil layer";
      SIunits.Length b_hcl_i "Head coil layer width";
      SIunits.Length[5,2] headcoil_layer_points
        "Points defining head coil rectangle";
      SIunits.Mass M_mst_R "Rotor's magnetic steel mass";
      SIunits.Mass M_cu_R "Total rotor copper mass";
      SIunits.MomentOfInertia I_R "Rotor inertia";
      SIunits.Mass M_R "Rotor mass";
      SIunits.Volume v_S_exp "Stator tooth expansion volume";
      SIunits.Volume v_S_tAVG "Stator tooth ring volume";
      SIunits.Volume v_S_mst_yoke "Stator yoke ring volume";
      SIunits.Volume v_S_cu_headW "Stator coil head volume";
      SIunits.Volume v_S_cu_headH "Stator tooth ring volume";
      VolumetricMomentOfInertia Iv_S_exp
        "Stator tooth expansion kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_tAVG
        "Stator tooth ring kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_mst_yoke
        "Stator yoke ring kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_cu_headW
        "Stator coil head kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_cu_headH
        "Stator coil head kind of volume inertia";
      SIunits.Density ro_tAVG "Tooth mean density";
      SIunits.Density ro_cu_tooth "Tooth copper mean density";
      SIunits.Density ro_mst_tooth "Tooth magnetic steel mean density";
      SIunits.Density ro_cu_headW "Tooth winding filling density";
      SIunits.Density ro_cu_headH "Tooth extra head area compensated density";
      SIunits.MomentOfInertia I_S_mst_yoke "Stator yoke ring moment of inertia";
      SIunits.MomentOfInertia I_S_tAVG "Stator tooth ring moment of inertia";
      SIunits.MomentOfInertia I_S_exp
        "Stator tooth expansion moment of inertia";
      SIunits.MomentOfInertia I_S_cu_head "Stator coil head moment of inertia";
      SIunits.Mass M_cu_S "Total copper mass";
      SIunits.Mass M_mst_S "Stator magnetic mass";
      SIunits.Mass M_S "Stator mass";
      SIunits.MomentOfInertia I_S "Stator total moment of inertia";
      SIunits.Volume v_S_body "Stator body case volume";
      SIunits.Volume v_S_front "Stator front body case volume";
      SIunits.Volume v_S_rear "Stator rear body case volume";
      SIunits.Volume v_S_front_hole "Stator front body case hole volume";
      SIunits.Volume v_S_rear_hole "Stator rear body case hole volume";
      VolumetricMomentOfInertia Iv_S_body "Stator body case of volume inertia";
      VolumetricMomentOfInertia Iv_S_front
        "Stator front body of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear "Stator rear body of volume inertia";
      VolumetricMomentOfInertia Iv_S_front_hole
        "Stator front body case hole of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear_hole
        "Stator rear body case hole of volume inertia";
      SIunits.MomentOfInertia I_S_body "Case moment of inertia";
      SIunits.MomentOfInertia I_S_front "Case front plate moment of inertia";
      SIunits.MomentOfInertia I_S_rear "Case rear plate moment of inertia";
      SIunits.MomentOfInertia I_S_case "Stator case moment of inertia";
      SIunits.Mass M_hou "Total housing mass";
      SIunits.Mass M_mst_Total "Total magnetic steel mass";
      SIunits.Mass M_cu_Total "Total copper mass";
      SIunits.Mass M "Total mass";
      SIunits.MomentOfInertia I_houS "Stator and housing moment of inertia";
      SIunits.MomentOfInertia I_Total "Total moment of inertia";
      SIunits.Length l_r "Rotor mensurable length";
      SIunits.Length l_s "Stator mensurable length";
      SIunits.Length d1 "Tooth square shoe height";
      SIunits.Length d2 "Tooth trapezoid shoe height";
      SIunits.Length D_or "Diameter of outer rotor";
      SIunits.Length h_PM "Magnetic circuit magnet's height";
      SIunits.Length l_PM "Magnet length";
      Real mcf_PM "Magnet cover factor";
      Real hcf "Hole cover factor";
      SIunits.Length h_ry "Rotor yoke height";
      SIunits.Length h_rh "Rotor hole height";
      Integer hole_rotor
        "Rotor with or without holes, 0-No holes, 1-Holes in rotor";
      SIunits.Density ro_PM "Magnet density";
      SIunits.Length l_R_magnets "Cooling ducts rotor magnet corrected length";
      SIunits.Volume v_R_magnets "Rotor magnets volume";
      VolumetricMomentOfInertia Iv_R_magnets
        "Rotor magnets kind of volume inertia";
      SIunits.Volume v_R_holes "Rotor holes volume";
      VolumetricMomentOfInertia Iv_R_holes
        "Rotor magnets kind of volume inertia";
      SIunits.Mass M_shaft_R "Rotor's shaft steel mass";
      SIunits.Mass M_PM_R "Total rotor magnets mass";

    algorithm
      // STEP 1: Internal function data gathering (from INPUTmotor)

      // Dimensions

      p:=INPUTmotor.p;

      // Stator

      L_brut:=INPUTmotor.stator.dim.l;
      l_r:=INPUTmotor.rotor.dim.l;
      l_s:=INPUTmotor.stator.dim.l;

      h_z:=INPUTmotor.stator.dim.h_z;
      b_z:=INPUTmotor.stator.dim.b_z;
      h_tr:=INPUTmotor.stator.dim.h_tr;
      b_s:=INPUTmotor.stator.dim.b_s;
      h_y:=INPUTmotor.stator.dim.h_y;
      d1:=INPUTmotor.stator.dim.d1;
      d2:=INPUTmotor.stator.dim.d2;

      delta_htr:=d1+d2;

      D_os:=INPUTmotor.stator.dim.D_os;
      D_is:=INPUTmotor.stator.dim.D_is;

      //K_fe:=INPUTmotor.stator.dim.K_fe;
      K_u_s:=INPUTmotor.stator.win.K_u;
      f_r_s:=INPUTmotor.stator.dim.f_r_s;

      // Winding Stator

      K_uhc:=INPUTmotor.stator.win.K_uhc;

      // Rotor
      D_or:=INPUTmotor.rotor.dim.D_or;
      D_ir:=INPUTmotor.rotor.dim.D_ir;
      D_shaft:=INPUTmotor.rotor.dim.D_shaft;
      D_shaft_internal:=INPUTmotor.rotor.dim.D_shaft_internal;
      //b_rp:=INPUTmotor.rotor.dim.b_rp;

      //externalrotorpoints:=INPUTmotor.rotor.geo.externalrotorpoints;
      //semi_coil_center:=INPUTmotor.rotor.geo.semi_coil_center;
      f_r_r:=INPUTmotor.rotor.dim.f_r;
      K_p:=1;
      K_ry:=f_r_r;
      K_u_r:=1;
      K_fe:=1/f_r_s;

      // Rotor magnet
      h_PM:=INPUTmotor.rotor.dim.h_PM;
      l_PM:=INPUTmotor.rotor.dim.l_PM;
      mcf_PM:=INPUTmotor.rotor.dim.mcf_PM;
      hcf:=INPUTmotor.rotor.dim.hcf;
      h_ry:=INPUTmotor.rotor.dim.h_ry;
      h_rh:=INPUTmotor.rotor.dim.h_rh;
      hole_rotor:=INPUTmotor.rotor.draw.hole_rotor;

      // Housing
      l_extraH:=INPUTmotor.stator.win.dim.l_extraH;
      l_extraW:=INPUTmotor.stator.win.dim.l_extraW;
      l_extra:=INPUTmotor.stator.win.dim.l_extra;
      l_case_exp_front:=INPUTmotor.casing.dim.l_case_exp_front;
      l_case_exp_rear:=INPUTmotor.casing.dim.l_case_exp_rear;
      l_case_exp_void:=INPUTmotor.casing.dim.l_case_exp_void;
      l_case:=INPUTmotor.casing.dim.l_case;
      h_case:=INPUTmotor.casing.dim.h_case;
      l_shaft:=INPUTmotor.rotor.dim.l_shaft;

      l_total:=INPUTmotor.casing.l_total;

      // Densities
      ro_case:=INPUTmotor.casing.dim.phy.ro_case;
      ro_shaft:=INPUTmotor.rotor.dim.phy.ro_shaft;

      ro_mstR:=INPUTmotor.rotor.dim.phy.ro_mst;
      //ro_cuR:=INPUTmotor.rotor.dim.phy.ro_cu;

      ro_mstS:=INPUTmotor.stator.dim.phy.ro_mst;
      ro_cuS:=INPUTmotor.stator.dim.phy.ro_cu;
      ro_PM:=INPUTmotor.rotor.dim.phy.ro_PM;

      // Flags & Shapes
      shape:=INPUTmotor.rotor.shape;
      case_type:=INPUTmotor.stator.case_type;

      Insulator_Steel_ro_ratio:=0.2;
      Insulator_Copper_ro_ratio:=0.1;
      headcoil_layers:=26;
      // Stator "stack factor + length"-corrected densities
      ro_mstSye:=(f_r_s + (1 - f_r_s)*Insulator_Steel_ro_ratio)/K_fe*ro_mstS;
      ro_mstSte:=(f_r_s + (1 - f_r_s)*Insulator_Steel_ro_ratio)/K_fe*ro_mstS;
      // Stator copper "fillinf factor + insulator"-corrected density
      ro_cuSe:=ro_cuS*(Insulator_Copper_ro_ratio + K_u_s*(1 - Insulator_Copper_ro_ratio));
      // Rotor "stack factor" addition
      ro_mstRe:=(f_r_r + (1 - f_r_r)*Insulator_Steel_ro_ratio)*ro_mstR;

      // Lengths correction (for rotor only)
      // Note that K_p and K_ry already contain the f_r_r and thus density does not require f_r_r correction
      // only the "glue" addition

      l_R_yoke:=l_r;
      l_R_magnets:=l_r;

      // STEP 2: Rotor

      // Rotor volumes (for masses)
      v_R_shaftB:=l_shaft*Constants.pi*(D_shaft/2)^2;
      v_R_shaftT:=L_brut*Constants.pi*((D_shaft_internal/2)^2 - (D_shaft/2)^2);
      // Rotor volume inertia (for inertias)
      Iv_R_shaftB:=0.5*v_R_shaftB*(D_shaft/2)^2;
      Iv_R_shaftT:=0.5*v_R_shaftT*((D_shaft_internal/2)^2 + (D_shaft/2)^2);
      // Rotor magnetic steel yoke
      v_R_yoke:=l_R_yoke*Constants.pi*((D_or/2)^2 - (D_shaft_internal/2)^2);
      Iv_R_yoke:=0.5*v_R_yoke*((D_or/2)^2 + (D_shaft_internal/2)^2);
      // Rotor magnets (as ring)
      v_R_magnets:=mcf_PM*l_R_yoke*Constants.pi*((D_or/2 + h_PM)^2 - (D_or/2)^2);
      Iv_R_magnets:=0.5*v_R_magnets*((D_or/2 + h_PM)^2 + (D_or/2)^2);

      if hole_rotor==1 then
        // rotor holes (as ring)
        v_R_holes:=hcf*l_R_yoke*Constants.pi*((D_or/2 - h_PM - h_ry)^2 - (D_or/2 - h_PM - h_ry - h_rh)^2);
        Iv_R_holes:=0.5*v_R_holes*((D_or/2 - h_PM - h_ry)^2 + (D_or/2 - h_PM - h_ry - h_rh)^2);
      else
        v_R_holes:=0;
        Iv_R_holes:=0;
      end if;

      // STEP 2.5: Rotor totals (sync updated)

      // Total shaft
      // Inner and out shaft mass sum
      M_shaft_R:=(v_R_shaftB + v_R_shaftT)*ro_shaft;
      // Total rotor magnetic steel mass
      // Yoke ring - hole if any
      M_mst_R:=ro_mstRe*(v_R_yoke - v_R_holes);
      // Total rotor magnets
      M_PM_R:=ro_PM*v_R_magnets;
      // Total rotor inertia (ShaftB + ShavtT) + Rotor Yoke + Magnets
      I_R:=(Iv_R_shaftB + Iv_R_shaftT)*ro_shaft + Iv_R_yoke*ro_mstRe + ro_PM*Iv_R_magnets;
      // Total rotor mass
      M_R:=M_mst_R + M_PM_R+M_shaft_R;

      // STEP 3: Stator

      // Volumes (for mass) magnetic stator
      v_S_exp:=L_brut*Constants.pi*((D_is/2 + delta_htr)^2 - (D_is/2)^2);
      v_S_tAVG:=L_brut*Constants.pi*((D_os/2 - h_y)^2 - ((D_os/2 - h_y - h_tr))^2);
      v_S_mst_yoke:=L_brut*Constants.pi*((D_os/2)^2 - ((D_os/2 - h_y))^2);

      // Multiplied by 2 because there is one in each side
      v_S_cu_headW:=2*l_extraW*Constants.pi*((D_os/2 - h_y)^2 - (D_os/2 - h_y - h_tr)^2);
      v_S_cu_headH:=2*l_extraH*Constants.pi*((D_os/2 - h_y)^2 - ((D_os/2 - h_y - h_tr))^2);

      // Volume inertia (for inertias) magnetic stator
      Iv_S_exp:=0.5*v_S_exp*((D_is/2 + delta_htr)^2 + (D_is/2)^2);
      Iv_S_tAVG:=0.5*v_S_tAVG*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);
      Iv_S_mst_yoke:=0.5*v_S_mst_yoke*((D_os/2)^2 + (D_os/2 - h_y)^2);

      Iv_S_cu_headW:=0.5*v_S_cu_headW*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);
      Iv_S_cu_headH:=0.5*v_S_cu_headH*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);

      ro_tAVG:=(b_s*K_u_s*ro_cuSe + b_z*ro_mstSte)/(b_s + b_z);
      ro_cu_tooth:=(b_s*K_u_s*ro_cuS)/(b_s + b_z);
      ro_mst_tooth:=(b_z*ro_mstSte)/(b_s + b_z);
      ro_cu_headW:=K_uhc*ro_cuSe;
      ro_cu_headH:=b_s*ro_cuSe/(b_s + b_z);

      // Inertia magnetic stator
      I_S_mst_yoke:=Iv_S_mst_yoke*ro_mstSye;
      I_S_tAVG:=Iv_S_tAVG*ro_tAVG;
      I_S_exp:=Iv_S_exp*ro_mstSte;
      I_S_cu_head:=Iv_S_cu_headW*ro_cu_headW + Iv_S_cu_headH*ro_cu_headH;

      // Step 3.5: Stator totals

      // Total copper mass (stator)
      M_cu_S:=v_S_cu_headW*ro_cu_headW + v_S_cu_headH*ro_cu_headH + v_S_tAVG*ro_cu_tooth;
      // Total magnetic mass (stator)
      M_mst_S:=v_S_exp*ro_mstSte + v_S_mst_yoke*ro_mstSye + v_S_tAVG*ro_mst_tooth;
      // Total mStator mass: tooth talon + AVG + Yoke + Winding_W + Winding_H
      M_S:=v_S_exp*ro_mstSte + v_S_mst_yoke*ro_mstSye + v_S_tAVG*ro_tAVG + v_S_cu_headW*ro_cu_headW + v_S_cu_headH*ro_cu_headH;
      // Total moment of inertia (stator), note that 'head' has already been multiplied by 2
      I_S:=I_S_mst_yoke + I_S_tAVG + I_S_exp + I_S_cu_head;

      // Step 4: Housing

      if case_type==1 then // Circular
        v_S_body:=l_case*Constants.pi*((h_case/2)^2 - (D_os/2)^2);
        v_S_front:=l_case_exp_front*Constants.pi*(h_case/2)^2;
        v_S_rear:=l_case_exp_rear*Constants.pi*(h_case/2)^2;
        v_S_front_hole:=l_case_exp_front*Constants.pi*(D_shaft/2)^2;
        v_S_rear_hole:=l_case_exp_rear*Constants.pi*(D_shaft/2)^2;
        Iv_S_body:=0.5*v_S_body*((h_case/2)^2 + (D_os/2)^2);
        Iv_S_front:=0.5*v_S_front*(h_case/2)^2;
        Iv_S_rear:=0.5*v_S_rear*(h_case/2)^2;
        Iv_S_front_hole:=0.5*v_S_front_hole*(D_shaft/2)^2;
        Iv_S_rear_hole:=0.5*v_S_rear_hole*(D_shaft/2)^2;
      elseif case_type==2 then // Solid squared
        v_S_body:=l_case*(h_case^2 - Constants.pi*(D_os/2)^2);
        v_S_front:=l_case_exp_front*h_case^2;
        v_S_rear:=l_case_exp_rear*h_case^2;
        v_S_front_hole:=l_case_exp_front*Constants.pi*(D_shaft/2)^2;
        v_S_rear_hole:=l_case_exp_front*Constants.pi*(D_shaft/2)^2;
        Iv_S_body:=l_case*(1/6*h_case^4 - Constants.pi/2*(D_os/2)^4);
        Iv_S_front:=l_case_exp_front*1/6*h_case^4;
        Iv_S_rear:=l_case_exp_rear*1/6*h_case^4;
        Iv_S_front_hole:=0.5*v_S_front_hole*(D_shaft/2)^2;
        Iv_S_rear_hole:=0.5*v_S_rear_hole*(D_shaft/2)^2;
      end if;

      // Inertia stator: housnig
      I_S_body:=Iv_S_body*ro_case;
      I_S_front:=(Iv_S_front - Iv_S_front_hole)*ro_case;
      I_S_rear:=(Iv_S_rear - Iv_S_rear_hole)*ro_case;
      I_S_case:=I_S_body + I_S_front + I_S_rear;

      // Total housing mass (stator)
      M_hou:=(v_S_body + v_S_front + v_S_rear - v_S_front_hole - v_S_rear_hole)*ro_case;
      // Totals: Case + Stator + Rotor
      // Total magnetic steel mass: Stator + Rotor
      M_mst_Total:=M_mst_R + M_mst_S;
      // Total stator mass: Case + Stator(av. ro) + Winding(two parts)
      M_cu_Total:=M_cu_S;

      // Motor totals
      M:=M_R + M_S + M_hou;
      I_houS:=I_S + I_S_case;
      I_Total:=I_R + I_S + I_S_case;

      // FINAL STEP: Data assign to motorOUTPUT
      motorOUTPUT:=INPUTmotor;
      motorOUTPUT.rotor.dim.M_mst_R:=M_mst_R;
      //motorOUTPUT.rotor.dim.cM:=M_cu_R;
      motorOUTPUT.rotor.dim.M_PM:=M_PM_R;
      motorOUTPUT.rotor.dim.M_R:=M_R;
      motorOUTPUT.rotor.dim.I_R:=I_R;

      motorOUTPUT.plate.M:=M;
      motorOUTPUT.plate.TmM:=M_mst_Total;
      motorOUTPUT.plate.I_Total:=I_Total;

      motorOUTPUT.stator.dim.mM:=M_mst_S;
      motorOUTPUT.stator.dim.cM:=M_cu_S;
      motorOUTPUT.stator.dim.I_S:=I_S;
      motorOUTPUT.stator.dim.M_S:=M_S;

      motorOUTPUT.casing.dim.M_S:=M_hou;
      motorOUTPUT.casing.dim.I_S:=I_S_case;

      Modelica.Utilities.Streams.print("*** End: MassandMOI Synchronous ***","");

    end MassandMOISMPMSM;

    function MassandMOISRM
      "Calculates Mass and Moments of Inertia of motorSRM, MassandMOISRM"
      import Modelica.SIunits;
      import Modelica.Constants;
      type VolumetricMomentOfInertia = Real (final quantity="Volumetric moment of Inertia", final unit="m5", min=0);
      parameter input SRM_Structure.motorSRM INPUTmotor;
      output SRM_Structure.motorSRM motorOUTPUT;

    protected
      Integer headcoil_layers=26
        "Number of layers to be calculated in headcoil iteration";
      SIunits.Length g "Airgap";
      SIunits.Length l "Length";
      SIunits.Length D "Bore diameter (inner stator diameter)";
      SIunits.Length D_os "Diameter outer stator";
      SIunits.Length D_is "Bore diameter (inner stator diameter)";
      Integer N_s "Stator poles";
      SIunits.Angle beta_s "Stator pole arc";
      SIunits.Length b_sp "Stator pole width";
      SIunits.Length h_sp "Stator pole height";
      SIunits.Length b_sy "Stator yoke width";
      Real coil_security_coefficient "Coil security coeficient";
      Real K_f "Filling factor by the actual used area";
      SIunits.Length b_sc "Stator coil width";
      SIunits.Length h_sc "Stator coil height";
      Real b_seg "Spacing between consecutive coils";
      SIunits.Length D_cond "Conductor diameter";
      Integer N_r "Rotor pole";
      SIunits.Angle beta_r "Rotor pole arc";
      SIunits.Length D_or "Diameter outer rotor";
      SIunits.Length D_ir "Diameter inner rotor";
      SIunits.Length D_shaft;
      SIunits.Length D_shaftB;
      SIunits.Length b_rp "Rotor pole width";
      SIunits.Length h_rp "Rotor pole height";
      SIunits.Length b_ry "Rotor yoke width";
      SIunits.Length l_extra "Extra head coil length";
      SIunits.Length l_extraW "Head Coil winding longitudinal extra length";
      SIunits.Length l_extraH "Head Coil longitudinal extra length";
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      SIunits.Length l_shaft "Total shaft length";
      SIunits.Length l_total "Total case length";
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Density ro_mstR "Magnetic steel density";
      SIunits.Density ro_mstS "Magnetic steel density";
      SIunits.Density ro_shaft "Shaft steel density";
      SIunits.Density ro_cu "Cu density";
      Real f_r "Lamination factor";
      Integer shape "Shape type";
      Integer case_type "Case type";
      SIunits.Density ro_mstRe;
      SIunits.Density ro_mstSe;
      SIunits.Density ro_cue;
      SIunits.Volume v_R_shaft "Shaft bottom volume";
      VolumetricMomentOfInertia Iv_R_shaft
        "Rotor shaft kind of volume of inertia";
      SIunits.Volume v_R_yoke "Yoke bottom volume";
      VolumetricMomentOfInertia Iv_R_yoke
        "Rotor yoke kind of volume of inertia";
      Real[:] beta_r_R;
      Real[:] theta_r_R;
      Real[201,2] rotor_pole_points;
      SIunits.Volume vp_R_pole "Non magnetic hub volume per pole";
      SIunits.MomentOfInertia I_R_pole "Hub-s moment of inertia";
      SIunits.Mass M_mst_R "Rotor's magnetic steel mass";
      SIunits.MomentOfInertia I_R "Rotor inertia";
      SIunits.Mass M_R "Rotor Mass";
      SIunits.Volume v_S_mst_yoke "Stator yoke ring volume";
      VolumetricMomentOfInertia Iv_S_mst_yoke
        "Stator yoke ring kind of volume inertia";
      SIunits.MomentOfInertia I_S_mst_yoke "Stator yoke ring moment of inertia";
      Real[:] theta_s_R;
      Real[:] beta_s_R;
      Real[201,2] stator_pole_points;
      SIunits.Volume vp_S_pole "Stator Pole Volume per pole";
      SIunits.MomentOfInertia Ip_S_pole
        "Stator Pole Moment of Inertia per pole";
      SIunits.Length[1,2] semi_coil_center "Head coil shape reposition";
      Real[:,:] semi_coil_points;
      Real[5,2] semi_coil_points_R;
      SIunits.Volume vp_S_semicoil "Stator semicoil volume per pole";
      SIunits.MomentOfInertia Ip_S_semicoil
        "Stator semicoil moment of inertia per pole";
      Real[5,2] semi_coil_points_Rs;
      SIunits.Volume vp2_S_semicoil "Stator semicoil volume per pole";
      SIunits.MomentOfInertia Ip2_S_semicoil
        "Stator semicoil moment of inertia per pole";
      SIunits.Length[1,2] headcoil_center "Head coil shape reposition";
      SIunits.Volume vp_S_headcoil "Head coil volume per pole per side";
      SIunits.MomentOfInertia Il_S_headcoil
        "Head coil moment of inertia per pole per side";
      SIunits.Length Deltaz "In z-axis raises of head coil approximation";
      SIunits.Length z_i "Z center position of the headcoil layer";
      SIunits.Length b_sc_i "In x-axis length of headcoil";
      SIunits.Length b_hcl_i "Head coil layer width";
      SIunits.Length[:,:] headcoil_layer_points
        "Points defining headcoil rectangle";
      SIunits.MomentOfInertia I_S_mstT "Stator machine moment of inertia";
      SIunits.Volume v_S_body "Stator body case volume";
      SIunits.Volume v_S_front "Stator front body case volume";
      SIunits.Volume v_S_rear "Stator rear body case volume";
      SIunits.Volume v_S_front_hole "Stator front body case hole volume";
      SIunits.Volume v_S_rear_hole "Stator rear body case hole volume";
      VolumetricMomentOfInertia Iv_S_body "Stator body case of volume inertia";
      VolumetricMomentOfInertia Iv_S_front
        "Stator front body of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear "Stator rear body of volume inertia";
      VolumetricMomentOfInertia Iv_S_front_hole
        "Stator front body case hole of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear_hole
        "Stator rear body case hole of volume inertia";
      SIunits.MomentOfInertia I_S_body "Case moment of inertia";
      SIunits.MomentOfInertia I_S_front "Case front plate moment of inertia";
      SIunits.MomentOfInertia I_S_rear "Case rear plate moment of inertia";
      SIunits.MomentOfInertia I_S_case "Stator case moment of inertia";
      SIunits.Mass houM "Total housing mass";
      SIunits.Mass cM "Total copper mass";
      SIunits.Mass mM "Stator magnetic mass";
      SIunits.MomentOfInertia I_S "Stator yoke ring moment of inertia";
      SIunits.Mass M_S "Stator mass";
      SIunits.Mass M "Total mass";

    algorithm
      Modelica.Utilities.Streams.print("START: MassandMOI","");

      // STEP 1: Internal function data gathering (from INPUTmotor)
      g:=INPUTmotor.g;
      l:=INPUTmotor.stator.dim.l;
      D:=INPUTmotor.stator.D;
      D_os:=INPUTmotor.stator.dim.D_os;
      D_is:=INPUTmotor.stator.dim.D_is;
      N_s:=INPUTmotor.stator.N_s;
      beta_s:=INPUTmotor.stator.dim.beta_s;
      b_sp:=INPUTmotor.stator.dim.b_sp;
      h_sp:=INPUTmotor.stator.dim.h_sp;
      b_sy:=INPUTmotor.stator.dim.b_sy;
      coil_security_coefficient:=INPUTmotor.specs.coil_security_coefficient;

      K_f:=INPUTmotor.stator.win.K_f_real;
      b_sc:=INPUTmotor.stator.win.dim.b_sc;
      h_sc:=INPUTmotor.stator.win.dim.h_sc;
      b_seg:=INPUTmotor.stator.win.dim.b_seg;
      D_cond:=INPUTmotor.stator.win.dim.D_cond;

      N_r:=INPUTmotor.rotor.dim.N_r;
      beta_r:=INPUTmotor.rotor.dim.beta_r;
      D_or:=INPUTmotor.rotor.dim.D_or;
      D_ir:=INPUTmotor.rotor.dim.D_ir;
      D_shaft:=INPUTmotor.rotor.dim.D_shaft;
      D_shaftB:=INPUTmotor.rotor.dim.D_shaft;
      b_rp:=INPUTmotor.rotor.dim.b_rp;
      h_rp:=INPUTmotor.rotor.dim.h_rp;
      b_ry:=INPUTmotor.rotor.dim.b_ry;

      l_extraH:=INPUTmotor.stator.win.dim.l_extraH;
      l_extraW:=INPUTmotor.stator.win.dim.l_extraW;
      l_extra:=INPUTmotor.stator.win.dim.l_extra;
      l_case_exp_front:=INPUTmotor.casing.dim.l_case_exp_front;
      l_case_exp_rear:=INPUTmotor.casing.dim.l_case_exp_rear;
      l_case_exp_void:=INPUTmotor.casing.dim.l_case_exp_void;
      l_case:=INPUTmotor.casing.dim.l_case;
      h_case:=INPUTmotor.casing.dim.h_case;
      l_shaft:=INPUTmotor.rotor.dim.l_shaft;

      l_total:=INPUTmotor.casing.l_total;

      ro_case:=INPUTmotor.casing.dim.phy.ro_case;
      ro_mstR:=INPUTmotor.rotor.dim.phy.ro_mst;

      ro_mstS:=INPUTmotor.stator.dim.phy.ro_mst;
      ro_cu:=INPUTmotor.stator.dim.phy.ro_cu;
      ro_shaft:=INPUTmotor.rotor.dim.phy.ro_shaft;

      f_r:=INPUTmotor.specs.f_r;

      shape:=INPUTmotor.rotor.shape;
      case_type:=INPUTmotor.stator.case_type;

      // Filling factor density correction for magnetic steel lamination
      ro_mstRe:=ro_mstR*(f_r + (1 - f_r)*0.2);
      ro_mstSe:=ro_mstS*(f_r + (1 - f_r)*0.2);
      // Filling factor density correction for copper
      ro_cue:=ro_cu*(1 + 0.1)*K_f;

      // STEP2: Rotor

      v_R_shaft:=l_shaft*Constants.pi*(D_shaft/2)^2;
      Iv_R_shaft:=v_R_shaft*0.5*(D_shaft/2)^2;
      v_R_yoke:=l*Constants.pi*((D_ir/2)^2 - (D_shaft/2)^2);
      Iv_R_yoke:=v_R_yoke*0.5*((D_ir/2)^2 + (D_shaft/2)^2);

      beta_r_R:=linspace(-beta_r/2,beta_r/2,100);
      theta_r_R:=linspace(asin(b_rp/2/(D/2 - g - h_rp)),-asin(b_rp/2/(D/2 - g - h_rp)),100);
      rotor_pole_points[1:end-1,:]:=[D_or/2*cos(matrix(beta_r_R)),D_or/2*sin(matrix(
         beta_r_R)); D_ir/2*cos(matrix(theta_r_R)),D_ir/2*sin(matrix(theta_r_R))];
      rotor_pole_points[end,:]:=rotor_pole_points[1, :];

      vp_R_pole:=MOIMA(rotor_pole_points,ro_mstRe,2)*l;
      I_R_pole:=MOIMA(rotor_pole_points,ro_mstRe,3)*l;
      M_mst_R:=ro_mstRe*(v_R_yoke + vp_R_pole*N_r);
      I_R:=Iv_R_shaft*ro_shaft + Iv_R_yoke*ro_mstRe + N_r*I_R_pole;
      M_R:=v_R_shaft*ro_shaft + (v_R_yoke + N_r*vp_R_pole)*ro_mstRe;

      // STEP3: Stator

      // Yoke
      v_S_mst_yoke:=l*Constants.pi*((D_os/2)^2 - ((D_os/2 - b_sy))^2);
      Iv_S_mst_yoke:=0.5*v_S_mst_yoke*((D_os/2)^2 + (D_os/2 - b_sy)^2);
      I_S_mst_yoke:=Iv_S_mst_yoke*ro_mstSe;

      // Pole
      theta_s_R:=linspace(-asin((b_sp/2)/(D_is/2 + h_sp)),asin((b_sp/2)/(D_is/2 + h_sp)),100);
      beta_s_R:=linspace(beta_s/2,-beta_s/2,100);
      stator_pole_points[1:end-1,:]:=[(D_is + 2*h_sp)/2*cos(matrix(theta_s_R)),(D_is +
        2*h_sp)/2*sin(matrix(theta_s_R)); D_is/2*cos(matrix(beta_s_R)),D_is/2*sin(
        matrix(beta_s_R))];
      stator_pole_points[end,:]:=stator_pole_points[1, :];
      vp_S_pole:=MOIMA(stator_pole_points,ro_mstRe,2)*l;
      Ip_S_pole:=MOIMA(stator_pole_points,ro_mstRe,3)*l;

      // Winding
      // First semicoil
      semi_coil_center:=[D/2 + h_sp*coil_security_coefficient + h_sc/2,b_sp/2 + b_sc/2];
      semi_coil_points:=[-h_sc/2,-b_sc/2; h_sc/2,-b_sc/2; h_sc/2,b_sc/2; -h_sc/2,b_sc/2; -h_sc/2,-
        b_sc/2];
      semi_coil_points_R[:,1]:=semi_coil_points[:,1].+semi_coil_center[1,1];
      semi_coil_points_R[:,2]:=semi_coil_points[:,2].+semi_coil_center[1,2];
      vp_S_semicoil:=MOIMA(semi_coil_points_R,ro_cue,2)*(l + D_cond);
      Ip_S_semicoil:=MOIMA(semi_coil_points_R,ro_cue,3)*(l + D_cond);
      // Second semicoil
      semi_coil_points_Rs[:,1]:=semi_coil_points[:,1].+semi_coil_center[1,1];
      semi_coil_points_Rs[:,2]:=semi_coil_points[:,2].-semi_coil_center[1,2];
      vp2_S_semicoil:=MOIMA(semi_coil_points_Rs,ro_cue,2)*(l + D_cond);
      Ip2_S_semicoil:=MOIMA(semi_coil_points_Rs,ro_cue,3)*(l + D_cond);
      // Winding "headcoils": layer iteration
      headcoil_center:=[D/2 + h_sp*coil_security_coefficient + h_sc/2,0];
      vp_S_headcoil:=0;
      Il_S_headcoil:=0;

      for i in 1:1:headcoil_layers loop
        Deltaz:=b_sc/headcoil_layers;
        z_i:=(i - 1)*Deltaz + Deltaz/2;
        b_sc_i:=sqrt(b_sc^2 - z_i^2);
        b_hcl_i:=b_sp + 2*b_sc_i;
        headcoil_layer_points:=[-h_sc/2,-b_hcl_i/2; h_sc/2,-b_hcl_i/2; h_sc/2,b_hcl_i/2; -h_sc/2,
          b_hcl_i/2; -h_sc/2,-b_hcl_i/2];
        headcoil_layer_points[:,1]:=headcoil_layer_points[:, 1].+ headcoil_center[1, 1];
        headcoil_layer_points[:,2]:=headcoil_layer_points[:, 2].+ headcoil_center[1, 2];
        vp_S_headcoil:=vp_S_headcoil + MOIMA(headcoil_layer_points,ro_cue,2)*Deltaz;
        Il_S_headcoil:=Il_S_headcoil + MOIMA(headcoil_layer_points,ro_cue,3)*Deltaz;
      end for;

      I_S_mstT:=I_S_mst_yoke + Ip_S_pole*N_s + (Il_S_headcoil*2 + Ip_S_semicoil +
        Ip2_S_semicoil)*N_s;

      // Housing
      if case_type==1 then // circular casing
        v_S_body:=l_case*Constants.pi*((h_case/2)^2 - (D_os/2)^2);
        v_S_front:=l_case_exp_front*Constants.pi*(h_case/2)^2;
        v_S_rear:=l_case_exp_rear*Constants.pi*(h_case/2)^2;
        v_S_front_hole:=l_case_exp_front*Constants.pi*(D_shaftB/2)^2;
        v_S_rear_hole:=l_case_exp_rear*Constants.pi*(D_shaftB/2)^2;

        Iv_S_body:=0.5*v_S_body*((h_case/2)^2 + (D_os/2)^2);
        Iv_S_front:=0.5*v_S_front*(h_case/2)^2;
        Iv_S_rear:=0.5*v_S_rear*(h_case/2)^2;
        Iv_S_front_hole:=0.5*v_S_front_hole*(D_shaftB/2)^2;
        Iv_S_rear_hole:=0.5*v_S_rear_hole*(D_shaftB/2)^2;
      elseif case_type==2 then // solid square casing
        v_S_body:=l_case*(h_case^2 - Constants.pi*(D_os/2)^2);
        v_S_front:=l_case_exp_front*h_case^2;
        v_S_rear:=l_case_exp_rear*h_case^2;
        v_S_front_hole:=l_case_exp_front*Constants.pi*(D_shaftB/2)^2;
        v_S_rear_hole:=l_case_exp_front*Constants.pi*(D_shaftB/2)^2;
        Iv_S_body:=l_case*(1/6*h_case^4 - Constants.pi/2*(D_os/2)^4);
        Iv_S_front:=l_case_exp_front*1/6*h_case^4;
        Iv_S_rear:=l_case_exp_rear*1/6*h_case^4;
        Iv_S_front_hole:=0.5*v_S_front_hole*(D_shaftB/2)^2;
        Iv_S_rear_hole:=0.5*v_S_rear_hole*(D_shaftB/2)^2;
      end if;

      I_S_body:=Iv_S_body*ro_case;
      I_S_front:=(Iv_S_front - Iv_S_front_hole)*ro_case;
      I_S_rear:=(Iv_S_rear - Iv_S_rear_hole)*ro_case;
      I_S_case:=I_S_body + I_S_front + I_S_rear;
      houM:=(v_S_body + v_S_front + v_S_rear - v_S_front_hole - v_S_rear_hole)*ro_case;
      cM:=((2*vp_S_headcoil + vp_S_semicoil + vp2_S_semicoil)*N_s)*ro_cue;
      mM:=(v_S_mst_yoke + vp_S_pole*N_s)*ro_mstS;
      I_S:=I_S_case + I_S_mstT;
      M_S:=cM + mM + houM;
      M:=M_S + M_R;

      // Final STEP: Data assign to motorOUTPUT
      motorOUTPUT:=INPUTmotor;
      motorOUTPUT.rotor.dim.M_mst_R:=M_mst_R;
      motorOUTPUT.rotor.dim.M_R:=M_R;
      motorOUTPUT.rotor.dim.I_R:=I_R;
      motorOUTPUT.plate.M:=M;
      motorOUTPUT.plate.TmM:=mM + M_mst_R;
      motorOUTPUT.stator.dim.mM:=mM;
      motorOUTPUT.stator.dim.cM:=cM;
      motorOUTPUT.casing.dim.M_S:=M_S;
      motorOUTPUT.casing.dim.I_S:=I_S;

      Modelica.Utilities.Streams.print("motorOUTPUT.plate.M"+Modelica.Math.Vectors.toString(vector(motorOUTPUT.plate.M)),"");

      Modelica.Utilities.Streams.print("End: MassandMOI","");

    end MassandMOISRM;

    function MassandMOISYN
      "Calculates Mass and Moments of Inertia of motorSYN, MassandMOISYN"
      import Modelica.SIunits;
      import Modelica.Constants;
      type VolumetricMomentOfInertia = Real (final quantity="Volumetric moment of Inertia", final unit="m5", min=0);
      parameter input SYN_Structure.motorSYN INPUTmotor;
      output SYN_Structure.motorSYN motorOUTPUT;

    protected
      Integer p "Number of pole pairs";
      SIunits.Length L_brut "Raw machine length";
      SIunits.Length h_z "Stator tooth height";
      SIunits.Length b_z "Stator tooth width @ D";
      SIunits.Length h_tr "Stator slot height";
      SIunits.Length b_s "Stator slot width @ D";
      SIunits.Length h_y "Stator yoke width";
      SIunits.Length delta_htr "height of teeth expansion";
      SIunits.Length D_os "Outer stator diameter";
      SIunits.Length D_is "Bore diameter";
      Real K_fe "Tooth linear correction factor";
      Real K_u_s "Stator slot filling factor";
      Real f_r_s "Stator lamination factor";
      Real K_uhc "Stator head coil filling factor";
      SIunits.Length D_ir "Radial distance of point2";
      SIunits.Length D_shaft "Shaft external diameter";
      SIunits.Length D_shaft_internal "Shaft internal diameter";
      SIunits.Length b_rp "Rotor pole width";
      Real[:,2] externalrotorpoints;
      SIunits.Length[1,2] semi_coil_center "Using MOI-COORDS";
      Real K_p "Pole linear correction factor";
      Real K_ry "Rotor yoke linear correction factor";
      Real K_u_r "Rotor solot filling factor";
      Real f_r_r "Rotor lamination factor";
      SIunits.Length b_rc;
      SIunits.Length D_cond_r "Conductor diameter";
      SIunits.Length l_extraH "Stator head coil longitudinal extra length";
      SIunits.Length l_extraW
        "Stator head coil winding longitudinal extra length";
      SIunits.Length l_extra;
      SIunits.Length l_case_exp_front "Extra case front length";
      SIunits.Length l_case_exp_rear "Extra case rear length";
      SIunits.Length l_case_exp_void "Internal margin length";
      SIunits.Length l_case "Square case length";
      SIunits.Length h_case "Square case height";
      SIunits.Length l_shaft "Total shaft length";
      SIunits.Length l_total "Total case length";
      SIunits.Density ro_case "Metal cabinet density";
      SIunits.Density ro_shaft "Magnetic steel density";
      SIunits.Density ro_mstR "Magnetic steel density";
      SIunits.Density ro_cuR "Cu density";
      SIunits.Density ro_mstS "Magnetic steel density";
      SIunits.Density ro_cuS "Cu density";
      Real shape;
      Integer case_type "1=Circular, 2=Square solid";
      SIunits.Length h_rc "Rotor coil height";
      Real Insulator_Steel_ro_ratio
        "Density ratio between steel and its 'glue'";
      Real Insulator_Copper_ro_ratio
        "Density ratio between copper and its insulator";
      Real headcoil_layers
        "Number of layers to be calculated in headcoil iteration";
      SIunits.Density ro_mstSye "Length-corrected stator yoke mass density";
      SIunits.Density ro_mstSte "Length-corrected stator tooth mass density";
      SIunits.Density ro_cuSe
        "'Insulation and fill factor'-corrected stator copper mass density";
      SIunits.Density ro_mstRe
        "Stack corrected rotor magnetic steel mass density";
      SIunits.Density ro_cuRe
        "'Insulation and fill factor'- corrected rotor copper mass density";
      SIunits.Density ro_cuRe2;
      SIunits.Density ro_cuRe3;
      SIunits.Density ro_cu_matR
        "Rotor copper material area-corrected mass density";
      SIunits.Length l_R_pole "Cooling ducts rotor pole corrected length";
      SIunits.Length l_R_yoke "Cooling ducts rotor yoke corrected length";
      SIunits.Volume v_R_shaftB "Shaft bottom volume";
      SIunits.Volume v_R_shaftT "Shaft top volume";
      Real[5,2] semi_coil_points;
      Real[5,2] semi_coil_points_R;
      Real[5,2] semi_coil_points_Rs;
      VolumetricMomentOfInertia Iv_R_shaftB
        "Shaft bottom kind of volume inertia";
      VolumetricMomentOfInertia Iv_R_shaftT "Shaft top kind of volume inertia";
      SIunits.Volume v_R_yoke "Shaft bottom volume";
      VolumetricMomentOfInertia Iv_R_yoke "Shaft top kind of volume inertia";
      SIunits.Volume v_R_poles "Magnetic steel points volume";
      SIunits.MomentOfInertia I_R_poles "Magnetic steel inertia";
      SIunits.Volume vp_R_semicoil "Stator semicoil volume per pole";
      SIunits.MomentOfInertia Ip_R_semicoil
        "Stator coil moment of inertia per pole";
      SIunits.Volume vp2_R_semicoil "Stator semicoil volume per pole";
      SIunits.MomentOfInertia Ip2_R_semicoil
        "Stator semicoil moment of inertia";
      SIunits.Length[1,2] headcoil_center "Head coil shape reposition";
      SIunits.Volume vp_R_headcoil "Head coil volume per pole per side";
      SIunits.MomentOfInertia Il_R_headcoil
        "Head coil moment of inertia per pole per side";
      SIunits.Length Deltaz "In Z-Axis raises of the head coil approximation";
      SIunits.Length z_i "Z center position of the head coil layer";
      SIunits.Length b_rc_i "In X-axis length of headcoil layer";
      SIunits.Length b_hcl_i "Head coil layer width";
      SIunits.Length[5,2] headcoil_layer_points
        "Points defining head coil rectangle";
      SIunits.Mass M_mst_R "Rotor's magnetic steel mass";
      SIunits.Mass M_cu_R "Total rotor copper mass";
      SIunits.MomentOfInertia I_R "Rotor inertia";
      SIunits.Mass M_R "Rotor mass";
      SIunits.Volume v_S_exp "Stator tooth expansion volume";
      SIunits.Volume v_S_tAVG "Stator tooth ring volume";
      SIunits.Volume v_S_mst_yoke "Stator yoke ring volume";
      SIunits.Volume v_S_cu_headW "Stator coil head volume";
      SIunits.Volume v_S_cu_headH "Stator tooth ring volume";
      VolumetricMomentOfInertia Iv_S_exp
        "Stator tooth expansion kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_tAVG
        "Stator tooth ring kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_mst_yoke
        "Stator yoke ring kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_cu_headW
        "Stator coil head kind of volume inertia";
      VolumetricMomentOfInertia Iv_S_cu_headH
        "Stator coil head kind of volume inertia";
      SIunits.Density ro_tAVG "Tooth mean density";
      SIunits.Density ro_cu_tooth "Tooth copper mean density";
      SIunits.Density ro_mst_tooth "Tooth magnetic steel mean density";
      SIunits.Density ro_cu_headW "Tooth winding filling density";
      SIunits.Density ro_cu_headH "Tooth extra head area compensated density";
      SIunits.MomentOfInertia I_S_mst_yoke "Stator yoke ring moment of inertia";
      SIunits.MomentOfInertia I_S_tAVG "Stator tooth ring moment of inertia";
      SIunits.MomentOfInertia I_S_exp
        "Stator tooth expansion moment of inertia";
      SIunits.MomentOfInertia I_S_cu_head "Stator coil head moment of inertia";
      SIunits.Mass M_cu_S "Total copper mass";
      SIunits.Mass M_mst_S "Stator magnetic mass";
      SIunits.Mass M_S "Stator mass";
      SIunits.MomentOfInertia I_S "Stator total moment of inertia";
      SIunits.Volume v_S_body "Stator body case volume";
      SIunits.Volume v_S_front "Stator front body case volume";
      SIunits.Volume v_S_rear "Stator rear body case volume";
      SIunits.Volume v_S_front_hole "Stator front body case hole volume";
      SIunits.Volume v_S_rear_hole "Stator rear body case hole volume";
      VolumetricMomentOfInertia Iv_S_body "Stator body case of volume inertia";
      VolumetricMomentOfInertia Iv_S_front
        "Stator front body of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear "Stator rear body of volume inertia";
      VolumetricMomentOfInertia Iv_S_front_hole
        "Stator front body case hole of volume inertia";
      VolumetricMomentOfInertia Iv_S_rear_hole
        "Stator rear body case hole of volume inertia";
      SIunits.MomentOfInertia I_S_body "Case moment of inertia";
      SIunits.MomentOfInertia I_S_front "Case front plate moment of inertia";
      SIunits.MomentOfInertia I_S_rear "Case rear plate moment of inertia";
      SIunits.MomentOfInertia I_S_case "Stator case moment of inertia";
      SIunits.Mass M_hou "Total housing mass";
      SIunits.Mass M_mst_Total "Total magnetic steel mass";
      SIunits.Mass M_cu_Total "Total copper mass";
      SIunits.Mass M "Total mass";
      SIunits.MomentOfInertia I_houS "Stator and housing moment of inertia";
      SIunits.MomentOfInertia I_Total "Total moment of inertia";

    algorithm
      // STEP 1: Internal function data gathering (from INPUTmotor)

      // Dimensions

      p:=INPUTmotor.p;

      // Stator

      L_brut:=INPUTmotor.stator.dim.l;

      h_z:=INPUTmotor.stator.dim.h_z;
      b_z:=INPUTmotor.stator.dim.b_z;
      h_tr:=INPUTmotor.stator.dim.h_tr;
      b_s:=INPUTmotor.stator.dim.b_s;
      h_y:=INPUTmotor.stator.dim.h_y;
      delta_htr:=INPUTmotor.stator.dim.delta_htr;

      D_os:=INPUTmotor.stator.dim.D_os;
      D_is:=INPUTmotor.stator.dim.D_is;

      K_fe:=INPUTmotor.stator.dim.K_fe;
      K_u_s:=INPUTmotor.stator.dim.K_u;
      f_r_s:=INPUTmotor.stator.dim.f_r_s;

      // Winding Stator

      K_uhc:=INPUTmotor.stator.win.K_uhc;

      // Rotor
      D_ir:=INPUTmotor.rotor.dim.D_ir;
      D_shaft:=INPUTmotor.rotor.dim.D_shaft;
      D_shaft_internal:=INPUTmotor.rotor.dim.D_shaft_internal;
      b_rp:=INPUTmotor.rotor.dim.b_rp;

      externalrotorpoints:=INPUTmotor.rotor.geo.externalrotorpoints;
      semi_coil_center:=INPUTmotor.rotor.geo.semi_coil_center;
      K_p:=INPUTmotor.rotor.dim.K_p;
      K_ry:=INPUTmotor.rotor.dim.K_ry;
      K_u_r:=INPUTmotor.rotor.dim.K_u;
      f_r_r:=INPUTmotor.rotor.dim.f_r;

      // Winding Rotor
      b_rc:=INPUTmotor.rotor.win.dim.b_rc;
      h_rc:=INPUTmotor.rotor.win.dim.h_rc;
      D_cond_r:=INPUTmotor.rotor.win.dim.D_cond_r;

      // Housing
      l_extraH:=INPUTmotor.stator.win.dim.l_extraH;
      l_extraW:=INPUTmotor.stator.win.dim.l_extraW;
      l_extra:=INPUTmotor.stator.win.dim.l_extra;
      l_case_exp_front:=INPUTmotor.casing.dim.l_case_exp_front;
      l_case_exp_rear:=INPUTmotor.casing.dim.l_case_exp_rear;
      l_case_exp_void:=INPUTmotor.casing.dim.l_case_exp_void;
      l_case:=INPUTmotor.casing.dim.l_case;
      h_case:=INPUTmotor.casing.dim.h_case;
      l_shaft:=INPUTmotor.rotor.dim.l_shaft;

      l_total:=INPUTmotor.casing.l_total;

      // Densities
      ro_case:=INPUTmotor.casing.dim.phy.ro_case;
      ro_shaft:=INPUTmotor.rotor.dim.phy.ro_shaft;

      ro_mstR:=INPUTmotor.rotor.dim.phy.ro_mst;
      ro_cuR:=INPUTmotor.rotor.dim.phy.ro_cu;

      ro_mstS:=INPUTmotor.stator.dim.phy.ro_mst;
      ro_cuS:=INPUTmotor.stator.dim.phy.ro_cu;

      // Flags & Shapes
      shape:=INPUTmotor.rotor.shape;
      case_type:=INPUTmotor.stator.case_type;

      Insulator_Steel_ro_ratio:=0.2;
      Insulator_Copper_ro_ratio:=0.1;
      headcoil_layers:=26;
      ro_mstSye:=(f_r_s + (1 - f_r_s)*Insulator_Steel_ro_ratio)/K_fe*ro_mstS;
      ro_mstSte:=(f_r_s + (1 - f_r_s)*Insulator_Steel_ro_ratio)/K_fe*ro_mstS;
      ro_cuSe:=ro_cuS*(Insulator_Copper_ro_ratio + K_u_s*(1 - Insulator_Copper_ro_ratio));
      ro_mstRe:=(f_r_r + (1 - f_r_r)*Insulator_Steel_ro_ratio)*ro_mstR;
      ro_cuRe:=ro_cuR*(Insulator_Copper_ro_ratio + K_u_r*(1 - Insulator_Copper_ro_ratio));
      ro_cuRe2:=ro_cuR*(K_u_r + (1 - K_u_r)*Insulator_Copper_ro_ratio);
      ro_cuRe3:=ro_cuR*(Insulator_Copper_ro_ratio + K_u_r*(1 - Insulator_Copper_ro_ratio));
      ro_cu_matR:=ro_cuR*K_u_r;
      l_R_pole:=L_brut/K_p;
      l_R_yoke:=L_brut/K_ry;

      // STEP 2: Rotor

      v_R_shaftB:=l_shaft*Constants.pi*(D_shaft/2)^2;
      v_R_shaftT:=L_brut*Constants.pi*((D_shaft_internal/2)^2 - (D_shaft/2)^2);
      Iv_R_shaftB:=0.5*v_R_shaftB*(D_shaft/2)^2;
      Iv_R_shaftT:=0.5*v_R_shaftT*((D_shaft_internal/2)^2 + (D_shaft/2)^2);
      v_R_yoke:=l_R_yoke*Constants.pi*((D_ir/2)^2 - (D_shaft_internal/2)^2);
      Iv_R_yoke:=0.5*v_R_yoke*((D_ir/2)^2 + (D_shaft_internal/2)^2);
      v_R_poles:=MOIMA(externalrotorpoints,ro_mstRe,2)*l_R_pole - l_R_pole*Constants.pi*(D_ir/2)^2;
      I_R_poles:=MOIMA(externalrotorpoints,ro_mstRe,3)*l_R_yoke - ro_mstRe*0.5*l_R_yoke*Constants.pi*(D_ir/2)^4;

      // Area centered at origin
      semi_coil_points:=[-h_rc/2,-b_rc/2; h_rc/2,-b_rc/2; h_rc/2,b_rc/2; -h_rc/2,b_rc/2; -h_rc/
        2,-b_rc/2];

      // First semicoil
      semi_coil_points_R[:,1]:=semi_coil_points[:, 1].+ semi_coil_center[1,1];
      semi_coil_points_R[:,2]:=semi_coil_points[:, 2].+ semi_coil_center[1, 2];

      vp_R_semicoil:=MOIMA(semi_coil_points_R,ro_cuRe,2)*(L_brut + D_cond_r);
      Ip_R_semicoil:=MOIMA(semi_coil_points_R,ro_cuRe,3)*(L_brut + D_cond_r);

      // Second semicoil
      semi_coil_points_Rs[:,1]:=semi_coil_points[:, 1] .+ semi_coil_center[1, 1];
      semi_coil_points_Rs[:,2]:=semi_coil_points[:, 2] .- semi_coil_center[1, 2];
      vp2_R_semicoil:=MOIMA(semi_coil_points_Rs,ro_cuRe,2)*(L_brut + D_cond_r);
      Ip2_R_semicoil:=MOIMA(semi_coil_points_Rs,ro_cuRe,3)*(L_brut + D_cond_r);
      headcoil_center:=[semi_coil_center[1, 1],0];
      vp_R_headcoil:=0;
      Il_R_headcoil:=0;

      for i in 1:1:headcoil_layers loop
        Deltaz:=b_rc/headcoil_layers;
        z_i:=(i - 1)*Deltaz + Deltaz/2;
        b_rc_i:=sqrt(b_rc^2 - z_i^2);
        b_hcl_i:=b_rp + 2*b_rc_i;

        // First and last point must be the same,rectangular shape
        headcoil_layer_points:=[-h_rc/2,-b_hcl_i/2; h_rc/2,-b_hcl_i/2; h_rc/2,b_hcl_i/2; -h_rc/2,b_hcl_i/2; -h_rc/2,-b_hcl_i/2];
        headcoil_layer_points[:,1]:=headcoil_layer_points[:, 1].+ headcoil_center[1, 1];
        headcoil_layer_points[:,2]:=headcoil_layer_points[:, 2].+ headcoil_center[1, 2];

        vp_R_headcoil:=vp_R_headcoil + MOIMA(headcoil_layer_points,ro_cuRe,2)*Deltaz;
        Il_R_headcoil:=Il_R_headcoil + MOIMA(headcoil_layer_points,ro_cuRe,3)*Deltaz;

      end for;

      // STEP 2.5: Rotor totals (sync updated)

      M_mst_R:=ro_mstRe*(v_R_yoke + v_R_poles);
      M_cu_R:=ro_cu_matR*(2*p)*2*(vp_R_headcoil + vp2_R_semicoil);
      I_R:=(Iv_R_shaftB + Iv_R_shaftT)*ro_shaft + Iv_R_yoke*ro_mstRe + I_R_poles + (2*p)*2*(Il_R_headcoil + Ip2_R_semicoil);
      M_R:=M_mst_R + M_cu_R;

      // STEP 3: Stator
      v_S_exp:=L_brut*Constants.pi*((D_is/2 + delta_htr)^2 - (D_is/2)^2);
      v_S_tAVG:=L_brut*Constants.pi*((D_os/2 - h_y)^2 - ((D_os/2 - h_y - h_tr))^2);
      v_S_mst_yoke:=L_brut*Constants.pi*((D_os/2)^2 - ((D_os/2 - h_y))^2);

      v_S_cu_headW:=2*l_extraW*Constants.pi*((D_os/2 - h_y)^2 - (D_os/2 - h_y - h_tr)^2);
      v_S_cu_headH:=2*l_extraH*Constants.pi*((D_os/2 - h_y)^2 - ((D_os/2 - h_y - h_tr))^2);

      Iv_S_exp:=0.5*v_S_exp*((D_is/2 + delta_htr)^2 + (D_is/2)^2);
      Iv_S_tAVG:=0.5*v_S_tAVG*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);
      Iv_S_mst_yoke:=0.5*v_S_mst_yoke*((D_os/2)^2 + (D_os/2 - h_y)^2);
      Iv_S_cu_headW:=0.5*v_S_cu_headW*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);
      Iv_S_cu_headH:=0.5*v_S_cu_headH*((D_os/2 - h_y)^2 + (D_os/2 - h_y - h_tr)^2);

      ro_tAVG:=(b_s*K_u_s*ro_cuSe + b_z*ro_mstSte)/(b_s + b_z);
      ro_cu_tooth:=(b_s*K_u_s*ro_cuS)/(b_s + b_z);
      ro_mst_tooth:=(b_z*ro_mstSte)/(b_s + b_z);
      ro_cu_headW:=K_uhc*ro_cuSe;
      ro_cu_headH:=b_s*ro_cuSe/(b_s + b_z);

      I_S_mst_yoke:=Iv_S_mst_yoke*ro_mstSye;
      I_S_tAVG:=Iv_S_tAVG*ro_tAVG;
      I_S_exp:=Iv_S_exp*ro_mstSte;
      I_S_cu_head:=Iv_S_cu_headW*ro_cu_headW + Iv_S_cu_headH*ro_cu_headH;

      // Step 3.5: Stator totals
      M_cu_S:=v_S_cu_headW*ro_cu_headW + v_S_cu_headH*ro_cu_headH + v_S_tAVG*ro_cu_tooth;
      M_mst_S:=v_S_exp*ro_mstSte + v_S_mst_yoke*ro_mstSye + v_S_tAVG*ro_mst_tooth;
      M_S:=v_S_exp*ro_mstSte + v_S_mst_yoke*ro_mstSye + v_S_tAVG*ro_tAVG + v_S_cu_headW*ro_cu_headW + v_S_cu_headH*ro_cu_headH;
      I_S:=I_S_mst_yoke + I_S_tAVG + I_S_exp + I_S_cu_head;

      // Step 4: Housing

      if case_type==1 then // Circular
        v_S_body:=l_case*Constants.pi*((h_case/2)^2 - (D_os/2)^2);
        v_S_front:=l_case_exp_front*Constants.pi*(h_case/2)^2;
        v_S_rear:=l_case_exp_rear*Constants.pi*(h_case/2)^2;
        v_S_front_hole:=l_case_exp_front*Constants.pi*(D_shaft/2)^2;
        v_S_rear_hole:=l_case_exp_rear*Constants.pi*(D_shaft/2)^2;
        Iv_S_body:=0.5*v_S_body*((h_case/2)^2 + (D_os/2)^2);
        Iv_S_front:=0.5*v_S_front*(h_case/2)^2;
        Iv_S_rear:=0.5*v_S_rear*(h_case/2)^2;
        Iv_S_front_hole:=0.5*v_S_front_hole*(D_shaft/2)^2;
        Iv_S_rear_hole:=0.5*v_S_rear_hole*(D_shaft/2)^2;
      elseif case_type==2 then // Solid squared
        v_S_body:=l_case*(h_case^2 - Constants.pi*(D_os/2)^2);
        v_S_front:=l_case_exp_front*h_case^2;
        v_S_rear:=l_case_exp_rear*h_case^2;
        v_S_front_hole:=l_case_exp_front*Constants.pi*(D_shaft/2)^2;
        v_S_rear_hole:=l_case_exp_front*Constants.pi*(D_shaft/2)^2;
        Iv_S_body:=l_case*(1/6*h_case^4 - Constants.pi/2*(D_os/2)^4);
        Iv_S_front:=l_case_exp_front*1/6*h_case^4;
        Iv_S_rear:=l_case_exp_rear*1/6*h_case^4;
        Iv_S_front_hole:=0.5*v_S_front_hole*(D_shaft/2)^2;
        Iv_S_rear_hole:=0.5*v_S_rear_hole*(D_shaft/2)^2;
      end if;

      I_S_body:=Iv_S_body*ro_case;
      I_S_front:=(Iv_S_front - Iv_S_front_hole)*ro_case;
      I_S_rear:=(Iv_S_rear - Iv_S_rear_hole)*ro_case;
      I_S_case:=I_S_body + I_S_front + I_S_rear;

      M_hou:=(v_S_body + v_S_front + v_S_rear - v_S_front_hole - v_S_rear_hole)*ro_case;
      M_mst_Total:=M_mst_R + M_mst_S;
      M_cu_Total:=M_cu_R + M_cu_S;
      M:=M_R + M_S + M_hou;
      I_houS:=I_S + I_S_case;
      I_Total:=I_R + I_S + I_S_case;

      // FINAL STEP: Data assign to motorOUTPUT
      motorOUTPUT:=INPUTmotor;
      motorOUTPUT.rotor.dim.M_mst_R:=M_mst_R;
      motorOUTPUT.rotor.dim.cM:=M_cu_R;
      motorOUTPUT.rotor.dim.M_R:=M_R;
      motorOUTPUT.rotor.dim.I_R:=I_R;

      motorOUTPUT.plate.M:=M;
      motorOUTPUT.plate.TmM:=M_mst_Total;
      motorOUTPUT.plate.I_Total:=I_Total;

      motorOUTPUT.stator.dim.mM:=M_mst_S;
      motorOUTPUT.stator.dim.cM:=M_cu_S;
      motorOUTPUT.stator.dim.I_S:=I_S;
      motorOUTPUT.stator.dim.M_S:=M_S;

      motorOUTPUT.casing.dim.M_S:=M_hou;
      motorOUTPUT.casing.dim.I_S:=I_S_case;

      Modelica.Utilities.Streams.print("*** End: MassandMOI Synchronous ***","");

    end MassandMOISYN;

    function quadrapack
      "This function obtains a number of conductors per area, given a diameter following a square pattern of circles"
      import Modelica.SIunits;
      import Modelica.Constants;
      parameter input SIunits.Length h_sc "Stator coil height";
      parameter input SIunits.Length b_sc "Stator coil width";
      parameter input SIunits.Length D_cond "Conductor diameter";
      output packr quadrpack;
    protected
      Real pila_n;
      Real n;
      Real pila_m;
      Real m;
      Real me;
      Real b_sc_real;
      Real h_sc_real "Actual occupied area";
      Real C_sc "Number of conductors per area";
      Real fill "Fill factor";
      Real fill_real "Real fill factor";
      Real[:,:] centers;
      Integer centers_counter=1;
      Real[:,:] limit;
      Real[:,:] limit_real;

    algorithm
      n:=floor(h_sc/D_cond);
      m:=floor(b_sc/D_cond);
      C_sc:=m*n;
      h_sc_real:=n*D_cond;
      b_sc_real:=m*D_cond;
      if C_sc==0 then
        fill:=0;
        fill_real:=0;
      else
        fill:=(C_sc*Constants.pi*(D_cond/2)^2)/(h_sc*b_sc);
        fill_real:=(C_sc*Constants.pi*(D_cond/2)^2)/(h_sc_real*b_sc_real);
      end if;

      // Centers positioning
      Modelica.Utilities.Streams.print("h_sc: "+Modelica.Math.Vectors.toString((vector(h_sc))),"");
      Modelica.Utilities.Streams.print("b_sc: "+Modelica.Math.Vectors.toString((vector(b_sc))),"");

      centers:=zeros(integer(m*n), 2);
      for i in 1:1:m loop // every column
        for j in 1:1:n loop // ever row
          centers[integer((j+(i-1)*n)),:]:={D_cond/2 + (j - 1)*D_cond,D_cond/2 + (i - 1)*D_cond};
        end for;
      end for;

      // Check
      limit:=[0,0; h_sc,0; h_sc,b_sc; 0,b_sc];
      limit_real:=[0,0; h_sc_real,0; h_sc_real,b_sc_real; 0,b_sc_real; 0,0];

       quadrpack.C_sc:=C_sc;
       quadrpack.D_cond:=D_cond;
       quadrpack.K_f:=fill;
       quadrpack.K_f_real:=fill_real;
       quadrpack.dim.h_sc_real:=h_sc_real;
       quadrpack.dim.b_sc_real:=b_sc_real;
       quadrpack.draw.centers:=centers;
       quadrpack.draw.Semi_coil_points:=limit;
       quadrpack.draw.Semi_coil_real_points:=limit_real;

    annotation (
        Documentation(info="<html>

<head>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b>quadrapack() </b>performs the following operations:</p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Calculate the number of conductors able to fit
in the area.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Calculate the actual area occupied by the
conductors.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Calculate the real filling factor (by the actual
area) and the filling factor taking into account all the viable allocable area.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-GB>-<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>Return the actual area position and the relative
position of each conductor’s centre.</span></p>

<p class=MsoNormal align=center style='text-align:center'><img width=253
height=204 id='Imagen 29' src='quadrapack_archivos/image001.jpg'></p>

</div>

</body>

</html>"));
    end quadrapack;

    function PowerDShaft
      "PowerDShaft Returns shaft diameter based on the desired Power"

      import Modelica.SIunits;
      parameter input SIunits.Power P_mec "Mechanical power";
      parameter input Real Internal_shaft_coefficient
        "Ratio between external and internal shafts";

      parameter output SIunits.Length D_shaft_external
        "Shaft external diameter";
      parameter output SIunits.Length D_shaft_internal
        "Shaft internal diameter";
    protected
      parameter Real[:,:] PowerDshaft=DataFiles.readCSVmatrix("PowerDshaft.txt")
        "Normalized shaft diameters by mechanical power";
      SIunits.Length D_shaft "Shaft diameter";

    algorithm
      for i in 1:size(PowerDshaft,1) loop
        if (PowerDshaft[i,1]*1000>=P_mec) then
          D_shaft:=PowerDshaft[i, 4]/1000;
          break;
        elseif (i==size(PowerDshaft,1)) then
          D_shaft:=PowerDshaft[size(PowerDshaft,1), 4]/1000;
        end if;
      end for;

      D_shaft_external:=D_shaft;
      D_shaft_internal:=0;
    end PowerDShaft;

    function ReducedInputs
      "ReducedInputs3 Checks pre-sizing electrical inputs data and coherency, generates subsequent power/circuit data"

    //  This program assumes I and E aligned, i.e. torque angle is 0. May be suitable
    //  for IM, SMPMSM, IPM and Syn. with the following restrictions:
    //
    //  for SMPMSM is a very good approximation.
    //  for IPM is a rather good approximation for first attempt, the less reluctance torque the better.
    //  for Syn is a rather good approximation because its torque angle adjustable ratio.
    //
    //  Torque angle consideration would require more inputs and some more complex approach.
    //  U,I must be RMS values.
    //  n,T must be rated values.
    //  If we supose torque angle = 0.
    //  If we supose machine working as motor P_e > P_mec.

      import Modelica.SIunits;

      parameter input SIunits.Voltage U_ph "Rated phase voltage";
      parameter input SIunits.Current I_ph "Rated stator current";
      parameter input Real PF "Power factor";
      parameter input Real K_E_U_ratio_in "Per phase E/U modulus ratio";
      parameter input Modelica.SIunits.Conversions.NonSIunits.Angle_deg delta=0
        "Torque/current angle at rated conditions (default 0 for SPMSM)";
      parameter input SIunits.Torque T_rat "Rated torque";
      parameter input
        Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm               n_rat
        "Rated speed";
      parameter input Integer m "Number of phases";
      parameter input Integer p "Number of pole pairs";
      parameter input Integer param "Other parameters and flags";
      output ReducedInputsr outs "Output record";

    protected
      Integer Motor_Generator=param;
      Real eff_usual=0.95
        "Value of efficiency for electrical machines for IE3 standard p.u";
      SIunits.Angle fi "U-I angle";
      SIunits.AngularVelocity n_rad "Mechanical speed";
      SIunits.AngularVelocity n_rad_elec "Electrical speed";
      SIunits.Power P_mec "Mechanical power";
      SIunits.Power P_elec "Electrical power";
      SIunits.Current I_fixed "Estimated RMS current if voltage is fixed";
      SIunits.Voltage U_fixed "Estimated RMS voltage if current is fixed";
      Real eff "Efficiency";
      SIunits.Voltage E_ph "Back EMF at rated conditions";
      SIunits.Resistance R_ph "Estimated phase resistance";
      SIunits.Inductance L_ph "Estimated phase inductance";
      SIunits.Resistance R_ml
        "Maximum value of phase resistance (if all losses were copper losses)";
      Real K_E_U_ratio=K_E_U_ratio_in;
      Real K_E_U_ratio_new;
      SIunits.Voltage E_ph_new;
      SIunits.Resistance R_ph_new;

    algorithm
      fi:=abs(acos(PF));
      n_rad:=Modelica.SIunits.Conversions.from_rpm(n_rat);
      n_rad_elec:=n_rad*p;
      P_mec:=n_rad*T_rat;
      P_elec:=m*U_ph*I_ph*cos(fi);

      if (Motor_Generator==0) then
        eff:=P_mec/P_elec;
        if (eff<eff_usual) then
          I_fixed:=P_mec/eff_usual/(m*U_ph*PF);
          U_fixed:=P_mec/eff_usual/(m*I_ph*PF);
          Modelica.Utilities.Streams.print("RI|- WARNING: efficiency may be lower than standards (IE3~=0.95)>"+Modelica.Math.Vectors.toString((vector(eff)))+" [p.u.] please revise U_ph and I_ph combination:","");
          Modelica.Utilities.Streams.print("RI|- WARNING: If you want to keep the desired Voltage value, current should around: "+Modelica.Math.Vectors.toString((vector(I_fixed)))+"[A_rms]","");
          Modelica.Utilities.Streams.print("RI|- WARNING: If you want to keep the desired Current value, voltage should around:"+Modelica.Math.Vectors.toString((vector(U_fixed)))+"[V_rms]","");
        end if;
        if (PF<K_E_U_ratio) then
          K_E_U_ratio:=PF;
        end if;
        E_ph:=U_ph*K_E_U_ratio;
        R_ph:=(U_ph*cos(fi) - E_ph)/I_ph;
        L_ph:=U_ph*sin(fi)/(n_rad_elec*I_ph);
        R_ml:=sqrt(abs(P_elec - P_mec)/I_ph/3);
        if (R_ml<R_ph) then     // if losses are not compatible for motors
          Modelica.Utilities.Streams.print("RI| - WARNING: Estimated losses incompatible with desired K_E_U_ratio and PF, adjusting K_E_U, E and R...","");
          K_E_U_ratio_new:=(U_ph*cos(fi) - R_ml*I_ph)/U_ph;
          E_ph_new:=U_ph*K_E_U_ratio_new;
          R_ph_new:=(U_ph*cos(fi) - E_ph_new)/I_ph;

          K_E_U_ratio:=K_E_U_ratio_new;
          E_ph:=E_ph_new;
          R_ph:=R_ph_new;
        end if;
      else
        eff:=P_elec/P_mec;
        if (eff<eff_usual) then
          I_fixed:=P_mec*eff_usual/(m*U_ph*PF);
          U_fixed:=P_mec*eff_usual/(m*I_ph*PF);
          Modelica.Utilities.Streams.print("RI|- WARNING: efficiency may be lower than standards (IE3~=0.95)>"+Modelica.Math.Vectors.toString((vector(eff)))+" [p.u.] please revise U_ph and I_ph combination:","");
          Modelica.Utilities.Streams.print("RI|- WARNING: If you want to keep the desired Voltage value, current should around: "+Modelica.Math.Vectors.toString((vector(I_fixed)))+"[A_rms]","");
          Modelica.Utilities.Streams.print("RI|- WARNING: If you want to keep the desired Current value, voltage should around:"+Modelica.Math.Vectors.toString((vector(U_fixed)))+"[V_rms]","");
        end if;

        if (PF>K_E_U_ratio) then
          K_E_U_ratio:=PF;
        end if;
        E_ph:=U_ph*K_E_U_ratio;
        R_ph:=(E_ph - U_ph*cos(fi))/I_ph;
        L_ph:=U_ph*sin(fi)/(n_rad_elec*I_ph);
        R_ml:=sqrt(abs(P_elec - P_mec)/I_ph);
        if (R_ml<R_ph) then    // if losses are not compatible for MOTORS
          Modelica.Utilities.Streams.print("RI| - WARNING: Estimated losses incompatible with desired K_E_U_ratio and PF, adjusting K_E_U, E and R...","");
          K_E_U_ratio_new:=(U_ph*cos(fi) - R_ml*I_ph)/U_ph;
          E_ph_new:=U_ph*K_E_U_ratio_new;
          R_ph_new:=(U_ph*cos(fi) - E_ph_new)/I_ph;
          K_E_U_ratio:=K_E_U_ratio_new;
          E_ph:=E_ph_new;
          R_ph:=R_ph_new;
        end if;
      end if;
      outs.K_E_U_ratio:=K_E_U_ratio;
      outs.E_ph:=E_ph;
      outs.R_ml:=R_ml;
      outs.R_ph:=R_ph;
      outs.L_ph:=L_ph;
      outs.P_mec:=P_mec;
      outs.P_elec:=P_elec;
      outs.eff:=eff;

    end ReducedInputs;

    record ReducedInputsr
      import Modelica.SIunits;
      Real K_E_U_ratio "Per phase E/U modulus ratio";
      SIunits.Voltage E_ph "Back EMF at rated conditions";
      SIunits.Resistance R_ml
        "Maximum value of phase resistance (if all losses were copper losses)";
      SIunits.Resistance R_ph "Estimated phase resistance";
      SIunits.Inductance L_ph "Estimated phase inductance";
      SIunits.Power P_mec "Mechanical power";
      SIunits.Power P_elec "Electrical power";
      Real eff "Efficiency";

    end ReducedInputsr;

    function Reflection
      "Performs basic algebraic reflection through axis situated at the angle position"
     input Real[:,2] pointsin;
     input Modelica.SIunits.Angle angle "Rotation angle";
     output Real[:,2] pointsout;
    protected
      Real[2,2] A "Reflection matrix";
    algorithm
     A:=[[cos(2*angle),sin(2*angle)]; [sin(2*angle),-cos(2*angle)]];
     pointsout:=zeros(size(pointsin,1),size(pointsin,2));
     for i in 1:1:size(pointsin,1) loop
       pointsout[i,:]:=(transpose(A*matrix(pointsin[i,:])))[1,:];
     end for;
     pointsout:=Modelica.Math.Matrices.flipUpDown(pointsout);

    end Reflection;

    function Reluctance
      "Reluctance returns the value of a cuboid shaped reluctance where flux flows in length direction"
      import Modelica.SIunits;
      parameter input SIunits.Length length "Length";
      parameter input SIunits.Length width "Width";
      parameter input SIunits.Length height "Height";
      parameter input SIunits.Permeability permeability "Permeability";
      output SIunits.Reluctance Reluctance "Reluctance";
    algorithm
      Reluctance:=length/(permeability*width*height);

    end Reluctance;

    function Resistance_IPM
      "This function computes R per phase value, Resistance 3"
      import Modelica.Constants;
      import Modelica.SIunits;
      constant Real c=68E-12 "Cu temperature constant";
      constant Real alpha=0.003862 "Cu temperature constant";

      parameter input IPM_Structure.motorIPM motorINPUT;
      output IPM_Structure.motorIPM motorOUTPUT;
    protected
      SIunits.Temperature t_aro "Relative temperature for alpha_ro";
      SIunits.Resistivity ro_t_aro;
      SIunits.Temperature t_work "Cu Working Temperatature";
      SIunits.Resistivity ro_work "Resistivity at Work Temperature";
      SIunits.Resistivity ro_work2
        "Resistivity at Work Temperature Second Method";
      SIunits.Length l_turn "Average Turn Length";

      SIunits.Temperature t_amb "Expected ambient temperature";
      SIunits.Temperature Delta_t_work
        "Expected increase of working temperature above ambient";
      //SIunits.Temperature t_aro_ref "Relative temperature for alpha_ro";
      //SIunits.Resistivity ro_t_aro_ref;
      SIunits.Area S_cond "Area of Conductor";
      Integer Q_s "Number of stator slots";
      Real N_ph "Number of turns per phase";
      Integer coilspan "Coil span";
      SIunits.Diameter D "??";
      SIunits.Height h_z "Stator tooth height";
      SIunits.Length l "Stack length";
      SIunits.Length l_extra "Head coil length start value";
      Real Resist[1,4]
        "Output matrix: phase resistance, working temperature, turn length, resistivity";
      SIunits.Resistance R_s "Phase resistance";
    algorithm
      Resist:=zeros(1, 4);
      t_amb:=motorINPUT.casing.dim.phy.t_amb;
      Delta_t_work:=motorINPUT.casing.dim.phy.Delta_t_work;
      t_aro:=motorINPUT.casing.dim.phy.t_aro;
      ro_t_aro:=motorINPUT.stator.win.phy.ro_t_aro;
      S_cond:=motorINPUT.stator.win.S_cond;
      Q_s:=motorINPUT.stator.Q_s;
      N_ph:=motorINPUT.stator.win.N_ph;
      coilspan:=motorINPUT.stator.win.coilspan;
      D:=motorINPUT.stator.dim.D_is;
      h_z:=motorINPUT.stator.dim.h_z;
      l:=motorINPUT.stator.dim.l;
      l_extra:=motorINPUT.stator.win.dim.l_extra;

      Modelica.Utilities.Streams.print("Start: Resistance","");
      Resist:=[0,0,0,0];
      if (t_aro==0 or ro_t_aro==0) then
        t_aro:=20;
        ro_t_aro:=1.68E-8;
      end if;
      t_work:=t_amb + Delta_t_work;
      ro_work:=ro_t_aro + c*(t_work - t_aro);
      ro_work2:=ro_t_aro*(1 + alpha*(t_work - t_aro));
      l_turn:=2*(2*Constants.pi)*coilspan/Q_s*(D/2 + h_z/2) + 2*l + 4*l_extra;

      // Resistance and working temperature outputs
      R_s:=N_ph*l_turn*ro_work/S_cond;                                            // Phase Resistance
      Resist[1,2]:=t_work;                                                        // Cu Working Temperatature
      Resist[1,3]:=l_turn;
      Resist[1,4]:=ro_work;

      // Output assignment
      motorOUTPUT:=motorINPUT;
      motorOUTPUT.stator.phy.R_s:=R_s;
      motorOUTPUT.stator.phy.t_work:=t_work;
      motorOUTPUT.stator.dim.l_turn:=l_turn;
      motorOUTPUT.stator.dim.phy.ro_work:=ro_work;

      Modelica.Utilities.Streams.print("End: Resistance","");
    annotation (
        Documentation(info="<html>

<head>
<title>resistencia() help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>Resistencia2()</span></b><span
lang=EN-GB> function which evaluates resistance given S_cond and other
geometrical parameters, see </span><span lang=EN-GB>Figure 1</span><span
lang=EN-GB>.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=401 height=298 src='resistencia_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019902'></a><a name='_Toc402539910'></a><a
name='_Ref391887975'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Resistance </span><span lang=EN-US>STEP</span><span
lang=EN-GB>s in main code</span></p>

<p class=MsoNormal><span lang=EN-US>There are three reference temperatures to
work with (</span><span lang=EN-GB>Table 1</span><span lang=EN-US>):</span></p>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=111 valign=top style='width:83.4pt;border:solid windowtext 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Variable Name</span></p>
  </td>
  <td width=180 valign=top style='width:134.65pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Description</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Comment</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_aro</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Reference temperature of relative
  resistivity provided</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>This temperature is required to obtain
  relative resistivity initial value, before raise of working temperature is
  applied.</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_amb</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Ambient temperature</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Is the working ambient temperature
  outside the machine.</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Delta_t_work</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Increase over ambient temperature of
  cooper</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase style='page-break-after:avoid'><span lang=EN-US>Is
  temperature’s raise allowed to copper due to respect ambient temperature.
  Usually cooper works up to 85C at an ambient temperature of 35C it’s a </span><span
  class=TablaCodigoCar><span lang=EN-US style='font-size:8.0pt'>Delta_t_work</span></span><span
  lang=EN-US> of 50° C. It must be noted that to probe internal temperature
  inside a slot is rather difficult so surface is usually probed and then an
  increase between 5 and 15° C is applied to it.</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019808'></a><a name='_Toc402539874'></a><a
name='_Ref391888065'><span lang=EN-GB>Table </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Temperatures used and its description.</span></p>

<p class=MsoNormal><span lang=EN-US>By definition resistance is:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=203 height=37 src='resistencia_archivos/image002.png'></span></p>

<p class=MsoNormal><span lang=EN-US>In this case:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=177 height=37 src='resistencia_archivos/image003.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Where N_ph and S_cond are already known
variables, l_turn is medium</span><span lang=EN-GB> </span><span lang=EN-US>turn
length of a coil, and is defined as:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=308 height=48 src='resistencia_archivos/image004.png'></span></p>

<p class=MsoNormal><span lang=EN-US>And it’s intrinsically a geometrical factor
except for head coil extra </span><span lang=EN-GB>length (l_extra</span><span
lang=EN-US>), a parameter widely variable that requires some extra information
because depends on winding type, kind of conductor, material, EMF, insulation,
etc. (see </span><span lang=EN-GB>Figure 2</span><span lang=EN-US>). </span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=536 height=358 src='resistencia_archivos/image005.png'></span></p>

<p class=MsoCaption><a name='_Toc414019903'></a><a name='_Toc402539911'></a><a
name='_Ref391888116'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>2</span><span lang=EN-GB>. Simplified representation of head coils
for turn and volume/inertia considerations</span></p>

<p class=MsoNormal><span lang=EN-GB>ro_work it’s resistivity</span><span
lang=EN-US> at working temperature and it can be evaluated by means of:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=195 height=17 src='resistencia_archivos/image006.png'></span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=165 height=19 src='resistencia_archivos/image007.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Where </span><span class=CdigoenWordCar><span
lang=EN-GB style='font-size:10.0pt'>c</span></span><span lang=EN-US> its
independent of cooper quality (is an element constant) resistivity at reference
temperature is </span><span lang=EN-GB>given by ro_t_aro of that</span><span
lang=EN-US> specific cooper quality:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=121 height=47 src='resistencia_archivos/image008.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Finally surface for one conductor (cross
section is evaluated as):</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=105 height=39 src='resistencia_archivos/image009.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Note that this is valid for a pre-design
but a final design would require a different process </span><span lang=EN-GB>because
S_cond is determined by standardized gauges</span><span lang=EN-US>.</span></p>

</div>

</body>

</html>
"));
    end Resistance_IPM;

    function Resistance_SMPMSM
      "This function computes R per phase value, Resistance 3"
      import Modelica.Constants;
      import Modelica.SIunits;
      constant Real c=68E-12 "Cu temperature constant";
      constant Real alpha=0.003862 "Cu temperature constant";

      parameter input SMPMSM_Structure.motorSMPMSM motorINPUT;
      output SMPMSM_Structure.motorSMPMSM motorOUTPUT;
    protected
      SIunits.Temperature t_aro "Relative temperature for alpha_ro";
      SIunits.Resistivity ro_t_aro;
      SIunits.Temperature t_work "Cu Working Temperatature";
      SIunits.Resistivity ro_work "Resistivity at Work Temperature";
      SIunits.Resistivity ro_work2
        "Resistivity at Work Temperature Second Method";
      SIunits.Length l_turn "Average Turn Length";

      SIunits.Temperature t_amb "Expected ambient temperature";
      SIunits.Temperature Delta_t_work
        "Expected increase of working temperature above ambient";
      //SIunits.Temperature t_aro_ref "Relative temperature for alpha_ro";
      //SIunits.Resistivity ro_t_aro_ref;
      SIunits.Area S_cond "Area of Conductor";
      Integer Q_s "Number of stator slots";
      Real N_ph "Number of turns per phase";
      Integer coilspan "Coil span";
      SIunits.Diameter D "??";
      SIunits.Height h_z "Stator tooth height";
      SIunits.Length l "Stack length";
      SIunits.Length l_extra "Head coil length start value";
      Real Resist[1,4]
        "Output matrix: phase resistance, working temperature, turn length, resistivity";
      SIunits.Resistance R_s "Phase resistance";
    algorithm
      Resist:=zeros(1, 4);
      t_amb:=motorINPUT.casing.dim.phy.t_amb;
      Delta_t_work:=motorINPUT.casing.dim.phy.Delta_t_work;
      t_aro:=motorINPUT.casing.dim.phy.t_aro;
      ro_t_aro:=motorINPUT.stator.win.phy.ro_t_aro;
      S_cond:=motorINPUT.stator.win.S_cond;
      Q_s:=motorINPUT.stator.Q_s;
      N_ph:=motorINPUT.stator.win.N_ph;
      coilspan:=motorINPUT.stator.win.coilspan;
      D:=motorINPUT.stator.dim.D_is;
      h_z:=motorINPUT.stator.dim.h_z;
      l:=motorINPUT.stator.dim.l;
      l_extra:=motorINPUT.stator.win.dim.l_extra;

      Modelica.Utilities.Streams.print("Start: Resistance","");
      Resist:=[0,0,0,0];
      if (t_aro==0 or ro_t_aro==0) then
        t_aro:=20;
        ro_t_aro:=1.68E-8;
      end if;
      t_work:=t_amb + Delta_t_work;
      ro_work:=ro_t_aro + c*(t_work - t_aro);
      ro_work2:=ro_t_aro*(1 + alpha*(t_work - t_aro));
      l_turn:=2*(2*Constants.pi)*coilspan/Q_s*(D/2 + h_z/2) + 2*l + 4*l_extra;

      // Resistance and working temperature outputs
      R_s:=N_ph*l_turn*ro_work/S_cond;                                            // Phase Resistance
      Resist[1,2]:=t_work;                                                        // Cu Working Temperatature
      Resist[1,3]:=l_turn;
      Resist[1,4]:=ro_work;

      // Output assignment
      motorOUTPUT:=motorINPUT;
      motorOUTPUT.stator.phy.R_s:=R_s;
      motorOUTPUT.stator.phy.t_work:=t_work;
      motorOUTPUT.stator.dim.l_turn:=l_turn;
      motorOUTPUT.stator.dim.phy.ro_work:=ro_work;

      Modelica.Utilities.Streams.print("End: Resistance","");
    annotation (
        Documentation(info="<html>

<head>
<title>resistencia() help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>Resistencia2()</span></b><span
lang=EN-GB> function which evaluates resistance given S_cond and other
geometrical parameters, see </span><span lang=EN-GB>Figure 1</span><span
lang=EN-GB>.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=401 height=298 src='resistencia_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019902'></a><a name='_Toc402539910'></a><a
name='_Ref391887975'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Resistance </span><span lang=EN-US>STEP</span><span
lang=EN-GB>s in main code</span></p>

<p class=MsoNormal><span lang=EN-US>There are three reference temperatures to
work with (</span><span lang=EN-GB>Table 1</span><span lang=EN-US>):</span></p>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=111 valign=top style='width:83.4pt;border:solid windowtext 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Variable Name</span></p>
  </td>
  <td width=180 valign=top style='width:134.65pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Description</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Comment</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_aro</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Reference temperature of relative
  resistivity provided</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>This temperature is required to obtain
  relative resistivity initial value, before raise of working temperature is
  applied.</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_amb</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Ambient temperature</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Is the working ambient temperature
  outside the machine.</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Delta_t_work</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Increase over ambient temperature of
  cooper</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase style='page-break-after:avoid'><span lang=EN-US>Is
  temperature’s raise allowed to copper due to respect ambient temperature.
  Usually cooper works up to 85C at an ambient temperature of 35C it’s a </span><span
  class=TablaCodigoCar><span lang=EN-US style='font-size:8.0pt'>Delta_t_work</span></span><span
  lang=EN-US> of 50° C. It must be noted that to probe internal temperature
  inside a slot is rather difficult so surface is usually probed and then an
  increase between 5 and 15° C is applied to it.</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019808'></a><a name='_Toc402539874'></a><a
name='_Ref391888065'><span lang=EN-GB>Table </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Temperatures used and its description.</span></p>

<p class=MsoNormal><span lang=EN-US>By definition resistance is:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=203 height=37 src='resistencia_archivos/image002.png'></span></p>

<p class=MsoNormal><span lang=EN-US>In this case:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=177 height=37 src='resistencia_archivos/image003.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Where N_ph and S_cond are already known
variables, l_turn is medium</span><span lang=EN-GB> </span><span lang=EN-US>turn
length of a coil, and is defined as:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=308 height=48 src='resistencia_archivos/image004.png'></span></p>

<p class=MsoNormal><span lang=EN-US>And it’s intrinsically a geometrical factor
except for head coil extra </span><span lang=EN-GB>length (l_extra</span><span
lang=EN-US>), a parameter widely variable that requires some extra information
because depends on winding type, kind of conductor, material, EMF, insulation,
etc. (see </span><span lang=EN-GB>Figure 2</span><span lang=EN-US>). </span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=536 height=358 src='resistencia_archivos/image005.png'></span></p>

<p class=MsoCaption><a name='_Toc414019903'></a><a name='_Toc402539911'></a><a
name='_Ref391888116'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>2</span><span lang=EN-GB>. Simplified representation of head coils
for turn and volume/inertia considerations</span></p>

<p class=MsoNormal><span lang=EN-GB>ro_work it’s resistivity</span><span
lang=EN-US> at working temperature and it can be evaluated by means of:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=195 height=17 src='resistencia_archivos/image006.png'></span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=165 height=19 src='resistencia_archivos/image007.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Where </span><span class=CdigoenWordCar><span
lang=EN-GB style='font-size:10.0pt'>c</span></span><span lang=EN-US> its
independent of cooper quality (is an element constant) resistivity at reference
temperature is </span><span lang=EN-GB>given by ro_t_aro of that</span><span
lang=EN-US> specific cooper quality:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=121 height=47 src='resistencia_archivos/image008.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Finally surface for one conductor (cross
section is evaluated as):</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=105 height=39 src='resistencia_archivos/image009.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Note that this is valid for a pre-design
but a final design would require a different process </span><span lang=EN-GB>because
S_cond is determined by standardized gauges</span><span lang=EN-US>.</span></p>

</div>

</body>

</html>
"));
    end Resistance_SMPMSM;

    function Resistance_SYN
      "This function computes R per phase value, Resistance 3"
      import Modelica.Constants;
      import Modelica.SIunits;
      constant Real c=68E-12 "Cu temperature constant";
      constant Real alpha=0.003862 "Cu temperature constant";

      parameter input SYN_Structure.motorSYN motorINPUT;
      output SYN_Structure.motorSYN motorOUTPUT;
    protected
      SIunits.Temperature t_aro "Relative temperature for alpha_ro";
      SIunits.Resistivity ro_t_aro;
      SIunits.Temperature t_work "Cu Working Temperatature";
      SIunits.Resistivity ro_work "Resistivity at Work Temperature";
      SIunits.Resistivity ro_work2
        "Resistivity at Work Temperature Second Method";
      SIunits.Length l_turn "Average Turn Length";

      SIunits.Temperature t_amb "Expected ambient temperature";
      SIunits.Temperature Delta_t_work
        "Expected increase of working temperature above ambient";
      //SIunits.Temperature t_aro_ref "Relative temperature for alpha_ro";
      //SIunits.Resistivity ro_t_aro_ref;
      SIunits.Area S_cond "Area of Conductor";
      Integer Q_s "Number of stator slots";
      Real N_ph "Number of turns per phase";
      Real coilspan "Coil span";
      SIunits.Diameter D "??";
      SIunits.Height h_z "Stator tooth height";
      SIunits.Length l "Stack length";
      SIunits.Length l_extra "Head coil length start value";
      Real Resist[1,4]
        "Output matrix: phase resistance, working temperature, turn length, resistivity";
      SIunits.Resistance R_s "Phase resistance";
    algorithm
      Resist:=zeros(1, 4);
      t_amb:=motorINPUT.casing.dim.phy.t_amb;
      Delta_t_work:=motorINPUT.casing.dim.phy.Delta_t_work;
      t_aro:=motorINPUT.casing.dim.phy.t_aro;
      ro_t_aro:=motorINPUT.stator.win.phy.ro_t_aro;
      S_cond:=motorINPUT.stator.win.S_cond;
      Q_s:=motorINPUT.stator.Q_s;
      N_ph:=motorINPUT.stator.win.N_ph;
      coilspan:=motorINPUT.stator.win.coilspan;
      D:=motorINPUT.stator.dim.D_is;
      h_z:=motorINPUT.stator.dim.h_z;
      l:=motorINPUT.stator.dim.l;
      l_extra:=motorINPUT.stator.win.dim.l_extra;

      Modelica.Utilities.Streams.print("Start: Resistance","");
      Resist:=[0,0,0,0];
      if (t_aro==0 or ro_t_aro==0) then
        t_aro:=20;
        ro_t_aro:=1.68E-8;
      end if;
      t_work:=t_amb + Delta_t_work;
      ro_work:=ro_t_aro + c*(t_work - t_aro);
      ro_work2:=ro_t_aro*(1 + alpha*(t_work - t_aro));
      l_turn:=2*(2*Constants.pi)*coilspan/Q_s*(D/2 + h_z/2) + 2*l + 4*l_extra;

      // Resistance and working temperature outputs
      R_s:=N_ph*l_turn*ro_work/S_cond;                                            // Phase Resistance
      Resist[1,2]:=t_work;                                                        // Cu Working Temperatature
      Resist[1,3]:=l_turn;
      Resist[1,4]:=ro_work;

      // Output assignment
      motorOUTPUT:=motorINPUT;
      motorOUTPUT.stator.phy.R_s:=R_s;
      motorOUTPUT.stator.phy.t_work:=t_work;
      motorOUTPUT.stator.dim.l_turn:=l_turn;
      motorOUTPUT.stator.dim.phy.ro_work:=ro_work;

      Modelica.Utilities.Streams.print("End: Resistance","");
    annotation (
        Documentation(info="<html>

<head>
<title>resistencia() help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>Resistencia2()</span></b><span
lang=EN-GB> function which evaluates resistance given S_cond and other
geometrical parameters, see </span><span lang=EN-GB>Figure 1</span><span
lang=EN-GB>.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=401 height=298 src='resistencia_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019902'></a><a name='_Toc402539910'></a><a
name='_Ref391887975'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Resistance </span><span lang=EN-US>STEP</span><span
lang=EN-GB>s in main code</span></p>

<p class=MsoNormal><span lang=EN-US>There are three reference temperatures to
work with (</span><span lang=EN-GB>Table 1</span><span lang=EN-US>):</span></p>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=111 valign=top style='width:83.4pt;border:solid windowtext 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Variable Name</span></p>
  </td>
  <td width=180 valign=top style='width:134.65pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Description</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=Tabla2><span lang=EN-US>Comment</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_aro</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Reference temperature of relative
  resistivity provided</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>This temperature is required to obtain
  relative resistivity initial value, before raise of working temperature is
  applied.</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>t_amb</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Ambient temperature</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Is the working ambient temperature
  outside the machine.</span></p>
  </td>
 </tr>
 <tr>
  <td width=111 style='width:83.4pt;border:solid windowtext 1.0pt;border-top:
  none;background:#B8CCE4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Delta_t_work</span></p>
  </td>
  <td width=180 style='width:134.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase><span lang=EN-US>Increase over ambient temperature of
  cooper</span></p>
  </td>
  <td width=323 valign=top style='width:242.45pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase style='page-break-after:avoid'><span lang=EN-US>Is
  temperature’s raise allowed to copper due to respect ambient temperature.
  Usually cooper works up to 85C at an ambient temperature of 35C it’s a </span><span
  class=TablaCodigoCar><span lang=EN-US style='font-size:8.0pt'>Delta_t_work</span></span><span
  lang=EN-US> of 50° C. It must be noted that to probe internal temperature
  inside a slot is rather difficult so surface is usually probed and then an
  increase between 5 and 15° C is applied to it.</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019808'></a><a name='_Toc402539874'></a><a
name='_Ref391888065'><span lang=EN-GB>Table </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Temperatures used and its description.</span></p>

<p class=MsoNormal><span lang=EN-US>By definition resistance is:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=203 height=37 src='resistencia_archivos/image002.png'></span></p>

<p class=MsoNormal><span lang=EN-US>In this case:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=177 height=37 src='resistencia_archivos/image003.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>Where N_ph and S_cond are already known
variables, l_turn is medium</span><span lang=EN-GB> </span><span lang=EN-US>turn
length of a coil, and is defined as:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=308 height=48 src='resistencia_archivos/image004.png'></span></p>

<p class=MsoNormal><span lang=EN-US>And it’s intrinsically a geometrical factor
except for head coil extra </span><span lang=EN-GB>length (l_extra</span><span
lang=EN-US>), a parameter widely variable that requires some extra information
because depends on winding type, kind of conductor, material, EMF, insulation,
etc. (see </span><span lang=EN-GB>Figure 2</span><span lang=EN-US>). </span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=536 height=358 src='resistencia_archivos/image005.png'></span></p>

<p class=MsoCaption><a name='_Toc414019903'></a><a name='_Toc402539911'></a><a
name='_Ref391888116'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>2</span><span lang=EN-GB>. Simplified representation of head coils
for turn and volume/inertia considerations</span></p>

<p class=MsoNormal><span lang=EN-GB>ro_work it’s resistivity</span><span
lang=EN-US> at working temperature and it can be evaluated by means of:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=195 height=17 src='resistencia_archivos/image006.png'></span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=165 height=19 src='resistencia_archivos/image007.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Where </span><span class=CdigoenWordCar><span
lang=EN-GB style='font-size:10.0pt'>c</span></span><span lang=EN-US> its
independent of cooper quality (is an element constant) resistivity at reference
temperature is </span><span lang=EN-GB>given by ro_t_aro of that</span><span
lang=EN-US> specific cooper quality:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=121 height=47 src='resistencia_archivos/image008.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Finally surface for one conductor (cross
section is evaluated as):</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=105 height=39 src='resistencia_archivos/image009.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Note that this is valid for a pre-design
but a final design would require a different process </span><span lang=EN-GB>because
S_cond is determined by standardized gauges</span><span lang=EN-US>.</span></p>

</div>

</body>

</html>
"));
    end Resistance_SYN;

    function Resistance_SRM "This function returns resistance per phase of SRM"
      import Modelica.SIunits;
      import Modelica.Constants;

      constant Real c=68E-12 "Cu temperature constant";
      constant Real alpha=0.003862 "Cu temperature constant";
      parameter input SIunits.Length l;
      parameter input SIunits.Length b_sp "Stator pole width";
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC t_amb
        "Ambient temperature";
      parameter input SIunits.Conversions.NonSIunits.Temperature_degC Delta_t_work
        "Temperature increase relative to ambient";
      input SIunits.Conversions.NonSIunits.Temperature_degC t_aro_in
        "Referency resistivity temperature";
      input SIunits.Resistivity ro_t_aro_in "Cu Resistivity";
      parameter input AuxiliaryFunc.packr Good_wiring;
      output res_output Resistance_output;
    protected
      parameter String AWGwiringsFile="AWGwirings.txt"
        "AWG wiring characteristics";
      parameter Real[:,:] AWGwirings=DataFiles.readCSVmatrix(AWGwiringsFile);
      SIunits.Resistance R_sc=99999 "Coil resistance";
      SIunits.Length l_coil=99999 "Coil wire length";
      SIunits.Conversions.NonSIunits.Temperature_degC t_work=99999
        "Cu working temperature";
      SIunits.Resistivity ro_work=99999 "Resistivity at work temperature";
      SIunits.Resistivity ro_work2=99999 "Resistivity at work temperature";
      SIunits.Resistance R_sc_t_aro=99999
        "Coil resistance at ambient temperature";
      Integer Gauge_st;
      Real C_sc "Number of conductors per area";
      SIunits.Length D_cond "Conductor diameter";
      Real K_f "Fill factor";
      Real K_f_real "Real fill factor";
      Real b_sc_real;
      Real h_sc_real "Actual occupied area";
      Real[:,:] centers;
      Real[:,:] Semi_coil_points;
      Real[:,:] Semi_coil_real_points;
      SIunits.Resistivity R_s_xm "Conductor resistivity";
      SIunits.Area A_cond "Conductor's area";
      SIunits.Conversions.NonSIunits.Temperature_degC t_aro=t_aro_in;
      SIunits.Resistivity ro_t_aro=ro_t_aro_in;

    algorithm
      // Good_wiring input data
      C_sc:=Good_wiring.C_sc;
      D_cond:=Good_wiring.D_cond;
      Gauge_st:=Good_wiring.Gauge_st;
      K_f:=Good_wiring.K_f;
      K_f_real:=Good_wiring.K_f_real;
      h_sc_real:=Good_wiring.dim.h_sc_real;
      b_sc_real:=Good_wiring.dim.b_sc_real;
      centers:=Good_wiring.draw.centers;
      Semi_coil_points:=Good_wiring.draw.Semi_coil_points;
      Semi_coil_real_points:=Good_wiring.draw.Semi_coil_real_points;

      //Modelica.Utilities.Streams.print("*** Start: SRM-Resistance ***","");

      R_s_xm:=AWGwirings[Gauge_st, 4]/1000;
      A_cond:=Constants.pi*(AWGwirings[Gauge_st, 2]/2000)^2;

      if ro_t_aro==0 then
        ro_t_aro:=(A_cond*R_s_xm)/1;
        t_aro:=20;
      end if;

      l_coil:=0;

      for i in 1:1:size(centers,1) loop
        l_coil:=l_coil + (D_cond/2 + Constants.pi/4*centers[i, 2] + b_sp/2)*4 + 2*l;
      end for;

      // intermediate variables
      t_work:=t_amb + Delta_t_work;
      ro_work:=ro_t_aro + c*(t_work - t_aro);
      ro_work2:=ro_t_aro*(1 + alpha*(t_work - t_aro));

      R_sc_t_aro:=l_coil*ro_t_aro/A_cond;
      R_sc:=l_coil*ro_work/A_cond;

      // Output assignment
      Resistance_output.R_sc:=R_sc;
      Resistance_output.l_coil:=l_coil;
      Resistance_output.t_work:=t_work;
      Resistance_output.ro_work:=ro_work;
      Resistance_output.R_sc_t_aro:=R_sc_t_aro;
    annotation (
        Documentation(info="<html>

<head>
<title>resistenciaSRM() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>ResistenciaSRM()</span></b><span
lang=EN-GB> </span><span lang=EN-US>Thanks to the methodology to fill winding
area, center position and area of each conductor is known. This allows
bypassing the usual “mean turn length” process in order to calculate the total
length of a winding. The process takes each conductor center position, pole
width, machine’s length and head coil separation and radius to calculate its
length. Coil to phase conversion is performed out of resistance function.</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=343 height=40 src='resistenciaSRM_archivos/image001.png'></span></p>

<p class=MsoNormal><span lang=EN-US>Resistivity correction is performed first
calculating the value based on tabulated area and resistance per km of AWG
tables at 20°C, and afterwards corrected using the working temperature obtained
by the </span><span lang=EN-GB>ambient (</span><span lang=EN-GB>t_amb</span><span
lang=EN-GB>) plus allowed raise variables (</span><span lang=EN-GB>Delta_t_work</span><span
lang=EN-GB>).</span><span lang=EN-GB> </span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end Resistance_SRM;

    record res_output "Record of outputs of ResistanceSRM function"
      import Modelica.SIunits;
      import Modelica.Constants;

      SIunits.Resistance R_sc "Coil resistance";
      SIunits.Length l_coil "Coil wire length";
      SIunits.Conversions.NonSIunits.Temperature_degC t_work
        "Cu working temperature";
      SIunits.Resistivity ro_work "Resistivity at work temperature";
      SIunits.Resistance R_sc_t_aro "Coil resistance at ambient temperature";
    end res_output;

    function Rotation
      "Performs basic algebraic rotation operation in cart coords"
     input Real[:,:] pointsin "Points in";
     input Modelica.SIunits.Angle angle "Rotation angle";
     output Real[:,:] pointsout "Points out";
    protected
      Real[:,:] A;
    algorithm
      A:=Maketurn(angle);
      pointsout :=zeros(size(pointsin,1),size(pointsin,2));

      for i in 1:1:size(pointsin,1) loop
        pointsout[i,:]:=vector(A*matrix(pointsin[i, :]));
      end for;
      pointsout:=Modelica.Math.Matrices.flipUpDown(pointsout);
    annotation (
        Documentation(info="<html>

<head>

<title>rotation() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b>Rotation()</b>  Performs the basic algebraic rotation in
cartesian coordinates</p>

<p class=MsoNormal>Rotation:  pointsout[x,y]=Refine( pointsin[x,y], angle )</p>

<p class=MsoNormal>pointsin[x,y] must be a two column matrix [n,2] with x
coords in first column and y<span style='font-size:12.0pt;line-height:115%'> </span>coords
in y column.</p>

<p class=MsoNormal> angle: must be a real number in RAD.</p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>
"));
    end Rotation;

    function RotorShape
      "Generates a valid rotor geometry for IPM machine, RotorShape17_2"
      import Modelica.Constants;
      import Modelica.SIunits;
      parameter input Integer Shape_in=0
        "Desired IPM rotor shape, 0=>Spoke, 1=>V-shape, 2=>Planar";
      parameter input SIunits.Length D_or_in=0.1865 "Outer rotor diameter";
      parameter input SIunits.Length D_shaft_in=0.1120
        "Shaft internal diameter";
      parameter input Integer p=5 "Number of pole pairs";
      parameter input SIunits.Length h_PM_in=1.0000e-004 "Magnet thickness";
      parameter input SIunits.Conversions.NonSIunits.Angle_deg alpha_v_in=10
        "Angle for the magnets aperture";
      parameter input Real mcf_PM=0.9 "Magnet cover factor";
      parameter input SIunits.Length PMSlotAir_in=0.0012 "Slot air length";
      parameter input SIunits.Length DeltaD_in=1.0000e-003
        "Distance between magnet pod and rotor surface";
      parameter input SIunits.Length D_hub_in=0.12
        "Rotor magnet hub barrier diameter";
      parameter input Integer Embedded=1;

      output IPM_Structure.motorIPM_rotor rotor "Structure with rotor info";
    protected
      Integer shapeflag=999
        "0-->Cross, 1-->V-Shaped, if 2-->Planar, if 3-->AntiV-Shaped";
      Integer failflag=0 "Shape calculation possibility flag";
      String failmessage="No errors to report.";
      Integer failpath
        "Path calculation possibility (using elllipse shapes) flag";
      Real alpha "Magnet cover factor";
      SIunits.Length D_or=D_or_in "Outer rotor diameter";
      SIunits.Length h_PM=h_PM_in "Magnet thickness";
      SIunits.Angle alpha_v;
      SIunits.Conversions.NonSIunits.Angle_deg cv=180/Constants.pi
        "Angle conversion constantAngle for the magnets aperture";
      SIunits.Length PMSlotAir=PMSlotAir_in "Slot air length";
      SIunits.Length DeltaD=DeltaD_in
        "Distance between magnet pod and rotor surface";
      SIunits.Length D_shaft=D_shaft_in "Shaft internal diameter";
      SIunits.Length D_hub=D_hub_in "Rotor magnet hub barrier diameter";
      Integer Shape=Shape_in
        "Desired IPM rotor shape, 0=>Spoke, 1=>V-shape, 2=>Planar";
      SIunits.Angle t_p "Angle correspoding to polar pitch";
      SIunits.Angle alpha_r
        "Angle between polar pitch limit and magnet's length orientation";
      Real Point1[2,1];
      Real Point4lim[2,1];
      SIunits.Angle alpha_cs;
      SIunits.Angle alpha_old;
      SIunits.Angle gir1;
      SIunits.Angle gir2;
      Real[2,1] Point1D;
      Real[2,1] Point2;
      Real[2,1] Point3;
      Real[2,1] Point33;
      Real[2,1] Point4;
      Integer limit "Magnet positioning counter limit";
      Integer ii "Magnet positioning counter";
      SIunits.Angle theta "Angle to position barrier";
      Real[2,1] Delta23;
      Real[2,1] Delta233;
      Real[2,1] Point2L;
      Real[2,1] Point2b;
      Real[2,1] Point3b;
      Real[2,1] Point6Dh;
      Real[2,1] Point22b;
      Real Ratio2_22;
      Real[2,1] Point222b;
      Real[2,1] Point22bD;
      Real[2,1] Point33b;
      Real[2,1] Point33L;
      Real[2,1] Point5BL;
      Real[2,1] Point4B;
      Real[2,1] Point4BL;
      Real[2,1] Point5xy;
      Real[2,1] Point5;
      Real[2,1] unoprima;
      Real[2,1] dosprima;
      Real[2,1] Point132;
      Real[2,1] Point2bD;
      Real[2,1] Point422b;
      Real[2,1] Point53b;
      Real[2,1] Point422bL;
      Real[2,1] Point53bL;
      SIunits.Length l_Tb "Top air-barrier flux path height";
      SIunits.Length h_Tf "Top iron-barrier flux path height";
      SIunits.Length l_Tf "Top iron-barrier flux path length";
      SIunits.Length h_Tb "Top barrier reluctance height";
      SIunits.Length l_Bb;
      SIunits.Length h_Bb;
      SIunits.Length l_Hub=0 "Bottom non-magnetic iron hub reluctance height";
      SIunits.Length h_Hub=0 "Bottom non-magnetic iron hub reluctance length";
      SIunits.Length l_fe_first;
      SIunits.Length l_fe_last;
      Real[2,1] MagnetCDG;
      SIunits.Area MagnetAirArea;
      Real[2,1] BottomAirCDG;
      SIunits.Area BottomAirArea;
      Real[2,1] TopAirCDG;
      SIunits.Area TopAirArea;
      SIunits.Area CartdrigeArea;
      SIunits.Length actualPMSlotAir;
      SIunits.Length l_web "Web length";
      SIunits.Length l_PM_max "Maximum manget allowed length";
      SIunits.Length l_PM "Magnet length";
      SIunits.Length h_fe_first "Top rotor reluctance length";
      Real K_fc "Flux concetration factor";
      SIunits.Area MagnetArea;
      Real[5,2] l_Tfpoints;
      ellipser l_Tfell;
      Real[5,2] l_Tbpoints;
      ellipser l_Tbell;
      Real[5,2] l_Hubpoints;
      ellipser l_Hubell;
      Real hprima2;
      Real[2,1] Point1D_hub;
      Real[5,2] l_Bpoints;
      ellipser l_Bell;
      Real[5,2] magnet_p;
      Real[5,2] magnet_pS;
      Real[5,2] magnet_pN;
      Real[8,2] SlotPM_p;
      Real[3,2] rotor_Hub_p;
      String msg_handle;

    algorithm
      Modelica.Utilities.Streams.print("START: RotorShape","");
      // STEP1: Data preparation

      D_or:=D_or*1000;
      h_PM:=h_PM*1000;
      alpha_v:=alpha_v_in/cv;
      alpha:=Constants.pi/p*mcf_PM;
      PMSlotAir:=PMSlotAir*1000;
      DeltaD:=DeltaD*1000;
      D_shaft:=D_shaft*1000;
      D_hub:=D_hub*1000;

      // STEP 2

      t_p:=Constants.pi/p;
      alpha_r:=(alpha_v - t_p)/2;

    //   if D_shaft==0 then
    //     D_shaft:=integer(D/2 + 3);
    //   end if;
      if h_PM==0 then
        h_PM:=2;
      end if;

      if D_shaft>D_hub then
        Modelica.Utilities.Streams.print("Error: D_shaft>D_hub","");
      end if;

      if D_shaft>D_or then
        Modelica.Utilities.Streams.print("Error: D_shaft>D","");
      end if;

      if D_hub>D_or then
        Modelica.Utilities.Streams.print("Error: D_hub>D","");
      end if;

      // STEP 3

      // Cross_Shape magnet limit angle based on shaft and inner rotor diameters
      // Air Barrier TOP Extreme Point (XX YY coords)
      Point1:=(D_or/2 - DeltaD)*[cos(alpha/2); sin(alpha/2)];
      Point4lim:=[0.9*D_shaft/2; 0];

      //in XX,YY coords
      alpha_cs:=atan(abs((Point1[2, 1] - Point4lim[2, 1])/(Point1[1, 1] - Point4lim[1, 1])));
      alpha_old:=alpha_r;

      // STEP 4: First angle limit check and geometry definition

      while shapeflag>2 loop
        if Shape==0 then  // Spoke
          if alpha_v<=t_p then
            Modelica.Utilities.Streams.print("------ Coherent Initial Conditions: IPM Spoke Geometry considered","");
          else
            Modelica.Utilities.Streams.print("------ NOT Coherent Initial Conditions: IPM Spoke Geometry considered based on desired Shape","");
          end if;
          // Changes in geometry
          alpha:=t_p;  // Alignment with pole limit
          alpha_r:=0;  // Alignment with pole
          alpha_v:=t_p;// Alignment with pole
          h_PM:=h_PM/2;// From magnet symmetry condition
          shapeflag:=0;
        elseif Shape==1 then // V-shape
          if (alpha_v/2<Constants.pi/2 and alpha_v/2>t_p) then
            if alpha_v/2>=alpha_cs then
              Modelica.Utilities.Streams.print("------ Coherent Initial Conditions: IPM Spoke Geometry considered","");
            else
              Modelica.Utilities.Streams.print("------ Coherent Initial Conditions: IPM Spoke Geometry considered. WARNING: V-shape might be to narrow.","");
            end if;
          elseif alpha_v/2<=t_p then
            Modelica.Utilities.Streams.print("------ NOT Coherent Initial Conditions: IPM V-Shape considered. Alpha_v is to low.=> Has been set to 110% pole pitch","");
            alpha_v:=1.1*t_p;
            alpha_r:=(alpha_v - t_p)/2;
          elseif alpha_v/2>=Constants.pi/2 then
            Modelica.Utilities.Streams.print("------ NOT Coherent Initial Conditions: IPM V-Shape considered. Alpha_v was to high.=> Has been set to 170º","");
            alpha_v:=170/180*Constants.pi;
            alpha_r:=(alpha_v - t_p)/2;
          end if;
          shapeflag:=1;
        elseif Shape==2 then // Planar
          if alpha_v/2>=Constants.pi/2 then
            Modelica.Utilities.Streams.print("------ Coherent Initial Conditions: IPM Planar Geometry considered","");
          elseif alpha_v/2<Constants.pi/2 then
            Modelica.Utilities.Streams.print("------ NOT Coherent Initial Conditions: IPM Planar considered. Alpha_v was to low. => Has been set to 180º","");
          end if;
          alpha_r:=Constants.pi/2*(1 - 1/p);
          alpha_v:=Constants.pi;
          alpha_r:=(alpha_v - t_p)/2;
          shapeflag:=2;
        else
          Shape:=2;
        end if;
      end while;

      // Angle between (D) and (E) and its corresponding Turn MATRIX
      gir1:=alpha_r + Constants.pi/p/2 - alpha/2;
      // Angle between (D) and (C) and its corresponding Turn (and reverse turn) MATRIX
      gir2:=alpha/2;

      Point1D:=(D_or/2)*[cos(Constants.pi/2/p); sin(Constants.pi/2/p)];
      Point2:=[0; 0];
      Point3:=[1; 1];
      Point33:=[0; 0];
      Point4:=[0; 0];
      limit:=600;
      ii:=0;
      theta:=0;

      // STEP 5
      while (abs(Point3[2,1])>=0.001 and ii<=limit) loop
        if shapeflag==2 then
          Delta23:=[-PMSlotAir; h_PM];
        elseif shapeflag==0 or shapeflag==1 then
          Delta23:=[-PMSlotAir; h_PM];
        end if;
        Delta233:=[0; h_PM];
        Point2:=[(D_or/2 - DeltaD)*cos(theta); (D_or/2 - DeltaD)*sin(theta)];
        Point3:=Point2 + Maketurn(gir1)*Delta23;
        Point33:=Point2 + Maketurn(gir1)*Delta233;

        // Reduction of barrier space if angle is near IPM limit
        if (Point3[2,1]<=0 and PMSlotAir>0 and shapeflag<>2) then
          PMSlotAir:=PMSlotAir - 0.001;
        else
          ii:=ii + 1;
          theta:=theta - 0.001*Point3[2, 1];
        end if;
        assert(not
                  (ii==limit-1),"******** ERROR: Convergence problem ********",level=AssertionLevel.error);
      end while;

      //Modelica.Utilities.Streams.print("Check2: RotorShape","");
      Point2L:=[0; 0];
      // Moving points from D:x,y to XX,YY coords
      Point2b:=Maketurn(gir2)*Point2;
      Point3b:=Maketurn(gir2)*Point3;
      // Magnetic steel - hub steel cross point
      Point6Dh:=[D_hub/2; 0];
      // 22b outer top magnet point in (XX YY coods)
      Point22b:=Maketurn(gir2)*(Point3 - Maketurn(gir1)*Delta233);
      Ratio2_22:=((D_or - 2*DeltaD)/2)/sqrt(Point22b[1, 1]^2 + Point22b[2, 1]^2);
      // limiter of whats considered Barrier height (XX YY coords)
      Point222b:=[Point22b[1, 1]*Ratio2_22; Point22b[2, 1]*Ratio2_22];
      //Radial Projection of Point22b to D.
      if Point22b[1,1]==0 and Point22b[2,1]==0 then
        Point22bD:=(D_or/2)*[1;0];
      else
        Point22bD:=(D_or/2)*[cos(atan(Point22b[2, 1]/Point22b[1, 1])); sin(atan(Point22b[2, 1]/Point22b[1, 1]))];
      end if;
      // 33b outer bottom magnet point in XX YY coods
      // 33L its corresponding Limit point (related to rotor outer diameter)
      Point33b:=Maketurn(gir2)*Point33;
      Point33L[1,1]:=(Point33b[1, 1]/tan(Constants.pi/(2*p)) + Point33b[2, 1])/(1/tan(Constants.pi/(2*p)) + tan(
        Constants.pi/(2*p)));
      Point33L[2,1]:=tan(Constants.pi/(2*p))*Point33L[1, 1];

      // Magnet Bottom limits definition: Point 3 and 5
      if shapeflag==0 then // Spoke geometry
        for i in D_hub/2:-0.001:D_shaft/2 loop
          Point5BL:=[i; 0];
          Point4B:=[i; -h_PM];
            if abs(Point4B[1,1]-sqrt((D_hub/2)^2-(h_PM)^2))<=0.001 then
              break;
            end if;
        end for;
        Point4B:=Maketurn(Constants.pi/2/p)*Point4B;
        Point5BL:=Maketurn(Constants.pi/2/p)*Point5BL;
        Point4BL:=[Point4B[1, 1]; 0];
        if Embedded==1 then
          // Bottom (inner) magnet points
          Point5xy:=[Point3[1, 1] - cartdistance(Point3b, Point5BL)*mcf_PM; 0];
          Point5:=Maketurn(gir2)*Point5xy;
          Point4:=Maketurn(gir2)*(Point5xy - Delta233);
        else
          Point5:=Point5BL;
          Point4:=Point4B;
        end if;
        // Point 1 is reallocated in the top corner of half a pole
        Point1:=D_or/2*[cos(Constants.pi/2/p); sin(Constants.pi/2/p)];
      elseif shapeflag==1 then // V-shape geometry
        // Point1 is relocated to optimize barrier
        if Point33b[1,1]==0 and Point33b[2,1]==0 then
          Point1:=(D_or/2 - DeltaD)*[1;0];
        else
          Point1:=(D_or/2 - DeltaD)*[cos(atan(Point33b[2, 1]/Point33b[1, 1])); sin(atan(Point33b[2, 1]/
          Point33b[1, 1]))];
        end if;
        // Max magnet is considered as the longest in any form
        // Bottom (inner) magnet points
        Point4[1,1]:=Point2b[1, 1] - Point2b[2, 1]/tan(alpha_r + Constants.pi/p/2);
        Point5:=Point4 + Maketurn(gir2)*Maketurn(gir1)*Delta233;
        Point4B:=Point4;
        Point4BL:=Point4;
        Point5BL:=Point5;
        // Top air-barrier triangle base point
        unoprima:=Maketurn(Constants.pi/2 - alpha_v/2)*Point1;
        dosprima:=Maketurn(Constants.pi/2 - alpha_v/2)*Point2b;
        Point132:=Maketurn(-(Constants.pi/2 - alpha_v/2))*(dosprima + [unoprima[1, 1] - dosprima[1, 1]; 0]);

      elseif shapeflag==2 then // Planar geometry
        // Point1 is relocated to optimize barrier
        if Point2b[2,1]>=Point1[2,1] then
          if Point33b[1,1]==0 and Point33b[2,1]==0 then
            Point1:=(D_or/2 - DeltaD)*[1; 0];
          else
            Point1:=(D_or/2 - DeltaD)*[cos(atan(Point33b[2, 1]/Point33b[1, 1])); sin(atan(Point33b[2, 1]/
            Point33b[1, 1]))];
          end if;
        end if;
        // Top air-barrier triangle base point
        Point132:=[Point1[1, 1]; Point2b[2, 1]];
        // Max magnet is considered as the longest in any form
        // Bottom (inner) magnet points
        Point4[1,1]:=Point2b[1, 1] - Point2b[2, 1]/tan(alpha_r + Constants.pi/p/2);
        Point5:=Point4 + Maketurn(gir2)*Maketurn(gir1)*Delta233;
        Point4B:=Point4;
        Point4BL:=Point4;
        Point5BL:=Point5;
      end if;

      // Parallel to magnet limit up to Dor/2
      Point2bD:=[Point2b[1, 1] + DeltaD*cos(atan((Point2b[2, 1] - Point4[2, 1])/(Point2b[1, 1] - Point4[1,
        1]))); Point2b[2, 1] + DeltaD*sin(atan((Point2b[2, 1] - Point4[2, 1])/(Point2b[1, 1] - Point4[1, 1])))];

      // Cartridge viability check: 4 checks
      // check 1
      if (cartdistance(Point33b,Point33L)/2<=DeltaD and shapeflag>0) then
        failflag:=11;
        failmessage:="******** WARNING: Web is smaller than Wedge (decrease mcf_PM or PMSlotAir) ********";
        Modelica.Utilities.Streams.print(failmessage,"");
      end if;
      if Point33[2,1]>tan(Constants.pi/2/p-alpha/2)*Point33[1,1] and shapeflag>0 then
        failflag:=1;
        failmessage:="******** ALLOCATION ERROR: Magnet pod exceeds the allocable space (reduce h_PM or angle) ********";
        Modelica.Utilities.Streams.print(failmessage,"");
      end if;

      // check 2
      if sqrt(Point5[1,1]^2+Point5[2,1]^2)<=D_shaft/2 and shapeflag>=1 then
        failflag:=2;
        failmessage:="******** ALLOCATION ERROR: Magnet May penetrate the shaft area ********";
        Modelica.Utilities.Streams.print(failmessage,"");
      end if;

      // check 3
      if shapeflag==0 and Point4B[2,1]<=0 then
        failflag:=3;
        failmessage:="******** ALLOCATION ERROR: SPOKE: Magnet or Magnet Pod is wider (h_PM) than allocable space ********";
        Modelica.Utilities.Streams.print(failmessage,"");
      end if;

      // check 4
      if shapeflag==1 and Point22b[2,1]<=0 then
        failflag:=31;
        failmessage:="******** ALLOCATION ERROR: V-SHAPE Magnet or Magnet Pod is crossing the semipole area: (h_PM > l_PM). ********";
        Modelica.Utilities.Streams.print(failmessage,"");
      end if;

      // Middle magnet points (at magnet faces)
      Point422b:=Point4 + Maketurn(gir1 + gir2)*[cartdistance(Point22b, Point4)/2; 0];
      Point53b:=Point5 + Maketurn(gir1 + gir2)*[cartdistance(Point22b, Point4)/2; 0];

      // Middle iron pole limit points (at semipole limits)
      if shapeflag==0 then // for spoke
        Point422bL:=Maketurn(t_p/4/p)*[D_or/2; 0];
        Point53bL:=Point53b;
      else // for V-shape and planar
        Point422bL:=[D_or/2*sin(acos(Point422b[2, 1]*2/D_or)); Point422b[2, 1]];
        Point53bL:=Maketurn(t_p/2)*[(D_or - DeltaD - D_shaft)/4 + D_shaft/2; 0];
      end if;
      //Modelica.Utilities.Streams.print("Check3: RotorShape","");
      l_Tb:=0;
      h_Tf:=0;
      l_Tf:=0;
      h_Tb:=0;
      l_Bb:=0;
      h_Bb:=0;
      l_Hub:=0;
      h_Hub:=0;
      l_fe_first:=0;
      l_fe_last:=0;
      MagnetCDG:=[0; 0];
      MagnetAirArea:=0;
      BottomAirCDG:=[0; 0];
      BottomAirArea:=0;
      TopAirCDG:=[0; 0];
      TopAirArea:=0;
      CartdrigeArea:=0;
      actualPMSlotAir:=PMSlotAir;

      // STEP 9: Shared geometry amon all shapes

      // Web length for planar and v-shape
      l_web:=sqrt((Point33L[1, 1] - Point33b[1, 1])^2 + (Point33L[2, 1] - Point33b[2, 1])^2);
      l_PM_max:=cartdistance(Point2b, Point4B);
      l_PM:=cartdistance(Point22b, Point4);
      // Top rotor reluctance length
      h_fe_first:=l_PM*sin(alpha_v/2);
      if Point2b[1,1]==0 and Point2b[2,1]==0 then
        K_fc:=1e16;
      else
        K_fc:=l_PM/(cartdistance(Point2b,[0;0])*atan(Point2b[2, 1]/Point2b[1, 1]));
      end if;

      // STEP 10: Individual geometry settings

      if shapeflag==1 or shapeflag==2 then // v-shape and planar
        TopAirCDG:=Point22b + Maketurn(alpha_v/2)*[cartdistance(Point22b, Point2b)/2 + cartdistance(
          Point132, Point1)/3; h_PM/2];
          TopAirArea:=cartdistance(Point22b, Point2b)*h_PM + cartdistance(Point132, Point1)*h_PM/2;

          // Magnet CDG
          if shapeflag==2 then
            MagnetCDG:=[Point4[1, 1] - h_PM/2; 0];
          else
            MagnetCDG:=Point4 + Maketurn(alpha_v/2)*[l_PM/2; +h_PM/2];
          end if;
          MagnetArea:=h_PM*l_PM;
          // For Rotor Voltages (following N-S lines)
          l_fe_first:=sqrt((Point422bL[1, 1] - Point422b[1, 1])^2 + (Point422bL[2, 1] - Point422b[2, 1])^2);
          l_fe_first:=cartdistance(Point422bL, Point422b);
          l_fe_last:=sqrt((Point53bL[1, 1] - Point53b[1, 1])^2 + (Point53bL[2, 1] - Point53b[2, 1])^2);
          l_fe_last:=cartdistance(Point53bL, Point53b);

          // TOP leakage data - iron "saturated-barrier"
          l_Tfpoints:=transpose(
          [Point22b,
          Point2b,
          Point2b + (Point3b - Point22b)/2 + Maketurn(alpha_v/2)*[PMSlotAir/3 + DeltaD + cartdistance(
            Point1, Point132)/2; 0],
          Point33b,
          Point3b]);
        l_Tfell:=ellipse(l_Tfpoints);                // structure with ellipse info for Top Iron
        if l_Tfell.degenerated>0 then
          failpath:=failpath + 1;
        end if;
        l_Tf:=l_Tfell.length - h_PM;
        h_Tf:=DeltaD*0.8;
        // TOP leakage data - air barrier
        l_Tbpoints:=transpose([Point22b - 0.025*(Point2b - Point4B),Point22b + (Point2b - Point22b)/4,
          TopAirCDG,Point3b - 0.025*(Point2b - Point4B),Point3b + (Point2b - Point22b)/4]);
        l_Tbell:=ellipse(l_Tbpoints); // structure with ellipse info for Top Air-barrier
        if l_Tbell.degenerated>0 then
        end if;
        l_Tb:=l_Tbell.length - h_PM;
        h_Tb:=cartdistance(Point22bD, Point2b) - DeltaD;
        h_Hub:=0;
        l_Hub:=0;

      elseif shapeflag==0 then // spoke
        // For rotor voltages (following N-S lines)
        l_fe_first:=sqrt((Point422bL[1, 1] - Point422b[1, 1])^2 + (Point422bL[2, 1] - Point422b[2, 1])^2);
        l_fe_last:=sqrt((Point53bL[1, 1] - Point53b[1, 1])^2 + (Point53bL[2, 1] - Point53b[2, 1])^2);
        // TOP leakage data - iron "saturated barrier"
        l_Tfpoints:=transpose([Point22b - 0.4*(Point22b - Point4)/2,Point22b + 0.8*(Point2bD - Point22b)/2,
          Point1D - DeltaD/2*[cos(Constants.pi/2/p); sin(Constants.pi/2/p)],Point22b + 2*(Point3b - Point22b) + 0.8*(Point2bD -
          Point22b)/2,Point22b + 2*(Point3b - Point22b) - 0.4*(Point22b - Point4)/2]);

        l_Tfell:=ellipse(l_Tfpoints);
        if l_Tfell.degenerated>0 then
          failpath:=failpath + 100;
        end if;
        l_Tf:=l_Tfell.length/2;
        h_Tf:=DeltaD;
        // Hub barrier points condition
        h_Hub:=cartdistance(Point4BL, D_hub/2*transpose([cos(Constants.pi/2/p),sin(Constants.pi/2/p)]));
        l_Hubpoints:=transpose([Point4 - 0.8*(Point4 - Point4B),Point4 + (Point22b - Point4)/5,Point5BL -
          (Point5BL - D_shaft/2*[cos(Constants.pi/2/p); sin(Constants.pi/2/p)])/2,Point5 + (Point22b - Point4)/5 + (Point5BL -
          Point4B),Point5 - 0.8*(Point4 - Point4B) + (Point5BL - Point4B)]);
        l_Hubell:=ellipse(l_Hubpoints);
        if l_Hubell.degenerated>0 then
          failpath:=failpath + 1000;
        end if;
        l_Hub:=l_Hubell.length/2;
        // Geo spoke - centers of gravity for magnet and air barriers
        hprima2:=D_or - sqrt(D_or^2 - h_PM^2);
        Point2L:=Maketurn(Constants.pi/2/p)*[D_or/2 - hprima2; 0];
        Point1D_hub:=D_hub/2*[cos(Constants.pi/2/p); sin(Constants.pi/2/p)];

        MagnetCDG:=Point53bL;
        MagnetArea:=cartdistance(Point22b, Point4)*h_PM;
        BottomAirCDG:=Point5BL + (Maketurn(gir1 + gir2)*[D_hub/2; 0] - Point5BL)/2;
        BottomAirArea:=(D_hub/2 - cartdistance(Point5BL,[0;0]))*h_PM/2;

        // Geo spoke - embedded
        if Embedded==1 then   // one plate
          TopAirCDG:=Maketurn(gir1 + gir2)*[cartdistance(Point5BL,[0;0]) + cartdistance(Point5BL, Point33b)/2
             + cartdistance(Point33b, Point3b)/4 - cartdistance(Point5BL, Point1D_hub)/4; 0];
          TopAirArea:=cartdistance(Point4B, Point2b)*h_PM + cartdistance(Point33b, Point1)*h_PM/2 -
            BottomAirArea;
          CartdrigeArea:=cartdistance(Point2b, Point4)*h_PM + hprima2*h_PM/2 - BottomAirArea + TopAirArea;
          // Bottom paths
          // Air barrier points condition
          h_Bb:=cartdistance(Point4, Point4B);
          l_Bpoints:=transpose([Point4 - (Point4 - Point4B)/4,Point4 + 2*(Point4 - Point4B)/4,Point5 - (
            Point4 - Point4B) + (Point5BL - Point4B)/2,Point5 + 2*(Point4 - Point4B)/4 + (Point5BL -
            Point4B),Point5 - (Point4 - Point4B)/4 + (Point5BL - Point4B)]);
          l_Bell:=ellipse(l_Bpoints);
          if l_Bell.degenerated>0 then
            failpath:=failpath + 10000;
          end if;
          l_Bb:=l_Bell.length - h_PM;
          // Air barrier
          h_Tb:=cartdistance(Point22b, Point2b);
          l_Tbpoints:=transpose([Point22b - (Point2b - Point22b)/3,Point22b + (Point2b - Point22b)/3,
            Point3b + 3/2*(Point33b - Point3b)/2,Point22b + 2*(Point3b - Point22b) + (Point2b - Point22b)/
            3,Point22b + 2*(Point3b - Point22b) - (Point2b - Point22b)/3]);
          l_Tbell:=ellipse(l_Tbpoints);
          if l_Tbell.degenerated>0 then
            failpath:=failpath + 100000;
          end if;
          l_Tb:=l_Tbell.length - h_PM;
        end if;

        // Geo spoke - not embedded
        if Embedded==0 then // several plates
          TopAirCDG:=Maketurn(gir1 + gir2)*[cartdistance(Point5BL, Point2L)/2 + cartdistance(Point5BL,[0;0]) +
            cartdistance(Point1D, Point33b)/4 - cartdistance(Point1D_hub, Point5BL)/4;0];
          TopAirArea:=cartdistance(Point4B, Point2bD)*h_PM + hprima2*h_PM/2 - BottomAirArea;
          h_Tf:=cartdistance(Point22b, Point2b);
          CartdrigeArea:=cartdistance(Point2b, Point4)*h_PM + hprima2*h_PM/2 - BottomAirArea + TopAirArea;
          h_Tb:=0;
          l_Tb:=0;

          h_Bb:=0;
          l_Bb:=0;
        end if;
      end if;

      // Data structure
      rotor.shape:=shapeflag;
      rotor.fail:=failflag;
      rotor.failpath:=failpath;
      rotor.K_fc:=K_fc;

      // Messages
      rotor.msg.failmessage:=failmessage;
      rotor.dim.l_PM_max:=l_PM_max/1000;
      rotor.dim.h_PM:=h_PM/1000;
      rotor.dim.l_PM:=l_PM/1000;
      rotor.dim.DeltaD:=DeltaD/1000;
      rotor.dim.actualPMSlotAir:=actualPMSlotAir/1000;
      rotor.dim.h_Tb:=h_Tb/1000;
      rotor.dim.l_Tb:=l_Tb/1000;
      rotor.dim.h_Tf:=h_Tf/1000;
      rotor.dim.l_Tf:=l_Tf/1000;
      rotor.dim.h_Bb:=h_Bb/1000;
      rotor.dim.l_Bb:=l_Bb/1000;
      rotor.dim.h_Hub:=h_Hub/1000;
      rotor.dim.l_Hub:=l_Hub/1000;
      rotor.dim.l_fe_first:=l_fe_first/1000;
      rotor.dim.h_fe_first:=h_fe_first/1000;
      rotor.dim.l_fe_last:=l_fe_last/1000;
      rotor.dim.l_web:=l_web*2/1000;

      // Positioning data
      rotor.pos.TopAirCDG:=TopAirCDG/1000;
      rotor.pos.TopAirArea:=TopAirArea/1E6;
      rotor.pos.MagnetCDG:=MagnetCDG/1000;
      rotor.pos.MagnetArea:=MagnetArea/1E6;
      rotor.pos.BottomAirCDG:=BottomAirCDG/1000;
      rotor.pos.BottomAirArea:=BottomAirArea/1E6;
      rotor.pos.CartdrigeArea:=CartdrigeArea/1E6;

      // dim2point/draw
      // Full magnet points
      magnet_p[1,:]:=(transpose(Point22b))[1,1:2];
      magnet_p[2,:]:=(transpose(Point4))[1,1:2];
      magnet_p[3,:]:=(transpose(Point5))[1,1:2];
      magnet_p[4,:]:=(transpose(Point3b))[1,1:2];
      magnet_p[5,:]:=(transpose(Point22b))[1,1:2];

      // First pole will always have notrh looking toward TOP
      magnet_pS[1,:]:=(transpose(Point22b + (Point3b - Point22b)/2))[1,1:2];
      magnet_pS[2,:]:=(transpose(Point4 + (Point3b - Point22b)/2))[1,1:2];
      magnet_pS[3,:]:=(transpose(Point5))[1,1:2];
      magnet_pS[4,:]:=(transpose(Point3b))[1,1:2];
      magnet_pS[5,:]:=(transpose(Point22b + (Point3b - Point22b)/2))[1,1:2];

      // North half magnet points
      magnet_pN[1,:]:=(transpose(Point22b))[1,1:2];
      magnet_pN[2,:]:=(transpose(Point4))[1,1:2];
      magnet_pN[3,:]:=(transpose(Point5 - (Point3b - Point22b)/2))[1,1:2];
      magnet_pN[4,:]:=(transpose(Point3b - (Point3b - Point22b)/2))[1,1:2];
      magnet_pN[5,:]:=(transpose(Point22b))[1,1:2];

      if shapeflag==0 then
        if Embedded==1 then
          SlotPM_p[1,:]:=(transpose(Point33b))[1,1:2];
          SlotPM_p[2,:]:=(transpose(Point2b))[1,1:2];
          SlotPM_p[3,:]:=(transpose(Point4B))[1,1:2];
          SlotPM_p[4,:]:=(transpose(Point5BL))[1,1:2];
          SlotPM_p[5,:]:=(transpose(Point33b))[1,1:2];
        elseif Embedded==0 then
          SlotPM_p[1,:]:=(transpose(Point1D))[1,1:2];
          SlotPM_p[2,:]:=(transpose(Point2bD))[1,1:2];
          SlotPM_p[3,:]:=(transpose(Point4))[1,1:2];
          SlotPM_p[4,:]:=(transpose(Point5BL))[1,1:2];
          SlotPM_p[5,:]:=(transpose(Point1D))[1,1:2];
        end if;
        rotor_Hub_p[1,:]:=(transpose(Point5BL))[1,1:2];
        rotor_Hub_p[2,:]:=(transpose(Point4B))[1,1:2];
        rotor_Hub_p[3,:]:=(transpose(Point6Dh))[1,1:2];
        rotor.draw.rotor_Hub_p:=rotor_Hub_p;
      elseif shapeflag==1 or shapeflag==2 then
        SlotPM_p[1,:]:=(transpose(Point1))[1, 1:2];
        SlotPM_p[2,:]:=(transpose(Point2b))[1, 1:2];
        SlotPM_p[3,:]:=(transpose(Point22b))[1, 1:2];
        SlotPM_p[4,:]:=(transpose(Point4BL))[1, 1:2];
        SlotPM_p[5,:]:=(transpose(Point5BL))[1, 1:2];
        SlotPM_p[6,:]:=(transpose(Point3b))[1, 1:2];
        SlotPM_p[7,:]:=(transpose(Point33b))[1, 1:2];
        SlotPM_p[8,:]:=(transpose(Point1))[1, 1:2];

        rotor_Hub_p[:,:]:=zeros(3,2);
        rotor.draw.rotor_Hub_p:=rotor_Hub_p;
      end if;

      // Failflag styles (1=default, 2=warning, 4=error)
      rotor.draw.magnetSlotPM_style:=1;
      rotor.draw.magneticSteel_style:=1;
      rotor.draw.magnet_style:=1;
      rotor.draw.rotorHub_style:=1;
      rotor.draw.shaft_style:=1;

      rotor.draw.fail_elements:=99999;

      if failflag==1 then  // pod touches pole limit area
        rotor.draw.magnetSlotPM_style:=4;
        rotor.draw.magnet_style:=4;
        rotor.draw.magneticSteel_style:=4;
        rotor.draw.fail.fail_element1:=[SlotPM_p[6:8, :]; SlotPM_p[2:3, :]];
        rotor.draw.fail.fail_element2:=[transpose(Point1D); [0;0]];
        rotor.draw.fail_elements:=2;
      elseif failflag==11 then  // similar to 1: l_web<DeltaD
        rotor.draw.magnetSlotPM_style:=4;
        rotor.draw.magnet_style:=4;
        rotor.draw.magneticSteel_style:=4;
        rotor.draw.fail.fail_element1:=SlotPM_p[7:8, :];
        rotor.draw.fail.fail_element2:=[transpose(Point1D); [0;0]];
        rotor.draw.fail_elements:=2;
      elseif failflag==2 then   // V-shape: pod gets into shaft area
        rotor.draw.magnetSlotPM_style:=4;
        rotor.draw.rotorSteel_style:=4;
        rotor.draw.shaft_style:=4;
        rotor.draw.magnet_style:=4;
        rotor.draw.fail.fail_element1:=SlotPM_p[3:6, :];
        rotor.draw.fail.fail_element2:=[1,0];
        rotor.draw.fail_elements:=2;
      elseif failflag==3 then  // Spoke: h_PM too high
        rotor.draw.magnetSlotPM_style:=4;
        rotor.draw.magnet_style:=4;
        rotor.draw.magneticSteel_style:=4;
        rotor.draw.fail.fail_element1:=SlotPM_p[2:4, :];
        rotor.draw.fail.fail_element2:=[[D_or/2,0]; [0; 0]];
        rotor.draw.fail_elements:=2;
      elseif failflag==31 then  // V-shape: h_PM > l_PM
        rotor.draw.magnetSlotPM_style:=4;
        rotor.draw.magnet_style:=4;
        rotor.draw.magneticSteel_style:=4;
        rotor.draw.fail.fail_element1:=[magnet_p[4, :]; magnet_p[1:2, :]];
        rotor.draw.fail.fail_element2:=[[D_or/2,0]; [0; 0]];
        rotor.draw.fail_elements:=2;
      else  // No fail: all default styles (1)
        rotor.draw.magnetSlotPM_style:=1;
        rotor.draw.magnet_style:=1;
        rotor.draw.rotorHub_style:=1;
        rotor.draw.rotorSteel_style:=1;
      end if;

      rotor.draw.magnetSlotPM_p:=SlotPM_p;
      rotor.draw.magnet_p:=magnet_p;
      rotor.draw.magnet_pN:=magnet_pN;
      rotor.draw.magnet_pS:=magnet_pS;
      Modelica.Utilities.Streams.print("End: RotorShape","");
    annotation (
        Documentation(info="<html>

<head>
<title>rotorshape() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><span lang=EN-GB>Rotorshape() geometry calculation
comprehends three kinds of machines at the same time. This allows switching
between them seamlessly and also gives the opportunity to the designer to
compare between designs of different kinds of geometry.</span></p>

<p class=MsoNormal><span lang=EN-GB>The basic designs are the 3 presented in </span><span lang=EN-GB>Figure 1</span><span lang=EN-GB>. The main variable to determine the
shape is called Shape. Sizing code will try to fulfil that shape but, in order
to do so, alpha_v </span><span lang=EN-GB>(which defines the V of a v-shaped
rotor)</span><span class=CdigoenWordCar><span lang=EN-GB style='font-size:10.0pt'>
</span></span><span lang=EN-GB>must be coherent with shape:</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>For angles =&lt; of the pole pitch rotors are
considered spoke. Shape==0</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-GB>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>For angles between pole pitch and 180º, rotors
are considered vshape. Shape==1</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-GB>-<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-GB>For angles =&gt;180º, rotors are considered
planar. Shape==2</span></p>

<p class=MsoNormal><span lang=EN-GB>A priori, pole pitch is unknown (because D
is unknown) so a desired alpha_v might lead to different shapes (spoke or
v-shaped). That is why variable Shape</span><span lang=EN-GB> exisits, program
will return the best result attainable with </span><span lang=EN-GB>alpha_v</span><span
lang=EN-GB> or forcing its value depending on situation. Situations considered
are: First, Geometry is ok, </span><span lang=EN-GB>Shape </span><span
lang=EN-GB>and </span><span lang=EN-GB>alpha_v</span><span lang=EN-GB> are
coherent. Second, both variables are not coherent, error handling feature will
warn the designer. Third (topmost part of </span><span
lang=EN-GB>Figure 1</span><span lang=EN-GB>)</span><span lang=EN-GB>, program
takes actions to correct </span><span lang=EN-GB>alpha_v</span><span
lang=EN-GB>. This third case automatically occurs only when </span><span
lang=EN-GB>alpha_v</span><span lang=EN-GB> is between 1 and 1.05 polar pitch
(i.e. it’s almost spoke). In that case, program forces it to be at least 1.05
that angle if v-shape was selected, although could make magnets end up inside
rotor’s area.  Or less than pole pitch if spoke was selected.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=604 height=457 src='rotorshape_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019892'></a><a name='_Toc402539901'></a><a
name='_Ref391887695'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Rotor configurations given </span><span
lang=EN-GB style='font-family:Consolas'>alpha_r</span><span lang=EN-GB> or </span><span
lang=EN-GB style='font-family:Consolas'>alpha_v</span><span lang=EN-GB> angles
(equivalent)</span></p>

<p class=MsoNormal><a name='_Ref391887633'><span lang=EN-GB>A priori it is
unknown if the given inputs can generate a valid geometry therefore, between several
validity checks are performed. These validity checks may change a variable
output flag (failflag) to warn external code about those issues and let the designer
act accordingly (see </span></a><span lang=EN-GB>Table 1</span><span
lang=EN-GB>).</span></p>

<div align=center>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:26.7pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=104 valign=top style='width:77.95pt;border:solid windowtext 1.0pt;
  background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Failflag value</span></b></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border:solid windowtext 1.0pt;
  border-left:none;background:#BFBFBF;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaBase align=center style='text-align:center'><b><span
  lang=EN-US style='font-family:'Times New Roman','serif''>Meaning</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>0</span></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Everything OK</span></p>
  </td>
 </tr>
 <tr>
  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>1</span></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Magnet pod exceeds allocable space
  (reduce h_PM or angle)</span></p>
  </td>
 </tr>
 <tr>
  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>11</span></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Web is smaller than Wedge (decrease
  mcf_PM or PMSlotAir)</span></p>
  </td>
 </tr>
 <tr>
  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>2</span></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>Magnet May penetrate shaft area</span></p>
  </td>
 </tr>
 <tr>
  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>3</span></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>SPOKE: Magnet or Magnet Pod is wider
  (h_PM) than allocable space</span></p>
  </td>
 </tr>
 <tr>
  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>31</span></p>
  </td>
  <td width=463 valign=top style='width:347.3pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=TablaCodigo><span lang=EN-US>V-SHAPE Magnet or Magnet Pod is
  crossing semipole area: (h_PM &gt; l_PM)</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoCaption><a name='_Toc414019801'></a><a name='_Toc402539865'></a><a
name='_Ref391887574'><span lang=EN-GB>Table </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. </span><span class=CdigoenWordCar><span
lang=EN-US>Failflag</span></span><span lang=EN-GB>’s</span><span lang=EN-GB>
possible values and meanings</span></p>

<p class=MsoNormal><span lang=EN-GB>General geometry nomenclature of this
function is shown in </span><span lang=EN-GB>Figure 2</span><span lang=EN-GB>.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=603 height=819 src='rotorshape_archivos/image002.png'></span></p>

<p class=MsoCaption><a name='_Toc414019893'></a><a name='_Toc402539902'></a><a
name='_Ref392234280'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>2</span><span lang=EN-GB>. Coordinates and Point location used by
Rotor Geometry function.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>
"));
    end RotorShape;

    function round "Rounds to closest integer"
     input Real a;
     output Integer b;

    algorithm
      if a-integer(a)<0.5 then
        b:=integer(a);
      else b:=integer(a) + 1;
      end if;
    end round;

    function round_Nph
      "Rounds N_ph taking into account the x*p*q=N_ph condition"
      input Real N_ph "Number of turns per phase";
      input Integer p "Pole pairs";
      input Real q "Slots per pole per phase";
      output Real N_ph_round "Rounded number of turns per phase";
    protected
      Real x "Integer value";
      Real x_min "Min. integer value";
      Real x_max "Max. integer value";
      Real err_min "Min. integer value relative error";
      Real err_max "Max. integer value relative error";
    algorithm
      x:=N_ph/(p*q);
      x_min:=floor(x);
      x_max:=x_min + 1;
      err_min:=abs((x - x_min)/x);
      err_max:=abs((x - x_max)/x);

      if err_min<err_max then
        N_ph_round:=x_min*p*q;
      elseif err_max<=err_min then
        N_ph_round:=x_max*p*q;
      end if;
    annotation (
        Documentation(info="<html>

<head>

<title>round_nph() help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b>round_nph()</b> N_ph taking into account the x·p·q=N_ph
condition.</p>

<p class=MsoNormal>x={1,2,3...} p=pole pairs q=slots per phase per pole.</p>

<p class=MsoNormal>N_ph_round=(N_ph, p, q, debug_mode)</p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>

"));
    end round_Nph;

    function TorquevsSpeed
      "This function returns motor torque-speed efficiency as a matrix TorquevsSpeed 7.2"
      import Modelica.Constants;
      import Modelica.SIunits;
      import Sizing;
      parameter input IPM_Structure.motorIPM motorINPUT;
      output IPM_Structure.motorIPM motorOUTPUT;
    protected
      parameter SIunits.Conversions.NonSIunits.AngularVelocity_rpm k=1;
      Integer p;
      Integer m;
      SIunits.MagneticFluxDensity B_max;
      SIunits.MagneticFlux Flux_PM;
      SIunits.Resistance R_s;
      SIunits.Inductance L_d;
      SIunits.Inductance L_q;
      SIunits.Current I_max;
      SIunits.Voltage U_smax;
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm w_rated;
      Real K_h;
      Real K_e;
      Real n_i;
      SIunits.Volume Vol;
      Integer R_fe_consider;
      Integer precision "Number of points in curves intersection";
      Real I_precision "Points per ampere in LUTS";
      SIunits.AngularVelocity delta_w_base;
      Real prec1;
      Real prec2;
      Real prec3;
      SIunits.AngularVelocity w_initiale "Seed value for speed ranging";
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm w_maxe
        "Maximum value for speed ranging";
      SIunits.Conversions.NonSIunits.AngularVelocity_rpm[:] W
        "Range of mecanical speeds for power losses curve";
      SIunits.Velocity[:] W_elec
        "Range of electrica speeds for power losses curve";
      SIunits.Frequency[:] f_elec "Range of requencies for power losses curve";
      Real[:,:] Pfef "LUT of iron losses";
      Real deltaw_mult_factor_small;
      Real deltaw_mult_factor_big;
      Real Thresshold "Maximum angle between intensities in intersection";
      Integer rfecounter_limit;
      SIunits.AngularVelocity[1500] delta_w
        "Increment of electrical speed for the actual step";
      SIunits.Current I_initial "Initial intensity value";
      SIunits.Current Intensity "Current intensity value";
      Real I_while_watchdog "Intensity watchdog";
      Real I_while_watchdog_limit "Intensity watchdog limit";
      Integer Int_counter;
      SIunits.Current R_circle "Circle radius";
      SIunits.Current xInitial_limit "X-cursor initial limit";
      SIunits.AngularVelocity w_elec "Current step electrical speed";
      Integer w_while_watchdog "Speed watchdog";
      Integer w_while_watchdog_limit "Speed watchdog limit";
      Integer LUT_row "Output cursor";
      Integer Power_barrage "Negative torque flag";
      SIunits.Resistance[1500] R_fe "Resistance for iron losses resistance";
      SIunits.Resistance Rfe_new
        "Reference iteration value for iron losses resistance";
      Real A,B,C,D,E,F;
      Real a,b,c,d,e,f;
      ellipser ellipse;
      SIunits.Current R_xx "Ellipse long axis";
      SIunits.Current R_yy "Ellipse short axis";
      SIunits.Current X_cc "Ellipse D-axis position";
      SIunits.Current Y_cc "Ellipse angle";
      SIunits.Current alpha "Ellipse angle";
      Real a2,b2,c2;
      Real y_1,y_2,yy;
      SIunits.Current I_d2 "Total d-axis current";
      SIunits.Current I_q2 "Total q-axis current";
      SIunits.Current I_od "Torque generating d-axis current";
      SIunits.Current I_oq "Torque generating q-axis current";
      SIunits.Resistance Rfe_rel_err
        "Relative error between interation of Iron Losses Resistance";
      SIunits.Current[2,1500] Int_vector "Stored total currents vector";
      parameter Integer I_size=integer(motorINPUT.stator.I_s)+1;
      Real[2000,8,I_size] TSI_lut;
      Real[2000,10,I_size] TSI_lut2;
      String msg_handle;
      Real[2000,8] TSI_lutT;
      Integer TSI_lutTcorrect
        "Torque vs Speed vs Efficiency temporal raw correct value counter";
      Real[2000,10] TSI_lut_Tcorrect
        "Torque vs Speed vs Efficiency temporal raw table";
      Real[2000,10] TSI_last
        "Torque vs Speed vs Efficiency temporal final part table";
      Integer i "Max torque per ampere index";
      SIunits.Voltage TemporalMaxVoltage "max V to values of max speed";
      Real[2000,10] TSI_Ini;
      SIunits.Current I_d3 "MTPA d-axis total current";
      SIunits.Current I_q3 "MTPA q-axis total current";
      SIunits.Resistance R_few_calc
        "Reevaluated resistance for iron losses resistance";
      Real w_elec2;
      SIunits.Resistance Rfe_neww
        "Reevaluated iterative resistance for iron losses resistance";
      SIunits.Current I_oq3 "Torque generating d-axis current";
      SIunits.Current I_od3 "Torque generating q-axis current";
      SIunits.Voltage U_d "Total d-axis voltage";
      SIunits.Voltage U_q "Total q-axis voltage";
      SIunits.Voltage U_3 "Total voltage";
      Integer MTPA_eff_position "Position of MTPA point efficiency in TSI_lut2";
      Real[ 200,4] MTPA_lut;
      Real percentage_done "Percentage done indicator";
      Real N_ph "Number of turns per phase integer";
      Real K_w "Winding factor";
      SIunits.MagneticFlux Link_flux "Linkage flux";
      SIunits.Current id_rated "Maximum i_d current for MTPA control";
      SIunits.Current iq_rated "Maximum i_q current for MTPA control";
      Real Pfe_seed "LUT of iron losses [rad/s w]";
      Real Pfe "LUT of iron losses [rad/s w]";
      Real[:] W_elec_I;
      Real[:] i_od;
      Real[:] i_oq;
      Real[:] T_I;
      Real[:] u_od;
      Real[:] u_oq;
      Real[:] Pfef_I "LUT of iron losses [rad/s W]";
      SIunits.Current[:] i_cd;
      SIunits.Current[:] i_cq;
      SIunits.Current[:] i_d;
      SIunits.Current[:] i_q;
      SIunits.Voltage[:] u_d;
      SIunits.Voltage[:] u_q;
      SIunits.Voltage[:] U;
      SIunits.Voltage[:] I;

    algorithm
      // Machine data
      p:=motorINPUT.p;
      m:=motorINPUT.m;
      B_max:=motorINPUT.airgap.phy.B_g;
      Flux_PM:=motorINPUT.airgap.phy.flux_g;
      N_ph:=motorINPUT.stator.win.N_ph;
      K_w:=motorINPUT.stator.win.K_w;
      R_s:=motorINPUT.stator.phy.R_s;
      L_d:=motorINPUT.stator.phy.L_dm;
      L_q:=motorINPUT.stator.phy.L_qm;
      I_max:=motorINPUT.stator.I_s*sqrt(2);
      U_smax:=motorINPUT.plate.RatedVoltage*sqrt(2);
      w_rated:=motorINPUT.plate.RatedSpeed*60;

      // R_fe effect
      R_fe_consider:=0;
      Link_flux:=Flux_PM*N_ph*K_w;
      id_rated:=(Link_flux - sqrt(Link_flux^2 + 8*(L_q - L_d)^2*I_max^2))/(4*(L_q -
        L_d));
      iq_rated:=sqrt(I_max^2 - id_rated^2);
      w_rated:=U_smax/sqrt((L_q*iq_rated)^2 + (L_d*id_rated + Link_flux)^2);

      // Percentage done
      percentage_done:=0;
      // Number of discretiaztion points
      precision:=3000;
      I_precision:=0.5;
      delta_w_base:=2*Constants.pi/30*p;

      // Quarters of precision used for graphical representation
      prec1:=floor(precision/4);
      prec2:=2*floor(precision/4);
      prec3:=3*floor(precision/4);

      Modelica.Utilities.Streams.print("*** Start: Torque-vs-Speed ***","");

      // Speed range
      w_initiale:=floor(w_rated/10)*Constants.pi/30*p;
      w_maxe:=(w_rated*60)*Constants.pi*p/30;
      W:=1:30:(w_rated*10)*10;
      W_elec:=W*Constants.pi/30*p;
      f_elec:=W/2/Constants.pi;

      if R_fe_consider==1 then
        K_h:=motorINPUT.stator.dim.phy.mst.K_h;
        K_e:=motorINPUT.stator.dim.phy.mst.K_e;
        n_i:=motorINPUT.stator.dim.phy.mst.n_i;
        B_max:=motorINPUT.airgap.phy.B_g;
        Vol:=motorINPUT.plate.TmM/motorINPUT.rotor.dim.phy.ro_mst;
        deltaw_mult_factor_small:=1.25;
        deltaw_mult_factor_big:=1.5;
        Thresshold:=1*Constants.pi/180;
        rfecounter_limit:=15;
      else
        K_h:=0;
        K_e:=0;
        n_i:=1;
        B_max:=0;
        Vol:=0;
        deltaw_mult_factor_small:=2;
        deltaw_mult_factor_big:=4;
        Thresshold:=2*Constants.pi/180;
        rfecounter_limit:=1;
      end if;

      if Link_flux-L_d*I_max<I_max then
        Modelica.Utilities.Streams.print("*** WARNING: Infinite Speed Motor ***","");
        deltaw_mult_factor_small:=8;
        deltaw_mult_factor_big:=16;
      end if;
      delta_w[1]:=delta_w_base;
      I_initial:=I_max;
      Intensity:=I_initial;
      I_while_watchdog:=1;
      I_while_watchdog_limit:=500;
      Int_counter:=1;
      TSI_lut[1:integer(I_while_watchdog_limit),:,1]:=zeros(integer(I_while_watchdog_limit), 8);

      while (Intensity>=1 and I_while_watchdog<=I_while_watchdog_limit) loop
        R_circle:=Intensity;
        xInitial_limit:=0;
        w_elec:=w_initiale;
        w_while_watchdog:=1;
        w_while_watchdog_limit:=3000;
        LUT_row:=1;
        Power_barrage:=0;

        while w_elec<=w_maxe and w_while_watchdog<=w_while_watchdog_limit loop
          Pfe_seed:=1E-15 + Vol*(K_h*(w_elec/2/Constants.pi)*B_max^n_i + K_e*B_max^2*(w_elec/2
            /Constants.pi)^2);
          R_fe[1]:=U_smax^2/Pfe_seed;
          for rfecounter in 1:1:rfecounter_limit loop
            // Coeficients of EMF equations
            Rfe_new:=R_fe[1];
            A:=(R_s + w_elec^2*L_d*L_q/Rfe_new)^2;
            B:=w_elec*L_q;
            C:=w_elec^2*L_q*Link_flux/Rfe_new;
            D:=w_elec*L_d;
            E:=(R_s + w_elec^2*L_d*L_q/Rfe_new)^2;
            F:=w_elec*Link_flux;
            // Coeficient of canonical equations of conic section
            a:=A^2 + D^2;
            b:=2*(D*A - B*A);
            c:=B^2 + A^2;
            d:=2*(A*C + D*F);
            e:=2*(A*F - B*C);
            f:=C^2 + F^2 - U_smax^2;
            // Ellipse calculation
            ellipse:=Sizing.AuxiliaryFunc.ellipse([a,b,c,d,e,f]);
            if ellipse.degenerated<>0 then
              motorOUTPUT:=motorINPUT;
              motorOUTPUT.specs.TSeff_lut[:,:,1]:=[99999,99999];
              motorOUTPUT.specs.MTPA_lut:=[99999,99999];
              Modelica.Utilities.Streams.print("*** Error: Torque-vs-Speed ***","");
              return;
            end if;
            // Ellipse data extraction
            R_xx:=ellipse.axisx;
            R_yy:=ellipse.axisy;
            X_cc:=ellipse.center[1,1];
            Y_cc:=ellipse.center[2,1];
            alpha:=ellipse.angle;
            for xx in xInitial_limit:-max(R_circle, abs(X_cc))/precision:-max(R_circle, abs(X_cc)) loop
              // Quadratic coefficients
              a2:=c;
              b2:=b*xx + e;
              c2:=a*xx^2 + d*xx + f;
              // Quadratic function solutions
              if (b2^2 - 4*a2*c2) < 0 then
                y_1:=0;
                y_2:=0;
              else
                y_1:=(-b2 + sqrt(b2^2 - 4*a2*c2))/(2*a2);
                y_2:=(-b2 - sqrt(b2^2 - 4*a2*c2))/(2*a2);
              end if;
              yy:=max(y_1, y_2);

              // First solution: point is a solution of E-C intersection
              if (1-(yy^2+xx^2)/R_circle^2)<=0.005 then
                I_d2:=xx;
                I_q2:=yy;
                if LUT_row>=2 then
                  xInitial_limit:=TSI_lut[LUT_row - 1,4,Int_counter]*0.9;
                else
                  xInitial_limit:=0;
                end if;
                break;
              end if;
            end for;
            // Once the main circle currents are found, the internal currents of the circuit can be found:
            I_od:=(I_d2 + (w_elec*L_q*I_q2)/Rfe_new - (w_elec^2*L_q*Link_flux)/
              Rfe_new^2)/(1 + (w_elec^2*L_d*L_q)/Rfe_new^2);
            I_oq:=I_q2 - w_elec*L_d*I_od/Rfe_new - w_elec*Link_flux/Rfe_new;
            Pfe:=1E-15 + Vol*(K_h*(w_elec/2/Constants.pi)*B_max^n_i + K_e*B_max^2*(
              w_elec/2/Constants.pi)^2);
            R_fe[1]:=((w_elec*L_q*I_oq)^2 + ((w_elec*L_d*I_od + w_elec*Link_flux)^2))/
              Pfe;
            Rfe_rel_err:=abs(R_fe[1] - Rfe_new)/R_fe[1];

            // Rfe for break condition
            if Rfe_rel_err<=0.005 then
              break;
            end if;
          end for;

          // Omega break condition and its positioned before the counters assignment
          if I_q2<0 or 3/2*p*((Link_flux+(L_d-L_q)*I_od)*I_oq)<0 then
            if Power_barrage==0 then
              Power_barrage:=1;
            else
              break;
            end if;
          end if;
          if w_while_watchdog==w_while_watchdog_limit then
            break;
          end if;
          Int_vector[:,LUT_row]:=[I_d2;I_q2][:,1];

          if LUT_row<2 then    // Two initial points, no actions to be performed
            TSI_lut[LUT_row,:,Int_counter]:={R_circle,w_elec,3/2*p*((Link_flux + (L_d - L_q)*I_od)*I_oq),I_d2,I_q2,I_od,I_oq,R_fe[1]};
            delta_w[w_while_watchdog]:=delta_w_base;
            w_elec:=w_elec - delta_w[w_while_watchdog];
            LUT_row:=LUT_row + 1;
          elseif LUT_row >=2 then    // There is no flux weakening yet, far from R_cicle
            if I_d2==0 and R_circle>=0.3*I_q2 then
              TSI_lut[LUT_row,:,Int_counter]:={R_circle,w_elec,3/2*p*((Link_flux + (L_d - L_q)*I_od)*I_oq),I_d2,I_q2,I_od,I_oq,R_fe[1]};
              delta_w[w_while_watchdog]:=delta_w_base*4;
              w_elec:=w_elec + delta_w[w_while_watchdog];
              LUT_row:=LUT_row + 1;
            elseif I_d2==0 and R_circle<0.3*I_q2 then    // There is no flux weakening yet, close to R_cicle
              TSI_lut[LUT_row,:,Int_counter]:={R_circle,w_elec,3/2*p*((Link_flux + (L_d - L_q)*I_od)*I_oq),I_d2,I_q2,I_od,I_oq,R_fe[1]};
              delta_w[w_while_watchdog]:=delta_w_base;
              w_elec:=w_elec + delta_w[w_while_watchdog];
              LUT_row:=LUT_row + 1;
            // The angle is too small, must be increased
            elseif v2angle([I_d2;I_q2],matrix(Int_vector[:,LUT_row-1]))<deltaw_mult_factor_big/2*Thresshold then
              TSI_lut[LUT_row,:,Int_counter]:={R_circle,w_elec,3/2*p*((Link_flux + (L_d - L_q)*I_od)*I_oq),
                I_d2,I_q2,I_od,I_oq,R_fe[1]};
              delta_w[w_while_watchdog]:=delta_w[w_while_watchdog - 1]*deltaw_mult_factor_small;
              if delta_w[w_while_watchdog]>deltaw_mult_factor_big*delta_w_base then
                delta_w[w_while_watchdog]:=deltaw_mult_factor_big*delta_w_base;
              end if;
              w_elec:=TSI_lut[LUT_row, 2, Int_counter] + delta_w[w_while_watchdog];
              LUT_row:=LUT_row + 1;
            // Angle is OK
            elseif v2angle([I_d2;I_q2],matrix(Int_vector[:,LUT_row-1]))>=deltaw_mult_factor_big*0.5*Thresshold and v2angle(matrix(Int_vector[:,LUT_row]),matrix(Int_vector[:,LUT_row-1]))<=deltaw_mult_factor_big*Thresshold then
              TSI_lut[LUT_row,:,Int_counter]:={R_circle,w_elec,3/2*p*((Link_flux + (L_d - L_q)*I_od)*I_oq),I_d2,I_q2,I_od,I_oq,R_fe[1]};
              delta_w[w_while_watchdog]:=delta_w[w_while_watchdog - 1];
              if delta_w[w_while_watchdog]<=1/(30/p/Constants.pi) then
                delta_w[w_while_watchdog]:=1/(30/p/Constants.pi);
              end if;
              w_elec:=TSI_lut[LUT_row, 2, Int_counter] + delta_w[w_while_watchdog];
              LUT_row:=LUT_row + 1;
              // Angle is too big, point must be recalculated
            elseif v2angle([I_d2;I_q2],matrix(Int_vector[:,LUT_row-1]))>deltaw_mult_factor_big*Thresshold and R_fe_consider==1 then
              delta_w[w_while_watchdog]:=delta_w[w_while_watchdog - 1]*0.5;
              if delta_w[w_while_watchdog]<=1/(30/p/Constants.pi) and R_circle/I_initial<0.1 then
                delta_w[w_while_watchdog]:=0.5/(30/p/Constants.pi);
              elseif delta_w[w_while_watchdog]<=1/(30/p/Constants.pi) then
                delta_w[w_while_watchdog]:=1/(30/p/Constants.pi);
              end if;
              w_elec:=TSI_lut[LUT_row - 1, 2, Int_counter] + delta_w[w_while_watchdog];
            // Angle is too big
            elseif v2angle([I_d2;I_q2],matrix(Int_vector[:,LUT_row-1]))>deltaw_mult_factor_big*Thresshold and R_fe_consider==0 then
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,1,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,2,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,3,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,4,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,5,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,6,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,7,:])),"");
              Modelica.Utilities.Streams.print("TSI_lut"+Modelica.Math.Matrices.toString(matrix(TSI_lut[:,8,:])),"");
              delta_w[w_while_watchdog]:=delta_w[w_while_watchdog - 1]*0.5;// Assignment failed
              if delta_w[w_while_watchdog]>deltaw_mult_factor_big*delta_w_base then
                delta_w[w_while_watchdog]:=deltaw_mult_factor_big*delta_w_base;
              end if;
              w_elec:=TSI_lut[LUT_row - 1, 2, Int_counter] + delta_w[w_while_watchdog];
            end if;
          end if;
          msg_handle:="Next LUT_row= " + Modelica.Math.Vectors.toString(vector(LUT_row));
          Modelica.Utilities.Streams.print(msg_handle,"");
          w_while_watchdog:=w_while_watchdog + 1;
        end while;

        percentage_done:=0.5*Int_counter/floor(I_initial*I_precision);
        Modelica.Utilities.Streams.print(Modelica.Math.Vectors.toString(vector(100*percentage_done))+"%","");

        Intensity:=I_initial*(1 - Int_counter/floor(I_initial*I_precision));
        Int_counter:=Int_counter + 1;
        I_while_watchdog:=I_while_watchdog + 1;
      end while;
      // New data table pre-allcoation : TSI_lut2
      Modelica.Utilities.Streams.print("***FINE 3","");
      TSI_lut2:=zeros(
        size(TSI_lut, 1),
        size(TSI_lut, 2) + 2,
        size(TSI_lut, 3));
      MTPA_lut:=zeros(size(TSI_lut, 3), 6);
      Modelica.Utilities.Streams.print("***FINE 4","");
      for ii in size(TSI_lut,3):-1:1 loop
        // For: from "rated speed" to flux weakening => TSI_last
       // TSI_lutTcorrect:=1;
        Modelica.Utilities.Streams.print("***FINE 5","");
        for iij in 1:1:size(TSI_lut,1) loop
          if (TSI_lut[iij,4,ii]<0 and TSI_lut[iij,3,ii]>0) then
            TSI_lutT[TSI_lutTcorrect,:]:=TSI_lut[iij,:, ii];
            TSI_lutTcorrect:=TSI_lutTcorrect + 1;
          end if;
        end for;
        Modelica.Utilities.Streams.print("***FINE 6","");
        // Max torque per ampere position
        (,i):=maxvector(TSI_lutT[:, 3]);
        // Flux weakening valid data table
        TSI_last:=[TSI_lutT[i:end, :],zeros(size(TSI_lutT, 1) - i + 1, 2)];
        // EMF for flux weakening calculation
        for iii in 1:1:size(TSI_last,1) loop
          TSI_last[iii,size(TSI_lut,2)+1]:=sqrt((R_s*TSI_last[iii, 4] - TSI_last[iii, 2]*L_q*TSI_last[
            iii, 7])^2 + (R_s*TSI_last[iii, 5] + TSI_last[iii, 2]*L_d*TSI_last[iii, 6] + Link_flux*
            TSI_last[iii, 2])^2);
          TemporalMaxVoltage:=max(TSI_last[:, 8]);
          for iik in 1:1:size(TSI_last,1) loop
            if TSI_last[iik,3]<=0 then
              TSI_last[iik,8]:=TemporalMaxVoltage;
            end if;
          end for;
        end for; // EMF for flux weakening

        // 0-Rated valid data table (empty):
        TSI_Ini:=zeros(size(TSI_lut, 1) - size(TSI_last, 1), size(TSI_lut2, 2));
        // Known data assignment

        W_elec_I:=linspace(0,TSI_last[1, 2] - 3,size(TSI_Ini, 1));
        i_od:=TSI_last[1, 6]*ones(size(W_elec_I,1));
        i_oq:=TSI_last[1, 7]*ones(size(W_elec_I,1));

        // A column vector for torque
        T_I:=3/2*p*((Link_flux.+ (L_d - L_q).*i_od).*i_oq);

        // A column vector for their matching voltages
        u_od:=-W_elec_I.*L_q.*i_oq;
        u_oq:=W_elec_I.*L_d.*i_od + W_elec_I.*Link_flux;

        // A column vector for the corresponding losses
        Pfef_I:=1E-15.+(Vol*(K_h*(1/2/Constants.pi)*W_elec_I.* B_max^n_i.+K_e*B_max^2*(1/2/Constants.pi)*W_elec_I.^2));

        // A column vector for Iron Losses Current (Rfe current= i_cd/q)
        R_fe:=(u_od.^2.+u_oq.^2)./Pfef_I;

        // A column vector for each current of Iron Losses = i_cd/q
        i_cd:=u_od ./ R_fe;
        i_cq:=u_oq ./ R_fe;

        // A column vector for each total current i_d/q
        i_d:=i_od + i_cd;
        i_q:=i_oq + i_cq;

        // A column vector for each total voltage u_d/q
        u_d:=R_s .* i_d + u_od;
        u_q:=R_s .* i_q + u_oq;

        // A column vector for total phase voltage and phase current.
        U:=sqrt(u_d .^ 2 + u_q .^ 2);
        I:=sqrt(i_d .^ 2 + i_q .^ 2);

        // Non field weakening matrix assembly
        TSI_Ini:=[I,W_elec_I,T_I,i_d,i_q,i_od,i_oq,R_fe,U,zeros(size(TSI_lut, 1) - size(TSI_last, 1), 1)];
        TSI_lut2[:,:,ii]:=[TSI_Ini; TSI_last];

        // Efficiency is assigned to the tenth column
        for iii in 1:1:size(TSI_lut2,1) loop
          //voltage=[R_s*TSI_lut2[iii,4,ii]-TSI_lut2[iii,2,ii]*L_q*TSI_lut2[iii,7,ii];R_s*TSI_lut2[iii,5,ii]+TSI_lut2[iii,2,ii]*L_d*TSI_lut2[iii,6,ii]+TSI_lut2[iii,2,ii]*Link_flux;];
          //PF_I=cos(v2angle(TSI_lut2(iii,4:5,ii)',voltage));

          TSI_lut2[iii,10,ii]:=TSI_lut2[iii, 2, ii]*TSI_lut2[iii, 3, ii]/p/(3/2*sqrt(TSI_lut2[iii, 4,ii]^2 + (TSI_lut2[iii, 5, ii])^2)*TSI_lut2[iii, 9, ii]);
          //TSI_lut2[iii,10,ii]:=TSI_lut2[iii, 2, ii]*TSI_lut2[iii, 3, ii]/p/(3/2*sqrt(TSI_lut2[iii, 4, ii]^2 + (
          //  TSI_lut2[iii, 5, ii])^2)*TSI_lut2[iii, 9, ii]*PF_I);
        end for;

        // Final data construction for each current
        TSI_lut2[:,:,ii]:=[TSI_Ini; TSI_last];

        // Efficiency (for all speed range)
        for iii in 1:1:size(TSI_lut2,1) loop
          TSI_lut2[iii,10,ii]:=TSI_lut2[iii, 2, ii]*TSI_lut2[iii, 3, ii]/p/(3/2*sqrt(TSI_lut2[iii, 4,
            ii]^2 + (TSI_lut2[iii, 5, ii])^2)*TSI_lut2[iii, 9, ii]);
        end for;
        // MTPA curve
        // MTPA_lut(ii,:)=[speed, torque, current, effiency]
        MTPA_eff_position:=size(TSI_lut2, 1) - size(TSI_last, 1) + 1;
        MTPA_lut[ii,:]:={TSI_last[1, 2],TSI_last[1, 3],TSI_last[1, 1],TSI_lut2[MTPA_eff_position, 10,
          ii]};
        percentage_done:=0.5*Int_counter/floor(I_initial*I_precision) + 0.5*(size(
          TSI_lut, 3) - ii)/size(TSI_lut, 3);
        Modelica.Utilities.Streams.print(Modelica.Math.Vectors.toString(vector(100*percentage_done))+"%","");
      end for;

      // Output Assignment
      motorOUTPUT:=motorINPUT;
      motorOUTPUT.specs.TSeff_lut:=TSI_lut2;
      motorOUTPUT.specs.MTPA_lut:=MTPA_lut;

      if ellipse.degenerated==0 then
        Modelica.Utilities.Streams.print("*** End: Torque-vs-Speed ***","");
      else
        Modelica.Utilities.Streams.print("*** *** ERROR End: Torque-vs-Speed ***","");
      end if;
    annotation (
        Documentation(info="<html>

<head>
<title>torquevsspeed() help files</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>TorquevsSpeed()</span></b><span
lang=EN-GB> function</span><span lang=EN-GB> </span><span lang=EN-US>and
returns a set of 3-D curves by means of iso-current lines. This chart includes
both non flux weakening and flux weakening regions and is able to take into
account iron losses in magnetic steel based on its volume (calculated in masses
and moments of inertia section) and copper loses in stator windings based on
copper resistance, since they are the main source of power losses in PMSMs.</span></p>

<p class=MsoNormal><span lang=EN-US>First the field weakening area is obtained,
and based on its Maximum Torque per Ampere (MTPA) per each iso-current, the
non-field weakening is calculated later.</span></p>

<p class=MsoNormal><span lang=EN-US>For Field Weakening Area, method consists
on a sweep of speeds per each iso-current line, those speeds are voltage
limited and current limited and those limitations define current angle per each
speed. Because of iron losses current angle requires a refinement in each
intersection. Therefore, there are 3 loops implied: a current loop for each
iso-current line, an inner loop to sweep speeds for each iso-current line and a
third inner loop for iron losses refinement. </span></p>

<p class=MsoNormal><span lang=EN-US>Systems solved by Blondel’s diagrams
usually apply lots of simplifications in order to make it easily understandable
and able to implement in motor controls. The approach taken in this work keeps
some of those simplifications and avoids others. </span></p>

<p class=MsoNormal><span lang=EN-US>Simplifications that are still applied:</span></p>

<p class=MsoNormal style='text-indent:35.4pt'><span lang=EN-US>-</span><span
lang=EN-US> Constant inductance at any current load. This simplification can be
avoided without much effort if a proper table of inductances per current is
provided.</span></p>

<p class=MsoNormal style='text-indent:35.4pt'><span lang=EN-US>- Only first
harmonic is considered for all magnitudes: current, voltage, torque, flux.</span></p>

<p class=MsoNormal style='text-indent:35.4pt'><span lang=EN-US>- Maximum Flux
and Maximum Torque per Flux trajectories are not considered in high weakening
field region.</span></p>

<p class=MsoNormal><span lang=EN-US>Simplifications not applied:</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-US>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Copper Losses: represented by coil </span><span
lang=EN-GB>Resistance (Rs or R_s variable). The value</span><span lang=EN-GB> </span><span
lang=EN-US>is considered fixed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US>-<span style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Iron losses: </span><span lang=EN-GB>represented
as Rfe or Rfe variable. The</span><span lang=EN-US> value is voltage and speed
dependent. </span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-US>-<span
style='font:7.0pt 'Times New Roman''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Saliency: </span><span lang=EN-GB>represented by
L<sub>d</sub> and L<sub>q</sub> (L_d and L_q variables). The</span><span
lang=EN-GB> </span><span lang=EN-US>values are considered fixed.</span></p>

<p class=MsoNormal><span lang=EN-US>Taking into account the conditions
explained in the previous section lumped parameters circuit ends up as shown in
</span><span lang=EN-GB>Figure 1</span><span lang=EN-US>, there’s one circuit
for each axis. In those circuits the following variables are unknown: total
voltage (modulus and angle), total current (angle), current over
torque-generating branch; i<sub>od</sub>, i<sub>oq</sub> (modulus). For Torque
vs. Speed chart, then, there is one different set of d-q circuits for each
speed and for each current modulus. Equations extracted from those circuits
must use total current as variables for Blondel’s representation (I<sub>d</sub>,
I<sub>q</sub> or, in code, variables I_d2 and I_q2), in order to intersect
them, equivalent to solving the equations system in an algebraic approach.</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraph align=center style='text-align:center;page-break-after:
avoid'><span lang=EN-GB><img width=401 height=202
src='torquevsspeed_archivos/image001.png'></span></p>

<p class=MsoCaption><a name='_Toc414019909'></a><a name='_Toc402539917'></a><a
name='_Ref401571037'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>1</span><span lang=EN-GB>. Equivalent circuits for d-axis (left) and
q-axis (right) taking into account copper and iron losses.</span></p>

<p class=MsoNormal align=center style='text-align:center;page-break-after:avoid'><span
lang=EN-GB><img width=498 height=894 src='torquevsspeed_archivos/image002.png'></span></p>

<p class=MsoCaption><a name='_Toc414019910'></a><a name='_Toc402539918'></a><a
name='_Ref401585219'><span lang=EN-GB>Figure </span></a><span
lang=EN-GB>2</span><span lang=EN-GB>. Step by Step field weakening part of
TorquevsSpeed function.</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>

"));
    end TorquevsSpeed;

    function tooth_dim
      "tooth_dim iterates stator tooth geometry in order to find a 3 section tooth"

      import Modelica.SIunits;
      parameter input SIunits.Area A_cu "Slot area for copper";
      parameter input SIunits.Angle tau_p "Slot pitch";
      parameter input SIunits.Angle tau_s "Stator pitch";
      parameter input SIunits.Length D_is "Diameter of inner stator";
      parameter input SIunits.Length b_zB "Tooth width at tip";
      parameter input SIunits.Length h_tip_ini "Tooth width";
      output tooth_dimr out;

    protected
      Real A_cu_allowed_rel_error "Slot copper allowed relative error";
      Real A_cu_rel_error "Slot copper area relative error";
      Real d1_prop "d1 measure proportion to d3";
      Real d2_prop "d2 measure proportion to d3";
      Real tip_prop "tip measure proportion to d3";
      SIunits.Length d3 "d3 tooth dimension (seed value)";
      SIunits.Angle tau_z "Tooth pitch";
      SIunits.Length b_sB "Slot width at bottom (arc)";
      SIunits.Length D_ms "Stator medium diameter";
      SIunits.Length b_zT "Tooth width at top (arc)";
      SIunits.Length b_sT "Slot width at top (arc)";
      SIunits.Area A_cu_temp "Calculated copper area";
      SIunits.Length h_tip;
      SIunits.Length d_3_new;
      SIunits.Length d1 "Tooth square shoe height";
      SIunits.Length d2 "Tooth trapezoid shoe height";
      tooth_dimr out_temp;

    algorithm
      A_cu_allowed_rel_error:=1e-4;
      A_cu_rel_error:=10;
      out.A_cu_rel_error:=A_cu_rel_error;

      d1_prop:=0.1;
      d2_prop:=0.05;
      tip_prop:=d1_prop + d2_prop;

      // Start
      d3:=A_cu/(tau_s*D_is/2 - b_zB);
      for i in 1:500 loop
        if (h_tip_ini==0) then
          h_tip:=tip_prop*d3;
        else
          h_tip:=h_tip_ini;
        end if;

        tau_z:=b_zB/(h_tip + D_is/2);
        b_sB:=tau_s*(D_is/2 + h_tip) - b_zB;
        D_ms:=D_is + 2*(1 + tip_prop)*d3;
        b_zT:=D_ms/2*tau_z;
        b_sT:=tau_s*D_ms/2 - b_zT;
        A_cu_temp:=d3*(b_sB + b_sT)/2;
        A_cu_rel_error:=abs((A_cu - A_cu_temp)/A_cu);
        d_3_new:=A_cu*2/(b_sB+b_sT);

        d1:=d1_prop*d3;
        d2:=d2_prop*d3;

        // Output stage

        out_temp.tau_z:=tau_z;
        out_temp.b_sB:=b_sB;
        out_temp.b_sT:=b_sT;
        out_temp.b_zT:=b_zT;
        out_temp.D_ms:=D_ms;
        out_temp.d1_prop:=d1_prop;
        out_temp.d2_prop:=d2_prop;
        out_temp.d1:=d1;
        out_temp.d2:=d2;
        out_temp.d3:=d3;
        out_temp.A_cu_temp:=A_cu_temp;
        out_temp.A_cu_rel_error:=A_cu_rel_error;

        // Only the best generated solution will be saved i.e. with the lowest A_cu_rel_error
        if (out.A_cu_rel_error>out_temp.A_cu_rel_error) then
          out:=out_temp;
        end if;

        // Good iteration stop condition
        if (A_cu_rel_error>=1) then      // worst case scenario if
          d3:=d_3_new;
        elseif (A_cu_rel_error>0.01) then  // slowdown if
          d3:=d_3_new;
        elseif (A_cu_rel_error>A_cu_allowed_rel_error) then  // super slowdown if
          d3:=(1 - A_cu_rel_error^1.5)*d3;
        else
          break;
        end if;
        // Bad iteration stop condition
        if (i==499) then
          Modelica.Utilities.Streams.print("TD ERROR: Maximun iterations reached without a Good Solution, ''best'' solution assigned as output","");
          break;
        end if;
      end for;

    end tooth_dim;

    record tooth_dimr "Record for tooth_dim output"
      import Modelica.SIunits;
      Real A_cu_rel_error
        "Slot copper area relative eroor of the best solution";
      SIunits.Angle tau_z "Tooth pitch";
      SIunits.Length b_sB "Slot width at bottom (arc)";
      SIunits.Length b_sT "Slot width at top (arc)";
      SIunits.Length b_zT "Tooth width at top (arc)";
      SIunits.Length D_ms "Stator medium diameter";
      Real d1_prop "d1 measure proportion to d3";
      Real d2_prop "d2 measure proportion to d3";
      SIunits.Length d1 "Tooth square shoe height";
      SIunits.Length d2 "Tooth trapezoid shoe height";
      SIunits.Length d3 "d3 tooth dimension (seed value)";
      SIunits.Area A_cu_temp "Calculated copper area";
    end tooth_dimr;

    function v2angle "Finds a smallest angle between two vectors"
     input Real[2,1] vector1in;
     input Real[2,1] vector2in;
     output Real theta;
    protected
      Real[2,1] vector1=vector1in;
      Real[2,1] vector2=vector2in;
    algorithm
      if vector1[1,1]==0 then
        vector1:=[1; 0];
      end if;
      if vector2[1,1]==0 then
        vector2:=[1; 0];
      end if;
      if vector1[1,1]==vector2[1,1] and vector1[2,1]==vector2[2,1] then
        theta:=0;
      else
        theta:=acos((vector1[1, 1]*vector2[1, 1] + vector1[2, 1]*vector2[2, 1])
          /sqrt(vector1[1, 1]^2 + vector1[2, 1]^2)/sqrt(vector2[1, 1]^2 +
          vector2[2, 1]^2));
      end if;
    annotation (
        Documentation(info="<html>

<head>

<title>vangle() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b>vangle()</b> Finds the SMALLEST angle between two
vectors.</p>

<p class=MsoNormal>theta = p2angle( first vector, second vector) vector order
its important and may change angle by pi rads.</p>

<p class=MsoNormal>Vectors must be 2x1 vectors (column). </p>

<p class=MsoNormal>If one vector is X-axis, just type 0. theta is returned in
RAD.</p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>

"));
    end v2angle;

    function windingf
      "windingf3 returns the winding factor of the fundamental frequency"
      import Modelica.Constants;
     // input parameter
      input Integer Qs "Number of stator slots";
      input Integer p "Number of pole pairs";
      input Integer m "Number of phases";
      input Integer coilspan "Coil span";
      input Real gamma "Gamma";
      input Integer n "Harmonic order";
      output Real[1] Kw "Winding factor of the nth harmonic";
    protected
      parameter Real pole_pitch=Qs/(2*p) "Pole pitch";
      Real Kp "Pitch factor";
      Real Kd "Distribution factor";
      Real Ks=1 "Skewing factor, 1 if no skewing";
      parameter Real q=Qs/(2*p*m) "Slots per pole per phase ratio";
      Real gamma_rad;
    algorithm
      Kw:=zeros(n);
      for i in 1:n loop
        Kp:=sin(i*Constants.pi/2*pole_pitch/coilspan);
        Kd:=sin(i*Constants.pi/(2*m))/(q*sin(i*Constants.pi/(2*m*q)));
        if gamma==0 then
          Ks:=1;
        else
          gamma_rad:=gamma*Constants.pi/180;
          Ks:=sin(i*p*gamma_rad/2)/(i*p*gamma_rad/2);
        end if;
        Kw[i]:=Kp*Kd*Ks;
      end for;
    annotation (
        Documentation(info="<html>

<head>

<title>windingf() help file</title>
<style>
</style>

</head>

<body lang=ES>

<div class=WordSection1>

<p class=MsoNormal><b><span lang=EN-GB>windingf()</span></b><span lang=EN-GB
style='color:#7030A0'> </span><span lang=EN-GB>function, which </span><span
lang=EN-GB>requires <b>Q_s</b>, <b>p</b>, <b>m</b>, <b>coilspan</b>, <b>gamma</b>
and the order</span><span lang=EN-GB> </span><b><i><span lang=EN-GB
style='font-family:'Cambria','serif''>i</span></i></b><span lang=EN-GB> (for
this work only first order </span><i><span lang=EN-GB style='font-family:'Cambria','serif''>i=1
</span></i><span lang=EN-GB style='font-family:'Cambria','serif''>is used</span><span
lang=EN-GB>). <b>coilspan</b> is defined as the distance, in number of slots,
between a coil conductor of a certain phase and its corresponding return one. And
gamma is the skewing angle.</span></p>

<p class=MsoNormal><span lang=EN-GB>This function </span><span lang=EN-GB>obtains
<b>K_w</b>,</span><span lang=EN-GB> or winding factor, via the product </span><span
lang=EN-GB>of <b>K_p</b>, <b>K_d</b>, <b>K_s</b>:</span></p>

<p class=MsoNormal><span
lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
width=108 height=19 src='windingf_archivos/image001.png'></span></p>

<p class=MsoNormal><span lang=EN-GB>if there is no skewing u</span><span
lang=EN-GB>se <b>gamma=0</b>. </span></p>

<p class=MsoNormal><span lang=EN-GB>First slots per pole per phase and pole
pitch are locally obtained:</span></p>

<div align=center>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='margin-left:76.3pt;border-collapse:collapse;border:none'>
 <tr>
  <td width=170 valign=top style='width:127.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=56 height=36 src='windingf_archivos/image002.png'></span></p>
  </td>
  <td width=189 valign=top style='width:5.0cm;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=103 height=36 src='windingf_archivos/image003.png'></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-GB>Then the values for each of the constant
are:</span></p>

<div align=center>

<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=205 valign=top style='width:153.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=152 height=36 src='windingf_archivos/image004.png'></span></p>
  </td>
  <td width=205 valign=top style='width:153.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=171 height=66 src='windingf_archivos/image005.png'></span></p>
  </td>
  <td width=205 valign=top style='width:153.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal><span
  lang=EN-GB style='font-size:11.0pt;font-family:'Calibri','sans-serif''><img
  width=158 height=59 src='windingf_archivos/image006.png'></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>

</div>

</body>

</html>

"));
    end windingf;

    package Paint

      function Rotation "Performs basic algebraic rotation in cart coords"
       input Real[1,2] pointsin;
       input Modelica.SIunits.Angle angle "Rotation angle";
       output Real[1,2] pointsout;
      protected
        Real[2,2] A "Reflection matrix";
      algorithm
       A:=Maketurn(angle);
       pointsout:=transpose(A*transpose(pointsin));
      end Rotation;

      function arcof "Reutrns angle of two lengths (valid for small angles)"
       import Modelica.SIunits;
       input SIunits.Length l;
       input SIunits.Length r;
       output SIunits.Angle theta;
      algorithm
        theta:=1/2*asin(2*l/r);
      end arcof;

      function Refine "Refines points disposed in variable angle position"
       input Real[1,2] pointsin;
       input Modelica.SIunits.Angle angle "Rotation angle";
       output Real[1,2] pointsout;

      algorithm
      end Refine;
    end Paint;

  end AuxiliaryFunc;

  annotation (uses(Modelica(version="3.2.1"), DataFiles(version="1.0.1")));
end Sizing;
